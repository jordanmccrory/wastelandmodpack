Trait Ideas
===========

**Fashion Purist**

*Bags and backpacks are just soo gaudy. Hates wearing backpacks and bags.*

- Bags hold less, and cause unhappyness when worn.


**Minimalist Dresser**

*Feeling constrained in layers of clothes is the worst! Underware is always optional.*

- Happyness bonus when wearing less than 4 pieces of clothing
- Unhappyness when wearing more than 7 pieces of clothing 


**Weak Lungs**

*Breathing is chore..*

- Cough when low on endurance
- Auto apply to smokers, or if you smoke too often


**Motion Sickness**

*Do you really need to drive so fast?*

- Nausea when in a moving car.