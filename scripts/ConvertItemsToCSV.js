const fs = require('fs');
const path = require('path');
const readline = require('readline');

// Define the CSV headers
const csvHeaders = [
  'Module',
  'Item',
  'DisplayName',
  'Weight',
  'EnduranceMod',
  'MaxRange',
  'MinAngle',
  'MinimumSwingTime',
  'SwingAmountBeforeImpact',
  'PushBackMod',
  'ConditionMax',
  'MaxHitCount',
  'MinRange',
  'SwingTime',
  'CriticalChance',
  'CritDmgMultiplier',
  'MinDamage',
  'MaxDamage',
  'BaseSpeed',
  'WeaponLength',
  "TwoHandWeapon",
];

const allItems = [];
let numRunning = 0;

// Function to parse a text file and extract item information
function parseTextFile(filePath, finishedCallback) {
  numRunning++;
  console.log(`Parsing file "${filePath}"...`);
  let currentModule = null;
  let currentItem = null;

  const rl = readline.createInterface({
    input: fs.createReadStream(filePath),
    crlfDelay: Infinity
  });

  rl.on('line', (line) => {
    line = line.trim();
    if (line.startsWith('module ')) {
      currentModule = line.replace('module ', '').replace("{", "").trim();
    } else if (line.startsWith('item ') && currentModule) {
      currentItem = {
        Module: currentModule,
        EnduranceMod: "1",
       };
      const itemName = line.replace('item ', '').trim();
      currentItem.Item = itemName.substring(0, itemName.indexOf('\n') !== -1 ? itemName.indexOf('\n') : itemName.length);
    } else if (line.includes("}") && currentItem) {
      allItems.push(currentItem);
      currentItem = null;
    } else if (line.includes('=') && currentItem ) {
      const [key, value] = line.replace(",", "").trim().split('=').map((part) => part.trim());
      if (key === 'Type' && value !== 'Weapon') {
        // Exclude non-weapon items
        currentItem = null;
      } else if (key === 'Ranged' && value === 'TRUE') {
        // Exclude ranged weapons
        currentItem = null;
      } else {
        currentItem[key] = value;
      }
    }
  });

  rl.on('close', () => {
    numRunning--;
    if (numRunning === 0) {
      finishedCallback();
    }
  });
}

// Function to scan the directory for text files in "scripts" subdirectories
function scanDirectory(directoryPath) {
  fs.readdirSync(directoryPath).forEach((item) => {
    const itemPath = path.join(directoryPath, item);
    const stat = fs.statSync(itemPath);
    if (stat.isDirectory()) {
      // Recursively scan subdirectories
      scanDirectory(itemPath);
    } else if (path.extname(item) === '.txt' && path.dirname(itemPath).includes('scripts')) {
      parseTextFile(itemPath, WriteIfReady);
    }
  });
}

// Function to write items to a CSV file
function writeItemsToCSV(items) {
  const csvFileName = 'AllHandWeapons.csv';
  const csvFilePath = path.join(__dirname, csvFileName);

  fs.writeFileSync(csvFilePath, csvHeaders.join(',') + '\n');

  items.forEach((item) => {
    const csvRow = csvHeaders.map((header) => (item[header] ? item[header].includes(",") ? `"${item[header].replace(/"/g, '""')}"` : item[header] : '')).join(',');
    fs.appendFileSync(csvFilePath, csvRow + '\n');
  });

  console.log(`CSV file "${csvFileName}" created successfully.`);
}

function WriteIfReady() {
  if (numRunning === 0) {
    writeItemsToCSV(allItems);
  }
}
// Start scanning the directory
scanDirectory('C:\\Program Files (x86)\\Steam\\steamapps\\common\\ProjectZomboid\\media');
scanDirectory('C:\\Program Files (x86)\\Steam\\steamapps\\workshop\\content\\108600');
