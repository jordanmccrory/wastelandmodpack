if isClient() then return end

PzWebStats = PzWebStats or {}
PzWebStats.UsernameGuidMap = {}

--- @param reason string
--- @param importance "low"|"medium"|"high"
PzWebStats.RequestReboot = function (reason, importance, extras)
    local fileWriter = getFileWriter("PzWebStats/reboot.txt", true, false)
    fileWriter:writeln(importance)
    fileWriter:writeln(reason)
    for key, value in pairs(extras or {}) do
        fileWriter:writeln(key .. "," .. value)
    end
    fileWriter:close()
end

PzWebStats.GetPlayerPerks = function (player)
    local skills = {}

    for i=1, Perks.getMaxIndex()-1 do
		local perk = Perks.fromIndex(i)
        if perk then
            local currentLevel = player:getPerkLevel(perk)
            local perkName = perk:getName()
            skills[perkName] = currentLevel
        end
    end

    return skills
end

PzWebStats.WriteStats = function (username, stats)
    local fileWriter = getFileWriter("PzWebStats/players.txt", true, true)
    fileWriter:writeln("===" .. username)
    for key, value in pairs(stats) do
        fileWriter:writeln(key .. "," .. value)
    end
    fileWriter:writeln("===")
    fileWriter:close()
end

PzWebStats.ExportPlayerStats = function ()
	sendServerCommand("PzWebStats", "SendStats", {})
end

PzWebStats.ProcessPlayerStats = function (player, stats)
    local username = player:getUsername()
    local perks = PzWebStats.GetPlayerPerks(player)
    for perk, data in pairs(perks) do
        stats["skill," .. perk] = data
    end
    PzWebStats.WriteStats(username, stats)
end

PzWebStats.DoAllItemDump = function ()
    local fileWriter = getFileWriter("PzWebStats/allitems.csv", true, false)
    local allItems = getScriptManager():getAllItems()
    for i=0, allItems:size()-1 do
        local item = allItems:get(i)
        if not item:isHidden() then
            local fullName = item:getFullName() or ""
            local displayName = item:getDisplayName() or ""
            local displayCategory = item:getDisplayCategory() or ""
            if displayCategory ~= "" then
                displayCategory = getText("IGUI_ItemCat_" .. displayCategory)
            end
            local categories = item:getCategories()
            local c = {}
            for ii=0, categories:size()-1 do
                c[#c+1] = categories:get(ii)
            end
            local otherCategories = table.concat(c, ",")
            fileWriter:writeln(fullName .. "," .. displayName .. "," .. displayCategory .. "," .. otherCategories)
        end
    end
    fileWriter:close()
end

PzWebStats.WriteGameTime = function ()
    local fileWriter = getFileWriter("PzWebStats/gametime.txt", true, false)
    local gameTime = getGameTime()
    local timeStr = gameTime:getYear() .. "," .. (gameTime:getMonth()+1) .. "," .. (gameTime:getDay()+1) .. "," .. gameTime:getHour()
    fileWriter:writeln(timeStr)
    fileWriter:close()
end

PzWebStats.WriteWeatherForecast = function (force)
    local gameTime = getGameTime()
    local climateManager = getClimateManager()
    local forecaster = climateManager:getClimateForecaster()
    local todaysForecast = forecaster:getForecast()
    if gameTime:getHour() ~= 6 and not force then
        return
    end
    local tempForecast = todaysForecast:getTemperature()
    local cloudyForecast = todaysForecast:getCloudiness()
    local foggy = todaysForecast:isHasFog()
    local rain = todaysForecast:isWeatherStarts()
    local heavyRain = todaysForecast:isHasHeavyRain()
    local blizzard = todaysForecast:isHasBlizzard()
    local storm = todaysForecast:isHasStorm()
    local snow = todaysForecast:isChanceOnSnow()

    local season = climateManager:getSeason():getSeason()
    local seasonName
    if season == 0 then
        seasonName = "Unknown"
    elseif season == 1 then
        seasonName = "Spring"
    elseif season == 2 then
        seasonName = "Summer"
    elseif season == 3 then
       seasonName = "Summer"
    elseif season == 4 then
        seasonName = "Autumn"
    elseif season == 5 then
        seasonName = "Winter"
    end

    local fileWriter = getFileWriter("PzWebStats/weatherforecast.txt", true, false)
    fileWriter:writeln("TempDayMin," .. tempForecast:getDayMin())
    fileWriter:writeln("TempDayMean," .. tempForecast:getDayMean())
    fileWriter:writeln("TempDayMax," .. tempForecast:getDayMax())
    fileWriter:writeln("TempNightMin," .. tempForecast:getNightMin())
    fileWriter:writeln("TempNightMean," .. tempForecast:getNightMean())
    fileWriter:writeln("TempNightMax," .. tempForecast:getNightMax())
    fileWriter:writeln("CloudyDayMin," .. cloudyForecast:getDayMin())
    fileWriter:writeln("CloudyDayMean," .. cloudyForecast:getDayMean())
    fileWriter:writeln("CloudyDayMax," .. cloudyForecast:getDayMax())
    fileWriter:writeln("CloudyNightMin," .. cloudyForecast:getNightMin())
    fileWriter:writeln("CloudyNightMean," .. cloudyForecast:getNightMean())
    fileWriter:writeln("CloudyNightMax," .. cloudyForecast:getNightMax())
    fileWriter:writeln("Foggy," .. tostring(foggy))
    fileWriter:writeln("Rain," .. tostring(rain))
    fileWriter:writeln("HeavyRain," .. tostring(heavyRain))
    fileWriter:writeln("Blizzard," .. tostring(blizzard))
    fileWriter:writeln("Storm," .. tostring(storm))
    fileWriter:writeln("Snow," .. tostring(snow))
    fileWriter:writeln("Season," .. seasonName)

    -- check if there is a pending weather event
    if WastelandSeasons and WastelandSeasons.Data and WastelandSeasons.Data.scheduledEvent then
        local scheduledEvent = WastelandSeasons.Data.scheduledEvent or ""
        local scheduledEventStart = WastelandSeasons.Data.scheduledEventStart or 0
        local scheduledEventEnd = WastelandSeasons.Data.scheduledEventEnd or 0
        fileWriter:writeln("WastelandSeasonsEvent," .. scheduledEvent)
        fileWriter:writeln("WastelandSeasonsStart," .. scheduledEventStart)
        fileWriter:writeln("WastelandSeasonsEnd," .. scheduledEventEnd)
    end

    fileWriter:close()
end

function PzWebStats.ReadUsernameGuidMap()
    local fileReader = getFileReader("PzWebStats/UsernameGuidMap.txt", false)
    if fileReader then
        local line = fileReader:readLine()
        while line do
            local items = line:split(",")
            if #items == 2 then
                PzWebStats.UsernameGuidMap[items[1]] = items[2]
            end
            line = fileReader:readLine()
        end
        fileReader:close()
    end
end

if PzWebStats.Initialized then return end

PzWebStats.Initialized = true

Events.EveryHours.Add(function ()
    PzWebStats.WriteGameTime()
    PzWebStats.ExportPlayerStats()
    PzWebStats.WriteWeatherForecast()
end)

Events.OnGameBoot.Add(function ()
    PzWebStats.DoAllItemDump()
    PzWebStats.WriteGameTime()
    PzWebStats.ReadUsernameGuidMap()
end)

Events.OnClientCommand.Add(function (module, command, player, args)
    if module == "PzWebStats" then
        if command == "MyStats" then
            PzWebStats.ProcessPlayerStats(player, args)
        end
        if command == "GetGuidForUsername" then
            local username = args[1]
            local guid = PzWebStats.UsernameGuidMap[username]
            if not guid then -- try to read the map again in case it was updated
                PzWebStats.ReadUsernameGuidMap()
                guid = PzWebStats.UsernameGuidMap[username]
            end
            sendServerCommand(player, "PzWebStats", "GuidForUsername", {guid})
        end
        if command == "Dump" then
            local type = args[1]
            if type == "allitems" then
                PzWebStats.DoAllItemDump()
            end
            if type == "gametime" then
                PzWebStats.WriteGameTime()
            end
            if type == "weatherforecast" then
                PzWebStats.WriteWeatherForecast(true)
            end
        end
    end
end)