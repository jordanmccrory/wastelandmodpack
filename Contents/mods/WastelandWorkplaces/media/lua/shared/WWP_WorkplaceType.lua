---
--- WWP_WorkplaceType.lua
--- 18/06/2023
---
require "XPController"
require "WL_Utils"

WWP_WorkplaceTypes = WWP_WorkplaceTypes or {}

-- WorkplaceType base class
---@public field name string
WWP_WorkplaceType = {}

---@param key string This MUST NOT EVER CHANGE once you put it in
---@param name string Human visible name. Change it as much as you want.
---@param percentXP number What % of rewards will be XP instead of loot items
---@param lootTable table of loot and weighting to how likely it appears
---@param xpTable table for XP, works similar to above but includes how much XP they get if it comes up as a reward
function WWP_WorkplaceType:new(key, name, percentXP, lootTable, xpTable)
	local obj = {}
	setmetatable(obj, self)
	self.__index = self
	obj.name = name
	obj.key = key
	obj.percentXP = percentXP
	obj.lootTable = {}
	obj:appendLootTable(lootTable)
	obj.xpTable = xpTable
	obj:initialise()
	WWP_WorkplaceTypes[key] = obj
	return obj
end

function WWP_WorkplaceType:initialise()
	self.totalXPWeight = 0
	for _, row in ipairs(self.xpTable) do
		self.totalXPWeight = self.totalXPWeight + row.weighting
	end
end

function WWP_WorkplaceType:appendLootTable(lootTable)
	for item, weight in pairs(lootTable) do
		self.lootTable[item] = weight
	end

	-- Recalculate total weight
	self.totalWeight = 0
	for item, weight in pairs(self.lootTable) do
		self.totalWeight = self.totalWeight + weight
	end
end

--- Get an array of string describing the benefits of the establishment
-- e.g. {"+ Energy", "+ Happiness" }
function WWP_WorkplaceType:getBenefits() return {} end

--- Applies benefits like + happiness or healing to the player inside
------@param player IsoGameCharacter
function WWP_WorkplaceType:applyBenefits(player) end

--- What percent of work points are taken from the player each hour at this workplace
function WWP_WorkplaceType:getWorkPercent() return 10 end

---@return boolean true if visitors don't get benefits if no employee is present (e.g. no doctor means no bonus healing from the clinic)
function WWP_WorkplaceType:requireEmployeesForBenefits() return true end

---@return boolean true if employees don't get paid/xp etc unless SOMEONE is there. Can be another employee!
function WWP_WorkplaceType:requireSomeonePresentForRewards() return true end

---@return boolean true if employees don't get paid/xp etc unless a non-employee is around for them to server
---Note that this won't do anything unless :requireSomeonePresentForRewards is also set to true.
function WWP_WorkplaceType:requireCustomersForRewards() return false end

---@return boolean true if we do speedy ticks for employee work points (2 minutes) instead of slow ticks (5 minutes)
function WWP_WorkplaceType:doSpeedyTicks() return false end

---@return number salary value of the job per tick. Should be either 1 or 0 right now, but we will support other values later
function WWP_WorkplaceType:getSalary() return 0 end

---@return string|nil the id of the sound for non-employees entering the workplace. This can be nil. It is played for all players
function WWP_WorkplaceType:getVisitorEnterSound() return nil end

---@return string|nil the id of the sound for non-employees exiting the workplace. This can be nil. It is played for all players
function WWP_WorkplaceType:getVisitorExitSound() return nil end

--- A callback every minute a player is in a workplace.
---@param player IsoGameCharacter
---@param workplaceZone WWP_WorkplaceZone
function WWP_WorkplaceType:onMinuteTick(player, workplaceZone) end

--- A callback every two minutes a player is in a workplace.
---@param player IsoGameCharacter
---@param workplaceZone WWP_WorkplaceZone
function WWP_WorkplaceType:onTwoMinuteTick(player, workplaceZone) end

--- A callback every five minutes a player is in a workplace.
---@param player IsoGameCharacter
---@param workplaceZone WWP_WorkplaceZone
function WWP_WorkplaceType:onFiveMinuteTick(player, workplaceZone) end

--- Generates a reward and returns a string describing it e.g. "Gained Item: Donuts" or "Gained Skill: Nimble 100XP"
--- If the workplace provides a salary too, it will append that on a new line afterwards, e.g. "Gained Currency: 1 Guilder"
---@param player IsoGameCharacter
function WWP_WorkplaceType:generateReward(player)
	local rewardString = ""
	local rewardGenerated = self:generateItemOrXpReward(player)
	if rewardGenerated then
		rewardString = "Gained " .. rewardGenerated .. "\n"
	end

	if self:getSalary() > 0 and getActivatedMods():contains("WastelandItemTweaks") then
		player:getEmitter():playSoundImpl("CoinsClinkHeavy", nil);
		local inventory = player:getInventory()

		local newItem
		for _ = 1, self:getSalary() do
			newItem = inventory:AddItem("GoldCurrency")
		end

		if(newItem) then
			WL_Utils.addToChat("Gained Item: " .. newItem:getName() .. " (" .. tostring(self:getSalary()) .. ")",
					{ color = "1.0,0.8,0.2", })
			rewardString = rewardString  .. "Gained Currency: "
					.. tostring(self:getSalary()) .. " " .. newItem:getName()
		end
	end
	return rewardString
end

--- Can return nil
function WWP_WorkplaceType:generateItemOrXpReward(player)
	local random = ZombRand(0,  100)
	if(random < self.percentXP) then
		local xpReward = self:giveRandomXpReward(player)
		if xpReward then
			return xpReward
		else
			return self:giveRandomLootReward(player)
		end
	else
		return self:giveRandomLootReward(player)
	end
end

---@return string|nil with the xp reward description or nil if they were level capped so didn't get one
function WWP_WorkplaceType:giveRandomXpReward(player)
	if WL_Utils.isEmpty(self.xpTable) then return nil end
	local perk, amount = self:rollXP()
	local cap = getLevelCap(player, perk)
	local isSkillCapped = player:getPerkLevel(perk) == 10 or ((cap ~= nil) and (player:getPerkLevel(perk) >= cap))
	if isSkillCapped then
		return nil
	end

	local perkBoost = player:getXp():getPerkBoost(perk)
	amount = amount * (perkBoost + 1) -- Not perfect but okay to increase XP gains
	WL_Utils.gainXP(perk, amount)
	local xpAmountString = tostring(amount) .. "XP"

	-- Check if we just leveled up
	isSkillCapped = (cap ~= nil) and (player:getPerkLevel(perk) >= cap)
	if isSkillCapped then  -- If we have hit the cap, set XP to the cap level exactly
		player:getXp():setXPToLevel(perk, cap)
		xpAmountString = "MAX LEVEL"
	else
		if self:getSalary() == 0 then -- Don't play this if the coins sound is playing or they leveled up
			player:getEmitter():playSoundImpl("GuitarSound", nil);
		end
	end

	return perk:getName() .. ": " .. xpAmountString
end


function WWP_WorkplaceType:rollXP()
	local randomNumber = ZombRand(1, self.totalXPWeight+1)
	local cumulativeWeighting = 0
	for _, row in ipairs(self.xpTable) do
		cumulativeWeighting = cumulativeWeighting + row.weighting
		if randomNumber <= cumulativeWeighting then
			return row.perk, row.amount
		end
	end
end

---@param player IsoGameCharacter
---@return string item name given or nil
function WWP_WorkplaceType:giveRandomLootReward(player)
	if WL_Utils.isEmpty(self.lootTable) then return nil end
	local randomNumber = ZombRand(1,  self.totalWeight+1)
	local cumulativeWeight = 0
	for item, weight in pairs(self.lootTable) do
		cumulativeWeight = cumulativeWeight + weight
		if randomNumber <= cumulativeWeight then
			local itemName = WL_Utils.addItemToInventory(item)
			if itemName then
				if self:getSalary() == 0 then -- Don't play this if the coins sound is playing
					player:getEmitter():playSoundImpl("BoxSound", nil);
				end
				return "Item: " .. itemName
			else
				return "ERROR: CANNOT FIND ITEM " .. item
			end
		end
	end
	return nil	-- shouldn't ever reach this point
end

--- Utility method
---@param player IsoGameCharacter
function WWP_WorkplaceType:decreaseUnhappy(player, amount)
	local sadnessAdjust = player:getBodyDamage():getUnhappynessLevel()
	if(sadnessAdjust > 0) then
		sadnessAdjust = math.max(0, sadnessAdjust - amount)
		player:getBodyDamage():setUnhappynessLevel(sadnessAdjust)
	end
end

--- Utility method
---@param player IsoGameCharacter
function WWP_WorkplaceType:decreaseBoredom(player, amount)
	local boredomAdjust =  player:getBodyDamage():getBoredomLevel()
	if(boredomAdjust > 0) then
		boredomAdjust = math.max(0, boredomAdjust - amount)
		player:getBodyDamage():setBoredomLevel(boredomAdjust)
	end
end

--- Utility method
---@param player IsoGameCharacter
function WWP_WorkplaceType:decreaseStress(player, amount)
	amount = amount / 100 -- Stress is a decimal from 0.0 to 1.0, so consistent!
	local stats = player:getStats()
	local stressAdjust = stats:getStress()
	if(stressAdjust > 0) then
		stressAdjust = math.max(0, stressAdjust - amount)
		stats:setStress(stressAdjust)
	end
end

--------------------------- Workplace Type Definitions  -------------------------------

WorkplaceBar = WWP_WorkplaceType:new("bar", "Bar (2G)", 10, {},
		{ 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Nimble, amount = 35, weighting = 3 },
})

function WorkplaceBar:getBenefits()
	return {"+ Happiness", "- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceBar:applyBenefits(player)
	WWP_WorkplaceType:decreaseBoredom(player, 10)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
end

function WorkplaceBar:requireCustomersForRewards()
	return true
end

function WorkplaceBar:getSalary() return 2 end

WorkplaceClinic = WWP_WorkplaceType:new("clinic", "Clinic", 70, {}, {
	{ perk = Perks.Doctor, amount = 50, weighting = 8 },
	{ perk = Perks.Lightfoot, amount = 50, weighting = 2 },
	{ perk = Perks.SmallBlade, amount = 50, weighting = 1 },
})

function WorkplaceClinic:getBenefits()
	return {"+ Healing"}
end

---@param player IsoGameCharacter
function WorkplaceClinic:applyBenefits(player)
	local sicknessAdjust = player:getBodyDamage():getFoodSicknessLevel() - 3
	sicknessAdjust = math.max(sicknessAdjust, 0)
	player:getBodyDamage():setFoodSicknessLevel(sicknessAdjust)

	for i = 0, player:getBodyDamage():getBodyParts():size() - 1 do
		local bodyPart = player:getBodyDamage():getBodyParts():get(i);

		local bleedingTime = bodyPart:getBleedingTime()
		if(bleedingTime > 0) then
			bleedingTime = math.max(0, bleedingTime - 0.1)
			bodyPart:setBleedingTime(bleedingTime)
		end

		local scratchTime = bodyPart:getScratchTime()
		if(scratchTime > 0) then
			scratchTime = math.max(0, scratchTime - 0.3)
			bodyPart:setScratchTime(scratchTime)
		end

		local cutTime = bodyPart:getCutTime()
		if(cutTime > 0) then
			cutTime = math.max(0, cutTime - 0.12)
			bodyPart:setCutTime(cutTime)
		end

		local deepWoundTime = bodyPart:getDeepWoundTime()
		if(deepWoundTime > 0) then
			deepWoundTime = math.max(0, deepWoundTime - 0.06)
			bodyPart:setDeepWoundTime(deepWoundTime)
		end

		local fractureTime = bodyPart:getFractureTime()
		if(fractureTime > 0) then
			fractureTime = math.max(0, fractureTime - 0.01)
			bodyPart:setFractureTime(fractureTime)
		end

		local biteTime = bodyPart:getBiteTime()
		if(biteTime > 0) then
			biteTime = math.max(0, biteTime - 0.2)
			bodyPart:setBiteTime(biteTime)
		end
	end
end

function WorkplaceClinic:requireCustomersForRewards()
	return true
end

function WorkplaceClinic:getSalary() return 2 end

WorkplaceLibrary = WWP_WorkplaceType:new("library","Library", 80, {
	["Base.SheetPaper2"] = 4,
	["Base.Notebook"] = 1,
	["Base.Book"] = 6,
	["Base.Newspaper"] = 2,
	["Base.Magazine"] = 2,
	["Base.Peppermint"] = 1,
}, {
	{ perk = Perks.Lightfoot, amount = 35, weighting = 2 },
})

function WorkplaceLibrary:getBenefits()
	return {"- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceLibrary:applyBenefits(player)
	WWP_WorkplaceType:decreaseBoredom(player, 10)
end

function WorkplaceLibrary:requireEmployeesForBenefits()
	return false
end

function WorkplaceLibrary:requireSomeonePresentForRewards()
	return false
end

function WorkplaceLibrary:getSalary() return 1 end
function WorkplaceLibrary:doSpeedyTicks() return true end


WorkplaceChineseRestaurant = WWP_WorkplaceType:new("restaurant_chinese", "Chinese Restaurant", 25, {
	["Base.Soysauce"] = 1,
	["Base.RiceVinegar"] = 1,
	["Base.OilVegetable"] = 1,
	["Base.Honey"] = 1,
	["Base.Butter"] = 1,
	["Base.Avocado"] = 3,
	["Base.Seaweed"] = 1,
	["Base.MeatDumpling"] = 2,
	["Base.ShrimpDumpling"] = 2,
	["Base.BakingSoda"] = 1,
	["Base.Shrimp"] = 5,
	["Base.Ramen"] = 3,
	["Base.Pepper"] = 1,
	["Base.Mustard"] = 1,
	["Base.DriedBlackBeans"] = 2,
	["Base.TofuFried"] = 2,
	["Base.Wasabi"] = 3,
	["Base.Peanuts"] = 3,
	["Base.Springroll"] = 3,
	["Base.Lobster"] = 3,
	["Base.Squid"] = 3,
	["Base.Crayfish"] = 3,
	["Base.BouillonCube"] = 2,
}, {
	{ perk = Perks.Cooking, amount = 70, weighting = 10 },
	{ perk = Perks.Nimble, amount = 30, weighting = 2 },
	{ perk = Perks.SmallBlade, amount = 35, weighting = 2 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceChineseRestaurant:appendLootTable({
		["SapphCooking.SoySauce_Sachet"] = 16,
		["SapphCooking.HotsaucePacket"] = 10,
		["SapphCooking.MonosodiumGlutamate_MSG"] = 2,
		["SapphCooking.SaltPacket"] = 6,
		["SapphCooking.CurryPowder"] = 3,
		["SapphCooking.BagofWontonWrappers"] = 4,
		["SapphCooking.MisoPaste"] = 4,
	})
end

function WorkplaceChineseRestaurant:getBenefits()
	return {"+ Happiness", "- Stress"}
end

---@param player IsoGameCharacter
function WorkplaceChineseRestaurant:applyBenefits(player)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
	WWP_WorkplaceType:decreaseStress(player, 10)
end

function WorkplaceChineseRestaurant:requireCustomersForRewards()
	return true
end

function WorkplaceChineseRestaurant:getSalary() return 1 end

WorkplaceFrenchRestaurant = WWP_WorkplaceType:new("restaurant_french", "French Restaurant", 25, {
	["Base.Wine"] = 4,
	["Base.Wine2"] = 12,
	["Base.FrogMeat"] = 2,
	["Base.Steak"] = 8,
	["Base.Yeast"] = 2,
	["Base.Cheese"] = 2,
	["Base.Pepper"] = 1,
	["Base.OilOlive"] = 1,
	["Base.Vinegar"] = 1,
	["Base.MeatPatty"] = 5,
	["Base.EggCarton"] = 3,
	["Base.Chocolate"] = 2,
	["Base.Milk"] = 3,
	["Base.Salmon"] = 6,
	["Base.CakeCheesecake"] = 1,
	["Base.CakeBlackForest"] = 1,
	["Base.CakeChocolate"] = 1,
	["Base.CakeRedVelvet"] = 1,
	["Base.CakeStrawberryShortcake"] = 1,
	["Base.PiePumpkin"] = 1,
	["Base.Cupcake"] = 1,
	["Base.SugarBrown"] = 3,
	["Base.Butter"] = 3,
	["Base.BakingSoda"] = 1,
}, {
	{ perk = Perks.Cooking, amount = 70, weighting = 10 },
	{ perk = Perks.Nimble, amount = 30, weighting = 2 },
	{ perk = Perks.SmallBlade, amount = 20, weighting = 2 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceFrenchRestaurant:appendLootTable({
		["SapphCooking.WhiteChocolate"] = 2,
		["SapphCooking.SapphCookingSugar"] = 3,
	})
end

function WorkplaceFrenchRestaurant:getBenefits()
	return {"+ Happiness", "- Stress"}
end

---@param player IsoGameCharacter
function WorkplaceFrenchRestaurant:applyBenefits(player)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
	WWP_WorkplaceType:decreaseStress(player, 10)
end

function WorkplaceFrenchRestaurant:requireCustomersForRewards()
	return true
end

function WorkplaceFrenchRestaurant:getSalary() return 1 end

WorkplaceTailor = WWP_WorkplaceType:new("tailor","Tailor", 50, {
	["Base.Yarn"] = 9,
	["Base.Thread"] = 13,
	["Base.Twine"] = 4,
	["Base.LeatherStrips"] = 7,
	["Base.DenimStrips"] = 5,
	["Base.RippedSheets"] = 4,
}, {
	{ perk = Perks.Tailoring, amount = 70, weighting = 10 },
	{ perk = Perks.Maintenance, amount = 20, weighting = 1 },
})

if getActivatedMods():contains("WastelandItemTweaks") then
	WorkplaceTailor:appendLootTable({
		["Base.KevlarSheet"] = 1,
	})
end

function WorkplaceTailor:doSpeedyTicks() return true end
function WorkplaceTailor:requireCustomersForRewards() return true end
function WorkplaceTailor:getVisitorEnterSound() return "ShopDoorBell" end
function WorkplaceTailor:getVisitorExitSound() return "ShopDoorBell2" end
function WorkplaceTailor:getSalary() return 2 end

WorkplaceGeneralStore= WWP_WorkplaceType:new("general_store", "General Store", 5, {
	["Base.EmptySandbag"] = 2,
	["Base.AlarmClock2"] = 1,
	["Base.Earbuds"] = 2,
	["Radio.WalkieTalkie3"] = 1,
	["Base.Extinguisher"] = 1,
	["Base.Candle"] = 2,
	["Base.HandTorch"] = 1,
	["Base.Matches"] = 2,
	["Base.Pot"] = 1,
	["Base.Pan"] = 1,
	["Base.Woodglue"] = 2,
	["Base.DuctTape"] = 2,
	["Base.FishingTackle"] = 1,
	["Base.FishingTackle2"] = 1,
	["Base.FishingLine"] = 2,
	["Base.EmptyPetrolCan"] = 1,
	["Base.GardeningSprayEmpty"] = 1,
	["Base.Battery"] = 2,
	["Base.LightBulb"] = 1,
	["Base.Rope"] = 2,
	["Base.NailsBox"] = 1,
	["Base.Screws"] = 1,
	["Base.WeldingRods"] = 1,
	["Base.Wire"] = 1,
	["Base.BucketEmpty"] = 1,
	["Base.Flour"] = 1,
	["Base.Vinegar"] = 1,
	["Base.Sugar"] = 1,
	["Base.Twine"] = 2,
	["Base.Cigarettes"] = 2,
	["Base.Notebook"] = 1,
	["Base.Coffee2"] = 1,
	["Base.CocoaPowder"] = 1,
	["Base.Teabag2"] = 1,
	["Base.KnittingNeedles"] = 1,
	["Base.Needle"] = 1,
	["Base.Fertilizer"] = 2,
	["Base.PlasterPowder"] = 2,
	["farming.BroccoliBagSeed"] = 1,
	["farming.CabbageBagSeed"] = 1,
	["farming.CarrotBagSeed"] = 1,
	["farming.PotatoBagSeed"] = 1,
	["farming.RedRadishBagSeed"] = 1,
	["farming.StrewberrieBagSeed"] = 1,
	["farming.TomatoBagSeed"] = 1,
	["farming.HandShovel"] = 1,
	["Base.GardenHoe"] = 1,
	["Base.GardenFork"] = 1,
	["Base.Pencil"] = 1,
	["Base.Eraser"] = 1,
	["Base.TrapCage"] = 1,

	-- Cans
	["Base.TinnedBeans"] = 1,
	["Base.CannedCorn"] = 1,
	["Base.CannedCornedBeef"] = 1,
	["Base.CannedFruitCocktail"] = 1,
	["Base.CannedMushroomSoup"] = 1,
	["Base.CannedPeaches"] = 1,
	["Base.CannedPeas"] = 1,
	["Base.CannedPineapple"] = 1,
	["Base.CannedPotato2"] = 1,
	["Base.CannedSardines"] = 2,
	["Base.TinnedSoup"] = 1,
	["Base.CannedBolognese"] = 1,
	["Base.CannedTomato2"] = 1,
	["Base.TunaTin"] = 1,
	["Base.Dogfood"] = 1,

	-- Jars
	["Base.CannedBellPepper"] = 1,
	["Base.CannedBroccoli"] = 1,
	["Base.CannedCabbage"] = 1,
	["Base.CannedCarrots"] = 1,
	["Base.CannedEggplant"] = 1,
	["Base.CannedLeek"] = 2,
	["Base.CannedPotato"] = 1,
	["Base.CannedRedRadish"] = 1,
	["Base.CannedTomato"] = 1,

	["Base.Bleach"] = 3,
	["Base.CleaningLiquid2"] = 3,
	["Base.Mop"] = 1,
	["Base.Hammer"] = 1,
	["Base.Saw"] = 1,
	["Base.Screwdriver"] = 1,
	["Base.WoodAxe"] = 1,
	["Base.Tarp"] = 1,
	["Base.Doll"] = 1,
	["Base.WaterDish"] = 1,
	["Base.DogChew"] = 1,

	-- Paint
	["Base.PaintBlack"] = 1,
	["Base.PaintBlue"] = 1,
	["Base.PaintBrown"] = 1,
	["Base.Paintbrush"] = 1,
	["Base.PaintCyan"] = 1,
	["Base.PaintGreen"] = 1,
	["Base.PaintGrey"] = 1,
	["Base.PaintLightBlue"] = 1,
	["Base.PaintLightBrown"] = 1,
	["Base.PaintOrange"] = 1,
	["Base.PaintPink"] = 1,
	["Base.PaintPurple"] = 1,
	["Base.PaintRed"] = 1,
	["Base.PaintTurquoise"] = 1,
	["Base.PaintWhite"] = 1,
	["Base.PaintYellow"] = 1,

}, {
	{ perk = Perks.Electricity, amount = 25, weighting = 1 },
	{ perk = Perks.Maintenance, amount = 30, weighting = 2 },
})

function WorkplaceGeneralStore:getSalary() return 1 end

function WorkplaceGeneralStore:doSpeedyTicks() return true end

function WorkplaceGeneralStore:getVisitorEnterSound() return "ShopDoorBell" end

function WorkplaceGeneralStore:getVisitorExitSound() return "ShopDoorBell2" end

if getActivatedMods():contains("SGarden-Homestead") then
	WorkplaceGeneralStore:appendLootTable({
		["Sprout.PearBagSeed"] = 1,
		["Sprout.WheatBagSeed"] = 1,
		["Sprout.ZucchiniBagSeed"] = 1,
		["Sprout.AppleBagSeed"] = 1,
		["Sprout.CherryBagSeed"] = 1,
		["Sprout.CornBagSeed"] = 1,
		["Sprout.GingerBagSeed"] = 1,
		["Sprout.GinsengBagSeed"] = 1,
		["Sprout.LeekBagSeed"] = 1,
		["Sprout.LettuceBagSeed"] = 1,
		["Sprout.OnionBagSeed"] = 1,
		["Sprout.PumpkinBagSeed"] = 1,
		["Sprout.SoyBeanBagSeed"] = 1,
	})
end

if getActivatedMods():contains("sapphcooking") then
	WorkplaceGeneralStore:appendLootTable({
		["SapphCooking.MetalWhisk"] = 1,
		["SapphCooking.DoughnutCutter"] = 1,
		["SapphCooking.PipingBags"] = 1,
		["SapphCooking.BakingMolds"] = 1,
		["SapphCooking.ClothFilter"] = 1,
		["SapphCooking.WoodenSkewers"] = 1,
		["SapphCooking.MessTray"] = 1,
		["SapphCooking.PizzaCutter"] = 1,
		["SapphCooking.Laddle"] = 1,
		["SapphCooking.MeatTenderizer"] = 1,
		["SapphCooking.WokPan"] = 1,
		["SapphCooking.EmptyThermos"] = 1,
		["SapphCooking.PlasticFilterHolder"] = 1,
		["SapphCooking.CoffeeGrinder"] = 1,
	})
end


WorkplaceConstruction = WWP_WorkplaceType:new("construction_services", "Construction Services", 30, {
	["Base.Vest_HighViz"] = 1,
	["Base.Hat_HardHat"] = 1,
	["Base.Toolbox"] = 1,
	["Base.Hammer"] = 1,
	["Base.Saw"] = 1,
	["Base.Screwdriver"] = 1,
	["Base.NailsBox"] = 2,
	["Base.Nails"] = 15,
	["Base.Screws"] = 8,
	["Base.Hinge"] = 4,
	["Base.Doorknob"] = 2,
	["Base.SheetMetal"] = 1,
	["Base.SmallSheetMetal"] = 1,
	["Base.PlasterPowder"] = 4,
	["Base.ConcretePowder"] = 1,
	["Base.Gravelbag"] = 8,
	["Base.BarbedWire"] = 3,
	["Base.Woodglue"] = 1,
	["Base.WeldingRods"] = 1,

	-- Paint
	["Base.PaintBlack"] = 1,
	["Base.PaintBlue"] = 1,
	["Base.PaintBrown"] = 1,
	["Base.Paintbrush"] = 1,
	["Base.PaintCyan"] = 1,
	["Base.PaintGreen"] = 1,
	["Base.PaintGrey"] = 1,
	["Base.PaintLightBlue"] = 1,
	["Base.PaintLightBrown"] = 1,
	["Base.PaintOrange"] = 1,
	["Base.PaintPink"] = 1,
	["Base.PaintPurple"] = 1,
	["Base.PaintRed"] = 1,
	["Base.PaintTurquoise"] = 1,
	["Base.PaintWhite"] = 1,
	["Base.PaintYellow"] = 1,
}, {
	{ perk = Perks.Woodwork, amount = 50, weighting = 10 },
	{ perk = Perks.MetalWelding, amount = 40, weighting = 5 },
	{ perk = Perks.Maintenance, amount = 20, weighting = 3 },
})

function WorkplaceConstruction:getSalary() return 1 end
function WorkplaceConstruction:doSpeedyTicks() return true end
function WorkplaceConstruction:requireSomeonePresentForRewards() return false end

WorkplaceMechanicShop = WWP_WorkplaceType:new("mechanic_shop", "Mechanic Shop", 50, {
	["Base.Wrench"] = 1,
	["Base.Jack"] = 1,
	["Base.LugWrench"] = 1,
	["Base.CarBatteryCharger"] = 1,
	["Base.TirePump"] = 1,
	["Base.BlowTorch"] = 1,
	["Base.EmptyPetrolCan"] = 6,
	["Base.LightBulb"] = 7,
	["Base.Screwdriver"] = 1,
	["Base.DuctTape"] = 5,
	["Base.ScrapMetal"] = 20,
	["Base.SheetMetal"] = 4,
	["Base.SmallSheetMetal"] = 8,
	["Base.MetalBar"] = 8,
	["Base.MetalPipe"] = 8,
	["Base.Hinge"] = 5,
	["Base.Screws"] = 15,
	["Base.EngineParts"] = 40,
	["Base.NormalSuspension1"] = 1,
	["Base.NormalSuspension2"] = 1,
	["Base.NormalSuspension3"] = 1,
	["Base.ModernSuspension1"] = 1,
	["Base.ModernSuspension2"] = 1,
	["Base.ModernSuspension3"] = 1,
	["Base.NormalCarMuffler1"] = 1,
	["Base.NormalCarMuffler2"] = 1,
	["Base.NormalCarMuffler3"] = 1,
	["Base.ModernCarMuffler1"] = 1,
	["Base.ModernCarMuffler2"] = 1,
	["Base.ModernCarMuffler3"] = 1,
	["Base.SmallGasTank1"] = 1,
	["Base.SmallGasTank2"] = 1,
	["Base.SmallGasTank3"] = 1,
	["Base.NormalGasTank1"] = 1,
	["Base.NormalGasTank2"] = 1,
	["Base.NormalGasTank3"] = 1,
	["Base.BigGasTank1"] = 1,
	["Base.BigGasTank2"] = 1,
	["Base.BigGasTank3"] = 1,
}, {
	{ perk = Perks.Mechanics, amount = 70, weighting = 10 },
	{ perk = Perks.Maintenance, amount = 20, weighting = 1 },
})

function WorkplaceMechanicShop:getSalary() return 1 end
function WorkplaceMechanicShop:doSpeedyTicks() return true end
function WorkplaceMechanicShop:requireSomeonePresentForRewards() return false end

if getActivatedMods():contains("ImprovisedGlass-Wasteland") then
	WorkplaceMechanicShop:appendLootTable({
		["ImprovisedGlass.GlassPane"] = 3,
	})
end

if getActivatedMods():contains("WastelandCarParts") then
	WorkplaceMechanicShop:appendLootTable({
		["Base.CuredRubber"] = 3,
	})
end


WorkplaceFarm = WWP_WorkplaceType:new("farm", "Farm", 35, {
	["farming.BroccoliSeed"] = 1,
	["farming.CabbageSeed"] = 1,
	["farming.CarrotSeed"] = 1,
	["farming.PotatoSeed"] = 1,
	["farming.RedRadishSeed"] = 1,
	["farming.StrewberrieSeed"] = 1,
	["farming.TomatoSeed"] = 1,
	["farming.Bacon"] = 2,
	["Base.Milk"] = 5,
	["Base.Butter"] = 1,
	["Base.Fertilizer"] = 3,
	["Base.Egg"] = 15,
	["Base.Chicken"] = 2,
	["Base.ChickenFoot"] = 2,
}, {
	{ perk = Perks.Farming, amount = 70, weighting = 10 },
	{ perk = Perks.Spear, amount = 10, weighting = 1 },
})

if getActivatedMods():contains("SGarden-Homestead") then
	WorkplaceFarm:appendLootTable({
		["Sprout.PearSeeds"] = 1,
		["Sprout.CommonMallowSeeds"] = 1,
		["Sprout.PlantainSeeds"] = 1,
		["Sprout.ComfreySeeds"] = 1,
		["Sprout.GarlicSeeds"] = 1,
		["Sprout.BlackSageSeeds"] = 1,
		["Sprout.AppleSeed"] = 1,
		["Sprout.AvocadoSeed"] = 1,
		["Sprout.BananaSeed"] = 1,
		["Sprout.BellPepperSeed"] = 1,
		["Sprout.BerryBlackSeed"] = 1,
		["Sprout.BerryBlueSeed"] = 1,
		["Sprout.CherrySeed"] = 1,
		["Sprout.CornSeed"] = 1,
		["Sprout.EggplantSeed"] = 1,
		["Sprout.GingerSeed"] = 1,
		["Sprout.GinsengSeed"] = 1,
		["Sprout.GrapefruitSeed"] = 1,
		["Sprout.GrapeSeed"] = 1,
		["Sprout.LeekSeed"] = 1,
		["Sprout.LemongrassSeed"] = 1,
		["Sprout.LemonSeed"] = 1,
		["Sprout.LettuceSeed"] = 1,
		["Sprout.LimeSeed"] = 1,
		["Sprout.MangoSeed"] = 1,
		["Sprout.MushroomSpores"] = 1,
		["Sprout.OliveSeed"] = 1,
		["Sprout.OnionSeed"] = 1,
		["Sprout.OrangeSeed"] = 1,
		["Sprout.PeachSeed"] = 1,
		["Sprout.PineappleSeed"] = 1,
		["Sprout.PumpkinSeed"] = 1,
		["Sprout.SoyBeanSeed"] = 1,
		["Sprout.SugarCaneSeed"] = 1,
		["Sprout.WatermelonSeed"] = 1,
		["Sprout.WheatSeed"] = 1,
		["Sprout.ZucchiniSeed"] = 1,
		["Sprout.RiceSeed"] = 1,
		["Sprout.PepperPlantSeed"] = 1,
		["Sprout.CottonSeed"] = 1,
		["Sprout.HopsSeed"] = 1,
		["Sprout.TeaSeed"] = 1,
		["Sprout.CoffeeSeed"] = 1,
		["Sprout.RubberSeed"] = 1,
	})
end

function WorkplaceFarm:doSpeedyTicks() return true end
function WorkplaceFarm:getSalary() return 1 end
function WorkplaceFarm:requireSomeonePresentForRewards() return false end

WorkplaceMunitionsFactory = WWP_WorkplaceType:new("munitions_factory", "Munitions Factory", 25, {
	["Base.GunPowder"] = 8,
	["Base.Dirtbag"] = 1,
	["Base.Tongs"] = 1,
	["Base.ScrapMetal"] = 10,
	["Base.Charcoal"] = 2,
}, {
	{ perk = Perks.MetalWelding, amount = 50, weighting = 2 },
	{ perk = Perks.Reloading, amount = 30, weighting = 4 },
	{ perk = Perks.Gunsmith, amount = 40, weighting = 10 },
})

if getActivatedMods():contains("WastelandFirearms") then
	WorkplaceMunitionsFactory:appendLootTable({
		["Base.ShellCasings"] = 15,
		["Base.LeadFilings"] = 1,
		["Base.BrassSheet"] = 3,
	})
end

if getActivatedMods():contains("firearmmod") then
	WorkplaceMunitionsFactory:appendLootTable({
		["Base.Bullets22"] = 2,
		["Base.Bullets3006"] = 2,
		["Base.Bullets38"] = 2,
		["Base.Bullets4440"] = 4,
		["Base.ShotgunShells"] = 2,
		["Base.Bullets45"] = 2,
	})
end

function WorkplaceMunitionsFactory:getSalary() return 1 end
function WorkplaceMunitionsFactory:doSpeedyTicks() return true end
function WorkplaceMunitionsFactory:requireSomeonePresentForRewards() return false end

WorkplaceGunFactory = WWP_WorkplaceType:new("gun_crafter", "Gun Crafter", 30, {
	["Base.Wire"] = 2,
	["Base.SmallSheetMetal"] = 4,
	["Base.MetalPipe"] = 3,
	["Base.Screws"] = 2,
	["Base.ScrapMetal"] = 5,
}, {
	{ perk = Perks.MetalWelding, amount = 30, weighting = 3 },
	{ perk = Perks.Gunsmith, amount = 40, weighting = 10 },
})

if getActivatedMods():contains("WastelandFirearms") then
	WorkplaceGunFactory:appendLootTable({
		["Base.ClipSpring"] = 1,
		["Base.MachinedClipSteel"] = 3,
		["Base.MachinedFirearmComponents"] = 5,
	})
end

function WorkplaceGunFactory:getSalary() return 1 end
function WorkplaceGunFactory:doSpeedyTicks() return true end
function WorkplaceGunFactory:requireSomeonePresentForRewards() return false end

WorkplaceDrugLab = WWP_WorkplaceType:new("drug_lab", "Drug Lab", 30, {
	["Base.PillsAntiDep"] = 2,
	["Base.Pills"] = 2,
	["Base.PillsBeta"] = 3,
	["Base.PillsSleepingTablets"] = 1,
	["Base.Antibiotics"] = 2,
	["Base.PillsVitamins"] = 1,
}, {
	{ perk = Perks.Doctor, amount = 35, weighting = 8 },
	{ perk = Perks.Cooking, amount = 20, weighting = 1 },
})

function WorkplaceDrugLab:getSalary() return 1 end
function WorkplaceDrugLab:doSpeedyTicks() return true end
function WorkplaceDrugLab:requireSomeonePresentForRewards() return false end

if getActivatedMods():contains("marchingpowder") then
	WorkplaceDrugLab:appendLootTable({
		["Base.CokeBaggie"] = 8,
	})
end

if getActivatedMods():contains("More_Medical") then
	WorkplaceDrugLab:appendLootTable({
		["MoreMedical.PillsCommercialPainkillers"] = 1, -- Neboline Pills
		["MoreMedical.PillsPrescriptionPainkillers"] = 1, -- Torcine Pills
		["MoreMedical.PillsCaffeine"] = 1,
		["MoreMedical.PillsCommercialAntibiotic"] = 1, -- Idivon Pills
		["MoreMedical.PillsAntiPoisoning"] = 1, -- Antipzidin Pills
		["MoreMedical.PillsCommercialSedative"] = 1, -- Hrenozepam Pills
		["MoreMedical.PillsPharmacyPrescription"] = 1, -- Prescription Pills
		["MoreMedical.SyretteHighGradePainkillers"] = 5, -- Morphine Syrette
		["MoreMedical.SyretteAdrenalin"] = 3, -- Adrenalin Syrette
		["MoreMedical.SyrettePrescriptionAntibiotic"] = 1, -- Denashol Syrette
		["MoreMedical.SyrettePrescriptionSedatives"] = 1, -- US Pоcaine Syrette
		["MoreMedical.BottleFluMedication"] = 1, -- TsarFlu Bottle
		["MoreMedical.PowderPackFluMedication"] = 1, -- TsarFlu Powder Pack
		["MoreMedical.BurnTreatment"] = 1, -- Pog Orel Gel
		["MoreMedical.PowderPackHemostatic"] = 1, -- Hemostatic powder
		["MoreMedical.BottleCoughSyrup"] = 1, -- Bottle of cough syrup
		["MoreMedical.NasalSpray"] = 1, -- SoPlaR Forte Nasal Spray
	})
end


WorkplaceSoupKitchen = WWP_WorkplaceType:new("soup_kitchen", "Soup Kitchen", 20, {
	["Base.CannedMushroomSoup"] = 9,
	["Base.TinnedSoup"] = 9,
	["Base.SoupBowl"] = 15,
	["Base.NoodleSoup"] = 4,
	["Base.Baguette"] = 3,
	["Base.Bread"] = 5,
	["Base.Pepper"] = 1,
	["Base.DriedLentils"] = 1,
	["Base.CannedTomato2"] = 2,
	["Base.CannedCarrots2"] = 2,
	["Base.CannedPotato2"] = 2,
}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Cooking, amount = 50, weighting = 13 },
	{ perk = Perks.Fitness, amount = 30, weighting = 2 },
})

function WorkplaceSoupKitchen:getBenefits()
	return {"+ Happiness"}
end

function WorkplaceSoupKitchen:getWorkPercent() return 5 end

---@param player IsoGameCharacter
function WorkplaceSoupKitchen:applyBenefits(player)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
end

function WorkplaceSoupKitchen:requireCustomersForRewards()
	return true
end

WorkplaceGym = WWP_WorkplaceType:new("gym", "Gym (+1 & Items)", 20, {
	["Base.WaterBottleFull"] = 8,
	["Base.BathTowel"] = 1,
	["Base.EggCarton"] = 4,
	["Base.JuiceBox"] = 2,
}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Strength, amount = 50, weighting = 3 },
	{ perk = Perks.Fitness, amount = 50, weighting = 7 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceGym:appendLootTable({
		["SapphCooking.CanofProteinPowder"] = 1,
		["SapphCooking.BottlewithProteinShake"] = 23,
		["SapphCooking.ProteinBar"] = 8,
		["SapphCooking.BottleofOrangeJuice"] = 1,
		["SapphCooking.BottleofAppleJuice"] = 1,
		["SapphCooking.BottleofStrawberryJuice"] = 1,
		["SapphCooking.BottleofPeachJuice"] = 1,
		["SapphCooking.BottleofGrapeJuice"] = 1,
		["SapphCooking.BottleofWatermelonJuice"] = 1,
		["SapphCooking.BottleofLemonJuice"] = 1,
	})
end

function WorkplaceGym:getBenefits()
	return {"+ Fitness", "+ Strength", "- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceGym:applyBenefits(player)
	-- These benefit from XP multipliers (the second bool arg) so we need to be careful with them
	-- e.g. 75 at 8+ with the 5x Book is 375 per minute, which is 22.5k an hour which is pretty nice
	-- However this requires an employee to be there for the whole time!
	player:getXp():AddXP(Perks.Fitness, 75, false, true, false);
	player:getXp():AddXP(Perks.Strength, 35, false, true, false);
	WWP_WorkplaceType:decreaseBoredom(player, 10)
end

function WorkplaceGym:requireCustomersForRewards()
	return true
end

function WorkplaceGym:getSalary() return 1 end

WorkplaceGym2 = WWP_WorkplaceType:new("gym2", "Gym (+2 & No Items)", 20, {}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Strength, amount = 50, weighting = 3 },
	{ perk = Perks.Fitness, amount = 50, weighting = 7 },
})

function WorkplaceGym2:getBenefits()
	return {"+ Fitness", "+ Strength", "- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceGym2:applyBenefits(player)
	-- These benefit from XP multipliers (the second bool arg) so we need to be careful with them
	-- e.g. 75 at 8+ with the 5x Book is 375 per minute, which is 22.5k an hour which is pretty nice
	-- However this requires an employee to be there for the whole time!
	player:getXp():AddXP(Perks.Fitness, 75, false, true, false);
	player:getXp():AddXP(Perks.Strength, 35, false, true, false);
	WWP_WorkplaceType:decreaseBoredom(player, 10)
end

function WorkplaceGym2:requireCustomersForRewards()
	return true
end

function WorkplaceGym2:getSalary() return 2 end

WorkplaceFishingPier = WWP_WorkplaceType:new("fishing_pier", "Fishing Pier", 35, {
	["Base.Shrimp"] = 8,
	["Base.Lobster"] = 4,
	["Base.Squid"] = 4,
	["Base.Crayfish"] = 9,
	["Base.Oysters"] = 7,
	["Base.Worm"] = 4,
	["Base.FishingTackle"] = 1,
	["Base.FishingTackle2"] = 1,
	["Base.FishingLine"] = 1,
	["Base.BaitFish"] = 5,
	["Base.FishRoe"] = 4,
	["Base.TunaTin"] = 6,
	["Base.CannedSardines"] = 6,
	["Base.Salmon"] = 12,
	["Base.Frog"] = 1,
	["Base.Seaweed"] = 3,
}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Fishing, amount = 30, weighting = 13 },
	{ perk = Perks.Spear, amount = 30, weighting = 1 },
})

function WorkplaceFishingPier:getSalary() return 1 end
function WorkplaceFishingPier:doSpeedyTicks() return true end
function WorkplaceFishingPier:requireSomeonePresentForRewards() return false end


if getActivatedMods():contains("sapphcooking") then
	WorkplaceFishingPier:appendLootTable({
		["SapphCooking.SapphSushiFish"] = 4,
	})
end

WorkplaceCafe = WWP_WorkplaceType:new("cafe", "Cafe", 30, {
	["Base.Coffee2"] = 4,
	["Base.CocoaPowder"] = 2,
	["Base.DoughnutJelly"] = 3,
	["Base.PiePumpkin"] = 3,
	["Base.CakeRedVelvet"] = 5,
	["Base.CakeStrawberryShortcake"] = 4,
	["Base.DoughnutPlain"] = 1,
	["Base.DoughnutFrosted"] = 3,
	["Base.DoughnutChocolate"] = 3,
	["Base.CookieChocolateChip"] = 4,
	["Base.Cupcake"] = 3,
	["Base.Croissant"] = 5,
	["Base.BagelPoppy"] = 3,
	["Base.BagelSesame"] = 3,
	["Base.CakeCarrot"] = 3,
	["Base.CakeCheesecake"] = 3,
	["Base.Painauchocolat"] = 3,
	["Base.Chococakes"] = 3,
	["Base.CakeChocolate"] = 3,
	["Base.QuaggaCakes"] = 1,
	["Base.Baguette"] = 7,
	["Base.Bread"] = 7,
	["Base.PancakeMix"] = 3,
	["Base.WafflesRecipe"] = 3,
	["Base.MapleSyrup"] = 1,
	["Base.SugarPacket"] = 2,
}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Cooking, amount = 50, weighting = 8 },
	{ perk = Perks.Nimble, amount = 35, weighting = 2 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceCafe:appendLootTable({
		["SapphCooking.StrawberryMilk"] = 2,
		["SapphCooking.AlmondMilk"] = 2,
		["SapphCooking.PlasticSpork"] = 2,
		["SapphCooking.CoffeePacket"] = 2,
		["SapphCooking.BoxofTeaBags"] = 3,
		["SapphCooking.Pretzel"] = 3,
		["SapphCooking.Eclair"] = 3,
		["SapphCooking.PackofCoffeeFilters"] = 5,
		["SapphCooking.CoffeeBeansBag"] = 5,
		["SapphCooking.PlasticFilterHolder"] = 1,
		["SapphCooking.ColaBottle"] = 3,
		["SapphCooking.BottleofOrangeJuice"] = 1,
		["SapphCooking.BottleofAppleJuice"] = 1,
		["SapphCooking.BottleofStrawberryJuice"] = 1,
		["SapphCooking.BottleofPeachJuice"] = 1,
		["SapphCooking.BottleofGrapeJuice"] = 1,
		["SapphCooking.BottleofWatermelonJuice"] = 1,
		["SapphCooking.BottleofLemonJuice"] = 1,
	})
end

function WorkplaceCafe:getBenefits()
	return {"+ Happiness", "- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceCafe:applyBenefits(player)
	WWP_WorkplaceType:decreaseBoredom(player, 10)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
end

function WorkplaceCafe:requireCustomersForRewards()
	return true
end

function WorkplaceCafe:getSalary() return 1 end

WorkplaceOffice = WWP_WorkplaceType:new("office","Office", 15, {
	["Base.SheetPaper2"] = 30,
	["Base.Notebook"] = 1,
	["Base.RedPen"] = 1,
	["Base.Pencil"] = 1,
	["Base.BluePen"] = 1,
	["Base.Eraser"] = 1,
	["Base.Scissors"] = 1,
	["Base.Coffee2"] = 2,
	["Base.Teabag2"] = 8,
	["Base.Sugar"] = 1,
	["Base.MugRed"] = 2,
	["Base.MugWhite"] = 2,
	["Base.MugSpiffo"] = 1,
	["Base.Kettle"] = 1,
	["Base.DoughnutPlain"] = 2,
	["Base.DoughnutFrosted"] = 2,
	["Base.DoughnutJelly"] = 2,
	["Base.MuffinFruit"] = 3,
	["Base.Snackcakes_Plonkies"] = 1,
}, {
	{ perk = Perks.Electricity, amount = 10, weighting = 5 },
	{ perk = Perks.Maintenance, amount = 10, weighting = 2 },
})

function WorkplaceOffice:getSalary() return 1 end

function WorkplaceOffice:requireCustomersForRewards()
	return true
end

WorkplaceLoggingCamp = WWP_WorkplaceType:new("logging_camp","Logging Camp", 80, {
	["Base.WildEggs"] = 10,
	["Base.Violets"] = 2,
	["Base.Rosehips"] = 2,
	["Base.Grasshopper"] = 3,
	["Base.Cricket"] = 2,
	["Base.CommonMallow"] = 2,
	["Base.Worm"] = 3,
	["Base.Twigs"] = 7,
	["Base.HandAxe"] = 1,
	["Base.DeadBird"] = 3,
	["Base.Apple"] = 2,
	["Base.Rope"] = 2,
}, {
	{ perk = Perks.Axe, amount = 10, weighting = 10 },
	{ perk = Perks.Woodwork, amount = 10, weighting = 5 },
	{ perk = Perks.Strength, amount = 40, weighting = 4 },
	{ perk = Perks.PlantScavenging, amount = 20, weighting = 2 },
})

function WorkplaceLoggingCamp:getSalary() return 1 end

---@return boolean true if we do speedy ticks for employee work points (2 minutes) instead of slow ticks (5 minutes)
function WorkplaceLoggingCamp:doSpeedyTicks() return true end

function WorkplaceLoggingCamp:requireSomeonePresentForRewards()
	return false
end

if getActivatedMods():contains("WastelandBuilds") then
	WorkplaceMine = WWP_WorkplaceType:new("mine","Mine", 80, {
		["Base.Stone"] = 30,
		["Base.SharpedStone"] = 40,
		["Base.HammerStone"] = 10,
		["Base.Shovel"] = 5,
		["Base.PickAxe"] = 3,
		["Base.Sledgehammer"] = 1,
		["Base.WastelandBuildsIronOre"] = 1,
		["Base.WastelandBuildsPyrite"] = 1,
		["Base.WastelandBuildsSaltpeter"] = 1,
	}, {
		{ perk = Perks.Strength, amount = 40, weighting = 5 },
		{ perk = Perks.PlantScavenging, amount = 20, weighting = 5 },
	})

	function WorkplaceMine:getSalary() return 1 end
	function WorkplaceMine:doSpeedyTicks() return true end
	function WorkplaceMine:requireSomeonePresentForRewards() return false end

	function WorkplaceMine:onMinuteTick(player, workplace)
		if not workplace or not workplace.open or not workplace:isEmployee(player) then return end
		local possibleObjects = {}
		local exhaustedTileName = WBStoning.GetExhaustedTile()
		for x = workplace.minX, workplace.maxX do
			for y = workplace.minY, workplace.maxY do
				for z = workplace.minZ, workplace.maxZ do
					local cSquare = getCell():getGridSquare(x, y, z)
					if cSquare then
						local objects = cSquare:getObjects()
						for i=0, objects:size()-1 do
							local object = objects:get(i)
							if object and object:getSprite() and object:getSprite():getName() == exhaustedTileName then
								table.insert(possibleObjects, object)
							end
						end
					end
				end
			end
		end
		if #possibleObjects == 0 then return "Warning: No mined tiles to restore" end
		local object = possibleObjects[ZombRand(#possibleObjects)+1]
		local squareToReplace = object:getSquare()
		local index = object:getObjectIndex()
		local name = object:getName()
		squareToReplace:transmitRemoveItemFromSquare(object)
		squareToReplace:getObjects():remove(object)
		local newObj = IsoObject.getNew(squareToReplace, WBStoning.GetRandomDiggableTile(), name, false)
		squareToReplace:transmitAddObjectToSquare(newObj, index)
		return "Restored a mined tile"
	end
end

WorkplaceWeaponsStore = WWP_WorkplaceType:new("weapon_store", "Weapon Store", 15, {
	["Base.GunPowder"] = 2,
	["Base.ScrapMetal"] = 8,
	["Base.Axe"] = 1,
	["Base.HandAxe"] = 2,
	["Base.MetalPipe"] = 6,
	["Base.HuntingKnife"] = 4,
	["Base.MeatCleaver"] = 1,
	["Base.SpearIcePick"] = 1,
	["Base.SpearBreadKnife"] = 1,
	["Base.SpearScissors"] = 1,
	["Base.SpearScrewdriver"] = 1,
	["Base.SpearKnife"] = 1,
	["Base.SpearHuntingKnife"] = 1,
	["Base.LeatherStrips"] = 10,
	["Base.Screws"] = 4,
	["Base.DuctTape"] = 1,
	["Base.SheetMetal"] = 1,
	["Base.SmallSheetMetal"] = 2,
}, {
	{ perk = Perks.MetalWelding, amount = 40, weighting = 3 },
	{ perk = Perks.Reloading, amount = 25, weighting = 2 },
	{ perk = Perks.Gunsmith, amount = 40, weighting = 4 },
})

if getActivatedMods():contains("WastelandFirearms") then
	WorkplaceWeaponsStore:appendLootTable({
		["Base.ShellCasings"] = 5,
		["GreysWLWeaponsFirearms.ScrapShards"] = 15,
		["GreysWLWeaponsMelee.Femur"] = 1,
		["Base.ClipSpring"] = 2,
	})
end

if getActivatedMods():contains("firearmmod") then
	WorkplaceWeaponsStore:appendLootTable({
		["Base.Bullets22"] = 4,
		["Base.Bullets3006"] = 3,
		["Base.Bullets38"] = 3,
		["Base.Bullets4440"] = 3,
		["Base.ShotgunShells"] = 2,
		["Base.Bullets45"] = 2,
	})
end

function WorkplaceWeaponsStore:getSalary() return 1 end
function WorkplaceWeaponsStore:doSpeedyTicks() return true end
function WorkplaceWeaponsStore:getVisitorEnterSound() return "ShopDoorBell" end
function WorkplaceWeaponsStore:getVisitorExitSound() return "ShopDoorBell2" end
function WorkplaceWeaponsStore:requireCustomersForRewards() return true end


WorkplaceMusicStore = WWP_WorkplaceType:new("music_store", "Music Store", 15, {
	["Base.Saxophone"] = 1,
	["Base.Trumpet"] = 1,
	["Base.Violin"] = 1,
	["Base.Drumstick"] = 1,
	["Base.Flute"] = 1,
	["Base.GuitarAcoustic"] = 1,
	["Base.GuitarElectricBassBlack"] = 1,
	["Base.GuitarElectricBassBlue"] = 1,
	["Base.GuitarElectricBassRed"] = 1,
	["Base.GuitarElectricBlack"] = 1,
	["Base.GuitarElectricBlue"] = 1,
	["Base.GuitarElectricRed"] = 1,
	["Tsarcraft.CassetteACDCHighwayToHell(1979)"] = 10,
	["Tsarcraft.CassetteAirSupplyMakingLoveOutOfNothingAtAll(1983)"] = 10,
	["Tsarcraft.CassetteAlabamaChristmasInDixie(1985)"] = 10,
	["Tsarcraft.CassetteAliceCooperPoison(1989)"] = 10,
	["Tsarcraft.CassetteBeeGeesStayinAlive(1977)"] = 10,
	["Tsarcraft.CassetteBlondieCallMe(1978)"] = 10,
	["Tsarcraft.CassetteBlondieHeartOfGlass(1976)"] = 10,
	["Tsarcraft.CassetteBobbyDarinDreamLover(1987)"] = 10,
	["Tsarcraft.CassetteBonJoviLivinOnAPrayer(1986)"] = 10,
	["Tsarcraft.CassetteBonJoviYouGiveLoveABadName(1986)"] = 10,
	["Tsarcraft.CassetteBoneyMRasputin(1978)"] = 10,
	["Tsarcraft.CassetteBonnieTylerHoldingOutForAHero(1984)"] = 10,
	["Tsarcraft.CassetteBonnieTylerTotalEclipseOfTheHeart(1983)"] = 10,
	["Tsarcraft.CassetteBryanAdamsIDoItForYou(1991)"] = 10,
	["Tsarcraft.CassetteCharleyPrideKissAnAngelGoodMorning(1971)"] = 10,
	["Tsarcraft.CassetteCyndiLauperTimeAfterTime(1983)"] = 10,
	["Tsarcraft.CassetteDeadOrAliveYouSpinMeRound(1984)"] = 10,
	["Tsarcraft.CassetteDepecheModePersonalJesus(1989)"] = 10,
	["Tsarcraft.CassetteDollyPartonHardCandyChristmas(1982)"] = 10,
	["Tsarcraft.CassetteFlattAndScruggsFoggyMountainBreakdown(1968)"] = 10,
	["Tsarcraft.CassetteForeignerIWantToKnowWhatLoveIs(1984)"] = 10,
	["Tsarcraft.CassetteGeorgeBensonNothingsGonnaChange(1985)"] = 10,
	["Tsarcraft.CassetteIsraelKamakawiwooleOverTheRainbow(1990)"] = 10,
	["Tsarcraft.CassetteJohnDenverTakeMeHomeCountryRoads(1971)"] = 10,
	["Tsarcraft.CassetteJonaLewieStopTheCavalry(1978)"] = 10,
	["Tsarcraft.CassetteJoseFelicianoFelizNavidad(1970)"] = 10,
	["Tsarcraft.CassetteJourneyDontStopBelievin(1981)"] = 10,
	["Tsarcraft.CassetteKennyLogginsDangerZone(1986)"] = 10,
	["Tsarcraft.CassetteKissIWasMadeForLovinYou(1979)"] = 10,
	["Tsarcraft.CassetteLindaRonstadtAndAaronNevilleDontKnowMuch(1989)"] = 10,
	["Tsarcraft.CassetteMetallicaNothingElseMatters(1991)"] = 10,
	["Tsarcraft.CassetteMetallicaTheUnforgiven(1991)"] = 10,
	["Tsarcraft.CassetteMichaelJacksonBillieJean(1982)"] = 10,
	["Tsarcraft.CassetteNirvanaSmellsLikeTeenSpirit(1991)"] = 10,
	["Tsarcraft.CassettePaulEngemannPushItToTheLimit(1983)"] = 10,
	["Tsarcraft.CassettePaulaAbdulStraightUp(1988)"] = 10,
	["Tsarcraft.CassettePepeShadilay(1986)"] = 10,
	["Tsarcraft.CassetteQueen39(1975)"] = 10,
	["Tsarcraft.CassetteQueenWeAreTheChampions(1977)"] = 10,
	["Tsarcraft.CassetteQueenWeWillRockYou(1977)"] = 10,
	["Tsarcraft.CassetteREMLosingMyReligion(1991)"] = 10,
	["Tsarcraft.CassetteRandyTravisDeeperThanTheHoller(1988)"] = 10,
	["Tsarcraft.CassetteRandyTravisOldTimeChristmas(1989)"] = 10,
	["Tsarcraft.CassetteScottMcKenzieSanFrancisco(1967)"] = 10,
	["Tsarcraft.CassetteSnapRhythmIsADancer(1992)"] = 10,
	["Tsarcraft.CassetteSurvivorEyeOfTheTiger(1982)"] = 10,
	["Tsarcraft.CassetteTheB52sLoveShack(1989)"] = 10,
	["Tsarcraft.CassetteTheBeatlesComeTogether(1969)"] = 10,
	["Tsarcraft.CassetteTheBeatlesEleanorRigby(1966)"] = 10,
	["Tsarcraft.CassetteThePoguesFairytaleOfNewYork(1987)"] = 10,
	["Tsarcraft.CassetteTheTemptationsPapaWasARollingStone(1972)"] = 10,
	["Tsarcraft.CassetteTheWeatherGirlsItsRainingMen(1983)"] = 10,
	["Tsarcraft.CassetteTotoAfrica(1982)"] = 10,
	["Tsarcraft.CassetteTotoHoldTheLine(1978)"] = 10,
	["Tsarcraft.CassetteWhamLastChristmas(1984)"] = 10,
	["Tsarcraft.CassetteWhitneyHoustonIWillAlwaysLoveYou(1992)"] = 10,

}, {
	{ perk = Perks.Music, amount = 50, weighting = 10 },
	{ perk = Perks.PseudonymousEdPiano, amount = 50, weighting = 10 },
})

function WorkplaceMusicStore:getSalary() return 1 end
function WorkplaceMusicStore:doSpeedyTicks() return true end
function WorkplaceMusicStore:getVisitorEnterSound() return "ShopDoorBell" end
function WorkplaceMusicStore:getVisitorExitSound() return "ShopDoorBell2" end
function WorkplaceMusicStore:requireCustomersForRewards() return true end

WorkplaceGenericShop = WWP_WorkplaceType:new("player_stocked_shop", "Store (2G)",
		0, {}, {} )
function WorkplaceGenericShop:getSalary() return 2 end
function WorkplaceGenericShop:doSpeedyTicks() return true end
function WorkplaceGenericShop:getVisitorEnterSound() return "ShopDoorBell" end
function WorkplaceGenericShop:getVisitorExitSound() return "ShopDoorBell2" end
function WorkplaceGenericShop:requireCustomersForRewards() return true end

WorkplaceGenericHospitality = WWP_WorkplaceType:new("player_stocked_hospitality", "Hospitality (2G)",
		0, {}, {} )
function WorkplaceGenericHospitality:getSalary() return 2 end
function WorkplaceGenericHospitality:requireCustomersForRewards() return true end
function WorkplaceGenericHospitality:getBenefits() return {"+ Happiness", "- Boredom"}  end
---@param player IsoGameCharacter
function WorkplaceGenericHospitality:applyBenefits(player)
	WorkplaceGenericHospitality:decreaseBoredom(player, 10)
	WorkplaceGenericHospitality:decreaseUnhappy(player, 10)
end

WorkplaceBrewery = WWP_WorkplaceType:new("brewery", "Brewery",
		35, {
			["Base.WhiskeyFull"] = 5,
			["Base.Sugar"] = 8,
			["Base.Yeast"] = 1,

		}, {
			{ perk = Perks.Brewing, amount = 50, weighting = 10 },
			{ perk = Perks.WineMaking, amount = 50, weighting = 10 },
		})
function WorkplaceBrewery:getSalary() return 1 end
function WorkplaceBrewery:doSpeedyTicks() return true end
function WorkplaceBrewery:requireSomeonePresentForRewards() return false end

if getActivatedMods():contains("sapphcooking") then
	WorkplaceBrewery:appendLootTable({
		["SapphCooking.SakeFull"] = 3,
		["SapphCooking.RumFull"] = 4,
		["SapphCooking.GinFull"] = 3,
		["SapphCooking.TequilaFull"] = 2,
		["SapphCooking.Vermouth"] = 2,
		["SapphCooking.LiqueurBottle"] = 2,
		["SapphCooking.VodkaFull"] = 8,
	})
end

if getActivatedMods():contains("MoreBrews") then
	WorkplaceBrewery:appendLootTable({
		["MoreBrews.CorksBag"] = 3,
		["MoreBrews.RubberBandsBag"] = 2,
		["MoreBrews.BottleCapsBag"] = 2,
	})
end


WorkplaceBarOld = WWP_WorkplaceType:new("bar_stocked", "Bar (1G + Item)", 10, {
	["Base.BeerBottle"] = 7,
	["Base.BeerCan"] = 2,
	["Base.WhiskeyFull"] = 5,
	["Base.Wine"] = 3,
	["Base.Wine2"] = 3,
	["Base.Peanuts"] = 5,
	["Base.Crisps"] = 1,
	["Base.Crisps2"] = 1,
	["Base.Pop"] = 1,
	["Base.Pop2"] = 1,
	["Base.TortillaChips"] = 1,
}, { 		-- see PerkFactory.Perks : zombie.characters.skills.PerkFactory.Perks
	{ perk = Perks.Nimble, amount = 35, weighting = 3 },
})

if getActivatedMods():contains("sapphcooking") then
	WorkplaceBarOld:appendLootTable({
		["Base.Egg"] = 1, -- Used in cocktails so keeping it here
		["Base.SugarPacket"] = 1, -- Used in cocktails so keeping it here
		["SapphCooking.SakeFull"] = 3,
		["SapphCooking.RumFull"] = 4,
		["SapphCooking.GinFull"] = 3,
		["SapphCooking.ColaBottle"] = 3,
		["SapphCooking.BottleofLemonJuice"] = 2,
		["SapphCooking.TequilaFull"] = 2,
		["SapphCooking.Vermouth"] = 2,
		["SapphCooking.HotsaucePacket"] = 1,
		["SapphCooking.LiqueurBottle"] = 2,
		["SapphCooking.HomemadeKetchup"] = 1,
		["SapphCooking.LowBallGlassCoffee"] = 1,
		["SapphCooking.VodkaFull"] = 8,
	})
end

function WorkplaceBarOld:getBenefits()
	return {"+ Happiness", "- Boredom"}
end

---@param player IsoGameCharacter
function WorkplaceBarOld:applyBenefits(player)
	WWP_WorkplaceType:decreaseBoredom(player, 10)
	WWP_WorkplaceType:decreaseUnhappy(player, 10)
end

function WorkplaceBarOld:requireCustomersForRewards() return true end
function WorkplaceBarOld:getSalary() return 1 end

