---
--- WWP_WorldMenu.lua
--- Right click context menu for Admins, used for managing workplaces
--- 18/06/2023
---
---
if not isClient() then return end

require "UI/WWP_CreateZone"
require "WL_ContextMenuUtils"
require "WL_Utils"

local WWP_WorldMenu = {}

WWP_WorldMenu.doMenu = function(playerIdx, context)
	local player = getPlayer(playerIdx)
	local x, y = ISCoordConversion.ToWorld(getMouseXScaled(), getMouseYScaled(), player:getZ())
	local zones = WWP_WorkplaceZone.getZonesAt(x, y, player:getZ())
	if WL_Utils.canModerate(player) then
		local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Zones")
		submenu:addOption("List Workplaces" , nil, WWP_WorldMenu.listZones)
		local startingCoordinates = {
			startX = x,
			startY = y,
			endX = player:getX(),
			endY = player:getY(),
		}
		submenu:addOption("New Workplace", startingCoordinates, WWP_WorldMenu.createZone)
	end

	for i=1,#zones do 	-- Very unlikely to be more than one here
		local zone = zones[i]
		if WL_Utils.canModerate(player) or zone:isEmployee(player) then
			context:addOption("Manage " .. zone.name, zone, WWP_WorldMenu.manageZone)
			local openClosed = zone.open and "Close " or "Open "
			context:addOption(openClosed.. zone.name, zone, WWP_WorldMenu.flipOpenClosed)
		elseif not zone:hasAnyEmployees() then
			context:addOption("Claim " .. zone.name, zone, WWP_WorldMenu.claimWorkplace)
		end
	end
end

function WWP_WorldMenu.listZones()
	WWP_ListWorkplaces:show()
end

function WWP_WorldMenu.createZone(startingCoordinates)
	WWP_CreateZone:show(startingCoordinates)
end

--- @param zone WWP_WorkplaceZone
function WWP_WorldMenu.manageZone(zone)
	WWP_ManageZone:show(zone, WL_Utils.canModerate(getPlayer()))
end

function WWP_WorldMenu.claimWorkplace(zone)
	zone:promoteEmployee(getPlayer():getUsername())
	WWP_ManageZone:show(zone, WL_Utils.canModerate(getPlayer()))
end

--- @param zone WWP_WorkplaceZone
function WWP_WorldMenu.flipOpenClosed(zone)
	zone.open = not zone.open
	zone:save()
end

Events.OnFillWorldObjectContextMenu.Add(WWP_WorldMenu.doMenu)