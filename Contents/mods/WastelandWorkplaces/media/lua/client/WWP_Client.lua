if not isClient() then return end

require "WWP_WorkplaceZone"

local Commands = {}

local lastTry = 0
local didGetIntialZones = false

local function checkForInitialZones()
    if didGetIntialZones then
        Events.OnTick.Remove(checkForInitialZones);
        return
    end
    if getTimestampMs() - lastTry < 2000 then return end
    lastTry = getTimestampMs()
    sendClientCommand(getPlayer(), "WastelandWorkplaces", "GetZones", {})
end

local function processServerCommand(module, command, args)
    if module ~= "WastelandWorkplaces" then return end
    if not Commands[command] then return end
    Commands[command](args)
end

function Commands.SyncZone(args)
    if WWP_WorkplaceZones[args.id] then
        for k,v in pairs(args) do
            WWP_WorkplaceZones[args.id][k] = v
        end
    else
        WWP_WorkplaceZones[args.id] = WWP_WorkplaceZone:loadFrom(args)
    end
end

function Commands.SyncZones(args)
    didGetIntialZones = true

    if args == nil then
        WWP_WorkplaceZones = {}
        return
    end

    local seenZoneIds = {}
    for _, zone in pairs(args) do
        seenZoneIds[zone.id] = true
        if WWP_WorkplaceZones[zone.id] then
            for k,v in pairs(zone) do
                WWP_WorkplaceZones[zone.id][k] = v
            end
        else
            WWP_WorkplaceZones[zone.id] = WWP_WorkplaceZone:loadFrom(zone)
        end
    end
    for id, _ in pairs(WWP_WorkplaceZones) do
        if not seenZoneIds[id] then
            WWP_WorkplaceZones[id].parentClass.delete(WWP_WorkplaceZones[id]) -- TODO: Improve this API
            WWP_WorkplaceZones[id] = nil
        end
    end
end

function Commands.Notify(args)
    if getPlayer():getModData()["WWP_DisableAlertFor_" .. args[1]] then return end
    local zone = WWP_WorkplaceZone.getZone(args[1])
    if not zone then return end
    if not zone:isPlayerInZone(getPlayer()) then return end

    WL_Utils.addToChat(args[2], {
        color = "0.8,0.8,0.8",
        chatId = WRC and WRC.OocTabId or 0,
    })
end

Events.OnServerCommand.Add(processServerCommand)
Events.OnInitWorld.Add(function()
    Events.OnTick.Add(checkForInitialZones)
end)