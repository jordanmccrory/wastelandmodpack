---
--- WWP_WorkplaceZone.lua
--- 18/06/2023
---

if not isClient() then return end

require "WWP_WorkplaceType"
require "WWP_PlayerStats"

--- @class WWP_WorkplaceZone
--- @inherits WL_Zone
--- @field id string
--- @field name string
--- @field location string
--- @field description string
--- @field typeKey string
--- @field mapType string
--- @field type WWP_WorkplaceType
--- @field open boolean
--- @field autoClose boolean
--- @field employees table<string, boolean>
WWP_WorkplaceZone = WWP_WorkplaceZone or WL_Zone:derive("WWP_WorkplaceZone")

--- @type WWP_WorkplaceZone[]
WWP_WorkplaceZones = {}

function WWP_WorkplaceZone.getAllZones()
	return WWP_WorkplaceZones
end


--- Get all zones at a given location
--- @param x number
--- @param y number
--- @param z number
---@return WWP_WorkplaceZone[]
function WWP_WorkplaceZone.getZonesAt(x, y, z)
	local zones = {}
	for _, zone in pairs(WWP_WorkplaceZones) do
		if zone:isInZone(x, y, z) then
			table.insert(zones, zone)
		end
	end
	return zones
end

function WWP_WorkplaceZone.hasWorkplaceAt(x, y, z)
	for _, zone in pairs(WWP_WorkplaceZones) do
		if zone:isInZone(x, y, z) then
			return true
		end
	end
	return false
end

function WWP_WorkplaceZone.getZone(zoneId)
	return WWP_WorkplaceZones[zoneId]
end

--- @param player IsoPlayer
--- @param container ItemContainer
--- @return boolean true if the container is locked for the player
function WWP_WorkplaceZone.isContainerLockedFor(player, container)
	if WL_Utils.isAtLeastGM(player) then
		return false
	end
	if container:isInCharacterInventory(player) then
		return false
	end
	local isoObject = container:getParent()
	local x, y, z
	if isoObject then
		x = isoObject:getX()
		y = isoObject:getY()
		z = isoObject:getZ()
	elseif container:getType() == "floor" then
		x = player:getX()
		y = player:getY()
		z = player:getZ()
	else
		return false
	end
	for _, workplace in pairs(WWP_WorkplaceZone.getAllZones()) do
		if not workplace:isAllowedToTakeItems(player, x, y, z) then
			return true
		end
	end
	return false
end

---@param name string Human visible name
function WWP_WorkplaceZone:new(name, type, x1, y1, x2, y2, zLevel)
	--- @type WWP_WorkplaceZone
	local o = WWP_WorkplaceZone.parentClass.new(self, x1, y1, zLevel, x2, y2, zLevel)  -- call inherited constructor
	setmetatable(o, self)
	self.__index = self
	o:init(name, type)
	o:save()
	return o
end

function WWP_WorkplaceZone:loadFrom(o)
	setmetatable(o, self)
	self.__index = self
	if o.minZ == nil then o.minZ = 0 end
	if o.maxZ == nil then o.maxZ = 0 end
	o.type = WWP_WorkplaceTypes[o.typeKey]
	return o
end

function WWP_WorkplaceZone:init(name, type)
	self.id = getRandomUUID()
	self.mapType = "Workplace"
	self.name = name
	self.type = type
	self.open = false
	self.autoClose = false
	self.employees = {}
end

function WWP_WorkplaceZone:save()
	sendClientCommand(getPlayer(), "WastelandWorkplaces", "SetZone", {
		id = self.id,
		name = self.name,
		location = self.location or "",
		description = self.description or "",
		typeKey = self.type.key,
		autoClose = self.autoClose,
		open = self.open,
		employees = self.employees,
		minX = self.minX,
		minY = self.minY,
		maxX = self.maxX,
		maxY = self.maxY,
		minZ = self.minZ,
		maxZ = self.maxZ,
	})
end

function WWP_WorkplaceZone:delete()
	sendClientCommand(getPlayer(), "WastelandWorkplaces", "DeleteZone", {id = self.id})
end

function WWP_WorkplaceZone:getMapName()
	return self.name
end

--- Checks if a tile is locked by this workplace. If so, it does not want players taking items unless they
--- are employees at this workplace.
---@param player IsoPlayer that is trying to take an item
---@param x number coordinate on X-axis of the tile where the item or container can be found
---@param y number coordinate on Y-axis of the tile where the item or container can be found
---@param z number coordinate on Z-axis of the tile where the item or container can be found
---@return boolean true if this workplace wants the tile locked so no items can be taken from it, like a safe-house.
function WWP_WorkplaceZone:isAllowedToTakeItems(player, x, y, z)
	if self.open then -- Let players configure this later? Add "always locked" and "always unlocked" options dropdown
		return true
	else
		if self:isInZone(x, y, z) then
			return self:isEmployee(player)
		else
			return true
		end
	end
end

function WWP_WorkplaceZone:hasAnyEmployees()
	for _, _ in pairs(self.employees) do
		return true
	end
	return false
end

function WWP_WorkplaceZone:isEmployee(player)
	return self.employees[player:getUsername()] ~= nil
end

function WWP_WorkplaceZone:isPartner(username)
	return self.employees[username]
end

function WWP_WorkplaceZone:fireEmployee(username)
	self.employees[username] = nil
	self:save()
end

function WWP_WorkplaceZone:addEmployee(username)
	if not self.employees[username] then
		self.employees[username] = false
		self:save()
	end
end

function WWP_WorkplaceZone:promoteEmployee(username)
	self.employees[username] = true
	self:save()
end

function WWP_WorkplaceZone:demoteEmployee(username)
	self.employees[username] = false
	self:save()
end

function WWP_WorkplaceZone:isPlayerInZone(player)
	return self:isInZone(player:getX(), player:getY(), player:getZ())
end

function WWP_WorkplaceZone:onEnter(player)
	local entryMessage = "Entering " .. self.name
	if(self:isEmployee(player)) then
		if not self.open then
			entryMessage = entryMessage .. "\nClosed: Open to gain work benefits"
			player:setHaloNote(entryMessage, 200, 200, 200, 200.0)
			return
		end

		if self.type:requireSomeonePresentForRewards() then
			local employees, visitors = self:countPlayersInZone()
			if self.type:requireCustomersForRewards() then
				if visitors == 0 then
					entryMessage = entryMessage .. "\nNo customers present\n"  .. self:getWorkPercentLeftString(player)
					player:setHaloNote(entryMessage, 200, 200, 200, 500.0)
					return
				end
			else -- Then anyone will do
				if visitors == 0 and employees < 2 then
					entryMessage = entryMessage .. "\nNobody around\n"  .. self:getWorkPercentLeftString(player)
					player:setHaloNote(entryMessage, 200, 200, 200, 500.0)
					return
				end
			end
		end

		entryMessage = entryMessage .. "\n" .. self:getWorkPercentLeftString(player)
		player:setHaloNote(entryMessage, 253, 216, 12, 500.0)
	else
		if not self.open then
			entryMessage = entryMessage .. "\n***  CLOSED  ***"
			player:setHaloNote(entryMessage, 250, 20, 60, 200.0)
			return
		end

		if self.type:getVisitorEnterSound() then
			getSoundManager():PlayWorldSound(self.type:getVisitorEnterSound(), player:getSquare(), 1, 100, 1, false);
		end

		if self.type:requireEmployeesForBenefits() and (#self.type:getBenefits()) > 0 then
			local employees, _ = self:countPlayersInZone()
			if employees == 0 then
				entryMessage = entryMessage .. "\nNo Employees present"
				player:setHaloNote(entryMessage, 200, 200, 200, 350.0)
				return
			end
		end

		for _, benefit in pairs(self.type:getBenefits()) do
			entryMessage = entryMessage .. "\n" .. benefit
		end
		player:setHaloNote(entryMessage, 124, 252, 0, 500.0)
	end
end

function WWP_WorkplaceZone:countPlayersInZone()
	local employeeCount = 0
	local visitorCount = 0
	local players = getOnlinePlayers()
	players = players or ArrayList.new()
	for playerIndex = 0, players:size() -1 do
		local p = players:get(playerIndex)
		local isInZone = self:isInZone(p:getX(), p:getY(), p:getZ())

		if isInZone then
			if self:isEmployee(p) then
				employeeCount = employeeCount + 1
			else
				visitorCount = visitorCount + 1
			end
		end
	end

	return employeeCount, visitorCount
end

function WWP_WorkplaceZone:getWorkPercentLeftString(player)
	local workData = WWP_PlayerStats.getPlayerWorkData(player)
	return "Work Remaining: " .. string.format("%d", workData.percentRemaining) .. "%"
end

function WWP_WorkplaceZone:onExit(player)
	local msg = "Leaving " .. self.name
	if self:isEmployee(player) then
		msg = msg .. "\n" .. self:getWorkPercentLeftString(player)
	else
		if self.type:getVisitorExitSound() then
			getSoundManager():PlayWorldSound(self.type:getVisitorExitSound(), player:getSquare(), 1, 100, 1, false);
		end
	end
	player:setHaloNote(msg, 200, 200, 200, 250.0)
end

function WWP_WorkplaceZone:perMinute(player)
	if not self.open then return end

	if self:isEmployee(player) then
		self.type:decreaseBoredom(player, 10)
	end

	if self.type:requireEmployeesForBenefits() and (#self.type:getBenefits()) > 0 then
		local employees, _ = self:countPlayersInZone()
		if employees == 0 then return end
	end

	self.type:applyBenefits(player)
	self.type:onMinuteTick(player, self)
end

function WWP_WorkplaceZone:perTwoMinutes(player)
	if self.type:doSpeedyTicks() then
		self:doWorkTick(player)
	end
	self.type:onTwoMinuteTick(player, self)
end

function WWP_WorkplaceZone:perFiveMinutes(player)
	if not self.type:doSpeedyTicks() then
		self:doWorkTick(player)
	end
	self.type:onFiveMinuteTick(player, self)
end

function WWP_WorkplaceZone:doWorkTick(player)
	if not self.open then return end
	if not self:isEmployee(player) then return end
	local workData = WWP_PlayerStats.getPlayerWorkData(player)
	if(workData.percentRemaining - self.type:getWorkPercent() >= 0) then
		if self.type:requireSomeonePresentForRewards() then
			local employees, visitors = self:countPlayersInZone()
			if self.type:requireCustomersForRewards() then
				if visitors == 0 then
					rewardString = "No customers present\n"  .. self:getWorkPercentLeftString(player)
					player:setHaloNote(rewardString, 200, 200, 200, 350.0)
					return
				end
			else -- Then anyone will do
				if visitors == 0 and employees < 2 then
					rewardString = "Nobody around\n"  .. self:getWorkPercentLeftString(player)
					player:setHaloNote(rewardString, 200, 200, 200, 350.0)
					return
				end
			end
		end

		workData.percentRemaining = workData.percentRemaining - self.type:getWorkPercent()
		local rewardString = self.type:generateReward(player)
		rewardString = rewardString .. "\n"  .. self:getWorkPercentLeftString(player)
		player:setHaloNote(rewardString, 253, 216, 12, 800.0)
	end
end

local bufferTimeout = 80
local takeItemBuffer = nil
local putItemBuffer = nil

local function insertToBuffer(buffer, zoneId, username, itemType)
	if not buffer[zoneId] then
		buffer[zoneId] = {}
	end
	if not buffer[zoneId][username] then
		buffer[zoneId][username] = {}
	end
	if not buffer[zoneId][username][itemType] then
		buffer[zoneId][username][itemType] = 0
	end
	buffer[zoneId][username][itemType] = buffer[zoneId][username][itemType] + 1
	bufferTimeout = 80
end

function WWP_WorkplaceZone:onPlayerPutItem(player, item)
	if self:isEmployee(player) and not WWP_WorkplaceZone.debug then return end
	if not putItemBuffer then putItemBuffer = {} end
	insertToBuffer(putItemBuffer, self.id, player:getUsername(), item:getFullType())
end

function WWP_WorkplaceZone:onPlayerTakeItem(player, item)
	if self:isEmployee(player) and not WWP_WorkplaceZone.debug then return end
	if not takeItemBuffer then takeItemBuffer = {} end
	insertToBuffer(takeItemBuffer, self.id, player:getUsername(), item:getFullType())
end

local function processBuffers()
	if bufferTimeout > 0 then
		bufferTimeout = bufferTimeout - 1
		return
	end
	if putItemBuffer then
		sendClientCommand(getPlayer(), "WastelandWorkplaces", "PutItems", putItemBuffer)
		putItemBuffer = nil
	end
	if takeItemBuffer then
		sendClientCommand(getPlayer(), "WastelandWorkplaces", "TakeItems", takeItemBuffer)
		takeItemBuffer = nil
	end
end

Events.OnTick.Add(processBuffers)