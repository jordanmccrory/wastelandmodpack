---
--- WLD_EnterZoneObjective.lua
--- 30/10/2023
---
---
require "WL_Zone"
require "WL_TriggerZones"

WLD_EnterZoneObjective = WL_Zone:derive("WLD_EnterZoneObjective")

function WLD_EnterZoneObjective:new(objectiveId, objectiveDescription, x1, y1, z1, x2, y2, z2)
	local o = WLD_EnterZoneObjective.parentClass.new(self, x1, y1, z1, x2, y2, z2)  -- call inherited constructor
	o.center = o:getCenterPoint()
	o.objectiveId = objectiveId
	o.objectiveDescription = objectiveDescription
	o.activated = false
	o.completed = false
	o.mapDisabled = true
	return o
end

--- Sets this objective to activated (available to be completed)
--- @return boolean true if this call actually set it, false if this objective was already activated
function WLD_EnterZoneObjective:activate(dungeon)
	if self.activated then return false end
	WL_Utils.addToChat("Added Objective: " .. self.objectiveDescription, { color = "1,0.7,0.4", })
	self.dungeon = dungeon
	WL_TriggerZones.addZone(self)
	self.homingArrow = WL_HomingArrows.addArrow(self.center.x, self.center.y, self.center.z)
	self.homingArrow.color = { r = 255, g = 215, b = 0, a = 0.8}
	self.homingArrow.range = 35
	self.activated = true
	return true
end

function WLD_EnterZoneObjective:onPlayerEnteredZone(player)
	if not self.activated then return end
	self:setComplete()
	self.dungeon:objectiveCompleted(player, self.objectiveId)
	addSound(player, player:getX(), player:getY(), player:getZ(), 150, 1)
end

--- Sets this objective to completed
--- @return boolean true if this call actually set it, false if this objective was already complete
function WLD_EnterZoneObjective:setComplete()
	if self.completed then return false end
	WL_Utils.addToChat("Objective Completed: " .. self.objectiveDescription, { color = "0,1,0.5", })
	WL_HomingArrows.removeArrow(self.homingArrow)
	WL_TriggerZones.removeZone(self)
	self.completed = true
	return true
end

