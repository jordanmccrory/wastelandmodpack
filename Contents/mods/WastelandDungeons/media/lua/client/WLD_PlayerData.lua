---
--- WLD_PlayerData.lua
--- 29/10/2023
---
require "WL_Utils"
require "PlayerReady"
require "WL_UserData"
require "WLD_Dungeon"

WLD_PlayerData = WLBaseObject:derive("WLD_PlayerData")

WLD_PlayerData.dungeonData = nil -- Invalid initially

--- Function that checks how long (in milliseconds) until a player can complete a dungeon again
--- @param dungeonId string ID of the dungeon
--- @param repeatMilliseconds int number of milliseconds until a player is able to complete the same dungeon again
--- @return number which is:
---  0 if they are able to complete it right now
--- -1 if the server hasn't sent us vital data yet (So error out)
---  1+ Any positive value is the milliseconds that they need to wait until they can complete it again
function WLD_PlayerData.getTimeUntilPlayerCanCompleteDungeon(dungeonId, repeatMilliseconds)
	if not WLD_PlayerData.dungeonData then return -1 end -- No data from server yet. Special error state.
	local thisDungeonData =  WLD_PlayerData.dungeonData.visitedDungeons[dungeonId]
	if not thisDungeonData then return 0 end -- Player has never visited it before
	local lastFinished = thisDungeonData.lastFinished or 0
	local timeUntilRepeat = repeatMilliseconds - (getTimestampMs() - lastFinished)
	if timeUntilRepeat < 0 then timeUntilRepeat = 0 end
	return timeUntilRepeat
end

function WLD_PlayerData.dungeonCompleted(dungeonId)
	local dungeonData = WLD_PlayerData.dungeonData.visitedDungeons[dungeonId]
	if not dungeonData then
		dungeonData = {}
		WLD_PlayerData.dungeonData.visitedDungeons[dungeonId] = dungeonData
	end
	dungeonData.lastFinished = getTimestampMs()
	WL_UserData.Set("WLD_DungeonData", WLD_PlayerData.dungeonData)
end

function WLD_PlayerData.receiveDataFromServer(data)
	if data.visitedDungeons then
		WLD_PlayerData.dungeonData = data
	else
		WLD_PlayerData.dungeonData = WLD_PlayerData.createNewPlayerData()
		WL_UserData.Set("WLD_DungeonData", WLD_PlayerData.dungeonData)
	end

	-- Call playerEntered on any dungeons the player is inside to activate objectives for it
	local player = getPlayer()
	for _, dungeon in pairs(WLD_All_Dungeons) do
		if dungeon.dungeonZone:isPlayerInZone(player) then
			dungeon:playerEntered(player)
		end
	end
end

function WLD_PlayerData.createNewPlayerData()
	return  { ["visitedDungeons"] = {} }
end

if isClient() then
	WL_PlayerReady.Add(function ()
		WL_UserData.Fetch("WLD_DungeonData", WLD_PlayerData.receiveDataFromServer)
	end)
else -- Singleplayer
	WLD_PlayerData.dungeonData = WLD_PlayerData.createNewPlayerData()
end

--- Used for debugging and testing
function WLD_ResetPlayerDungeonData()
	WLD_PlayerData.dungeonData = WLD_PlayerData.createNewPlayerData()
	WL_UserData.Set("WLD_DungeonData", WLD_PlayerData.dungeonData)
end