---
--- WLD_Dungeon.lua
--- 19/10/2023
---

require "WLBaseObject"
require "WL_Utils"
require "WL_MapUtils"
require "WL_Zone"
require "WLP_Portal"
require "WLP_PortalRegistry"
require "WLD_DungeonPortalController"
require "WLD_PlayerData"
require "WLD_EnterZoneObjective"
require "WLD_LootTable"

WLD_Dungeon = WLBaseObject:derive("WLD_Dungeon")

-- Map of dungeonId : dungeon
WLD_All_Dungeons = {}

--- Creates a new Dungeon
--- @param id string unique identifier
--- @param name string human readable name
--- @param difficulty int where 1=easy 2=normal 3=hard
--- @param repeatHours int the number of hours until a player can repeat this same dungeon again
--- @param mapCells table in the format { {1, 3}, {2, 3}, {3, 3} } whereby the two numbers are x and y coords of cells
function WLD_Dungeon:new(id, name, difficulty, repeatHours, mapCells)
	local o = WLD_Dungeon.parentClass.new(self)
	o.id = id
	o.name = name
	o.difficulty = difficulty
	o.repeatMilliseconds = repeatHours * 3600000
	o.dungeonZone = WL_Zone:new(WL_MapUtils.cellsToMapCoords(mapCells))
	o.state = "INVALID"
	o.objectives = {}
	o.lootTable = WLD_LootTable:new(difficulty)
	WLD_All_Dungeons[id] = o
	return o
end

function WLD_Dungeon:addPortal(portal, isEntrance)
	portal:setPortalController(WLD_DungeonPortalController:new(self, isEntrance))
	if isEntrance then
		portal.homingArrow.color =  { r = 100, g = 255, b = 100, a = 0.8}
	else
		portal.homingArrow.color =  { r = 200, g = 100, b = 100, a = 0.8}
	end
end

function WLD_Dungeon:addObjective(objective)
	self.objectives[objective.objectiveId] = objective
end

function WLD_Dungeon:getPortalTooltipSprite()
	return self.tooltipSprite
end

function WLD_Dungeon:canPlayerEnter(player)
	if player:isGodMod() then return true end
	if self.state == "INVALID" then return false end

	return (self.state == "ACTIVE")
			or WLD_PlayerData.getTimeUntilPlayerCanCompleteDungeon(self.id, self.repeatMilliseconds) == 0
end

function WLD_Dungeon:playerEntered(player)
	if player:isGodMod() then return end
	getPlayer():getEmitter():playSoundImpl(self:getEntranceSoundId(), nil)

	if  WLD_PlayerData.getTimeUntilPlayerCanCompleteDungeon(self.id, self.repeatMilliseconds) == 0 then
		WLD_Client.startingDungeon(player, self.id)
	end
end

function WLD_Dungeon:getEntranceSoundId()
	if self.difficulty == 3 then -- Hard
		return "EnterDungeon2"
	elseif self.difficulty == 2 then -- Medium
		return "EnterDungeon3"
	else -- Easy/unknown
		return "EnterDungeon1"
	end
end

--- @param serverDungeonData table in the form { dungeonId = "yourDungeonId", isActive = true/false,
--- playersAllowedToComplete = {"username1" = true, "username2" = true }, completedObjectives = {"kill_boss" = true }}
function WLD_Dungeon:updateFromServer(serverDungeonData)
	if serverDungeonData.isActive then
		self.state = "ACTIVE"
	else
		self.state = "READY"
	end

	if serverDungeonData.playersAllowedToComplete[getPlayer():getUsername()]
			and (WLD_PlayerData.getTimeUntilPlayerCanCompleteDungeon(self.id, self.repeatMilliseconds) == 0) then

		for objectiveKey, objective in pairs(self.objectives) do
			if serverDungeonData.completedObjectives[objectiveKey] then
				local wasCompletedNow = objective:setComplete()
				if wasCompletedNow then
					getPlayer():setHaloNote("Objective completed by ally", 255, 215, 0, 300.0)
					getPlayer():getEmitter():playSoundImpl("GuitarSound3", nil)
				end
			else
				objective:activate(self)
			end
		end

		if self:areAllObjectivesComplete() then
			WLD_PlayerData.dungeonCompleted(self.id)
			self.lootTable:rollLoot()
			getPlayer():setHaloNote("Dungeon Completed", 255, 215, 0, 300.0)
			WL_Utils.addToChat("Dungeon Completed", { color = "0,0.8,0", })
			getPlayer():getEmitter():playSoundImpl("GuitarSound2", nil)
		end
	end
end

function WLD_Dungeon:areAllObjectivesComplete()
	for _, objective in pairs(self.objectives) do
		if not objective.completed then
			return false
		end
	end
	return true
end

function WLD_Dungeon:playerExited(player)
	WLD_Client.fetchDungeonData(player, self.id)
end

function WLD_Dungeon:objectiveCompleted(player, objectiveId)
	getPlayer():setHaloNote("Objective completed", 255, 215, 0, 300.0)
	getPlayer():getEmitter():playSoundImpl("GuitarSound3", nil)
	WLD_Client.objectiveComplete(player, self.id, objectiveId)
end

function WLD_Dungeon:generatePortalEntranceTooltipText(player)
	return "Dungeon: " .. self.name .. " "  -- need the space here or we lose a word
			.. self:getDungeonDifficultyString()
			.. self:getPlayerStatusString(player)
end

function WLD_Dungeon:getDungeonDifficultyString()
	local str = "<LINE><RGB:1,1,1> Difficulty:" .. WL_Utils.MagicSpace
	if self.difficulty == 1 then
		str = str .. "<RGB:0,1,0> Easy "
	elseif self.difficulty == 2 then
		str = str .. "<RGB:1,0.647,0> Moderate "
	else -- Assume = 3
		str = str .. "<RGB:1,0,0> Hard "
	end
	return str
end

function WLD_Dungeon:getPlayerStatusString(player)
	local timeUntilCanComplete = WLD_PlayerData.getTimeUntilPlayerCanCompleteDungeon(self.id, self.repeatMilliseconds)

	local str = "<LINE> <RGB:1,1,1> Status:" .. WL_Utils.MagicSpace
	if timeUntilCanComplete == -1 or (self.state == "INVALID") then
		str = str .. "<RGB:1,0,0> WAITING FOR SERVER DATA"
		return str
	end

	if timeUntilCanComplete == 0 then
		if self.state == "READY" then
			str = str .. "<RGB:0,1,0> Available" -- Brand new dungeon
		else -- Someone started it, may or may not be able to get reward depending on if player enters soon enough
			str = str .. "<RGB:1,0.647,0> Active"
		end
	else
		if self.state == "ACTIVE" then
			str = str .. "<RGB:1,0.647,0> Active " -- Player can still enter dungeon (Just not complete it)
		else -- ie dungeon is READY
			str = str .. "<RGB:1,0,0> Unable to enter " -- Don't let player start a dungeon if they cant complete it
		end
		str = str .. "<LINE> You cannot complete this dungeon again for another "
				.. WL_Utils.toHumanReadableTime(timeUntilCanComplete)
	end
	return str
end

function WLD_Dungeon:generatePortalExitTooltipText(player)
	return "Leave the dungeon"
end


--TODO temporary code
local exampleDungeon = WLD_Dungeon:new("nuclear_plant", "Nuclear Power Plant", 2, 24,
		{{10, 55}, {10, 56}})
exampleDungeon.tooltipSprite = "industry_ddd_03_22"

local objective = WLD_EnterZoneObjective:new("office", "Go to the manager's office", 3141,16747,1, 3144, 16750, 1)
exampleDungeon:addObjective(objective)

local objective2 = WLD_EnterZoneObjective:new("control_room", "Find the east Control Room", 3174,16685,0, 3178, 16688, 0)
exampleDungeon:addObjective(objective2)

local objective3 = WLD_EnterZoneObjective:new("generator", "Check the pump room in the northeast", 3154,16561,0, 3158, 16564, 0)
exampleDungeon:addObjective(objective3)

local entrancePortal = WLP_PortalRegistry.createPortal({ 11151, 6038, 0 }, { 11153, 6040, 0 }, { 3161, 16893, 0 })
entrancePortal.markerRange = 0
entrancePortal.fadeoutTime = 310
entrancePortal.soundName = "ForestWalking"
exampleDungeon:addPortal(entrancePortal, true)

local exit = WLP_PortalRegistry.createPortal({ 3159, 16894, 0 }, {3164, 16898, 0}, { 11152, 6040, 0 })
exit.fadeoutTime = 30
exit.soundName = "ForestWalking"
exampleDungeon:addPortal(exit, false)

--- Recommended fadeout durations for sounds:
--- BusDriving 450
--- ElevatorDoors 310
--- RocksFalling 230