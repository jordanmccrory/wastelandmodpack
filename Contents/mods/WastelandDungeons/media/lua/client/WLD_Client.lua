---
--- WLD_Client.lua
--- 01/11/2023
---

if not isClient() then return end

WLD_Client = {}
local Commands = {}
local lastTry = 0
local haveInitialDungeonData = false

local function fetchLatestDungeonData()
	if haveInitialDungeonData then
		Events.OnTick.Remove(fetchLatestDungeonData);
		return
	end
	if getTimestampMs() - lastTry < 2000 then return end
	lastTry = getTimestampMs()
	sendClientCommand(getPlayer(), "WastelandDungeons", "getAllDungeonData", {})
end

--- This should ONLY be called by players that aren't restricted by user-data timer on repeating dungeons
function WLD_Client.startingDungeon(player, dungeonId)
	sendClientCommand(player, "WastelandDungeons", "startingDungeon", {dungeonId = dungeonId})
end

--- Called when a player completes an objective, to notify the server and other players of the new state
function WLD_Client.objectiveComplete(player, dungeonId, objectiveId)
	sendClientCommand(player, "WastelandDungeons", "objectiveComplete",
			{dungeonId = dungeonId, objectiveId = objectiveId })
end

--- Called to get the latest data on a single data
function WLD_Client.fetchDungeonData(player, dungeonId)
	sendClientCommand(player, "WastelandDungeons", "getDungeonData", {dungeonId = dungeonId})
end

function Commands.receiveAllDungeonData(tableOfAllDungeons)
	local needInitDungeons = not haveInitialDungeonData
	haveInitialDungeonData = true

	if tableOfAllDungeons then -- Server sends nil for empty table annoyingly
		for id, dungeonData in pairs(tableOfAllDungeons) do
			WLD_All_Dungeons[id]:updateFromServer(dungeonData)
		end
	end

	if needInitDungeons then -- This was our very first server message, we need to set the dungeons as ready if not
		for _, dungeon in pairs(WLD_All_Dungeons) do
			if dungeon.state == "INVALID" then
				dungeon.state = "READY"
			end
		end
	end
end

function Commands.receiveDungeonData(dungeonData)
	WLD_All_Dungeons[dungeonData.dungeonId]:updateFromServer(dungeonData)
end

local function processServerCommand(module, command, args)
	if module ~= "WastelandDungeons" then return end
	if not Commands[command] then return end
	Commands[command](args)
end

Events.OnServerCommand.Add(processServerCommand)
Events.OnInitWorld.Add(function()
	Events.OnTick.Add(fetchLatestDungeonData)
end)