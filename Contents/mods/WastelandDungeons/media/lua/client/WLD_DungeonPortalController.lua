---
--- WLD_DungeonPortalController.lua
--- 19/10/2023
---

require "WLP_PortalController"

WLD_DungeonPortalController = WLP_PortalController:derive("WLD_DungeonPortalController")

function WLD_DungeonPortalController:new(dungeon, isEntrance)
	local o = WLD_DungeonPortalController.parentClass.new(self)
	o.dungeon = dungeon
	o.isEntrance = isEntrance
	return o
end

function WLD_DungeonPortalController:getEnterText()
	if self.isEntrance then
		return "Enter Dungeon"
	else
		return "Exit Dungeon"
	end
end

function WLD_DungeonPortalController:generateTooltip(player)
	if self.isEntrance then
		return self.dungeon:generatePortalEntranceTooltipText(player)
	else
		return self.dungeon:generatePortalExitTooltipText(player)
	end
end

function WLD_DungeonPortalController:canEnter(player)
	if self.isEntrance then
		return self.dungeon:canPlayerEnter(player)
	else
		return true
	end
end

function WLD_DungeonPortalController:getTooltipSprite()
	if self.isEntrance then
		return self.dungeon:getPortalTooltipSprite()
	else
		return nil
	end
end

function WLD_DungeonPortalController:playerTeleported(player)
	if self.isEntrance then
		self.dungeon:playerEntered(player)
	else
		self.dungeon:playerExited(player)
	end
end