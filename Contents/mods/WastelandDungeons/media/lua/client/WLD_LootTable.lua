---
--- WLD_LootTable.lua
--- 08/11/2023
---

require "WLBaseObject"
require "WL_Utils"

WLD_LootTable = WLBaseObject:derive("WLD_LootTable")

WLD_LootTable.tables = {
	[1] = {
		{ weight = 1, items = { "Base.x4Scope", "Base.HuntingRifle", { "Base.308Box", 4 } } },
		{ weight = 5, items = { "Base.Pistol", { "Base.9mmClip", 3 }, "Base.Bullets9mmBox" } },
		{ weight = 2, items = { "Base.ColtPython", { "Base.Bullets357Box", 3 } } },
		{ weight = 2, items = { "Base.Mossberg500Tactical", { "Base.ShotgunShellsBox", 3 } } },
		{ weight = 4, items = { "Base.SKS", { "Base.762x39Box", 3 } } },
		{ weight = 1, items = { "Base.M1Garand", { "Base.M1GarandClip", 3 }, "Base.Bullets3006Box" } },
		{ weight = 2, items = { "Base.Sling", {"Base.Bullets22Box", 6} } },
		{ weight = 2, items = { "Base.Sling_Leather", {"Base.Bullets4440Box", 5} } },
		{ weight = 2, items = { "Base.Sling_Olive", {"Base.Bullets3006Box", 6} } },
		{ weight = 2, items = { "Base.Sling_Camo", {"Base.ShotgunShellsBox", 5} } },
		{ weight = 2, items = { "Base.Sling", {"Base.Bullets38Box", 4} } },
		{ weight = 2, items = { "Base.Sling_Leather", {"Base.Bullets357Box", 5} } },
		{ weight = 2, items = { "GreysWLWeaponsFirearms.LastResort", {"Base.ShotgunShellsBox", 2} } },
		{ weight = 1, items = { "Base.22Silencer", {"Base.Bullets22Box", 7 } } },
	},

	[2] = {
		{ weight = 1, items = { "Base.Glock17", { "Base.Glock17Mag", 3 }, {"Base.Bullets9mmBox", 6}  } },
		{ weight = 1, items = { "Base.FN_FAL", {"Base.FN_FAL_Mag", 3} , {"Base.762x51Box", 6} } },
		{ weight = 2, items = { "Base.AssaultRifle2", {"Base.M14Clip", 3}, {"Base.308Box", 6} } },
		{ weight = 2, items = { "Base.LAW12", { "Base.ShotgunShellsBox", 6 } } },
		{ weight = 1, items = { "Base.M24Rifle", { "Base.762x51Box", 6 } } },
		{ weight = 3, items = { "Base.ColtPythonHunter", { "Base.Bullets357Box", 6 } } },
		{ weight = 1, items = { "Base.RiotShieldPolice", "Base.Nightstick", { "Base.Molotov", 3}, "ImprovisedGunToolKit" } },
		{ weight = 2, items = { "Base.AR15", { "Base.556Clip", 3 }, {"Base.556Box", 6} } },
		{ weight = 2, items = { "GreysWLWeaponsFirearms.ScrapM4", { "GreysWLWeaponsFirearms.ScrapAssaultRifleMag", 3 }, {"Base.762x39Box", 6} } },
		{ weight = 2, items = { "GreysWLWeaponsFirearms.ScrapThompson45v2", { "GreysWLWeaponsFirearms.ScrapThompsonMag", 3 }, {"Base.Bullets45Box", 6} } },
		{ weight = 3, items = { "ImprovisedGunToolKit", {"Base.Bullets9mmBox", 12} } },
		{ weight = 3, items = { "Base.HandTorch", {"Base.Bullets45Box", 12} } },
		{ weight = 3, items = { "Base.HandTorch", {"Base.Bullets44Box", 12} } },
		{ weight = 3, items = { "Base.Sling", {"Base.762x39Box", 12} } },
		{ weight = 3, items = { "ImprovisedGunToolKit", {"Base.Bullets357Box", 12} } },
	},

	[3] = {
		{ weight = 2, items = {"Base.M733", { "Base.556Clip", 3 }, {"Base.556Box", 6} } },
		{ weight = 2, items = { "Base.SPAS12", { "Base.ShotgunShellsBox", 6 } } },
		{ weight = 1, items = { "Base.RiotShieldSwat", "Base.Katana", {"Base.762x39Box", 10} } },
		{ weight = 1, items = {"Base.M60", {"Base.M60Mag", 3}, {"Base.762x51Box", 6} } },
		{ weight = 4, items = {"Base.AK47", {"Base.AK_Mag", 3} , {"Base.762x39Box", 6} } },
		{ weight = 2, items = {"Base.MP5", {"Base.MP5Mag", 3}, {"Base.Bullets9mmBox", 6} } },
		{ weight = 1, items = { "Base.9mmSilencer", {"Base.Bullets9mmBox", 8} } },
		{ weight = 1, items = { "Base.45Silencer", {"Base.Bullets45Box", 8} } },
		{ weight = 1, items = { "Base.223Silencer", {"Base.223Box", 8} } },
		{ weight = 1, items = { "Base.308Silencer", {"Base.308Box", 8} } },
		{ weight = 1, items = { "Base.ShotgunSilencer", {"Base.ShotgunShellsBox", 8} } },
		{ weight = 1, items = { "Base.38Silencer", {"Base.Bullets38Box", 8} } },
		{ weight = 4, items = { {"Base.762x51Box", 12} } },   -- Interchangeable with .308
		{ weight = 4, items = { {"Base.308Box", 12} } }, -- Interchangeable with 762x51
		{ weight = 4, items = { {"Base.556Box", 12} } },  -- Interchangeable with .223
		{ weight = 4, items = { {"Base.223Box", 12} } }, -- Interchangeable with 556
	},
}

---@param lootTier number from 1-3 (currently lines up with the dungeon difficulties)
function WLD_LootTable:new(lootTier)
	local o = WLD_LootTable.parentClass.new(self)
	o.lootTier = lootTier
	return o
end

--- Rolls loot and gives it to the player
function WLD_LootTable:rollLoot()
	local lootTable = WLD_LootTable.tables[self.lootTier]

	if not lootTable then
		WL_Utils.addErrorToChat("ERROR - No loot table for tier: " .. tostring(self.lootTier))
		return
	end

	local lootItems = self:rollOnLootTable(lootTable)

	if not lootItems then
		WL_Utils.addErrorToChat("ERROR - No loot produced from roll. Tier: " .. tostring(self.lootTier))
		return
	end

	self:addItemsToInventory(lootItems)
end

function WLD_LootTable:rollOnLootTable(lootTable)
	local totalWeight = 0
	for _, entry in ipairs(lootTable) do
		totalWeight = totalWeight + entry.weight
	end

	local randomValue = ZombRand(0, totalWeight) + 1  -- Random number between 1 and totalWeight

	for _, entry in ipairs(lootTable) do 	-- Find the item that corresponds to the random value
		randomValue = randomValue - entry.weight
		if randomValue <= 0 then
			return entry.items
		end
	end
	return nil -- Shouldn't happen
end

function WLD_LootTable:addItemsToInventory(items)
	for _, item in ipairs(items) do
		if type(item) == "string" then
			WL_Utils.addItemToInventory(item)
		elseif type(item) == "table" then
			WL_Utils.addItemToInventory(item[1], item[2])
		end
	end
end

--- Testing and debugging function
function WLD_testLoot(tier)
	local lootTable = WLD_LootTable:new(tier)
	lootTable:rollLoot(getPlayer())
end