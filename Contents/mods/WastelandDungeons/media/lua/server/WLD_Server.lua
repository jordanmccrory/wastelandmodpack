---
--- WLD_Server.lua
--- 01/11/2023
---
if not isServer() then return end

--- Dictionary of dungeonId : dungeonData
--- These tables are sent to clients to update them
WLD_ServerDungeonData = WLD_ServerDungeonData or {}

--- Dictionary of dungeonId: timeMillis (of the first time someone started the dungeon)
--- This map does not leave the server side, but influences the dungeon data (above)
WLD_ServerDungeonStartTimes = WLD_ServerDungeonStartTimes or {}

--- This determines the window of time in which someone can join a dungeon after it has been started and still
--- get the objectives and completion rewards. (ie. their name goes into dungeonData.playersAllowedToComplete)
WLD_COOP_JOIN_WINDOW_MILLIS = 600000 -- 10 minutes

local function getDataForDungeon(dungeonId)
	local dungeonData = WLD_ServerDungeonData[dungeonId]
	if not dungeonData then
		dungeonData = {}
		dungeonData.dungeonId = dungeonId -- Need to set in here too as some dungeons are sent on their own
		dungeonData.isActive = false
		dungeonData.playersAllowedToComplete = {}
		dungeonData.completedObjectives = {}
		WLD_ServerDungeonData[dungeonId] = dungeonData
	end
	return dungeonData
end

local function sendAllDungeonsToClient(player)
	sendServerCommand(player, "WastelandDungeons", "receiveAllDungeonData", WLD_ServerDungeonData)
end

local function sendDungeonToClient(player, dungeonId)
	sendServerCommand(player, "WastelandDungeons", "receiveDungeonData", getDataForDungeon(dungeonId))
end

local function sendDungeonToAllClients(dungeonId)
	sendServerCommand("WastelandDungeons", "receiveDungeonData", getDataForDungeon(dungeonId))
end

local Commands = {}

--- Gets data for every dungeon (mostly just used on player login)
---@param args table can be nil (unused)
function Commands.getAllDungeonData(player, args)
	sendAllDungeonsToClient(player)
end

--- Gets data for a single dungeon
---@param args table in the form { dungeonId = "yourDungeonId" }
function Commands.getDungeonData(player, args)
	sendDungeonToClient(player, args.dungeonId)
end

--- Sent by clients when they start a dungeon, but only if their personal time restriction for claiming rewards is over
---@param args table in the form { dungeonId = "yourDungeonId" }
function Commands.startingDungeon(player, args)
	local startedTime = WLD_ServerDungeonStartTimes[args.dungeonId]
	if not startedTime then
		startedTime = getTimestampMs()
		WLD_ServerDungeonStartTimes[args.dungeonId] = startedTime
	end

	local dungeonData = getDataForDungeon(args.dungeonId)
	local wasActive = dungeonData.isActive
	dungeonData.isActive = true -- Ensure dungeon is active

	if (getTimestampMs() - startedTime) < WLD_COOP_JOIN_WINDOW_MILLIS then
		dungeonData.playersAllowedToComplete[player:getUsername()] = true
	end

	if wasActive then
		sendDungeonToClient(player, args.dungeonId) -- Lets the player know if they got in on time
	else
		sendDungeonToAllClients(args.dungeonId) -- dungeon just became active, everyone needs to know the new state
	end
end

--- Sent by clients when they complete a dungeon objective
---@param args table in the form { dungeonId = "yourDungeonId", objectiveId = "killTheBoss" }
function Commands.objectiveComplete(player, args)
	local dungeonData = getDataForDungeon(args.dungeonId)
	dungeonData.isActive = true -- Ensure dungeon is active
	dungeonData.completedObjectives[args.objectiveId] = true
	sendDungeonToAllClients(args.dungeonId) -- update everyone, dungeon state almost certainly changed
end

local function processClientCommand(module, command, player, args)
	if module ~= "WastelandDungeons" then return end
	if not Commands[command] then return end
	Commands[command](player, args)
end

Events.OnClientCommand.Add(processClientCommand)
