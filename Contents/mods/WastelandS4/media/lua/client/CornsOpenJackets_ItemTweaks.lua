if getActivatedMods():contains("ItemTweakerAPIExtraClothingAddon") then 
	require("ItemTweaker_ExtraClothingOptions");
else return end

local function ModifyParamsFromOriginalItem(originalItemName, newItemName, resistanceModifier)
	local originalItem = ScriptManager.instance:getItem(originalItemName);
	local newItem = ScriptManager.instance:getItem(newItemName);
	
	if ((not originalItem) or (not newItem)) then return end;
	
	if resistanceModifier then 
		-- modify resistance
		TweakItem(newItemName, "Insulation", originalItem:getInsulation()*resistanceModifier);
		TweakItem(newItemName, "WindResistance", originalItem:getWindresist()*resistanceModifier);
		TweakItem(newItemName, "WaterResistance", originalItem:getWaterresist()*resistanceModifier);
	end;
end

--Automates the adding of context menus
local function AddNewExtraItem(originalItem, newItem, originalContextMenu, newContextMenu, resistanceModifier)

	ItemTweaker.AddOrReplaceClothingOption(originalItem, newItem, newContextMenu);
	ItemTweaker.AddOrReplaceClothingOption(newItem, originalItem, originalContextMenu);
	
	TweakItem(originalItem, "clothingExtraSubmenu", originalContextMenu);
	TweakItem(newItem, "clothingExtraSubmenu", newContextMenu);
	
	
	ModifyParamsFromOriginalItem(originalItem, newItem, resistanceModifier);
end


function Add4WayExtraItem(item1, item2, item3, item4, context1, context2, context3, context4, resistanceModifier)
	AddNewExtraItem(item1, item2, context1, context2);
	AddNewExtraItem(item2, item3, context2, context3, resistanceModifier);
	AddNewExtraItem(item1, item4, context1, context4, resistanceModifier);
	AddNewExtraItem(item2, item4, context2, context4);
	AddNewExtraItem(item1, item3, context1, context3);
	AddNewExtraItem(item4, item3, context4, context3);
	
end


-- jackets
AddNewExtraItem("LeatherVest_I80", "LeatherVestOPEN_I80", "CloseJacket", "OpenJacket", 0.75);