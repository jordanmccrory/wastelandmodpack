
local function isBunker(spawnLocation)
    return spawnLocation == "Bunker"
end

local function isExodus(spawnLocation)
    -- return true
    return spawnLocation == "Exodus"
end

local function isMainSeason(spawnLocation)
    return not isBunker(spawnLocation) and not isExodus(spawnLocation)
end

local function exodusFood(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        { "DriedApple", 1},
        { "DriedBanana", 1},
        { "DriedAvocado", 1},
        { "DriedBlackBerry", 1},
        { "DriedBlueBerry", 1},
        { "DriedGrapefruit", 1},
        { "DriedGrape", 1},
        { "DriedLemon", 1},
        { "DriedLime", 1},
        { "DriedMango", 1},
        { "DriedOrange", 1},
        { "DriedPeach", 1},
        { "DriedWatermelon", 1},
        { "CannedBellPepper", 1},
        { "CannedBroccoli", 1},
        { "CannedCabbage", 1},
        { "CannedCarrots", 1},
        { "CannedEggplant", 1},
        { "CannedLeek", 1},
        { "CannedPotato", 1},
        { "CannedRedRadish", 1},
        { "CannedTomato", 1},
        { "Rice", 1},
        { "DriedBlackBeans", 1},
        { "DriedWhiteBeans", 1},
        { "DriedChickpeas", 1},
        { "DriedKidneyBeans", 1},
        { "DriedWhiteBeans", 1},
        { "DriedLentils", 1},
        { "DriedSplitPeas", 1},
        { "BeefJerky", 1 },
        { "TrailMix", 3 },
        { "HardTack", 3 },
        { "Pemmican", 3 },
    })
end

Events.OnGameStart.Add(function()
    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isBunker(spawnLocation)
        end,
        stripAll = true,
        overrideClothing = true,
        overrideBags = true,
        clothing = {
            {rolls = 1, items = {
                { "Shoes_ArmyBoots", 1 },
                { "Shoes_ArmyBootsDesert", 1 },
            }},
            {rolls = 1, items = {
                { "Socks2", 1 },
                { "Socks_Ankle", 1 },
            }},
            "Haven_Pants",
            "Haven_Top",
        },
        bags = {
            "Haven_DuffelBag",
        },
        items = {
            {id = "Torch", inHands = true},
            {id = "Battery", count = 2},
            { id = "CreditCard", customName = "Bunker ID Card: {username}" },
            WSP.Utilities.funJunk(2, 50), -- 2 rolls, 50% chance each roll
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isBunker(spawnLocation) and spawnCount > 0
        end,
        allowOnRespawn = true,
        stripAll = true,
        overrideClothing = true,
        clothing = {
            "Haven_Pants",
            "Haven_Top",
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isExodus(spawnLocation)
        end,
        stripItems = true,
        items = {
            {rolls = 1, items = {
                {"Radio.WalkieTalkie1", 50 },
                {"Radio.WalkieTalkie2", 20 },
                {"Radio.WalkieTalkie3", 10 },
                {"Radio.WalkieTalkie4", 1 },
            }},
            {id = "Torch", inHands = true, chance = 30},
            {id = "Battery", count = 2},
            {id = "PaperNapkins", customName = "Exodus Paperwork: {username}"},
            WSP.Utilities.booksAndMags(4, 50), -- 4 rolls, 50%  chance each roll
            WSP.Utilities.plushies(1, 20), -- 1 roll, 20% chance
            exodusFood(4, 50), -- 4 rolls, 50%  chance each roll
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isMainSeason(spawnLocation)
        end,
        stripItems = true,
        items = {
            { id = "Radio.WalkieTalkie2", inHands = true },
            exodusFood(4, 50), -- 4 rolls, 50%  chance each roll TODO: Change this
        },
    })
end)
