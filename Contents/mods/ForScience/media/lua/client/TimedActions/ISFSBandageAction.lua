require "TimedActions/ISBaseTimedAction"

ISFSBandageAction = ISBaseTimedAction:derive("ISFSBandageAction");

function ISFSBandageAction:isValid()
	return self.character:getInventory():containsType(self.bandageType);
end

function ISFSBandageAction:update()
end

function ISFSBandageAction:start()	
	self:setOverrideHandModels(nil, nil)
	if self.doIt then
		local bandage = self.character:getInventory():getFirstTypeRecurse(self.bandageType);
		if bandage then
			if self.doctorLvl < 4 then
				local bodyPart = nil; -- Get random body part that is not already bandaged
				local index = ZombRand(BodyPartType.ToIndex(BodyPartType.MAX))
				local count = 0;
				while (bodyPart == nil) and (count < BodyPartType.ToIndex(BodyPartType.MAX)) do
					if self.character:getBodyDamage():getBodyPart(BodyPartType.FromIndex(index)):bandaged() == false then
						bodyPart = self.character:getBodyDamage():getBodyPart(BodyPartType.FromIndex(index));
					end
					count = count + 1;
					index = index + 1;
					if index >= BodyPartType.ToIndex(BodyPartType.MAX) then
						index = 0;
					end
				end
				
				if bodyPart then
					local step1 = ISApplyBandage:new(self.character, self.character, bandage, bodyPart, true);
					ISTimedActionQueue.addAfter(self, step1);
					local step2 = ISApplyBandage:new(self.character, self.character, nil, bodyPart, false);
					ISTimedActionQueue.addAfter(step1, step2);
					ISTimedActionQueue.addAfter(step2, ISFSBandageAction:new(self.character, bandage, false));
					
				else
					self.character:Say(getText("IGUI_PlayerText_FSBandageNoLimb"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
					self:forceComplete();
				end
			else
				self.character:Say(getText("IGUI_PlayerText_FSBandageMastered"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
				self:forceComplete();
			end
		else
			self.character:Say(getText("IGUI_PlayerText_FSBandageNeedMore"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
			self:forceComplete();
		end
	end
end

function ISFSBandageAction:stop()
	ISBaseTimedAction.stop(self)
end

function ISFSBandageAction:perform()	
	if self.doIt == false then
		local bandage = self.character:getInventory():getFirstType(self.bandageType);
		if bandage then
			local luckRoll = ZombRand(10 + self.doctorLvl*5);
			if luckRoll < 5 then
				self.character:getInventory():Remove(bandage);					
				if luckRoll == 0 then				
					self.character:Say(getText("IGUI_PlayerText_FSBandageFrayed"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
				else
					self.character:getInventory():AddItem(self.bandageReplaceType);	
					self.character:Say(getText("IGUI_PlayerText_FSBandageDirtied"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
				end		
			else
				local xpEarned = 5 * self.xpModifier;
				self.character:getXp():AddXP(Perks.Doctor, xpEarned);
			end
			
			bandage = self.character:getInventory():getFirstTypeRecurse(self.bandageType);
			if (bandage == nil) then
				self.character:Say(getText("IGUI_PlayerText_FSBandageNeedMore"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
				self:forceComplete();
			else
				local transferAct = ISInventoryTransferAction:new(self.character, bandage, bandage:getContainer(), self.character:getInventory(), nil);
				ISTimedActionQueue.addAfter(self, transferAct);
				ISTimedActionQueue.addAfter(transferAct, ISFSBandageAction:new(self.character, bandage, true));
			end
		else
			self.character:Say(getText("IGUI_PlayerText_FSBandageNeedMore"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
			self:forceComplete();
		end
	end
	-- needed to remove from queue / start next.
	ISBaseTimedAction.perform(self)
end

function ISFSBandageAction:new(character, bandage, doIt)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character;
	o.bandageType = bandage:getType();
	o.bandageReplaceType = bandage:getModule() .. '.' .. bandage:getReplaceOnUse();
	o.xpModifier = bandage:getBandagePower() / 4.0;
	if (bandage:isAlcoholic()) then
		o.xpModifier = o.xpModifier * 1.5;
	end
	o.doIt = doIt;
	o.doctorLvl = character:getPerkLevel(Perks.Doctor);
	o.stopOnWalk = true;
	o.stopOnRun = true;
	o.maxTime = 1;
	o.forceProgressBar = true;
	return o
end