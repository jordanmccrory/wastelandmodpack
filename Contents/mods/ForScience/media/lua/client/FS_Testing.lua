-----------------------------------------------------------------------
local debugItems = false;

---
-- Add some items to the player's inventory for testing purposes.
-- Nothing to see here
local function giveItems()
    if debugItems then
        local player = getSpecificPlayer(0);
		
		if player:getInventory():containsTypeRecurse("Base.Scalpel") ~= true then
			player:getInventory():AddItems("Base.Scalpel", 5);
		end
		if player:getInventory():containsTypeRecurse("Base.BookMedicalJournal") ~= true then
			player:getInventory():AddItem("Base.BookMedicalJournal");
		end
		if player:getInventory():containsTypeRecurse("Base.BookElectricManual") ~= true then
			player:getInventory():AddItem("Base.BookElectricManual");
		end
		if player:getInventory():containsTypeRecurse("Base.Screwdriver") ~= true then
			player:getInventory():AddItem("Base.Screwdriver");
		end
		if player:getInventory():containsTypeRecurse("Base.Bandage") ~= true then
			player:getInventory():AddItems("Base.Bandage", 10);
		end
		if player:getInventory():containsTypeRecurse("Base.ElectronicsScrap") ~= true then
			player:getInventory():AddItems("Base.ElectronicsScrap", 10);
		end
    end
end

-- local function getRandomCondition(_item)
	-- _item:setCondition(ZombRand(_item:getConditionMax())+1);
-- end

-- ------------------------------------------------
-- Game hooks
-- ------------------------------------------------
Events.OnGameStart.Add(giveItems);
