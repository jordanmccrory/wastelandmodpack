if isClient() then return end

WGN = WGN or {}

WGN._npcData = {}

function WGN.SendGlobal(command, options)
    if options == nil then
        options = {}
    end
    sendServerCommand("WGN", command, options)
end

function WGN.SendUser(username, command, options)
    if options == nil then
        options = {}
    end

    local players = getOnlinePlayers()
    for i=0, players:size()-1 do
        local player = players:get(i)
        if player:getUsername() == username then
            sendServerCommand(player, "WGN", command, options)
            return
        end
    end
    print("WGN:SendUser:Player not found: " .. username)
end

function WGN.CreateNPC(name, x, y, z)
    WGN._npcData[name] = {
        name = name,
        x = x,
        y = y,
        z = z
    }
    ModData.add("WGN:Npcs", WGN._npcData)
    ModData.transmit("WGN:Npcs")
end

function WGN.RemoveNPC(name)
    WGN._npcData[name] = nil
    ModData.add("WGN:Npcs", WGN._npcData)
    ModData.transmit("WGN:Npcs")
end