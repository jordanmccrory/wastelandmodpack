if isClient() then return end

local InCommands = require "GptNpcs/InCommands"

local InCommandParser = {}

function InCommandParser:new(lines)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:Init(lines)
    return o
end

function InCommandParser:Init(lines)
    self.commands = {}
    self.lines = lines
    self.currentLine = 1
end

function InCommandParser:Parse()
    local totalLines = #self.lines
    while self.currentLine <= totalLines do
        local line = self.lines[self.currentLine]
        if line == "ChatMessage" then
            self:parseChatMessage()
        elseif line == "ChatStatus" then
            self:parseChatStatus()
        elseif line == "PlayerMessage" then
            self:parsePlayerMessage()
        elseif line == "GiveItem" then
            self:parseGiveItem()
        elseif line == "HarmPlayer" then
            self:parseHarmPlayer()
        elseif line == "NpcList" then
            self:parseNpcList()
        elseif line == "" then
            -- Do nothing, skip empty lines
        else
            print("WGN:InCommandParser:Unknown command: " .. line)
        end
        self.currentLine = self.currentLine + 1
    end
end

function InCommandParser:parseChatMessage()
    local npc = self.lines[self.currentLine + 1]
    local player = self.lines[self.currentLine + 2]
    local messageLineCount = tonumber(self.lines[self.currentLine + 3])
    local messageLines = {}
    for i = 1, messageLineCount do
        table.insert(messageLines, self.lines[self.currentLine + 3 + i])
    end
    local message = table.concat(messageLines, "\n")
    table.insert(self.commands, InCommands.ChatMessage:new(npc, player, message))
    self.currentLine = self.currentLine + 3 + messageLineCount
end

function InCommandParser:parseGiveItem()
    local npc = self.lines[self.currentLine + 1]
    local player = self.lines[self.currentLine + 2]
    local item = self.lines[self.currentLine + 3]
    local count = tonumber(self.lines[self.currentLine + 4])
    table.insert(self.commands, InCommands.GiveItem:new(npc, player, item, count))
    self.currentLine = self.currentLine + 4
end

function InCommandParser:parseNpcList()
    local count = tonumber(self.lines[self.currentLine + 1])
    local npcs = {}
    for i = 1, count do
        table.insert(npcs, self.lines[self.currentLine + 1 + i])
    end
    table.insert(self.commands, InCommands.NpcList:new(npcs))
    self.currentLine = self.currentLine + 1 + count
end

function InCommandParser:parsePlayerMessage()
    local player = self.lines[self.currentLine + 1]
    local type = self.lines[self.currentLine + 2]
    local messageLineCount = tonumber(self.lines[self.currentLine + 3])
    local messageLines = {}
    for i = 1, messageLineCount do
        table.insert(messageLines, self.lines[self.currentLine + 3 + i])
    end
    local message = table.concat(messageLines, "<LINE>")
    table.insert(self.commands, InCommands.PlayerMessage:new(player, type, message))
    self.currentLine = self.currentLine + 3 + messageLineCount
end

function InCommandParser:parseHarmPlayer()
    local npc = self.lines[self.currentLine + 1]
    local player = self.lines[self.currentLine + 2]
    local type = self.lines[self.currentLine + 3]
    table.insert(self.commands, InCommands.HarmPlayer:new(npc, player, type))
    self.currentLine = self.currentLine + 3
end

function InCommandParser:parseChatStatus()
    local npc = self.lines[self.currentLine + 1]
    local player = self.lines[self.currentLine + 2]
    local status = self.lines[self.currentLine + 3]
    table.insert(self.commands, InCommands.ChatStatus:new(npc, player, status))
    self.currentLine = self.currentLine + 3
end

function InCommandParser:ExecuteCommands()
    for _, command in ipairs(self.commands) do
        command:Execute()
    end
end

return InCommandParser