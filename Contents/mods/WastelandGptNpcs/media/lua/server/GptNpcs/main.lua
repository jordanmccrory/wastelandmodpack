if isClient() then return end

require "GtpNpcs/lib"

local FileMonitor = require "GptNpcs/FileMonitor"
local InCommandParser = require "GptNpcs/InCommandParser"
local OutCommandWriter = require "GptNpcs/OutCommandWriter"
local OutCommands = require "GptNpcs/OutCommands"

local ClientCommands = {}

local out = OutCommandWriter:new("npc-in")
local fileMonitor = FileMonitor:new("npc-out.txt", InCommandParser)

function ClientCommands.PlayerEntered(player, args)
    local username = player:getUsername()
    local npc = args[1]

    out:WriteCommand(OutCommands.PlayerEntered:new(username, npc))
end

function ClientCommands.PlayerLeft(player, args)
    local username = player:getUsername()
    local npc = args[1]

    out:WriteCommand(OutCommands.PlayerLeft:new(username, npc))
end

function ClientCommands.ChatMessage(player, args)
    local username = player:getUsername()
    local npc = args[1]
    local message = args[2]

    out:WriteCommand(OutCommands.ChatMessage:new(username, npc, message))
end

function ClientCommands.SpawnNpc(_, args)
    local npc = args[1]
    local x = tonumber(args[2])
    local y = tonumber(args[3])
    local z = tonumber(args[4])

    WGN.CreateNPC(npc, x, y, z)
end

function ClientCommands.DespawnNpc(_, args)
    local npc = args[1]

    WGN.RemoveNPC(npc)
end

local function OnClientCommand(module, command, player, args)
    if module ~= "WGN" then return end

    if ClientCommands[command] then
        ClientCommands[command](player, args)
    else
        print("WGN:OnClientCommand Unknown " .. command)
    end
end


local function OnInitGlobalModData()
    WGN._npcData = ModData.getOrCreate("WGN:Npcs")
    WGN._possibleNpcs = ModData.getOrCreate("WGN:AvailableNpcs")

    fileMonitor:Start()
    out:WriteCommand(OutCommands.System:new("Boot"))
    out:WriteCommand(OutCommands.System:new("NpcList"))
end

Events.OnClientCommand.Add(OnClientCommand)
Events.OnInitGlobalModData.Add(OnInitGlobalModData)