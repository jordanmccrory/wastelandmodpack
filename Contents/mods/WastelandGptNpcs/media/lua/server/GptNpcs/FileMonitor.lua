if isClient() then return end

local FileReader = require "GptNpcs/FileReader"

--- @class FileMonitor
local FileMonitor = {}
FileMonitor._instances = FileMonitor._instances or {}
FileMonitor._tickDelay = 20

--- Creates a new FileMonitor instance.
--- @param filePath The path to the file to monitor.
--- @param parser The parser to use.
--- @return FileMonitor
function FileMonitor:new(filePath, parser)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:Init(filePath, parser)
    self._instances[filePath] = o
    return o
end

--- Initializes the FileMonitor instance.
--- @param filePath The path to the file to monitor.
--- @param parser InCommandParser The parser to use.
function FileMonitor:Init(filePath, parser)
    self.filePath = filePath
    self.parser = parser
    self.fileReader = FileReader:new(self.filePath)
    self.isRunning = false
end

function FileMonitor:Start()
    self.isRunning = true
end

function FileMonitor:ReadFile()
    local lines = self.fileReader:ReadLines()
    self.fileReader:ClearFile()
    local parser = self.parser:new(lines)
    parser:Parse()
    parser:ExecuteCommands()
end

function FileMonitor.OnTick()
    FileMonitor._tickDelay = FileMonitor._tickDelay - 1
    if FileMonitor._tickDelay <= 0 then

        for _, v in pairs(FileMonitor._instances) do
            if v.isRunning then
                v:ReadFile()
            end
        end
        FileMonitor._tickDelay = 20
    end
end

if not isClient() then
    Events.OnTick.Add(FileMonitor.OnTick)
end

return FileMonitor