if isClient() then return end

local InCommand = WLBaseObject:derive("InCommand")

function InCommand:Execute()
    print(self._type .. " is not implemented")
end

local ChatMessage = InCommand:derive("ChatMessage")

--- InChatMessage
--- @param npc string the npc name
--- @param player string The player Username
--- @param message string the message
function ChatMessage:new(npc, player, message)
    local o = self:super()
    o.npc = npc
    o.player = player
    o.message = message
    return o
end

function ChatMessage:Execute()
    WGN.SendGlobal("ChatMessage", {self.npc, self.player, self.message})
end

local GiveItem = InCommand:derive("GiveItem")

--- InGiveItem
--- @param npc string the npc name
--- @param player string The player Username
--- @param item string the item name
--- @param count number|nil the item count
function GiveItem:new(npc, player, item, count)
    local o = self:super()
    o.npc = npc
    o.player = player
    o.item = item
    o.count = count or 1
    return o
end

function GiveItem:Execute()
    WGN.SendUser(self.player, "GiveItem", {self.npc, self.item, self.count})
end

local GiveQuest = InCommand:derive("GiveQuest")

--- InGiveQuest
--- @param npc string the npc name
--- @param player string The player Username
--- @param quest string the quest name
--- @param options table|nil the quest options
function GiveQuest:new(npc, player, quest, options)
    local o = self:super()
    o.npc = npc
    o.player = player
    o.quest = quest
    o.options = options
    return o
end

function GiveQuest:Execute()
    WGN.SendUser(self.player, "GiveQuest", {self.npc, self.quest, self.options})
end

local PlayerMessage = InCommand:derive("PlayerMessage")

--- InPlayerMessage
--- @param player string The player Username
--- @params type string The message type
--- @param message string the message
function PlayerMessage:new(player, type, message)
    local o = self:super()
    o.player = player
    o.type = type
    o.message = message
    return o
end

function PlayerMessage:Execute()
    WGN.SendUser(self.player, "PlayerMessage", {self.type, self.message})
end

local NpcList = InCommand:derive("NpcList")

--- InNpcList
--- @param npcs table the npc names
function NpcList:new(npcs)
    local o = self:super()
    o.npcs = npcs
    return o
end

function NpcList:Execute()
    ModData.add("WGN:AvailableNpcs", self.npcs)
    ModData.transmit("WGN:AvailableNpcs")
end

local HarmPlayer = InCommand:derive("HarmPlayer")

--- InHarmPlayer
--- @param npc string the npc name
--- @param player string The player Username
--- @param type string the harm type
function HarmPlayer:new(npc, player, type)
    local o = self:super()
    o.npc = npc
    o.player = player
    o.type = type
    return o
end

function HarmPlayer:Execute()
    WGN.SendUser(self.player, "HarmPlayer", {self.npc, self.type})
end

local ChatStatus = InCommand:derive("ChatStatus")

--- InChatStatus
--- @param npc string the npc name
--- @param player string The player Username
--- @param status string the status
function ChatStatus:new(npc, player, status)
    local o = self:super()
    o.npc = npc
    o.player = player
    o.status = status
    return o
end

function ChatStatus:Execute()
    WGN.SendUser(self.player, "ChatStatus", {self.npc, self.status})
end

return {
    ChatMessage = ChatMessage,
    GiveItem = GiveItem,
    GiveQuest = GiveQuest,
    NpcList = NpcList,
    PlayerMessage = PlayerMessage,
    HarmPlayer = HarmPlayer,
    ChatStatus = ChatStatus,
}
