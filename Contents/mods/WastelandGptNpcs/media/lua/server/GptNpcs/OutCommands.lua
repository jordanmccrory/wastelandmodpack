if isClient() then return end

--- @class OutCommand
local OutCommand = WLBaseObject:derive("OutCommand")
function OutCommand:GetLines()
    print(self._type .. " is not implemented")
    return {}
end

local System = OutCommand:derive("System")

--- System
--- @param type string The type of the system event
function System:new(type)
    local o = self:super()
    o.type = type
    return o
end

function System:GetLines()
    local lines = {}
    table.insert(lines, "System")
    table.insert(lines, self.type)
    return lines
end

local ChatMessage = OutCommand:derive("ChatMessage")

--- OutChatMessage
--- @param fromPlayer string The player Username
--- @param toNpc string the npc name
--- @param message string the message
function ChatMessage:new(fromPlayer, toNpc, message)
    local o = self:super()
    o.fromPlayer = fromPlayer
    o.toNpc = toNpc
    o.message = message
    return o
end

function ChatMessage:GetLines()
    local lines = {}
    table.insert(lines, "ChatMessage")
    table.insert(lines, self.fromPlayer)
    table.insert(lines, self.toNpc)
    table.insert(lines, self.message)
    return lines
end

local PlayerEntered = OutCommand:derive("PlayerEntered")

--- OutPlayerEntered
--- @param player string The player Username
--- @param npc string the npc name
function PlayerEntered:new(player, npc)
    local o = self:super()
    o.player = player
    o.npc = npc
    return o
end

function PlayerEntered:GetLines()
    local lines = {}
    table.insert(lines, "PlayerEntered")
    table.insert(lines, self.player)
    table.insert(lines, self.npc)
    return lines
end

local PlayerLeft = OutCommand:derive("PlayerLeft")

--- OutPlayerLeft
--- @param player string The player Username
--- @param npc string the npc name
function PlayerLeft:new(player, npc)
    local o = self:super()
    o.player = player
    o.npc = npc
    return o
end

function PlayerLeft:GetLines()
    local lines = {}
    table.insert(lines, "PlayerLeft")
    table.insert(lines, self.player)
    table.insert(lines, self.npc)
    return lines
end

return {
    System = System,
    ChatMessage = ChatMessage,
    PlayerEntered = PlayerEntered,
    PlayerLeft = PlayerLeft,
}