
WGN = WGN or {}
WGN._npcData = {}
WGN._availableNpcs = {}

function WGN.SendCommand(command, args)
    if args == nil then
        args = {}
    end
    sendClientCommand(getPlayer(), "WGN", command, args)
end

function WGN.IsPlayerNear(player, npcData, distance)
    if player:getZ() ~= npcData.z then return false end
    local dx = npcData.x - player:getX()
    local dy = npcData.y - player:getY()
    return dx * dx + dy * dy <= distance * distance
end

function WGN.SetCurrentNpc(npcName)
    local player = getPlayer()
    local modData = player:getModData()
    modData["WGN_NPC"] = npcName
end

function WGN.GetCurrentNpc()
    local player = getPlayer()
    local modData = player:getModData()
    local npc = modData["WGN_NPC"]
    if npc == nil then
        return false
    end
    local npcData = WGN._npcData[npc]
    if npcData == nil then
        return false
    end
    return npcData;
end

function WGN.GetNearbyNpc()
    local player = getPlayer()
    for _, data in pairs(WGN._npcData) do
        if WGN.IsPlayerNear(player, data, 5) then
            return data
        end
    end
end

function WGN.InfoMessage(message)
    WL_Utils.addToChat(message, {
        color = "0.3,0.3,0.8",
    })
end

function WGN.PositiveMessage(message)
    WL_Utils.addToChat(message, {
        color = "0.3,0.8,0.3",
    })
end

function WGN.WarningMessage(message)
    WL_Utils.addToChat(message, {
        color = "0.8,0.8,0.3",
    })
end

function WGN.ErrorMessage(message)
    WL_Utils.addToChat(message, {
        color = "0.8,0.3,0.3",
    })
end