local function SpawnNpc(npcName, x, y, z)
    WGN.InfoMessage("Spawning " .. npcName .. " at " .. tostring(x) .. ", " .. tostring(y) .. ", " .. tostring(z))
    WGN.SendCommand("SpawnNpc", {npcName, x, y, z})
end

local function TeleportToNpc(npcName)
    local npcData = WGN._npcData[npcName]
    if not npcData then return end

    WL_Utils.teleportPlayerToCoords(getPlayer(), npcData.x, npcData.y, npcData.z)
end

local function DespawnNpc(npcName)
    WGN.InfoMessage("Despawning " .. npcName)
    WGN.SendCommand("DespawnNpc", {npcName})
end

local function SpeakToNpc(npcName)
    WGN.InfoMessage("Attempting to start a conversation with " .. npcName)
    WGN.SendCommand("PlayerEntered", {npcName})
end

local function StopSpeakingToNpc(npcName)
    WGN.InfoMessage("Attempting to end the conversation with " .. npcName)
    WGN.SendCommand("PlayerLeft", {npcName})
end

local function worldMenu(playerIdx, context)
    local player = getPlayer(playerIdx)
    local modData = player:getModData()
    local x, y = ISCoordConversion.ToWorld(getMouseXScaled(), getMouseYScaled(), player:getZ())
    x, y = math.floor(x), math.floor(y)
    local z = player:getZ()

    if modData["WGN_NPC"] then
        local npcData = WGN._npcData[modData["WGN_NPC"]]
        if not npcData then
            modData["WGN_NPC"] = nil
        else
            context:addOption("Finish conversation with " .. modData["WGN_NPC"], modData["WGN_NPC"], StopSpeakingToNpc)
        end
    else
        for npcName,npcData in pairs(WGN._npcData) do
            if x == npcData.x and y == npcData.y and z == npcData.z and WGN.IsPlayerNear(player, npcData, 5) then
                context:addOption("Start conversation with " .. npcName, npcName, SpeakToNpc)
            end
        end
    end

    if not isAdmin() then return end

    local submenuContext = WL_ContextMenuUtils.getOrCreateSubMenu(context, "GPT NPCs")

    if #WGN._availableNpcs > 0 then
        for _, npc in ipairs(WGN._availableNpcs) do
            local npcData = WGN._npcData[npc]

            local menuTitle = npc
            if npcData and npcData.x == x and npcData.y == y and npcData.z == z then
                menuTitle = menuTitle .. "*"
            end

            local npcOption = submenuContext:addOption(menuTitle, nil, nil)
            local npcContext = submenuContext:getNew(submenuContext)
            submenuContext:addSubMenu(npcOption, npcContext)

            if npcData then
                npcContext:addOption("Teleport to", npc, TeleportToNpc)
                npcContext:addOption("Despawn", npc, DespawnNpc)
            else
                npcContext:addOption("Spawn", npc, SpawnNpc, x, y, z)
            end
        end
    end
end


Events.OnFillWorldObjectContextMenu.Add(worldMenu)