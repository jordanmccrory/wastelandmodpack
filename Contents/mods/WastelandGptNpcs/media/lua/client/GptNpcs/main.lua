require "GptNpcs/lib"
require "Chat/WRC"

local nearAlert = {}
local walkingAwayAlert = {}

local function OnPlayerMove(player)
    if not player then return end
    local currentNpc = WGN.GetCurrentNpc()
    if currentNpc then
        if not WGN.IsPlayerNear(player, currentNpc, 8) then
            WGN.SendCommand("PlayerLeft", {currentNpc.name})
            WGN.ErrorMessage("Walked away from " .. currentNpc.name)
            WGN.SetCurrentNpc(nil)
        elseif not WGN.IsPlayerNear(player, currentNpc, 5) then
            if not walkingAwayAlert[currentNpc.name] then
                WGN.WarningMessage("You are moving away from " .. currentNpc.name .. ". If you move too far away, you will leave the conversation.")
                walkingAwayAlert[currentNpc.name] = true
            end
        else
            walkingAwayAlert[currentNpc.name] = nil
        end
    else
        local npc = WGN.GetNearbyNpc()
        if npc and not nearAlert[npc] then
            WGN.InfoMessage("You are now near " .. npc.name .. ". To speak with them, right click them, and select 'Start Conversation'.")
            nearAlert[npc] = true
        end
        if not npc then
            for inpc, _ in pairs(nearAlert) do
                nearAlert[inpc] = nil
            end
        end
    end
end

local function OnReceiveGlobalModData(key, data)
    if key == "WGN:Npcs" then
        WGN._npcData = data
    elseif key == "WGN:AvailableNpcs" then
        WGN._availableNpcs = data
    end
end

local function OnConnected()
	ModData.request("WGN:Npcs")
    ModData.request("WGN:AvailableNpcs")
end

local function OnChatMessage(parsedMessage)
    local currentNpc = WGN.GetCurrentNpc()
    if currentNpc and (parsedMessage.chatModifier == nil or parsedMessage.chatModifier == "me") then
        local text = "/me"
        for _, part in ipairs(parsedMessage.parts) do
            if part.type == "emote" then
                text = text .. " " .. part.text
            elseif part.type == "text" then
                text = text .. " \"" .. part.text .. "\""
            end
        end
        WGN.SendCommand("ChatMessage", {currentNpc.name, text})
    end
end

local fakePlayerData = {}
function fakePlayerData:new(username, x, y, z)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.username = username
    o.x = x
    o.y = y
    o.z = z
    return o
end

function fakePlayerData:getUsername()
    return self.username
end

function fakePlayerData:getX()
    return self.x
end

function fakePlayerData:getY()
    return self.y
end

function fakePlayerData:getZ()
    return self.z
end

local Commands = {}
function Commands.ChatMessage(args)
    local npc = args[1]
    local targetPlayer = args[2]
    local message = args[3]
    local npcData = WGN._npcData[npc]

    if npcData and WGN.IsPlayerNear(getPlayer(), npcData, 20) then
        local targetPlayerName = WRC.Meta.GetName(targetPlayer)
        local fakePlayerData = fakePlayerData:new(npcData.name .. " to " .. targetPlayerName, npcData.x, npcData.y, npcData.z)
        message = WRC.Parsing.PrependPlayerData(fakePlayerData, message)
        local fakeMessage = WL_FakeMessage:new(message, {
            author = npc,
            chatId = 1,
        })
        WRC.Handlers.AddLineInChat(fakeMessage, 1)
    end
end

function Commands.PlayerMessage(args)
    local type = args[1]
    local message = args[2]
    if type == "Error" then
        WGN.ErrorMessage(message)
    elseif type == "Warning" then
        WGN.WarningMessage(message)
    elseif type == "Informational" then
        WGN.InfoMessage(message)
    else
        print("WGN:Commands.PlayerMessage:Unknown type: " .. type)
    end
end

function Commands.GiveItem(args)
    local npc = args[1]
    local item = args[2]
    local count = args[3]

    -- Give item to player
    local player = getPlayer()
    for i = 1, count do
        player:getInventory():AddItem(item)
    end
    WGN.InfoMessage("You have received " .. count .. " " .. item .. " from " .. npc)
end

function Commands.HarmPlayer(args)
    local npc = args[1]
    local type = args[2]
    if type == "Punch" then
        local player = getPlayer()
        local bodyPartIndex = ZombRand(0, BodyPartType.MAX:index())
        local bodyPartType = BodyPartType.FromIndex(bodyPartIndex)
        player:getBodyDamage():AddDamage(bodyPartType, 5.0)
        local stiffness = player:getBodyDamage():getBodyPart(bodyPartType):getStiffness()
        player:getBodyDamage():getBodyPart(bodyPartType):setStiffness(stiffness + 2.5)
        WGN.WarningMessage("You have been punched by " .. npc .. " in the " .. BodyPartType.ToString(bodyPartType))
    else
        print("WGN:Commands.HarmPlayer:Unknown type: " .. type)
    end
end

function Commands.ChatStatus(args)
    local npc = args[1]
    local status = args[2]
    if status == "Started" then
        WGN.SetCurrentNpc(npc)
        WGN.PositiveMessage("Conversation started with " .. npc)
    elseif status == "Ended" then
        WGN.SetCurrentNpc(nil)
        WGN.WarningMessage("Conversation ended with " .. npc)
    else
        print("WGN:Commands.ChatStatus:Unknown status: " .. status)
    end
end

function OnServerCommand(module, command, args)
    if module ~= "WGN" then return end

    if Commands[command] then
        Commands[command](args)
    else
        print("WGN:OnServerCommand Unknown " .. command)
    end
end

table.insert(WRC.CustomChatCallbacks, OnChatMessage)
Events.OnReceiveGlobalModData.Add(OnReceiveGlobalModData)
Events.OnConnected.Add(OnConnected)
Events.OnPlayerMove.Add(OnPlayerMove)
Events.OnServerCommand.Add(OnServerCommand)