WastelandSeasons = WastelandSeasons or {}
WastelandSeasons.Events = {
    ColdSnap = {
        name = "Cold Snap",
        tempAdjust = {-20, -10},
        durationDays = {7, 14},
        cooldownDays = {20, 40},
        chance = 10,
    },
    HeatWave = {
        name = "Heat Wave",
        tempAdjust = {10, 20},
        durationDays = {7, 14},
        cooldownDays = {20, 40},
        chance = 10,
    },
    FreakBlizzard = {
        name = "Freak Blizzard",
        tempAdjust = {-20, -10},
        durationDays = {3, 10},
        cooldownDays = {20, 40},
        chance = 1,
        trigger = "blizzard",
        seasons = {"Winter"},
    },
    FreakDryspell = {
        name = "Freak Dryspell",
        tempAdjust = {10, 20},
        durationDays = {3, 10},
        cooldownDays = {20, 40},
        chance = 1,
        precipitation = "none",
        seasons = {"Early Summer", "Late Summer"},
    },
}

-- We do not do adjustments on client, only server
if isClient() then return end

-- this is temporarily disabled until we can figure out how why its erroring
-- local function OnInitSeasons(season)
--     local tempMin = SandboxVars.WastelandSeasons.TempMin
--     local tempMax = SandboxVars.WastelandSeasons.TempMax
--     if WastelandSeasons.Data and WastelandSeasons.Data.adjustedTemp then
--         print("WastelandSeasons adjusting temp " .. WastelandSeasons.Data.adjustedTemp)
--         tempMin = tempMin + WastelandSeasons.Data.adjustedTemp
--         tempMax = tempMax + WastelandSeasons.Data.adjustedTemp
--     end
--     print("WastelandSeasons:OnInitSeasons")
--     season:init(
--         SandboxVars.WastelandSeasons.Latitude,
--         tempMin,
--         tempMax,
--         SandboxVars.WastelandSeasons.TempDiff,
--         season:getSeasonLag(),
--         season:getHighNoon(),
--         season:getSeedA(),
--         season:getSeedB(),
--         season:getSeedC()
--     );
-- end

-- Events.OnInitSeasons.Add(OnInitSeasons)