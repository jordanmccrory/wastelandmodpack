if isClient() then return end

WastelandSeasons = WastelandSeasons or {}

function WastelandSeasons.RollEvent()
    local season = WastelandSeasons.GetSeason()
    local possibleEvents = {}
    for _, event in pairs(WastelandSeasons.Events) do
        local useEvent = true
        if event.seasons then
            for _, seasonName in pairs(event.seasons) do
                if seasonName == season then
                    useEvent = true
                    break
                end
            end
        end
        if useEvent then
            table.insert(possibleEvents, event)
        end
    end
    local event = WL_Utils.weightedRandom(possibleEvents, "chance")
    if event then
        WastelandSeasons.Data.scheduledEvent = event
        WastelandSeasons.Data.scheduledEventStart = WastelandSeasons.GetStartDay()
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Scheduled event " .. event.name .. " to start in " .. WastelandSeasons.Data.scheduledEventStart .. " days")
    end
end

function WastelandSeasons.TryAddEvent()
    if WastelandSeasons.Data.eventCooldown then
        print("[Wasteland Seasons] Event cooldown " .. WastelandSeasons.Data.eventCooldown)
        WastelandSeasons.Data.eventCooldown = WastelandSeasons.Data.eventCooldown - 1
        if WastelandSeasons.Data.eventCooldown <= 0 then
            WastelandSeasons.Data.eventCooldown = nil
        end
        WastelandSeasons.Save()
    elseif SandboxVars.WastelandSeasons.EnableEvents then
        local roll = ZombRand(SandboxVars.WastelandSeasons.EventChance)
        print("[Wasteland Seasons] Rolled " .. roll .. " for event chance " .. SandboxVars.WastelandSeasons.EventChance)
        if roll == 0 then
            WastelandSeasons.RollEvent()
        end
    end
end

function WastelandSeasons.ProcessCurrentEvent()
    local event = WastelandSeasons.Events[WastelandSeasons.Data.scheduledEvent]
    if WastelandSeasons.Data.scheduledEventStart > 0 then
        WastelandSeasons.Data.scheduledEventStart = WastelandSeasons.Data.scheduledEventStart - 1
        print("[Wasteland Seasons] Event " .. event.name .. " starting in " .. WastelandSeasons.Data.scheduledEventStart .. " days")
        if WastelandSeasons.Data.scheduledEventStart <= 0 then
            WastelandSeasons.Data.scheduledEventStart = 0
            WastelandSeasons.Data.scheduledEventEnd = ZombRand(event.durationDays[1], event.durationDays[2])
            if event.precipitation then
                WastelandSeasons.Data.setPrecipitation = event.precipitation
                WastelandSeasons.Data.clearPrecipitation = false
            end
            if event.trigger then
                WastelandSeasons.Data.trigger = event.trigger
                WastelandSeasons.TriggerWeather()
            end
            WastelandSeasons.Data.adjustedTemp = ZombRand(event.tempAdjust[1], event.tempAdjust[2])
            WastelandSeasons.WriteTemps()
            PzWebStats.RequestReboot("Weather Change", "medium")
            print("[Wasteland Seasons] Event " .. event.name .. " starting for " .. WastelandSeasons.Data.scheduledEventEnd .. " days")
        end
    elseif WastelandSeasons.Data.scheduledEventEnd > 0 then
        WastelandSeasons.Data.scheduledEventEnd = WastelandSeasons.Data.scheduledEventEnd - 1
        print("[Wasteland Seasons] Event " .. event.name .. " ending in " .. WastelandSeasons.Data.scheduledEventEnd .. " days")
        if WastelandSeasons.Data.scheduledEventEnd <= 0 then
            WastelandSeasons.Data.scheduledEventEnd = 0
            WastelandSeasons.Data.eventCooldown = ZombRand(event.cooldownDays[1], event.cooldownDays[2])
            WastelandSeasons.Data.scheduledEvent = nil
            WastelandSeasons.Data.scheduledEventStart = nil
            WastelandSeasons.Data.scheduledEventEnd = nil
            WastelandSeasons.Data.adjustedTemp = nil
            WastelandSeasons.Data.clearPrecipitation = true
            WastelandSeasons.Data.trigger = nil
            WastelandSeasons.Save()
            WastelandSeasons.WriteTemps()
            PzWebStats.RequestReboot("Weather Change", "medium")
            print("[Wasteland Seasons] Event " .. event.name .. " ending")
        end
    end
    WastelandSeasons.Save()
end

function WastelandSeasons.WriteTemps()
    local fileWriter = getFileWriter("PzWebStats/weatherchange.txt", true, false)
    if WastelandSeasons.Data.adjustedTemp then
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMin + WastelandSeasons.Data.adjustedTemp))
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMax + WastelandSeasons.Data.adjustedTemp))
        print("[Wasteland Seasons] Adjusted temp " .. WastelandSeasons.Data.adjustedTemp)
    else
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMin))
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMax))
        print("[Wasteland Seasons] Reset temp")
    end
    fileWriter:close()
end

function WastelandSeasons.CheckDaily()
    local gameTime = getGameTime()
    if gameTime:getMonth() == 11 and gameTime:getDay() == 24 then
        WastelandSeasons.Data.setPrecipitation = "mediumsnow"
        WastelandSeasons.Data.clearPrecipitation = false
        WastelandSeasons.Save()
        WastelandSeasons.DoPrecipitation()
    elseif gameTime:getMonth() == 11 and gameTime:getDay() == 25 then
        WastelandSeasons.Data.setPrecipitation = nil
        WastelandSeasons.Data.clearPrecipitation = true
        WastelandSeasons.Save()
        WastelandSeasons.DoPrecipitation()
    end
    if WastelandSeasons.Data.scheduledEvent then
        if WastelandSeasons.Events[WastelandSeasons.Data.scheduledEvent] then
            WastelandSeasons.ProcessCurrentEvent()
        else
            WastelandSeasons.Data.scheduledEvent = nil
            WastelandSeasons.Data.scheduledEventStart = nil
            WastelandSeasons.Data.scheduledEventEnd = nil
            WastelandSeasons.Data.adjustedTemp = nil
            if WastelandSeasons.Data.setPrecipitation then
                WastelandSeasons.Data.clearPrecipitation = true
            end
            WastelandSeasons.Save()
        end
    else
        WastelandSeasons.TryAddEvent()
    end
end

function WastelandSeasons.GetStartDay()
    return ZombRand(SandboxVars.WastelandSeasons.LeadUpDaysMin, SandboxVars.WastelandSeasons.LeadUpDaysMax)
end

function WastelandSeasons.Save()
    ModData.add("WastelandSeasons", WastelandSeasons.Data)
end

local FLOAT_PRECIPITATION_INTENSITY = 3;
local BOOL_IS_SNOW = 0;

function WastelandSeasons.TriggerWeather()
    if not WastelandSeasons.Data.trigger then return end
    if not WastelandSeasons.Data.scheduledEventEnd then return end
    if WastelandSeasons.Data.trigger == "blizzard" then
        local dur = WastelandSeasons.Data.scheduledEventEnd * 24.0 - 2.0
        getClimateManager():triggerCustomWeatherStage(WeatherPeriod.STAGE_BLIZZARD, dur);
        WastelandSeasons.Data.trigger = nil
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Triggered blizzard for " .. dur .. " hours")
    end
end

function WastelandSeasons.DoTriggerWeather()
    WastelandSeasons.TriggerWeather()
    Events.EveryOneMinutes.Remove(WastelandSeasons.DoTriggerWeather)
end

function WastelandSeasons.DoPrecipitation()
    if WastelandSeasons.Data.setPrecipitation then
        -- Set Precipitation
        local clim = getClimateManager()
        if WastelandSeasons.Data.setPrecipitation == "none" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0)
            print("[Wasteland Seasons] Set precipitation to none")
        elseif WastelandSeasons.Data.setPrecipitation == "lightrain" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.3)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(false)
            print("[Wasteland Seasons] Set precipitation to light rain")
        elseif WastelandSeasons.Data.setPrecipitation == "mediumrain" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.6)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(false)
            print("[Wasteland Seasons] Set precipitation to medium rain")
        elseif WastelandSeasons.Data.setPrecipitation == "heavyrain" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(1)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(false)
            print("[Wasteland Seasons] Set precipitation to heavy rain")
        elseif WastelandSeasons.Data.setPrecipitation == "lightsnow" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.3)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(true)
            print("[Wasteland Seasons] Set precipitation to light snow")
        elseif WastelandSeasons.Data.setPrecipitation == "mediumsnow" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.6)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(true)
            print("[Wasteland Seasons] Set precipitation to medium snow")
        elseif WastelandSeasons.Data.setPrecipitation == "heavysnow" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(1)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(true)
            print("[Wasteland Seasons] Set precipitation to heavy snow")
        end
    elseif WastelandSeasons.Data.clearPrecipitation then
        -- FORCE NO PRECIPITATION
        local clim = getClimateManager()
        clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(false)
        clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(false)
        WastelandSeasons.Data.clearPrecipitation = nil
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Cleared precipitation")
    end
end

function WastelandSeasons.OnInitGlobalModData()
    WastelandSeasons.Data = ModData.getOrCreate("WastelandSeasons")
    WastelandSeasons.DoPrecipitation()
    if WastelandSeasons.Data.trigger then
        Events.EveryOneMinutes.Add(WastelandSeasons.DoTriggerWeather)
    end
end

function WastelandSeasons.OnClientCommand(module, command, sendingPlayer, args)
    if module ~= "WastelandSeasons" then return end
    if command == "CheckDaily" then
        WastelandSeasons.CheckDaily()
        print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced checked daily")
    elseif command == "CancelCurrent" then
        if WastelandSeasons.Data.scheduledEvent then
            if WastelandSeasons.Data.scheduledEventStart > 0 then
                WastelandSeasons.Data.scheduledEvent = nil
                WastelandSeasons.Data.scheduledEventStart = nil
                WastelandSeasons.Data.scheduledEventEnd = nil
                WastelandSeasons.Save()
            else
                WastelandSeasons.Data.scheduledEventEnd = 1
                WastelandSeasons.CheckDaily()
            end
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " cancelled current event")
        end
    elseif command == "ForceEvent" then
        local event = args[1]
        if WastelandSeasons.Events[event] then
            WastelandSeasons.Data.scheduledEvent = event
            WastelandSeasons.Data.scheduledEventStart = WastelandSeasons.GetStartDay()
            WastelandSeasons.Save()
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced event " .. event)
        end
    elseif command == "ForceNow" then
        if WastelandSeasons.Data.scheduledEvent then
            WastelandSeasons.Data.scheduledEventStart = 1
            WastelandSeasons.ProcessCurrentEvent()
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced event to run now")
        end
    elseif command == "ForcePrecipitation" then
        local precipitation = args and args[1] or nil
        if not precipitation then
            WastelandSeasons.Data.setPrecipitation = nil
            WastelandSeasons.Data.clearPrecipitation = true
            WastelandSeasons.Save()
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced precipitation to clear")
        else
            WastelandSeasons.Data.setPrecipitation = precipitation
            WastelandSeasons.Data.clearPrecipitation = false
            WastelandSeasons.Save()
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced precipitation to " .. precipitation)
        end
        WastelandSeasons.DoPrecipitation()
    elseif command == "ForceTrigger" then
        local trigger = args and args[1] or nil
        if trigger then
            WastelandSeasons.Data.trigger = trigger
            WastelandSeasons.Data.scheduledEventEnd = 1
            WastelandSeasons.Save()
            WastelandSeasons.TriggerWeather()
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced trigger to " .. trigger)
        end
    end
end

Events.OnClientCommand.Add(WastelandSeasons.OnClientCommand)
Events.EveryDays.Add(WastelandSeasons.CheckDaily)
Events.OnInitGlobalModData.Add(WastelandSeasons.OnInitGlobalModData)