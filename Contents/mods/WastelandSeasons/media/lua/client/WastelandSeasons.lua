WastelandSeasons = WastelandSeasons or {}

local lastContext = nil

function WastelandSeasons.doMenu(context, data)
    local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
    local seasonsMenu = WL_ContextMenuUtils.getOrCreateSubMenu(submenu, "Weather Events")

    seasonsMenu:addOption("Force Daily Check Now", nil, function()
        sendClientCommand(getPlayer(), "WastelandSeasons", "CheckDaily", {})
    end)

    if data.scheduledEvent then
        local event = WastelandSeasons.Events[data.scheduledEvent]
        if data.scheduledEventStart > 0 then
            seasonsMenu:addOption("== " .. event.name .. " in " .. data.scheduledEventStart .. " days", nil, nil)
            seasonsMenu:addOption("Cancel Event", nil, function()
                sendClientCommand(getPlayer(), "WastelandSeasons", "CancelCurrent", {})
            end)
            seasonsMenu:addOption("Force Event Now", nil, function()
                sendClientCommand(getPlayer(), "WastelandSeasons", "ForceNow", {})
            end)
        else
            seasonsMenu:addOption("== " .. event.name .. " for " .. data.scheduledEventEnd .. " days", nil, nil)
            seasonsMenu:addOption("Cancel Event", nil, function()
                sendClientCommand(getPlayer(), "WastelandSeasons", "CancelCurrent", {})
            end)
        end
    else
        seasonsMenu:addOption("== No Event Scheduled", nil, nil)
        local scheduleMenu = WL_ContextMenuUtils.getOrCreateSubMenu(seasonsMenu, "Schedule Event:")
        for key, event in pairs(WastelandSeasons.Events) do
            scheduleMenu:addOption(event.name, nil, function()
                sendClientCommand(getPlayer(), "WastelandSeasons", "ForceEvent", {key})
            end)
        end
    end

    if data.setPrecipitation then
        seasonsMenu:addOption("== Precipitation: " .. data.setPrecipitation, nil, nil)
        seasonsMenu:addOption("Clear Precipitation", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {nil})
        end)
    else
        seasonsMenu:addOption("== No Precipitation Scheduled", nil, nil)
        local startMenu = WL_ContextMenuUtils.getOrCreateSubMenu(seasonsMenu, "Start Precipitation:")
        startMenu:addOption("None", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"none"})
        end)
        startMenu:addOption("Light Rain", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"lightrain"})
        end)
        startMenu:addOption("Medium Rain", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"mediumrain"})
        end)
        startMenu:addOption("Heavy Rain", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"heavyrain"})
        end)
        startMenu:addOption("Light Snow", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"lightsnow"})
        end)
        startMenu:addOption("Medium Snow", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"mediumsnow"})
        end)
        startMenu:addOption("Heavy Snow", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {"heavysnow"})
        end)
    end
    if getDebug() then
        seasonsMenu:addOption("DEBUG TriggerBlizzard", nil, function()
            sendClientCommand(getPlayer(), "WastelandSeasons", "ForceTrigger", {"blizzard"})
        end)
    end
end

function WastelandSeasons.OnFillWorldObjectContextMenu(playerIdx, context)
    if not isAdmin() then return end
    ModData.request("WastelandSeasons")
    lastContext = context
end

function WastelandSeasons.OnReceiveGlobalModData(key, modData)
    if key == "WastelandSeasons" then
        if lastContext then
            WastelandSeasons.doMenu(lastContext, modData)
        end
    end
end

Events.OnReceiveGlobalModData.Add(WastelandSeasons.OnReceiveGlobalModData)
Events.OnFillWorldObjectContextMenu.Add(WastelandSeasons.OnFillWorldObjectContextMenu)