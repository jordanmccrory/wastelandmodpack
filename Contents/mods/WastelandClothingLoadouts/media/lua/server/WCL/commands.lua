if not isServer() then return end

local Json = require "json"

local loadouts = {}
local loadoutsByPlayer = {}
local wasLoaded = false
local function loadFromDisk()
    print("Loading loadouts from disk")
    wasLoaded = true
    local fileReaderObj = getFileReader("WastelandClothingLoadouts.json", true)
    local json = ""
    local line = fileReaderObj:readLine()
    while line ~= nil do
        json = json .. line
        line = fileReaderObj:readLine()
    end
    fileReaderObj:close()
    if json and json ~= "" then
        local decoded = Json.Decode(json)
        if decoded then
            loadouts = decoded.global or {}
            loadoutsByPlayer = decoded.players or {}
        end
    end
end

local function writeToDisk()
    print("Writing loadouts to disk")
    local fileWriterObj = getFileWriter("WastelandClothingLoadouts.json", true, false)
    local json = Json.Encode({
        global = loadouts,
        players = loadoutsByPlayer
    })
    fileWriterObj:write(json)
    fileWriterObj:close()
end

local function loadIfNeeded()
    if not wasLoaded then
        loadFromDisk()
    end
end

local function sendPlayerLoadoutsToClient(player)
    local playerLoadouts = loadoutsByPlayer[player:getUsername()]
    if not playerLoadouts then return end
    sendServerCommand(player, "WastelandClothingLoadouts", "SyncPlayerLoadouts", playerLoadouts)
end

local function sendLoadoutsToClient(player)
    sendServerCommand(player, "WastelandClothingLoadouts", "SyncLoadouts", loadouts)
end

local function sendLoadoutToAll(loadoutName)
    sendServerCommand("WastelandClothingLoadouts", "SyncLoadout", {name = loadoutName, loadout = loadouts[loadoutName]})
end

local function sendLoadoutsToAll()
    sendServerCommand("WastelandClothingLoadouts", "SyncLoadouts", loadouts)
end

local Commands = {}
function Commands.SaveLoadout(player, args)
    loadIfNeeded()
    local loadoutName = args.name
    if not loadoutName then return end
    loadouts[loadoutName] = args.loadout
    sendLoadoutToAll(loadoutName)
    writeToDisk()
end

function Commands.SavePlayerLoadout(player, args)
    loadIfNeeded()
    local loadoutName = args.name
    if not loadoutName then return end
    local playerName = player:getUsername()
    loadoutsByPlayer[playerName] = loadoutsByPlayer[playerName] or {}
    loadoutsByPlayer[playerName][loadoutName] = args.loadout
    sendPlayerLoadoutsToClient(player)
    writeToDisk()
end

function Commands.DeleteLoadout(player, args)
    loadIfNeeded()
    local loadoutName = args.name
    if not loadoutName then return end
    loadouts[loadoutName] = nil
    sendLoadoutsToAll()
    writeToDisk()
end

function Commands.DeletePlayerLoadout(player, args)
    loadIfNeeded()
    local loadoutName = args.name
    if not loadoutName then return end
    local playerName = player:getName()
    loadoutsByPlayer[playerName] = loadoutsByPlayer or {}
    loadoutsByPlayer[playerName][loadoutName] = nil
    sendPlayerLoadoutsToClient(player)
    writeToDisk()
end

function Commands.GetLoadouts(player, args)
    loadIfNeeded()
    sendLoadoutsToClient(player)
    sendPlayerLoadoutsToClient(player)
end

local function processClientCommand(module, command, player, args)
    if module ~= "WastelandClothingLoadouts" then return end
    if not Commands[command] then return end
    Commands[command](player, args)
end

Events.OnClientCommand.Add(processClientCommand)