WCL_Loadouts = WCL_Loadouts or {}
WCL_Loadouts.Loadouts = WCL_Loadouts.Loadouts or {}
WCL_Loadouts.PlayerLoadouts = WCL_Loadouts.PlayerLoadouts or {}

function WCL_Loadouts.getCurrentClothing(player)
    local wornItems = {}
    local playerItems = player:getInventory():getItems()
    for i=0, playerItems:size()-1 do
        local item = playerItems:get(i)
        if player:isEquippedClothing(item) then
            local location = item:getBodyLocation()
            if location and location ~= "" then
                local color = item:getColor()
                local tint = item:getVisual():getTint(item:getClothingItem())
                table.insert(wornItems, {
                    name = item:getName(),
                    itemType = item:getFullType(),
                    location = location,
                    red = color:getRedFloat(),
                    green = color:getGreenFloat(),
                    blue = color:getBlueFloat(),
                    tintRed = tint:getRedFloat(),
                    tintGreen = tint:getGreenFloat(),
                    tintBlue = tint:getBlueFloat(),
                    tintAlpha = tint:getAlphaFloat(),
                    baseTexture = item:getVisual():getBaseTexture(),
                    textureChoice = item:getVisual():getTextureChoice(),
                    decal = item:getVisual():getDecal(item:getClothingItem()),
                })
            end
        end
    end
    return wornItems
end

function WCL_Loadouts.removeClothing(player, inventory, trigger)
    if not inventory then
        inventory = player:getInventory()
    end
    if trigger == nil then
        trigger = true
    end

    local playerItems = inventory:getItems()
    for i=playerItems:size()-1,0,-1  do
        local item = playerItems:get(i)
        if player:isEquippedClothing(item) then
            player:setWornItem(item:getBodyLocation(), nil)
            inventory:Remove(item)
        end
    end
    -- here we handle raising the mohawk!
    if player:getHumanVisual():getHairModel():contains("MohawkFlat") then
        player:getHumanVisual():setHairModel("Mohawk")
        player:resetModel()
    end
    if trigger then
        triggerEvent("OnClothingUpdated", player)
    end
end

function WCL_Loadouts.getLoadout(loadoutName)
    if WCL_Loadouts.PlayerLoadouts[loadoutName] then
        return WCL_Loadouts.PlayerLoadouts[loadoutName]
    end
    return WCL_Loadouts.Loadouts[loadoutName]
end

function WCL_Loadouts.wearLoadoutClothingReset(player, loadout)
    local inventory = player:getInventory()

    WCL_Loadouts.removeClothing(player, inventory, false)

    for _, item in pairs(loadout) do
        local newItem = inventory:AddItem(item.itemType)
        if newItem then
            newItem:setName(item.name)
            if item.tintRed or item.tintGreen or item.tintBlue or item.tintAlpha then
                local tint = ImmutableColor.new(item.tintRed, item.tintGreen, item.tintBlue, item.tintAlpha)
                newItem:getVisual():setTint(tint)
            end
            local color = Color.new(item.red, item.green, item.blue, 1)
            newItem:setColor(color)
            if item.baseTexture then
                newItem:getVisual():setBaseTexture(item.baseTexture)
            end
            if item.textureChoice then
                newItem:getVisual():setTextureChoice(item.textureChoice)
            end
            if item.decal then
                newItem:getVisual():setDecal(item.decal)
            end
            newItem:synchWithVisual()
            player:setWornItem(item.location, newItem)

			-- here we handle flating the mohawk!
			if player:getHumanVisual():getHairModel():contains("Mohawk") and (item.location == "Hat" or item.location == "FullHat") then
				player:getHumanVisual():setHairModel("MohawkFlat")
				player:resetModel()
			end
        end
    end
	triggerEvent("OnClothingUpdated", player)
end

function WCL_Loadouts.saveLoadout(player, name, loadout)
    if isClient() then
        sendClientCommand(player, "WastelandClothingLoadouts", "SaveLoadout", {name = name, loadout = loadout})
    else
        WCL_Loadouts.Loadouts[name] = loadout
    end
end

function WCL_Loadouts.savePlayerLoadout(player, name, loadout)
    if isClient() then
        sendClientCommand(player, "WastelandClothingLoadouts", "SavePlayerLoadout", {name = name, loadout = loadout})
    else
        WCL_Loadouts.Loadouts[name] = loadout
    end
end

function WCL_Loadouts.deleteLoadout(player, name)
    if isClient() then
        sendClientCommand(player, "WastelandClothingLoadouts", "DeleteLoadout", {name = name})
    else
        WCL_Loadouts.Loadouts[name] = nil
    end
end

function WCL_Loadouts.deletePlayerLoadout(player, name)
    if isClient() then
        sendClientCommand(player, "WastelandClothingLoadouts", "DeletePlayerLoadout", {name = name})
    else
        WCL_Loadouts.Loadouts[name] = nil
    end
end