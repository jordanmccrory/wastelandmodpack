require "ISUI/ISInventoryPane"

local function doRead(items, playerObj)
    for _, item in ipairs(items) do
        local playerInv = playerObj:getInventory()
        if luautils.haveToBeTransfered(playerObj, item, true) then
            ISTimedActionQueue.add(ISInventoryTransferAction:new(playerObj, item, item:getContainer(), playerInv))
        end
        ISTimedActionQueue.add(ISReadABook:new(playerObj, item, 150))
    end
end

local function OnPreFillInventoryObjectContextMenu(player, context, items)
    local actualItems = ISInventoryPane.getActualItems(items)
    for _, item in ipairs(actualItems) do
        if item:getCategory() ~= "Literature" or item:canBeWrite() then
            return
        end
    end

    local playerObj = getSpecificPlayer(player)
    if playerObj:getTraits():isIlliterate() then
        return
    end

    local readBooks = playerObj:getAlreadyReadBook()
    local items = {}
	for _,item in ipairs(actualItems) do
        local cantRead = false
        local alreadyKnown = false
        local alreadyRead = false

        local skillBook = SkillBook[item:getSkillTrained()]
        if skillBook then
            local perkLevel = playerObj:getPerkLevel(skillBook.perk)
            local minLevel = item:getLvlSkillTrained()
            local maxLevel = item:getMaxLevelTrained()
            if perkLevel + 1 < minLevel or perkLevel + 1 > maxLevel then
                cantRead = true
            end
            local readPages = playerObj:getAlreadyReadPages(item:getFullType())
            if readPages >= item:getNumberOfPages() then
                alreadyKnown = true
            end
        end
        if item:getTeachedRecipes() and not item:getTeachedRecipes():isEmpty() then
            if playerObj:getKnownRecipes():containsAll(item:getTeachedRecipes()) then
                alreadyKnown = true
            end
        end
        if readBooks:contains(item:getFullType()) then
            alreadyRead = true
        end

        if not cantRead and not alreadyKnown and not alreadyRead then
            table.insert(items, item)
        end
    end
    if #items > 1 then
        context:addOption(getText("ContextMenu_ReadAllUnread"), items, doRead, playerObj)
    end
end

Events.OnPreFillInventoryObjectContextMenu.Add(OnPreFillInventoryObjectContextMenu)