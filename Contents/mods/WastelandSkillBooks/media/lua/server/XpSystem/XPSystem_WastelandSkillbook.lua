
SkillBook["Fitness"] = {};
SkillBook["Fitness"].perk = Perks.Fitness;
SkillBook["Fitness"].maxMultiplier1 = 2;
SkillBook["Fitness"].maxMultiplier2 = 2;
SkillBook["Fitness"].maxMultiplier3 = 3;
SkillBook["Fitness"].maxMultiplier4 = 5;
SkillBook["Fitness"].maxMultiplier5 = 6;

SkillBook["Strength"] = {};
SkillBook["Strength"].perk = Perks.Strength;
SkillBook["Strength"].maxMultiplier1 = 2;
SkillBook["Strength"].maxMultiplier2 = 2;
SkillBook["Strength"].maxMultiplier3 = 3;
SkillBook["Strength"].maxMultiplier4 = 5;
SkillBook["Strength"].maxMultiplier5 = 6;

SkillBook["Gunsmith"] = {};
SkillBook["Gunsmith"].perk = Perks.Gunsmith;
SkillBook["Gunsmith"].maxMultiplier1 = 3;
SkillBook["Gunsmith"].maxMultiplier2 = 5;
SkillBook["Gunsmith"].maxMultiplier3 = 8;
SkillBook["Gunsmith"].maxMultiplier4 = 12;
SkillBook["Gunsmith"].maxMultiplier5 = 16;
