if not isClient() then return end -- only in MP
WRC = WRC or {}

WRC.Indicator = WRC.Indicator or {
    players = {},
    tickDelay = 0,
    muteTyping = false,
}

function WRC.Indicator.shouldSync()
    return not WRC.Indicator.muteTyping and not getPlayer():isGhostMode()
end

local onTypingDebounce = 0
local clearDebouce = 0
local emptyObject = {}
local lastWasClear = false
local nextXyRange = 0
local nextZRange = 0
function WRC.Indicator.onTyping(xyRange, zRange)
    if not WRC.Indicator.shouldSync() then
        return
    end
    nextXyRange = xyRange
    nextZRange = zRange
    lastWasClear = false
    clearDebouce = 0
    if lastWasClear then
        onTypingDebounce = 1
    elseif onTypingDebounce == 0 then
        onTypingDebounce = 45
    end
end

function WRC.Indicator.onCleared(immediately)
    onTypingDebounce = 0
    if lastWasClear then
        return
    end
    lastWasClear = true
    clearDebouce = immediately and 1 or 45
end

function WRC.Indicator.doLog(text)
    local p = getPlayer()
    local x = math.floor(p:getX())
    local y = math.floor(p:getY())
    local z = math.floor(p:getZ())
    local currentLanguage = WRC.Meta.GetCurrentLanguage(p:getUsername())
    sendClientCommand(p, 'WRC', 'doLog', {x, y, z, text, currentLanguage})
end

function WRC.Indicator.update()
    if onTypingDebounce > 0 then
        if onTypingDebounce == 1 then
            sendClientCommand(getPlayer(), 'WRC', 'onTyping', {nextXyRange, nextZRange})
        end
        onTypingDebounce = onTypingDebounce - 1
    end

    if clearDebouce > 0 then
        if clearDebouce == 1 then
            sendClientCommand(getPlayer(), 'WRC', 'onCleared', emptyObject)
        end
        clearDebouce = clearDebouce - 1
    end

    if WRC.Indicator.tickDelay > 0 then
        WRC.Indicator.tickDelay = WRC.Indicator.tickDelay - 1
        return
    end
    WRC.Indicator.tickDelay = 30

    local toRemove = {}
    for username, lastTs in pairs(WRC.Indicator.players) do
        if lastTs + 8000 < getTimestampMs() then
            table.insert(toRemove, username)
        end
    end
    for _, username in pairs(toRemove) do
        WRC.Indicator.players[username] = nil
    end
end
WRC.Indicator.IndicatorWidth = getTextManager():MeasureStringX(UIFont.Small, "...")
WRC.Indicator.IndicatorHeight = getTextManager():MeasureStringY(UIFont.Small, "...")
WRC.Indicator.UiElements = WRC.Indicator.UiElements or {}
function WRC.Indicator.DrawOverheads()
    local zoom = getCore():getZoom(0)
    local me = getPlayer()
    local c = math.floor(getTimestampMs()/1000) % 3
    local typingText = string.rep(".", c + 1)
    for _,x in pairs(WRC.Indicator.UiElements) do x.seen = false end
    for username, _ in pairs(WRC.Indicator.players) do
        local player = getPlayerFromUsername(username)
        if player and me:CanSee(player) then
            local x = isoToScreenX(0, player:getX(), player:getY(), player:getZ())
            local y = isoToScreenY(0, player:getX(), player:getY(), player:getZ())
            y = y - (130 / zoom) - (3*zoom)
            local ele = WRC.Indicator.UiElements[username]
            if ele then
                ele:setX(x - (ele.width / 2))
                ele:setY(y)
            else
                ele = ISUIElement:new(x - (WRC.Indicator.IndicatorWidth/2), y, WRC.Indicator.IndicatorWidth, WRC.Indicator.IndicatorHeight)
                ele.anchorTop = false
                ele.anchorBottom = true
                ele:initialise()
                ele:addToUIManager()
                ele:backMost()
                WRC.Indicator.UiElements[username] = ele
            end
            ele.seen = true
            ele:drawTextCentre(typingText, WRC.Indicator.IndicatorWidth/2, 0, 1, 1, 1, 1, UIFont.Small)
        end
    end
    for k,v in pairs(WRC.Indicator.UiElements) do
        if not v.seen then
            v:removeFromUIManager()
            WRC.Indicator.UiElements[k] = nil
        end
    end
end

local fntSize = getTextManager():getFontFromEnum(UIFont.Small):getLineHeight()
function WRC.Indicator.DrawTypingInChat(chatInstance)
    local typers = {}
    for username, _ in pairs(WRC.Indicator.players) do
        table.insert(typers, WRC.Meta.GetName(username))
    end

    if #typers > 0 then
        table.sort(typers)

        local text = getText("UI_WRC_Typing") .. table.concat(typers, ", ")

        local textEntry = chatInstance.textEntry
        local x = textEntry:getX() + 2
        local y = textEntry:getY() - fntSize - 2
        local width = getTextManager():MeasureStringX(UIFont.Small, text)
        if width > textEntry:getWidth() then
            text = getText("UI_WRC_ManyTyping")
        end
        chatInstance:drawText(text, x, y, 1, 1, 1, 1, UIFont.Small)
    end
end