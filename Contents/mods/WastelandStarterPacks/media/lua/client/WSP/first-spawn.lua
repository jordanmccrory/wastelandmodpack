
WL_PlayerReady.Add(function ()
    local player = getPlayer()
    if not player:getModData().WSP_DidSpawn then
        WL_UserData.Fetch("WSP", player:getUsername(), function (data)
            player:getModData().WSP_DidSpawn = true
            local spawnCount = data and data.SpawnCount or 0
            local kit = WSP.KitGenerator.GetKitForPlayer(player, spawnCount)
            kit:apply(player)
            WL_UserData.Set("WSP", {SpawnCount = spawnCount + 1},player:getUsername())
        end)
    end
end)