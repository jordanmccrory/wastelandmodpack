---
--- WL_Utils.lua
---
--- Utility functions for Wasteland RP
---
--- 17/10/2023
---

WL_Utils = WL_Utils or {}
WL_Utils.MagicSpace = "� �� "

--- Checks to see if a table is empty and returns true if so. Also returns true if the table is nil.
--- @param table table to check, can be nil
function WL_Utils.isEmpty(table)
    if table == nil then return true end
    for _, _ in pairs(table) do
        return false
    end
    return true
end

--- Uses DoParam to set the properties of an item.
--- WARNING: This often seems to only affect newly created instances and not existing ones.
--- @param itemID string class id of the item e.g. Base.Pistol
--- @param propertiesTable table of props, e.g. { ["MinDamage"] = 0.45, ["MaxDamage"] = 1.65 }
function WL_Utils.setItemProperties(itemID, propertiesTable)
    local item = ScriptManager.instance:getItem(itemID)
    if not item then
        print("ERROR: Item not found to modify: " .. itemID)
        return
    end

    for key, value in pairs(propertiesTable) do
        item:DoParam(key .." = " .. tostring(value))
    end
end

--- Teleports a player to a given location
--- If the player is in a vehicle, it will be stopped and the player will be ejected
--- May need to be called multiple times, as the player may be in a moving vehicle
--- @param player IsoPlayer
--- @param x number
--- @param y number
--- @param z number
--- @return boolean True if the player was teleported, false if they were in a moving vehicle
function WL_Utils.teleportPlayerToCoords(player, x, y, z)
    local vehicle = player:getVehicle()
    if vehicle then
        if vehicle:getDriver() == player and vehicle:getSpeed2D() > 0 then
            vehicle:setForceBrake()
            return false
        end
        vehicle:exit(player)
    end

    if x - math.ceil(x) == 0 and y - math.ceil(y) == 0 then
        x = x + 0.5
        y = y + 0.5
    end

    player:setX(x)
    player:setY(y)
    player:setZ(z)
    player:setLx(x)
    player:setLy(y)
    player:setLz(z)
    return true
end

--- Returns true if the player is a moderator or admin
--- @param player IsoPlayer|nil will use getPlayer() if nil
--- @return boolean
function WL_Utils.canModerate(player)
    if not isClient() and not isServer() then return true end -- SP
    if not player then player = getPlayer() end
    local accessLevel = player:getAccessLevel()
    return accessLevel == "Moderator" or accessLevel == "Admin"
end

--- Returns true if the player has any staff access level (Admin, Moderator, Overseer, GM or Observer)
--- @param player IsoPlayer
--- @return boolean
function WL_Utils.isStaff(player)
    if not isClient() and not isServer() then return true end -- SP
    if not player then return false end
    local accessLevel = player:getAccessLevel()
    return accessLevel ~= "None"
end

--- Returns true if the player has GM level or higher (Admin, Moderator, Overseer or GM)
--- @param player IsoPlayer
--- @return boolean
function WL_Utils.isAtLeastGM(player)
    if not isClient() and not isServer() then return true end -- SP
    if not player then return false end
    local accessLevel = player:getAccessLevel()
    return accessLevel ~= "None" and accessLevel ~= "Observer"
end

--- Expensive function to determine the distance between two X,Y coordinates.
--- Do not use this to check if you are within a certain distance, for that purpose just compare the squared products
function WL_Utils.distance2d(x1, y1, x2, y2)
    local dx = x2 - x1
    local dy = y2 - y1
    return math.sqrt(dx * dx + dy * dy)
end

--- Function to convert a table to a string for debugging
function WL_Utils.tableToString(tbl, indent)
    if not tbl then
        return "nil"
    end

    if not indent then indent = 0 end
    local str = "{"  -- Start with "{" for non-empty tables
    local newline = "\n"
    local tab = "  "
    local empty = true  -- Flag to track if the table is empty

    for k, v in pairs(tbl) do
        if empty then
            str = str .. newline
            empty = false
        end

        local formatting = string.rep(tab, indent)
        if type(v) == "table" then
            str = str .. formatting .. k .. ": "
            str = str .. WL_Utils.tableToString(v, indent + 1) .. ","
        else
            str = str .. formatting .. k .. ": " .. tostring(v) .. ","
        end
    end

    -- Remove the trailing comma if the table was not empty
    if not empty then
        str = str:sub(1, -2) -- Remove the trailing comma
    end

    str = str .. newline .. string.rep(tab, indent - 1) .. "}"
    return str
end

function WL_Utils.toHumanReadableTime(milliseconds)
    local totalMinutes = milliseconds / 60000
    local days = math.floor(totalMinutes / 1440)  -- 1440 minutes in a day
    local hours = math.floor((totalMinutes % 1440) / 60)
    local minutes = math.floor(totalMinutes % 60)
    if days > 0 then
        return string.format("%02d Days %02d Hours %02d Minutes", days, hours, minutes)
    elseif hours > 0 then
        return string.format("%02d Hours %02d Minutes", hours, minutes)
    else
        return string.format("%02d Minutes", minutes)
    end
end

--- Clones an InventoryItem
--- @param item InventoryItem the item to be cloned
--- @return InventoryItem|nil the cloned item
function WL_Utils.cloneItem(item)
    if not item then return end
    local newItem = InventoryItemFactory.CreateItem(item:getFullType())
    if not newItem then return end

    newItem:setAge(item:getAge())
    newItem:setCondition(item:getCondition(), false)
    local vis = item:getVisual();
    if vis then
        newItem:getVisual():copyFrom(vis)
        newItem:synchWithVisual()
    end
    newItem:setBroken(item:isBroken())
    newItem:setCustomColor(item:isCustomColor())
    newItem:setColor(item:getColor())

    newItem:setName(item:getName())
    newItem:setCustomName(item:isCustomName())

    newItem:setCustomWeight(item:isCustomWeight())
    newItem:setActualWeight(item:getActualWeight())

    newItem:setCooked(item:isCooked())
    if item:isCooked() then
        newItem:setCookedString(item:getCookedString())
    end

    newItem:setBurnt(item:isBurnt())
    if item:isBurnt() then
        newItem:setBurntString(item:getBurntString())
    end

    if item:hasModData() then
        newItem:copyModData(item:getModData())
    end

    if item:isRecordedMedia() then
        newItem:setMediaType(item:getMediaType())
        newItem:setRecordedMediaData(item:getMediaData())
    end

    if instanceof(item, "Literature") then
        newItem:setCanBeWrite(item:canBeWrite())
        newItem:setLockedBy(item:getLockedBy())
        newItem:setCustomPages(item:getCustomPages())
    end

    if instanceof(item, "Clothing") then
        item:copyPatchesTo(newItem)
        newItem:setPalette(item:getPalette())
        newItem:setSpriteName(item:getSpriteName())
    end

    if instanceof(item, "DrainableComboItem") then
        newItem:setUsedDelta(item:getUsedDelta())
        newItem:updateWeight()
    end

    if instanceof(item, "Food") then
        newItem:setCalories(item:getCalories())
        newItem:setCarbohydrates(item:getCarbohydrates())
        newItem:setProteins(item:getProteins())
        newItem:setLipids(item:getLipids())
        newItem:setWeight(item:getWeight())
        newItem:setHungChange(item:getHungChange())
        newItem:setUnhappyChange(item:getUnhappyChange())
        newItem:setBoredomChange(item:getBoredomChange())
        newItem:setStressChange(item:getStressChange())
        newItem:setEnduranceChange(item:getEnduranceChange())
        newItem:setPainReduction(item:getPainReduction())
        newItem:setThirstChange(item:getThirstChange())
        newItem:setCookedInMicrowave(item:isCookedInMicrowave())
        newItem:setSpices(item:getSpices())
    end

    if instanceof(item, "HandWeapon") then
        local parts = item:getAllWeaponParts()
        for i=0,parts:size()-1 do
            local newPart = WL_Utils.cloneItem(parts:get(i))
            if newPart then
                newItem:attachWeaponPart(newPart)
            end
        end
        if item:isContainsClip() then
            newItem:setContainsClip(item:isContainsClip())
            newItem:setCurrentAmmoCount(item:getCurrentAmmoCount())
        end
        if item:haveChamber() then
            newItem:setRoundChambered(item:isRoundChambered())
        end
        newItem:setMinDamage(item:getMinDamage());
        newItem:setMaxDamage(item:getMaxDamage());
        newItem:setMinAngle(item:getMinAngle());
        if newItem:isRanged() then
            newItem:setMinRangeRanged(item:getMinRangeRanged());
        else
            newItem:setMinRange(item:getMinRange());
        end
        newItem:setMaxRange(item:getMaxRange());
        newItem:setAimingTime(item:getAimingTime());
        newItem:setRecoilDelay(item:getRecoilDelay());
        newItem:setReloadTime(item:getReloadTime());
        newItem:setClipSize(item:getClipSize());
    end

    if instanceof(item, "Key") then
        newItem:setKeyId(item:getKeyId())
        newItem:setDigitalPadlock(item:isDigitalPadlock())
        newItem:setPadlock(item:isPadlock())
        newItem:setNumberOfKey(item:getNumberOfKey())
    end

    if instanceof(item, "KeyRing") then
        local keys = item:getKeys()
        for i=0,keys:size()-1 do
            local newKey = WL_Utils.cloneItem(keys:get(i))
            if newKey then
                newItem:addKey(newKey)
            end
        end
    end

    if item:IsInventoryContainer() then
        local items = item:getInventory():getItems()
        for i=0,items:size()-1 do
            local newItem2 = WL_Utils.cloneItem(items:get(i))
            if newItem2 then
                newItem:getInventory():AddItem(newItem2)
            end
        end
    end

    return newItem
end

--- @class WeightedObject
--- @field chance number

-- function to take in a table of objects and pick one at random
-- based on the the weight of each object.
--- @param objects WeightedObject[] The objects to choose from
--- @return WeightedObject
function WL_Utils.weightedRandom(objects)
    local totalWeight = 0
    for _, object in ipairs(objects) do
        totalWeight = totalWeight + object.chance
    end
    local random = ZombRand(totalWeight)
    local currentWeight = 0
    for _, object in ipairs(objects) do
        currentWeight = currentWeight + object.chance
        if random < currentWeight then
            return object
        end
    end
    return objects[#objects]
end
