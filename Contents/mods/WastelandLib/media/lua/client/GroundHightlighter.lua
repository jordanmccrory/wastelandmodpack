--- @class Bounds
--- @field x1 number
--- @field y1 number
--- @field x2 number
--- @field y2 number
---
--- @class Color
--- @field r number
--- @field g number
--- @field b number
--- @field a number
---
--- @class Center
--- @field x number
--- @field y number
--- @field z number
---
--- @class GroundHightlighter
--- @field type string none | square | circle
--- @field bounds Bounds
--- @field radius number
--- @field center Center
--- @field color Color
--- @field xray boolean
GroundHightlighter = {}

--- @return GroundHightlighter
function GroundHightlighter:new()
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.type = "none"
    o.bounds = {x1 = 0, y1 = 0, x2 = 0, y2 = 0}
    o.radius = 0
    o.center = {x = 0, y = 0, z = 0}
    o.color = {r = 0.0, g = 1.0, b = 0, a = 1.0}
    o.xray = false
    return o
end

function GroundHightlighter:isPointInRadius(x, y)
    local dx = x - self.center.x
    local dy = y - self.center.y
    return (dx * dx) + (dy * dy) <= (self.radius * self.radius)
end

function GroundHightlighter:isVisible(x, y)
    if self.type == "square" then
        return x >= self.bounds.x1 and x <= self.bounds.x2 and y >= self.bounds.y1 and y <= self.bounds.y2
    end
    if self.type == "circle_edge" then
        -- should only highlight the edge of the circle
        local dx = x - self.center.x
        local dy = y - self.center.y
        local dist = (dx * dx) + (dy * dy)
        local r2 = self.radius * self.radius
        return dist >= r2 - 1.5 and dist <= r2 + 1.5
    end
    return self:isPointInRadius(x, y)
end

function GroundHightlighter:tryHighlightWorldSquare(sq, enabled)
    if enabled and not self:isVisible(sq:getX(), sq:getY()) then
        return
    end
    local objs = sq:getObjects()
    for i = 0, objs:size() - 1 do
        local obj = objs:get(i)
        if obj:isFloor() or self.xray or not enabled then
            obj:setHighlighted(enabled, false)
            if enabled then
                obj:setHighlightColor(self.color.r, self.color.g, self.color.b, self.color.a)
            end
        end
    end
end

function GroundHightlighter:setHightlighted(enabled)
    local cell = getCell()
    for x = self.bounds.x1, self.bounds.x2 do
        for y = self.bounds.y1, self.bounds.y2 do
            local sq = cell:getOrCreateGridSquare(x, y, self.center.z)
            if sq then
                self:tryHighlightWorldSquare(sq, enabled)
            end
        end
    end
end

function GroundHightlighter:remove()
    if self.type ~= "none" then
        self:setHightlighted(false)
        self.type = "none"
    end
end

function GroundHightlighter:setColor(r, g, b, a)
    self.color.r = r
    self.color.g = g
    self.color.b = b
    self.color.a = a or 1.0
    if self.type ~= "none" then
        self:setHightlighted(false)
        self:setHightlighted(true)
    end
end

function GroundHightlighter:enableXray(enabled)
    self.xray = enabled
    if self.type ~= "none" then
        self:remove()
        self:setHightlighted(true)
    end
end

function GroundHightlighter:highlightSquare(x1, y1, x2, y2, z)
    self:remove()
    self.type = "square"
    self.bounds.x1 = math.floor(x1)
    self.bounds.y1 = math.floor(y1)
    self.bounds.x2 = math.floor(x2)
    self.bounds.y2 = math.floor(y2)
    self.center.x = math.floor((x1 + x2) / 2)
    self.center.y = math.floor((y1 + y2) / 2)
    self.center.z = z or 0
    self:setHightlighted(true)
end

function GroundHightlighter:highlightCircle(x, y, radius, z)
    self:remove()
    self.type = "circle"
    self.radius = radius
    self.center.x = math.floor(x)
    self.center.y = math.floor(y)
    self.center.z = z or 0
    self.bounds.x1 = math.floor(x - radius)
    self.bounds.y1 = math.floor(y - radius)
    self.bounds.x2 = math.floor(x + radius)
    self.bounds.y2 = math.floor(y + radius)
    self:setHightlighted(true)
end

--- this is broken.. do not use yet
-- function GroundHightlighter:highlightCircleEdge(x, y, radius, z)
--     self:remove()
--     self.type = "circle_edge"
--     self.radius = radius
--     self.center.x = math.floor(x)
--     self.center.y = math.floor(y)
--     self.center.z = z or 0
--     self.bounds.x1 = math.floor(x - radius)
--     self.bounds.y1 = math.floor(y - radius)
--     self.bounds.x2 = math.floor(x + radius)
--     self.bounds.y2 = math.floor(y + radius)
--     self:setHightlighted(true)
-- end