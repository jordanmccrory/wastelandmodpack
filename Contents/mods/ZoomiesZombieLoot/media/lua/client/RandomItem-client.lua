
ZZL_URL = {}

ZZL_URL.lastSpawn = nil
function ZZL_URL.debugNoise(player, message)
    if getDebug() then
        player:addLineChatElement(message, 1.0, 1.0, 1.0)
    end
end

function ZZL_URL.sendDropReceivedToServer(player, item)
    local args = {item}
    sendClientCommand(player, "ZoomiesZombieLoot", "itemSpawned", args)
end

function ZZL_URL.checkRandomTime(player)
    if ZZL_URL.lastSpawn == nil then
        return false
    end
    local maxSeconds = SandboxVars.ZoomiesZombieLoot.UltraRareLootSeconds or 1000000
    local chance = math.max(0, maxSeconds - math.min(getTimestamp() - ZZL_URL.lastSpawn, maxSeconds))
    local rand = ZombRand(0, chance + 1)
    ZZL_URL.debugNoise(player, "ZZL:URL rolled " .. rand .. " out of " .. chance .. " chance")
    return rand == 0
end

function ZZL_URL.shouldSpawn(zombie)
    local attackedBy = zombie:getAttackedBy()
    if not attackedBy then return false end

    if not instanceof(attackedBy, "IsoPlayer") then return false end

    if not ZZL_URL.checkRandomTime(attackedBy) then return false end

    return true
end

function ZZL_URL.doSpawn(zombie)
    local player = zombie:getAttackedBy()
    if getPlayer() ~= player then return end
    if player then
        local items = {}
        for item in string.gmatch(SandboxVars.ZoomiesZombieLoot.UltraRareItems, "([^;]+)") do
            table.insert(items, item)
        end
        if #items == 0 then
            print("NO ITEMS FOUND FOR ULTRA RARE LOOT")
            return
        end
        local item = items[ZombRand(1, #items + 1)]

        ZZL_URL.sendDropReceivedToServer(player, item)

        if SandboxVars.ZoomiesZombieLoot.UltraRareLootEnabled then
            local inventory = zombie:getInventory()
            inventory:AddItem(item)
            player:addLineChatElement(getText("IGUI_SawRareItem_" .. ZombRand(1, 10)), 1.0, 1.0, 1.0)
        end
    end
end

function ZZL_URL.OnZombieDead(zombie)
    if ZZL_URL.shouldSpawn(zombie) then
        ZZL_URL.doSpawn(zombie)
    end
end

function ZZL_URL.onServerCommand(module, command, args)
    if module == "ZoomiesZombieLoot" and command == "lastSpawnUpdate" then
        ZZL_URL.lastSpawn = args[1]
    end
end

Events.OnZombieDead.Add(ZZL_URL.OnZombieDead)

Events.OnServerCommand.Add(ZZL_URL.onServerCommand)