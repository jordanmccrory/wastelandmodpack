---
--- zzAdjustContainerLoot.lua
--- 30/12/2023
---


require "LootTableEditor"

local addItem = LootTableEditor.addItem
local removeFromTable = LootTableEditor.removeFromTable
local setWeighting = LootTableEditor.setWeighting

-- More Gunpowder for ammo crafting
addItem("GunStoreCounter", "Base.GunPowder", 10)
addItem("GunStoreShelf", "Base.GunPowder", 10)
addItem("GunStoreDisplayCase", "Base.GunPowder", 10)
addItem("ArmyStorageGuns", "Base.GunPowder", 6)
addItem("FirearmWeapons", "Base.GunPowder", 4)
addItem("GarageFirearms", "Base.GunPowder", 5)
addItem("PoliceStorageAmmunition", "Base.GunPowder", 10)

-- Used in Pneumatic Rifle
addItem("FireDeptLockers", "Base.Extinguisher", 10)
addItem("FireStorageTools", "Base.Extinguisher", 10)
addItem("ForestFireTools", "Base.Extinguisher", 10)
addItem("CrateTools", "Base.Extinguisher", 1)
addItem("GarageTools", "Base.Extinguisher", 1)
addItem("ArmyHangarTools", "Base.Extinguisher", 1)
addItem("ClosetShelfGeneric", "Base.Extinguisher", 0.1)

-- Used in Pneumatic Pistol
addItem("BathroomCabinet", "Base.Hairspray", 10)
addItem("BathroomCounter", "Base.Hairspray", 10)
addItem("BathroomCounterNoMeds", "Base.Hairspray", 10)
addItem("BathroomShelf", "Base.Hairspray", 10)
addItem("SalonShelfHaircare", "Base.Hairspray", 10)
addItem("SalonCounter", "Base.Hairspray", 10)

if getActivatedMods():contains("WastelandFirearms") then

	-- Used in Last Resort crafted gun
	addItem("ForestFireTools", "Base.FlareGun", 8)
	addItem("PoliceStorageGuns", "Base.FlareGun", 1)
	addItem("CampingLockers", "Base.FlareGun", 6)
	addItem("CampingStoreGear", "Base.FlareGun", 10)
	addItem("CrateCamping", "Base.FlareGun", 5)
	addItem("Hiker", "Base.FlareGun", 5)

	-- Shell casing boxes
	addItem("GunStoreCounter", "Base.ShellCasingsBox", 10)
	addItem("GunStoreShelf", "Base.ShellCasingsBox", 10)
	addItem("GunStoreDisplayCase", "Base.ShellCasingsBox", 10)
	addItem("ArmyStorageGuns", "Base.ShellCasingsBox", 6)
	addItem("FirearmWeapons", "Base.ShellCasingsBox", 4)
	addItem("GarageFirearms", "Base.ShellCasingsBox", 5)
end

if getActivatedMods():contains("SGarden-Homestead") or getActivatedMods():contains("SGarden-Homestead+") or getActivatedMods():contains("SGarden") or getActivatedMods():contains("SGarden-GardenOnly") then
	addItem("BookstoreMisc", "Base.FlowerPaintMag1", 1)
	addItem("BookstoreMisc", "Base.FlowerPaintMag2", 1)
	addItem("BookstoreMisc", "Base.NatDyeMag1", 1)
	addItem("BookstoreMisc", "Base.FlowerMag", 1)
	addItem("CrateMagazines", "Base.FlowerPaintMag1", 0.2)
	addItem("CrateMagazines", "Base.FlowerPaintMag2", 0.2)
	addItem("CrateMagazines", "Base.NatDyeMag1", 0.2)
	addItem("CrateMagazines", "Base.FlowerMag", 0.2)
	addItem("LibraryBooks", "Base.FlowerPaintMag1", 0.5)
	addItem("LibraryBooks", "Base.FlowerPaintMag2", 0.5)
	addItem("LibraryBooks", "Base.NatDyeMag1", 0.5)
	addItem("LibraryBooks", "Base.FlowerMag", 0.5)
	addItem("LivingRoomShelf", "Base.FlowerPaintMag1", 0.001)
	addItem("LivingRoomShelf", "Base.FlowerPaintMag2", 0.001)
	addItem("LivingRoomShelf", "Base.NatDyeMag1", 0.001)
	addItem("LivingRoomShelf", "Base.FlowerMag", 0.001)
	addItem("LivingRoomShelfNoTapes", "Base.FlowerPaintMag1", 0.001)
	addItem("LivingRoomShelfNoTapes", "Base.FlowerPaintMag2", 0.001)
	addItem("LivingRoomShelfNoTapes", "Base.NatDyeMag1", 0.001)
	addItem("LivingRoomShelfNoTapes", "Base.FlowerMag", 0.001)
	addItem("MagazineRackMixed", "Base.FlowerPaintMag1", 0.01)
	addItem("MagazineRackMixed", "Base.FlowerPaintMag2", 0.01)
	addItem("MagazineRackMixed", "Base.NatDyeMag1", 0.01)
	addItem("MagazineRackMixed", "Base.FlowerMag", 0.01)
	addItem("ShelfGeneric", "Base.FlowerPaintMag1", 0.001)
	addItem("ShelfGeneric", "Base.FlowerPaintMag2", 0.001)
	addItem("ShelfGeneric", "Base.NatDyeMag1", 0.001)
	addItem("ShelfGeneric", "Base.FlowerMag", 0.001)
end