---
--- LootTableEditor.lua
---
--- Utility class to modify and debug loot tables
---
--- 11/07/2023
---

LootTableEditor = {}

function LootTableEditor.findItemIndex(tableToModify, itemClass)
	for i = 1, #tableToModify, 2 do
		if tableToModify[i] == itemClass then
			return i -- Return the index of the item
		end
	end
	return nil -- Return nil if itemClass is not found
end

function LootTableEditor.removeFromTable(tableName, itemClass)
	local tableToModify = ProceduralDistributions.list[tableName]
	if not tableToModify or not tableToModify.items then return end

	local indexToRemove = LootTableEditor.findItemIndex(tableToModify.items, itemClass)
	while indexToRemove do
		table.remove(tableToModify.items, indexToRemove + 1) -- Remove the number following the item
		table.remove(tableToModify.items, indexToRemove) -- Remove the item itself
		indexToRemove = LootTableEditor.findItemIndex(tableToModify.items, itemClass) --Check if the item is still there
	end
end

function LootTableEditor.setWeighting(tableName, itemClass, newWeighting)
	local tableToModify = ProceduralDistributions.list[tableName]
	if not tableToModify or not tableToModify.items then return end

	local indexToModify = LootTableEditor.findItemIndex(tableToModify.items, itemClass)
	if indexToModify then
		tableToModify.items[indexToModify + 1] = newWeighting
	end
end

function LootTableEditor.addItem(tableName, itemClass, weighting)
	local tableToModify = ProceduralDistributions.list[tableName]
	if not tableToModify or not tableToModify.items then return end

	table.insert(tableToModify.items, itemClass)
	table.insert(tableToModify.items, weighting)
end

function LootTableEditor.printTable(tableName)
	local tableToPrint = ProceduralDistributions.list[tableName]
	if not tableToPrint or not tableToPrint.items then return end

	print("Loot Table: " .. tableName)
	for i = 1, #tableToPrint.items, 2 do
		local item = tableToPrint.items[i]
		local weighting = tableToPrint.items[i + 1]
		if not item then
			item = "nil"
		end
		if not weighting then
			weighting = "nil"
		end
		print("Item: " .. item .. ", Weighting: " .. weighting)
	end
end

function LootTableEditor.printAllProceduralDistributions()
	local listAsString = "ProceduralDistributions.list = {\n"
	listAsString = listAsString .. tableToString(ProceduralDistributions.list, 4)
	listAsString = listAsString .. "}\n"

	local fileWriterObj = getFileWriter("ProceduralDistributions.txt", true, false)
	fileWriterObj:write(listAsString)
	fileWriterObj:close()
end

function tableToString(tbl, indent)
	indent = indent or 0
	local result = ""
	for key, value in pairs(tbl) do
		if type(value) == "table" then
			result = result .. string.rep(" ", indent) .. key .. " = {\n"
			result = result .. tableToString(value, indent + 4)
			result = result .. string.rep(" ", indent) .. "},\n"
		else
			if type(key) == "number" then
				if key % 2 == 0 then
					result = result .. string.rep(" ", indent) .. '"' .. tbl[key - 1] .. '": ' .. tostring(value) .. ",\n"
				end
			end
		end
	end
	return result
end