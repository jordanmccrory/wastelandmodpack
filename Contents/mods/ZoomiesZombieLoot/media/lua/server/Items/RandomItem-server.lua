if isClient() or not isServer() then return end

local function setLastSpawnNow()
    ModData.get("ZoomiesZombieLoot").lastSpawn = getTimestamp()
end

local function sendLastSpawnToClients()
    local lastSpawn = ModData.get("ZoomiesZombieLoot").lastSpawn
    sendServerCommand("ZoomiesZombieLoot", "lastSpawnUpdate", {lastSpawn})
end

Events.OnInitGlobalModData.Add(function ()
    local zzl = ModData.getOrCreate("ZoomiesZombieLoot")
    if not zzl.lastSpawn then
        zzl.lastSpawn = getTimestamp()
    end
    Events.EveryTenMinutes.Add(sendLastSpawnToClients)
end)

Events.OnClientCommand.Add(function (module, command, player, args)
    if module == "ZoomiesZombieLoot" and command == "itemSpawned" then
        local item = args[1]
        if SandboxVars.ZoomiesZombieLoot.UltraRareLootDebug then
            local username = player:getUsername()
            writeLog("DebugRandomDrop", username .. " triggered a spawn of " .. item)
        end
        setLastSpawnNow()
        sendLastSpawnToClients()
    end
end)