---
--- AdjustAttachedWeapons.lua
---
--- Some zombies spawn carrying weapons, this file is where we adjust those.
---
--- 24/08/2023
---


if AttachedWeaponDefinitions.assaultRifleOnBack then
	AttachedWeaponDefinitions.assaultRifleOnBack.weapons = {} -- Remove the actual Assault Rifles
	if getActivatedMods():contains("firearmmod") then -- Add some varied rifles from b41 firearms
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Winchester94")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Winchester73")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Rossi92")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Rugerm7722")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.VarmintRifle")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.HuntingRifle")
	else
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.HuntingRifle")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.VarmintRifle")
	end
end

if AttachedWeaponDefinitions.handgunHolster then
	AttachedWeaponDefinitions.handgunHolster.weapons = { 	-- Add back all but the m9
		"Base.Pistol2",
		"Base.Pistol3",
		"Base.Revolver",
		"Base.Revolver_Long",
		"Base.Revolver_Short",
	}
	if getActivatedMods():contains("firearmmod") then -- Increase variety of pistols and revolvers
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtSingleAction22")
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtPeacemaker")
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtAce")
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtAnaconda")
	end
end