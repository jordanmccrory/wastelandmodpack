---
--- zzAdjustVehicleLoot.lua
--- 29/12/2023
---

require "WL_Utils"

local function removeItemWithName(itemName)
	for _, data in pairs(VehicleDistributions) do
		if data.items then
			local items = data.items
			local i = 1
			while i <= #items do
				if items[i] == itemName then
					table.remove(items, i) -- Remove the item with the exact name
					table.remove(items, i) -- Remove the number following the item name
				else
					i = i + 2 -- Move to the next item name
				end
			end
		end

		if data.junk and data.junk.items then
			local junkItems = data.junk.items
			local j = 1
			while j <= #junkItems do
				if junkItems[j] == itemName then
					table.remove(junkItems, j) -- Remove the item with the exact name
					table.remove(junkItems, j) -- Remove the number following the item name
				else
					j = j + 2 -- Move to the next item name
				end
			end
		end
	end
end

removeItemWithName("Pistol")
removeItemWithName("AssaultRifle2")

--local str = WL_Utils.tableToString(VehicleDistributions)
--local fileWriterObj = getFileWriter("VehicleDistributions.txt", true, false)
--fileWriterObj:write(str)
--fileWriterObj:close()
