WBStoning = {}

local diggableTiles = ArrayList.new()
diggableTiles:add('blends_natural_01_16')
diggableTiles:add('blends_natural_01_21')
diggableTiles:add('blends_natural_01_22')
diggableTiles:add('blends_natural_01_23')

diggableTiles:add('blends_natural_01_32')
diggableTiles:add('blends_natural_01_37')
diggableTiles:add('blends_natural_01_38')
diggableTiles:add('blends_natural_01_39')

diggableTiles:add('blends_natural_01_48')
diggableTiles:add('blends_natural_01_53')
diggableTiles:add('blends_natural_01_54')
diggableTiles:add('blends_natural_01_55')

diggableTiles:add('blends_natural_01_64')
diggableTiles:add('blends_natural_01_69')
diggableTiles:add('blends_natural_01_70')
diggableTiles:add('blends_natural_01_71')

diggableTiles:add('blends_natural_01_80')
diggableTiles:add('blends_natural_01_85')
diggableTiles:add('blends_natural_01_86')
diggableTiles:add('blends_natural_01_87')

local mineableTile = 'floors_exterior_natural_01_10'
local exhaustedTile = 'floors_exterior_natural_01_12'

local function predicateNotBroken(item)
    return not item:isBroken()
end

function WBStoning.IsDiggableTile(tileName)
    return diggableTiles:contains(tileName)
end

function WBStoning.GetRandomDiggableTile()
    return diggableTiles:get(ZombRand(diggableTiles:size() - 1))
end

function WBStoning.IsExhaustedTile(tileName)
    return tileName == exhaustedTile
end

function WBStoning.GetExhaustedTile()
    return exhaustedTile
end

function WBStoning.IsMineableTile(tileName)
    return tileName == mineableTile
end

function WBStoning.GetMineableTile()
    return mineableTile
end

function WBStoning.CanDigForStones(square, playerIdx)
    if not WBStoning.IsSuitableSquare(square) then return false end
    if not WBStoning.GetShovel(playerIdx) then return false end
    if not WBStoning.HasEnoughEndurance(playerIdx) then return false end
    if not WBStoning.IsSuitableZone(square) then return false end
    if not WBStoning.IsClear(square) then return false end

    return true
end

function WBStoning.IsSuitableSquare(square)
    local objects = square:getObjects()
    if objects:size() == 0 then return false end

    local object = objects:get(0)
    if not object then return false end
    if object:getModData().WB_NoStones then return false end

    local objectTextureName = object:getTextureName()

    if not WBStoning.IsDiggableTile(objectTextureName) then
        return false
    end

    return true
end

function WBStoning.GetShovel(playerIdx)
    local player = getSpecificPlayer(playerIdx)
    local inv = player:getInventory()
    local shovel = inv:getFirstTagEvalRecurse("TakeDirt", predicateNotBroken)
    return shovel
end

function WBStoning.HasEnoughEndurance(playerIdx)
    local player = getSpecificPlayer(playerIdx)
    if player:getStats():getEndurance() <= 0.2 then
        return false
    end
    return true
end

function WBStoning.IsSuitableZone(square)
    local zone = forageSystem.getForageZoneAt(square:getX(), square:getY())
    if not zone or zone.name ~= "DeepForest" then
        return false
    end
    return true
end

function WBStoning.IsClear(square)
    local objects = square:getObjects()
    if objects:size() > 1 then
        return false
    end
    return true
end

function WBStoning.GetStonesCount(player)
    return player:getInventory():getCountTypeRecurse("Base.Stone")
end

function WBStoning.GetStone(player)
    return player:getInventory():getFirstTypeRecurse("Base.Stone")
end

function WBStoning.GetCrushingHammer(player)
    local hammer = player:getInventory():getFirstTypeEvalRecurse("HammerStone", predicateNotBroken)
    if not hammer then
        hammer = player:getInventory():getFirstTypeEvalRecurse("ClubHammer", predicateNotBroken)
    end
    return hammer
end