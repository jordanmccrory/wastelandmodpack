WastelandBuilds = WastelandBuilds or {}
WastelandBuilds.Config = WastelandBuilds.Config or {}

require "WastelandBuilds/Configs/BrickWalls"
require "WastelandBuilds/Configs/ConcreteWalls"
require "WastelandBuilds/Configs/Doors"
require "WastelandBuilds/Configs/Floors"
require "WastelandBuilds/Configs/MetalFences"
require "WastelandBuilds/Configs/Stairs"
require "WastelandBuilds/Configs/ScrapWalls"
require "WastelandBuilds/Configs/StoneWalls"
require "WastelandBuilds/Configs/WoodFences"
require "WastelandBuilds/Configs/WoodWalls"

WastelandBuilds.Menu = { ["WL Builds"] = {items = {}, subMenu = {}}}

for _, config in ipairs(WastelandBuilds.Config) do
    local menu = WastelandBuilds.Menu["WL Builds"]
    for i = 1, #config.Menu do
        local name = config.Menu[i]
        if not menu.subMenu[name] then
            menu.subMenu[name] = {items = {}, subMenu = {}}
        end
        menu = menu.subMenu[name]
    end
    table.insert(menu.items, config)
end