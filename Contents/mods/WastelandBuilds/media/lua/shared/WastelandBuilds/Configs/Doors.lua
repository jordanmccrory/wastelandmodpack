
table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Scrap" },
    Name = "Metal Scrap Door",
    Description = "A door made from scrap metal",
    Tiles = { 'fixtures_doors_Simon_MD_24', 'fixtures_doors_Simon_MD_25', 'fixtures_doors_Simon_MD_26', 'fixtures_doors_Simon_MD_27' },
    Type = "door",
    Health = 100,
    RequiredSkills = { MetalWelding = 2 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.ScrapMetal', Amount = 4 },
        { Id = 'Base.Screws', Amount = 5 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Scrap" },
    Name = "Metal Wood and Metal Door",
    Description = "A door made from scrap wood and metal",
    Tiles = { 'fixtures_doors_Simon_MD_20', 'fixtures_doors_Simon_MD_21', 'fixtures_doors_Simon_MD_22', 'fixtures_doors_Simon_MD_23' },
    Type = "door",
    Health = 100,
    RequiredSkills = { MetalWelding = 1, Woodwork = 1 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.ScrapMetal', Amount = 2 },
        { Id = 'Base.Plank', Amount = 2 },
        { Id = 'Base.Screws', Amount = 2 },
        { Id = 'Base.Nails', Amount = 2 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
        {"Base.Saw"},
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "Rough Wooden Door",
    Description = "A rough wooden door",
    Tiles = { 'edit_ddd_RUS_Forest Survival_01_8', 'edit_ddd_RUS_Forest Survival_01_9', 'edit_ddd_RUS_Forest Survival_01_10', 'edit_ddd_RUS_Forest Survival_01_11' },
    Type = "door",
    Health = 100,
    BonusHealthSkill = { Woodwork = 10 },
    BonusHealthTrait = { Handy = 100 },
    Xp = { Woodwork = 5 },
    RequiredSkills = { Woodwork = 2 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Plank', Amount = 4 },
        { Id = 'Base.Nails', Amount = 4 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "Simple Wooden Door",
    Description = "A simple wooden door",
    Tiles = { 'fixtures_doors_01_8', 'fixtures_doors_01_9', 'fixtures_doors_01_10', 'fixtures_doors_01_11' },
    Type = "door",
    Health = 300,
    BonusHealthSkill = { Woodwork = 50 },
    BonusHealthTrait = { Handy = 200 },
    Xp = { Woodwork = 25 },
    RequiredSkills = { Woodwork = 4 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Plank', Amount = 4 },
        { Id = 'Base.Nails', Amount = 4 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "White Simple Wooden Door",
    Description = "A simple wooden door",
    Tiles = { 'd_doors_01_52', 'd_doors_01_53', 'd_doors_01_54', 'd_doors_01_55' },
    Type = "door",
    Health = 300,
    BonusHealthSkill = { Woodwork = 50 },
    BonusHealthTrait = { Handy = 200 },
    Xp = { Woodwork = 25 },
    RequiredSkills = { Woodwork = 4 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Plank', Amount = 4 },
        { Id = 'Base.Nails', Amount = 4 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "Simple Wooden Door",
    Description = "A simple wooden door",
    Tiles = { 'fixtures_doors_01_8', 'fixtures_doors_01_9', 'fixtures_doors_01_10', 'fixtures_doors_01_11' },
    Type = "door",
    Health = 300,
    RequiredSkills = {},
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.WastelandBuildsSuppliesWoodDoor', Amount = 1 },
    },
    RequiredTools = {
        {"Base.Screwdriver"},
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "White Simple Wooden Door",
    Description = "A simple wooden door",
    Tiles = { 'd_doors_01_52', 'd_doors_01_53', 'd_doors_01_54', 'd_doors_01_55' },
    Type = "door",
    Health = 300,
    RequiredSkills = {},
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.WastelandBuildsSuppliesWoodDoor', Amount = 1 },
    },
    RequiredTools = {
        {"Base.Screwdriver"},
    },
})


table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "Dark Wooden Door with Window",
    Description = "A dark wooden door with a window",
    Tiles = { 'pert_doors_01_0', 'pert_doors_01_1', 'pert_doors_01_2', 'pert_doors_01_3' },
    Type = "door",
    Health = 300,
    BonusHealthSkill = { Woodwork = 50 },
    BonusHealthTrait = { Handy = 200 },
    Xp = { Woodwork = 25 },
    RequiredSkills = { Woodwork = 8 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Plank', Amount = 4 },
        { Id = 'Base.Nails', Amount = 5 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})


table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Wooden" },
    Name = "Dark Wooden Door",
    Description = "A dark wooden door",
    Tiles = { 'pert_doors_01_8', 'pert_doors_01_9', 'pert_doors_01_10', 'pert_doors_01_11' },
    Type = "door",
    Health = 300,
    BonusHealthSkill = { Woodwork = 50 },
    BonusHealthTrait = { Handy = 200 },
    Xp = { Woodwork = 25 },
    RequiredSkills = { Woodwork = 8 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Plank', Amount = 4 },
        { Id = 'Base.Nails', Amount = 5 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})


table.insert(WastelandBuilds.Config, {
    Menu = { "Doors", "Metal" },
    Name = "Dark Metal Door with Window",
    Description = "A dark metal door with a window",
    Tiles = { 'pert_doors_01_16', 'pert_doors_01_17', 'pert_doors_01_18', 'pert_doors_01_19' },
    Type = "door",
    Health = 600,
    BonusHealthSkill = { MetalWelding = 50 },
    BonusHealthTrait = { Handy = 200 },
    Xp = { MetalWelding = 25 },
    RequiredSkills = { MetalWelding = 8 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.SheetMetal', Amount = 3 },
        { Id = 'Base.MetalBar', Amount = 1 },
        { Id = 'Base.Doorknob', Amount = 1 },
        { Id = 'Base.Hinge', Amount = 2 },
        { Id = 'Base.BlowTorch', Amount = 2 },
    },
    RequiredTools = {
        {"Tag:Hammer"},
        {"Base.Screwdriver"},
    },
})