local function makeConcreteWall(
    type, name, description,
    twall1, twall2, twin1, twin2, tdoor1, tdoor2, tpill, tcorner)

    local health = 400
    local xp = { Woodwork = 5 }
    local skills = { Woodwork = 6 }
    local tools = { "farming.HandShovel" }
    local baseConcrete = 2

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Concrete", type },
        Name = name .. " Wall",
        Description = string.format(description, "wall"),
        Tiles = {
            twall1,
            twall2,
            tpill,
        },
        Type = "wall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.BucketConcreteFull', Amount = baseConcrete },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Concrete", type },
        Name = name .. " Wall [NW Corner]",
        Description = string.format(description, "wall"),
        Tiles = {
            tcorner,
            tcorner,
        },
        Type = "pillar",
        Xp = xp,
        Health = health * 2,
        BonusHealthSkill = { Woodwork = 100 },
        BonusHealthTrait = { Handy = 400 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.BucketConcreteFull', Amount = baseConcrete * 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Concrete", type },
        Name = name .. " Window Frame",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin1,
            twin2,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.BucketConcreteFull', Amount = math.ceil(baseConcrete * 0.75) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Concrete", type },
        Name = name .. " Door Frame",
        Description = string.format(description, "door frame"),
        Tiles = {
            tdoor1,
            tdoor2,
            tpill,
        },
        Type = "doorwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.BucketConcreteFull', Amount = math.ceil(baseConcrete * 0.5) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Concrete", type },
        Name = name .. " Pillar",
        Description = string.format(description, "pillar"),
        Tiles = {
            tpill,
            tpill,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        BonusHealthSkill = { Woodwork = 25 },
        BonusHealthTrait = { Handy = 100 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.BucketConcreteFull', Amount = math.ceil(baseConcrete * 0.25) },
        },
        RequiredTools = tools,
    })
end

makeConcreteWall("Light", "Light Concrete", "A light concrete %s.",
    "LC_Walls_88", "LC_Walls_89",
    "LC_Walls_101", "LC_Walls_102",
    "LC_Walls_94", "LC_Walls_95",
    "LC_Walls_91", "LC_Walls_90"
)

makeConcreteWall("Dark", "Dark Concrete", "A dark concrete %s.",
    "LC_Walls_104", "LC_Walls_105",
    "LC_Walls_118", "LC_Walls_119",
    "LC_Walls_110", "LC_Walls_111",
    "LC_Walls_107", "LC_Walls_106"
)
