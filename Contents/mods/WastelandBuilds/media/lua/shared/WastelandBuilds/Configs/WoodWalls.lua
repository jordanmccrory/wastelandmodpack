local function makeLogWall(
    type, name, description,
    twall1, twall2, twin1, twin2, tdoor1, tdoor2, tpill, tcorner, tend1, tend2, tend3, tend4)

    local health = 200
    local xp = { Woodwork = 5 }
    local skills = { Woodwork = 4 }
    local tools = { "Tag:Hammer", "Tag:Saw" }
    local baseLogs = 4
    local baseNails = 4

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Wall",
        Description = string.format(description, "wall"),
        Tiles = {
            twall1,
            twall2,
            tpill,
        },
        Type = "wall",
        Xp = xp,
        Health = health,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = baseLogs },
            { Id = 'Base.Nails', Amount = baseNails },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Wall [NW Corner]",
        Description = string.format(description, "wall"),
        Tiles = {
            tcorner,
            tcorner,
        },
        Type = "pillar",
        Xp = xp,
        Health = health * 2,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = baseLogs * 2 },
            { Id = 'Base.Nails', Amount = baseNails * 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Window Frame",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin1,
            twin2,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.75) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.75) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Door Frame",
        Description = string.format(description, "door frame"),
        Tiles = {
            tdoor1,
            tdoor2,
            tpill,
        },
        Type = "doorwall",
        Xp = xp,
        Health = health,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.5) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.5) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Pillar [SW]",
        Description = string.format(description, "pillar"),
        Tiles = {
            tpill,
            tpill,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.25) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.25) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Pillar [N]",
        Description = string.format(description, "pillar"),
        Tiles = {
            tend1,
            tend1,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.25) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.25) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Pillar [S]",
        Description = string.format(description, "pillar"),
        Tiles = {
            tend2,
            tend2,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.25) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.25) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Pillar [W]",
        Description = string.format(description, "pillar"),
        Tiles = {
            tend3,
            tend3,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.25) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.25) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Log", type },
        Name = name .. " Pillar [E]",
        Description = string.format(description, "pillar"),
        Tiles = {
            tend4,
            tend4,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Log', Amount = math.ceil(baseLogs * 0.25) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.25) },
        },
        RequiredTools = tools,
    })
end

local function makeFancyWoodWall(
    type, name, description,
    twall1, twall2, twin1, twin2, tdoor1, tdoor2, tpill, tcorner)

    local health = 200
    local xp = { Woodwork = 5 }
    local skills = { Woodwork = 8 }
    local tools = { "Tag:Hammer", "Tag:Saw" }
    local basePlanks = 4
    local baseNails = 4

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Wood", type },
        Name = name .. " Wall",
        Description = string.format(description, "wall"),
        Tiles = {
            twall1,
            twall2,
            tpill,
        },
        Type = "wall",
        Xp = xp,
        Health = health,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = basePlanks },
            { Id = 'Base.Nails', Amount = baseNails },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Wood", type },
        Name = name .. " Wall [NW Corner]",
        Description = string.format(description, "wall"),
        Tiles = {
            tcorner,
            tcorner,
        },
        Type = "pillar",
        Xp = xp,
        Health = health * 2,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = basePlanks * 2 },
            { Id = 'Base.Nails', Amount = baseNails * 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Wood", type },
        Name = name .. " Window Frame",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin1,
            twin2,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = math.ceil(basePlanks * 0.75) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.75) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Wood", type },
        Name = name .. " Door Frame",
        Description = string.format(description, "door frame"),
        Tiles = {
            tdoor1,
            tdoor2,
            tpill,
        },
        Type = "doorwall",
        Xp = xp,
        Health = health,
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = math.ceil(basePlanks * 0.5) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.5) },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Wood", type },
        Name = name .. " Pillar",
        Description = string.format(description, "pillar"),
        Tiles = {
            tpill,
            tpill,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = math.ceil(basePlanks * 0.25) },
            { Id = 'Base.Nails', Amount = math.ceil(baseNails * 0.25) },
        },
        RequiredTools = tools,
    })
end


makeLogWall("Dark", "Dark Logs", "A dark log %s.",
    "pert_walls_02_0", "pert_walls_02_1",
    "pert_walls_02_8", "pert_walls_02_9",
    "pert_walls_02_10", "pert_walls_02_11",
    "pert_walls_02_3", "pert_walls_02_2",
    "pert_walls_02_38", "pert_walls_02_36", "pert_walls_02_32", "pert_walls_02_34"
)

makeLogWall("Light", "Light Logs", "A light log %s.",
    "pert_walls_02_16", "pert_walls_02_17",
    "pert_walls_02_24", "pert_walls_02_25",
    "pert_walls_02_26", "pert_walls_02_27",
    "pert_walls_02_19", "pert_walls_02_18",
    "pert_walls_02_39", "pert_walls_02_37", "pert_walls_02_33", "pert_walls_02_35"
)

makeFancyWoodWall("Washed", "Washed Wood", "A washed out wooden %s.",
    "walls_exterior_wooden_01_28", "walls_exterior_wooden_01_29",
    "walls_exterior_wooden_01_36", "walls_exterior_wooden_01_37",
    "walls_exterior_wooden_01_38", "walls_exterior_wooden_01_39",
    "walls_exterior_wooden_01_31", "walls_exterior_wooden_01_30"
)

makeFancyWoodWall("Barn", "Red Barn Wood", "A red barn wooden %s.",
    "location_barn_01_0", "location_barn_01_1",
    "location_barn_01_8", "location_barn_01_9",
    "location_barn_01_10", "location_barn_01_11",
    "location_barn_01_3", "location_barn_01_2"
)

makeFancyWoodWall("Dark Squares", "Dark Squared Detailed Wood", "A dark squared detailed wooden %s.",
    "walls_Simon_MD_64", "walls_Simon_MD_65",
    "walls_Simon_MD_68", "walls_Simon_MD_69",
    "walls_Simon_MD_70", "walls_Simon_MD_71",
    "walls_Simon_MD_67", "walls_Simon_MD_66"
)

makeFancyWoodWall("Light Squares", "Light Squared Detailed Wood", "A light squared detailed wooden %s.",
    "walls_Simon_MD_72", "walls_Simon_MD_73",
    "walls_Simon_MD_76", "walls_Simon_MD_77",
    "walls_Simon_MD_78", "walls_Simon_MD_79",
    "walls_Simon_MD_75", "walls_Simon_MD_74"
)

makeFancyWoodWall("Light Horizontal", "Light Horizontal Planks", "A light horizontal wooden %s.",
    "walls_Simon_MD_0", "walls_Simon_MD_1",
    "walls_Simon_MD_4", "walls_Simon_MD_5",
    "walls_Simon_MD_6", "walls_Simon_MD_7",
    "walls_Simon_MD_3", "walls_Simon_MD_2"
)

makeFancyWoodWall("Dark Horizontal", "Dark Horizontal Planks", "A dark horizontal wooden %s.",
    "walls_Simon_MD_8", "walls_Simon_MD_9",
    "walls_Simon_MD_12", "walls_Simon_MD_13",
    "walls_Simon_MD_14", "walls_Simon_MD_15",
    "walls_Simon_MD_11", "walls_Simon_MD_10"
)

makeFancyWoodWall("Red Horizontal", "Red Horizontal Planks", "A red horizontal wooden %s.",
    "walls_Simon_MD_32", "walls_Simon_MD_33",
    "walls_Simon_MD_36", "walls_Simon_MD_37",
    "walls_Simon_MD_38", "walls_Simon_MD_39",
    "walls_Simon_MD_35", "walls_Simon_MD_34"
)