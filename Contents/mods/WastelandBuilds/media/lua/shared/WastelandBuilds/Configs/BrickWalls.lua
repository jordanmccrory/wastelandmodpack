local function makeBrickWall(
    type, name, description,
    twall1, twall2, twin1, twin2, tdoor1, tdoor2, tpill, tcorner)

    local health = 500
    local xp = { Woodwork = 5 }
    local skills = { Woodwork = 8 }
    local tools = { "farming.HandShovel" }
    local baseBricks = 4

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Wall",
        Description = string.format(description, "wall"),
        Tiles = {
            twall1,
            twall2,
            tpill,
        },
        Type = "wall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = baseBricks },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Wall [NW Corner]",
        Description = string.format(description, "wall"),
        Tiles = {
            tcorner,
            tcorner,
        },
        Type = "pillar",
        Xp = xp,
        Health = health * 2,
        BonusHealthSkill = { Woodwork = 100 },
        BonusHealthTrait = { Handy = 400 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = baseBricks * 2 },
            { Id = 'Base.BucketPlasterFull', Amount = 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Window Frame",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin1,
            twin2,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Door Frame",
        Description = string.format(description, "door frame"),
        Tiles = {
            tdoor1,
            tdoor2,
            tpill,
        },
        Type = "doorwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.5) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Pillar",
        Description = string.format(description, "pillar"),
        Tiles = {
            tpill,
            tpill,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        BonusHealthSkill = { Woodwork = 25 },
        BonusHealthTrait = { Handy = 100 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.25) },
        },
        RequiredTools = tools,
    })
end

makeBrickWall(
    "Red",
    "Red Brick",
    "A red brick %s.",
    "TCMWalls_02_20", "TCMWalls_02_21",
    "TCMWalls_02_28", "TCMWalls_02_29",
    "TCMWalls_02_30", "TCMWalls_02_31",
    "TCMWalls_02_23", "TCMWalls_02_22")

makeBrickWall(
    "Dark Brown",
    "Dark Brown Brick",
    "A dark brown brick %s.",
    "TCMWalls_02_36", "TCMWalls_02_37",
    "TCMWalls_02_44", "TCMWalls_02_45",
    "TCMWalls_02_46", "TCMWalls_02_47",
    "TCMWalls_02_39", "TCMWalls_02_38")

makeBrickWall(
    "Red Grey",
    "Red Grey Brick",
    "A red grey brick %s.",
    "TCMWalls_01_0", "TCMWalls_01_1",
    "TCMWalls_01_8", "TCMWalls_01_9",
    "TCMWalls_01_10", "TCMWalls_01_11",
    "TCMWalls_01_3", "TCMWalls_01_2")

makeBrickWall(
    "White",
    "White Brick",
    "A white brick %s.",
    "TCMWalls_01_32", "TCMWalls_01_33",
    "TCMWalls_01_40", "TCMWalls_01_41",
    "TCMWalls_01_42", "TCMWalls_01_43",
    "TCMWalls_01_35", "TCMWalls_01_34")

makeBrickWall(
    "Eggshell",
    "Eggshell Brick",
    "An eggshell brick %s.",
    "melos_tiles_walls_brick_raw_01_0", "melos_tiles_walls_brick_raw_01_1",
    "melos_tiles_walls_brick_raw_01_8", "melos_tiles_walls_brick_raw_01_9",
    "melos_tiles_walls_brick_raw_01_10", "melos_tiles_walls_brick_raw_01_11",
    "melos_tiles_walls_brick_raw_01_3", "melos_tiles_walls_brick_raw_01_2")

makeBrickWall(
    "Brown",
    "Brown Brick",
    "An brown brick %s.",
    "melos_tiles_walls_brick_02_brown_0", "melos_tiles_walls_brick_02_brown_1",
    "melos_tiles_walls_brick_02_brown_8", "melos_tiles_walls_brick_02_brown_9",
    "melos_tiles_walls_brick_02_brown_10", "melos_tiles_walls_brick_02_brown_11",
    "melos_tiles_walls_brick_02_brown_3", "melos_tiles_walls_brick_02_brown_2")
