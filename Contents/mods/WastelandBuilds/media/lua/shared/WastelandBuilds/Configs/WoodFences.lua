local function makeSimpleWoodenFence(type, name, description, health, skill, materials, t1, t2, t3, t4)
    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Wooden", type },
        Name = name .. " Fencing",
        Description = description,
        Tiles = { t1, t2, t3 },
        Type = "wall",
        Health = health,
        Xp = { Woodwork = 4 },
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = { Woodwork = skill },
        RequiredTraits = {},
        RequiredMaterials = materials,
        RequiredTools = {
            "Tag:Hammer",
        },
    })

    if t4 then
        local cornerMats = {}
        for _, mat in ipairs(materials) do
            table.insert(cornerMats, { Id = mat.Id, Amount = mat.Amount * 2 })
        end

        table.insert(WastelandBuilds.Config, {
            Menu = { "Fences", "Wooden", type },
            Name = name .. " Fencing [NW Corner]",
            Description = description,
            Tiles = { t4 },
            Type = "pillar",
            Health = health,
            Xp = { Woodwork = 5 },
            BonusHealthSkill = { Woodwork = 50 },
            BonusHealthTrait = { Handy = 200 },
            RequiredSkills = { Woodwork = skill },
            RequiredTraits = {},
            RequiredMaterials = cornerMats,
            RequiredTools = {
                "Tag:Hammer",
            },
        })
    end

    if t3 then
        local pillarMats = {}
        for _, mat in ipairs(materials) do
            table.insert(pillarMats, { Id = mat.Id, Amount = math.ceil(mat.Amount * 0.5) })
        end

        table.insert(WastelandBuilds.Config, {
            Menu = { "Fences", "Wooden", type },
            Name = name .. " Post",
            Description = description,
            Tiles = { t3 },
            Type = "pillar",
            Health = health,
            Xp = { Woodwork = 5 },
            BonusHealthSkill = { Woodwork = 50 },
            BonusHealthTrait = { Handy = 200 },
            RequiredSkills = { Woodwork = skill },
            RequiredTraits = {},
            RequiredMaterials = pillarMats,
            RequiredTools = {
                "Tag:Hammer",
            },
        })
    end

end

makeSimpleWoodenFence("Picket", "White Picket", "A cute white picket fence.", 200, 2, {
    { Id = 'Base.Plank', Amount = 2 },
    { Id = 'Base.Nails', Amount = 4 },
}, "fencing_01_4", "fencing_01_5", "fencing_01_7", nil)

makeSimpleWoodenFence("Trellis", "Tall Trellis", "A tall trellis.", 200, 2, {
    { Id = 'Base.Plank', Amount = 2 },
    { Id = 'Base.Nails', Amount = 4 },
}, "TCM_Garden_01_8", "TCM_Garden_01_9", nil, "TCM_Garden_01_10")

makeSimpleWoodenFence("Trellis", "Tall White Trellis", "A tall white trellis.", 200, 2, {
    { Id = 'Base.Plank', Amount = 2 },
    { Id = 'Base.Nails', Amount = 4 },
}, "TCM_Garden_01_11", "TCM_Garden_01_12", nil, "TCM_Garden_01_13")