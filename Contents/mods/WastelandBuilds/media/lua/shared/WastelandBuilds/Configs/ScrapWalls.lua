local wallVarient = 0

local function addScrapWall(tile1, tile2, tpill)
    wallVarient = wallVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Scrap", "Wall" },
        Name = "Scrap Wall " .. wallVarient,
        Description = "A cheap wall made from scraps.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "wall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 1, MetalWelding = 1},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = 1 },
            { Id = 'Base.ScrapMetal', Amount = 2 },
            { Id = 'Base.Nails', Amount = 2 },
            { Id = 'Base.Screws', Amount = 2 },
        },
        RequiredTools = {
            "Tag:Hammer",
            "Base.Screwdriver",
            "Tag:Saw"
        },
    })
end

local windowVarient = 0
local function addScrapWindow(tile1, tile2, tpill)
    windowVarient = windowVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Scrap", "Window" },
        Name = "Scrap Window Frame " .. windowVarient,
        Description = "A cheap wall made from scraps.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "windowwall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 1, MetalWelding = 1},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = 1 },
            { Id = 'Base.ScrapMetal', Amount = 2 },
            { Id = 'Base.Nails', Amount = 2 },
            { Id = 'Base.Screws', Amount = 2 },
        },
        RequiredTools = {
            "Tag:Hammer",
            "Base.Screwdriver",
            "Tag:Saw"
        },
    })
end

local doorVarient = 0
local function addScrapDoor(tile1, tile2, tpill)
    doorVarient = doorVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Scrap", "Door" },
        Name = "Scrap Door Frame " .. doorVarient,
        Description = "A cheap wall made from scraps.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "doorwall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 1, MetalWelding = 1},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = 1 },
            { Id = 'Base.ScrapMetal', Amount = 2 },
            { Id = 'Base.Nails', Amount = 2 },
            { Id = 'Base.Screws', Amount = 2 },
        },
        RequiredTools = {
            "Tag:Hammer",
            "Base.Screwdriver",
            "Tag:Saw"
        },
    })
end

addScrapWall("walls_Simon_MD_92", "walls_Simon_MD_80", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_93", "walls_Simon_MD_81", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_94", "walls_Simon_MD_82", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_95", "walls_Simon_MD_83", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_96", "walls_Simon_MD_84", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_97", "walls_Simon_MD_85", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_98", "walls_Simon_MD_86", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_99", "walls_Simon_MD_87", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_100", "walls_Simon_MD_88", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_101", "walls_Simon_MD_89", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_102", "walls_Simon_MD_90", "walls_Simon_MD_75")
addScrapWall("walls_Simon_MD_103", "walls_Simon_MD_91", "walls_Simon_MD_75")

addScrapWindow("walls_Simon_MD_111", "walls_Simon_MD_104", "walls_Simon_MD_75")
addScrapWindow("walls_Simon_MD_110", "walls_Simon_MD_105", "walls_Simon_MD_75")
addScrapWindow("walls_Simon_MD_109", "walls_Simon_MD_106", "walls_Simon_MD_75")
addScrapWindow("walls_Simon_MD_108", "walls_Simon_MD_107", "walls_Simon_MD_75")

addScrapDoor("walls_Simon_MD_119", "walls_Simon_MD_112", "walls_Simon_MD_75")
addScrapDoor("walls_Simon_MD_118", "walls_Simon_MD_113", "walls_Simon_MD_75")
addScrapDoor("walls_Simon_MD_117", "walls_Simon_MD_114", "walls_Simon_MD_75")
addScrapDoor("walls_Simon_MD_116", "walls_Simon_MD_115", "walls_Simon_MD_75")