local function makeLargeMetalFence(
    type, name, description,
    health, skill, materials,
    t1, t2, t3, t4, t5, t6)

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Inner",
        Description = description,
        Tiles = { t3, t2, t6 },
        Type = "wall",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = materials,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Ends",
        Description = description,
        Tiles = { t4, t1 },
        Type = "wall",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = materials,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    local cornerMats = {}
    for _, mat in ipairs(materials) do
        table.insert(cornerMats, { Id = mat.Id, Amount = mat.Amount * 2 })
    end

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Corner [NW]",
        Description = description,
        Tiles = { t5 },
        Type = "pillar",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = cornerMats,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    local pillerMats = {}
    for _, mat in ipairs(materials) do
        table.insert(pillerMats, { Id = mat.Id, Amount = math.ceil(mat.Amount * 0.5) })
    end

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Pillar",
        Description = description,
        Tiles = { t6 },
        Type = "pillar",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = pillerMats,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })
end

local function makeWeightedMetalFence(
    type, name, description,
    health, skill, materials,
    t1, t2, t3, t4, t5)

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " 1",
        Description = description,
        Tiles = { t3, t2, t6 },
        Type = "wall",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = materials,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " 2",
        Description = description,
        Tiles = { t4, t1 },
        Type = "wall",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = materials,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    local cornerMats = {}
    for _, mat in ipairs(materials) do
        table.insert(cornerMats, { Id = mat.Id, Amount = mat.Amount * 2 })
    end

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Corner [NW]",
        Description = description,
        Tiles = { t5 },
        Type = "pillar",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = cornerMats,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })
end

local function makeStandardMetalFence(
    type, name, description,
    health, skill, materials,
    t1, t2, t3, t4)

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name,
        Description = description,
        Tiles = { t1, t2 },
        Type = "wall",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = materials,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    local cornerMats = {}
    for _, mat in ipairs(materials) do
        table.insert(cornerMats, { Id = mat.Id, Amount = mat.Amount * 2 })
    end

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Corner [NW]",
        Description = description,
        Tiles = { t4 },
        Type = "pillar",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = cornerMats,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })

    local pillerMats = {}
    for _, mat in ipairs(materials) do
        table.insert(pillerMats, { Id = mat.Id, Amount = math.ceil(mat.Amount * 0.5) })
    end

    table.insert(WastelandBuilds.Config, {
        Menu = { "Fences", "Metal", type },
        Name = name .. " Pillar",
        Description = description,
        Tiles = { t3 },
        Type = "pillar",
        Health = health,
        BonusHealthSkill = { MetalWelding = 50 },
        BonusHealthTrait = { Handy = 200 },
        Xp = { MetalWelding = 25 },
        RequiredSkills = { MetalWelding = skill },
        RequiredTraits = {},
        RequiredMaterials = pillerMats,
        RequiredTools = {
            {"Tag:Hammer"},
            {"Base.Screwdriver"},
        },
    })
end

makeLargeMetalFence("Reinforced", "Reinforced Metal Fence", "An expensive concrete reinforced fence, with barbed wire to stop anyone climbing over", 1700, 8, {
    { Id = 'Base.Wire', Amount = 1 },
    { Id = 'Base.BarbedWire', Amount = 1 },
    { Id = 'Base.BucketConcreteFull', Amount = 1 },
    { Id = 'Base.MetalBar', Amount = 2 },
    { Id = 'Base.Screws', Amount = 10 }
}, 'fencing_01_48', 'fencing_01_49', 'fencing_01_50', 'fencing_01_51', 'fencing_01_52', 'fencing_01_53')

makeLargeMetalFence("Fancy", "Fancy Metal Fence", "A sturdy fence for an estate or official building, but can be climbed over, so not entirely secure", 1000, 10, {
    { Id = 'Base.MetalBar', Amount = 4 },
    { Id = 'Base.Screws', Amount = 8 }
}, 'fencing_01_64', 'fencing_01_65', 'fencing_01_66', 'fencing_01_67', 'fencing_01_68', 'fencing_01_69')

makeLargeMetalFence("ChainLink", "Chain Link", "A cheap and easy to build chain link fence, can be climbed over", 700, 1, {
    { Id = 'Base.WastelandBuildsSuppliesChainLink', Amount = 1 },
}, 'fencing_01_56', 'fencing_01_57', 'fencing_01_58', 'fencing_01_59', 'fencing_01_60', 'fencing_01_61')

makeWeightedMetalFence("Weighted", "Weighted Metal Fence", "A fence weighted by bricks to keep it from being falling over easily, though it can be climbed over",900, 6, {
    { Id = 'Base.Wire', Amount = 1 },
    { Id = 'Base.MetalBar', Amount = 2 },
    { Id = 'Base.Screws', Amount = 6 },
    { Id = 'Base.Stone', Amount = 2 }
}, 'fencing_01_80', 'fencing_01_81', 'fencing_01_82', 'fencing_01_83', 'fencing_01_84')

makeWeightedMetalFence("Weighted Barbed", "Weighted Metal Fence with Barbed Wire", "A fence weighted by bricks to keep it from being falling over easily and with barbed wire to stop anyone climbing it", 900, 6, {
    { Id = 'Base.Wire', Amount = 1 },
    { Id = 'Base.BarbedWire', Amount = 1 },
    { Id = 'Base.MetalBar', Amount = 2 },
    { Id = 'Base.Screws', Amount = 6 },
    { Id = 'Base.Stone', Amount = 2 }
}, 'fencing_01_88', 'fencing_01_89', 'fencing_01_90', 'fencing_01_91', 'fencing_01_92')


makeStandardMetalFence("Corrugated", "Corrugated Metal Fence", "A cheap and easy to build corrugated metal fence, can be climbed over", 500, 2, {
    { Id = 'Base.SheetMetal', Amount = 2 },
    { Id = 'Base.Screws', Amount = 4 },
}, 'fencing_ddd_01_40', 'fencing_ddd_01_41', 'fencing_ddd_01_43', 'fencing_ddd_01_42')