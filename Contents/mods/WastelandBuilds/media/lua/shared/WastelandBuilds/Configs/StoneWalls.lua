local function makeStoneWall(
    type, name, description,
    twall1, twall2, twin1, twin2, tdoor1, tdoor2, tpill, tcorner)

    local health = 300
    local xp = { Woodwork = 5 }
    local skills = { Woodwork = 4 }
    local tools = { "farming.HandShovel" }
    local baseStone = 4

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Stone", type },
        Name = name .. " Wall",
        Description = string.format(description, "wall"),
        Tiles = {
            twall1,
            twall2,
            tpill,
        },
        Type = "wall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Stone', Amount = baseStone },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Stone", type },
        Name = name .. " Wall [NW Corner]",
        Description = string.format(description, "wall"),
        Tiles = {
            tcorner,
            tcorner,
        },
        Type = "pillar",
        Xp = xp,
        Health = health * 2,
        BonusHealthSkill = { Woodwork = 100 },
        BonusHealthTrait = { Handy = 400 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Stone', Amount = baseStone * 2 },
            { Id = 'Base.BucketPlasterFull', Amount = 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Stone", type },
        Name = name .. " Window Frame",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin1,
            twin2,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Stone', Amount = math.ceil(baseStone * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Stone", type },
        Name = name .. " Door Frame",
        Description = string.format(description, "door frame"),
        Tiles = {
            tdoor1,
            tdoor2,
            tpill,
        },
        Type = "doorwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Stone', Amount = math.ceil(baseStone * 0.5) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Stone", type },
        Name = name .. " Pillar",
        Description = string.format(description, "pillar"),
        Tiles = {
            tpill,
            tpill,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        BonusHealthSkill = { Woodwork = 25 },
        BonusHealthTrait = { Handy = 100 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Stone', Amount = math.ceil(baseStone * 0.25) },
        },
        RequiredTools = tools,
    })
end

makeStoneWall(
    "Fine Cobble",
    "Fine Cobblestone",
    "A cobblestone %s made of smaller stones.",
    "pert_walls_01_20", "pert_walls_01_21",
    "pert_walls_01_28", "pert_walls_01_29",
    "pert_walls_01_30", "pert_walls_01_31",
    "pert_walls_01_23",
    "pert_walls_01_22")

makeStoneWall(
    "Regular Cobble",
    "Cobblestone",
    "A cobblestone %s made of medium stones.",
    "pert_walls_01_0", "pert_walls_01_1",
    "pert_walls_01_8", "pert_walls_01_9",
    "pert_walls_01_10", "pert_walls_01_11",
    "pert_walls_01_3",
    "pert_walls_01_2"
)

makeStoneWall(
    "Rough Cobble",
    "Rough Cobblestone",
    "A cobblestone %s made of larger stones.",
    "pert_walls_01_16", "pert_walls_01_17",
    "pert_walls_01_24", "pert_walls_01_25",
    "pert_walls_01_26", "pert_walls_01_27",
    "pert_walls_01_19",
    "pert_walls_01_18"
)