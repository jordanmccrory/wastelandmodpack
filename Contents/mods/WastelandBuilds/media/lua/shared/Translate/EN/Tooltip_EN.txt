Tooltip_EN = {
    Tooltip_Brick = "A durable building material.",
    Tooltip_BrickMold_Empty = "A mold for making bricks. Break stones with a hammer and add water then bake.",
    Tooltip_BrickMold_Wet = "Put it in the oven and wait.",
    Tooltip_BrickMold_Dry = "Ready to use. Try not to break the mold.",
}