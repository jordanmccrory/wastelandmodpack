require "BuildingObjects/ISBuildingObject"

WBDigCursor = ISBuildingObject:derive("WBDigCursor")
WBDigCursor.currentCursor = nil

local function walkToSquare(playerObj, square)
	if playerObj:isTimedActionInstant() then
		return true;
	end
	if AdjacentFreeTileFinder.isTileOrAdjacent(playerObj:getCurrentSquare(), square) then
		return true
	end
	local adjacent = AdjacentFreeTileFinder.Find(square, playerObj)
	if adjacent == nil then return false end
	ISTimedActionQueue.add(ISWalkToTimedAction:new(playerObj, adjacent))
	return true
end

function WBDigCursor:create(x, y, z, north, sprite)
    self:hideTooltip()
    local square = getCell():getGridSquare(x, y, z)
    if not walkToSquare(self.character, square) then return end
    local shovel = WBStoning.GetShovel(self.player)
    if not shovel then return end
    ISInventoryPaneContextMenu.transferIfNeeded(self.character, shovel)
    ISTimedActionQueue.add(ISEquipWeaponAction:new(self.character, shovel, 50, true, true))
    ISTimedActionQueue.add(WBDigForStones:new(self.character, square:getObjects():get(0), shovel))
end

function WBDigCursor:render(x, y, z, square)
    if not WBDigCursor.floorSprite then
        WBDigCursor.floorSprite = IsoSprite.new()
        WBDigCursor.floorSprite:LoadFramesNoDirPageSimple('media/ui/FloorTileCursor.png')
    end
    local hc = getCore():getGoodHighlitedColor()
    if not self:isValid(square) then
        hc = getCore():getBadHighlitedColor()
    end
    self.sq = square
    WBDigCursor.floorSprite:RenderGhostTileColor(x, y, z, hc:getR(), hc:getG(), hc:getB(), 0.8)

    self:renderTooltip()
end

-- Called by IsoCell.setDrag()
function WBDigCursor:deactivate()
    self:hideTooltip()
end

function WBDigCursor:hideTooltip()
    if self.tooltip then
        self.tooltip:removeFromUIManager()
        self.tooltip:setVisible(false)
        self.tooltip = nil
    end
end

function WBDigCursor:renderTooltip()
    if not self.tooltip then
        self.tooltip = ISWorldObjectContextMenu.addToolTip()
        self.tooltip:setVisible(true)
        self.tooltip:addToUIManager()
        self.tooltip.followMouse = not self.joyfocus
        self.tooltip.maxLineWidth = 1000
    else
        self.tooltip.description = "Investigate the ground and look for a stone deposit."
        if not WBStoning.IsSuitableSquare(self.sq) then
            self.tooltip.description = self.tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Must be on a diggable tile."
        end
        if not WBStoning.HasEnoughEndurance(self.player) then
            self.tooltip.description = self.tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Too Tired"
        end
        if not WBStoning.IsSuitableZone(self.sq) then
            self.tooltip.description = self.tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Must be in Deep Forest"
        end
        if not WBStoning.IsClear(self.sq) then
            self.tooltip.description = self.tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Ground must be clear of foilage and items."
        end
    end
end

function WBDigCursor.IsVisible()
    return WBDigCursor.currentCursor and getCell():getDrag(0) == WBDigCursor.currentCursor
end

function WBDigCursor:isValid(square)
    if ISTimedActionQueue.isPlayerDoingAction(self.character) then return false end
    if not WBStoning.CanDigForStones(square, self.player) then return false end
    return true
end

function WBDigCursor:new(character)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:init()
    o.character = character
    o.player = character:getPlayerNum()
    o.noNeedHammer = true
    o.skipBuildAction = true
    WBDigCursor.currentCursor = o
    return o
end
