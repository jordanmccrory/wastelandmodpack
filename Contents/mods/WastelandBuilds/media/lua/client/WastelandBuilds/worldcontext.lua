WastelandBuilds = WastelandBuilds or {}

function WastelandBuilds.OnWorldContext(playerIdx, context)
    local player = getSpecificPlayer(playerIdx)
    WastelandBuilds.MenuHelper.DoMenu(context, WastelandBuilds.Menu, player)
end

Events.OnPreFillWorldObjectContextMenu.Add(WastelandBuilds.OnWorldContext)