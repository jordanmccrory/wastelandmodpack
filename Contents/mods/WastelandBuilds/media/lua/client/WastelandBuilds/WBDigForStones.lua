require "TimedActions/ISBaseTimedAction"

WBDigForStones = ISBaseTimedAction:derive("WBDigForStones")

function WBDigForStones:isValid()
    return self.tile ~= nil and self.tile:getObjectIndex() >= 0 and
            self.character:getPrimaryHandItem() == self.shovel
end

function WBDigForStones:waitToStart()
    self.character:faceThisObject(self.tile)
    return self.character:shouldBeTurning()
end

function WBDigForStones:update()
    self.character:faceThisObject(self.tile)
    self.character:setMetabolicTarget(Metabolics.DiggingSpade);
end

function WBDigForStones:start()
    if self.character:isSitOnGround() then
		self.character:setVariable("sitonground", false);
		self.character:setVariable("forceGetUp", true);
	end
    self:setActionAnim(ISFarmingMenu.getShovelAnim(self.shovel));
    self:setOverrideHandModels(self.shovel, nil);
    self.sound = self.character:playSound("Shoveling")
end

function WBDigForStones:stop()
    self.character:stopOrTriggerSound(self.sound)
    ISBaseTimedAction.stop(self)
end

function WBDigForStones:perform()
    self.character:stopOrTriggerSound(self.sound)

    if self.shovel:isUseEndurance() then
        local use = self.shovel:getWeight() * self.shovel:getFatigueMod(self.character) * self.character:getFatigueMod() * self.shovel:getEnduranceMod() * 0.03
        self.character:getStats():setEndurance(self.character:getStats():getEndurance() - use)
    end

    local foragingSkill = self.character:getPerkLevel(Perks.PlantScavenging)
    local chanceToFind = 25 + foragingSkill * 5
    local roll = ZombRand(100)
    print("roll: " .. roll .. " chanceToFind: " .. chanceToFind)
    if roll <= chanceToFind then
        self:swapTile(WBStoning.GetMineableTile())
        if not self.tile:getModData().WB_StonesRemaining then
            self.tile:getModData().WB_StonesRemaining = ZombRand(8, 16)
            self.tile:transmitModData()
        end
        self.character:getXp():AddXP(Perks.PlantScavenging, 5)
        self.character:addLineChatElement("I found some stones!", 0.8, 0.8, 1.0)
    else
        self:swapTile(WBStoning.GetExhaustedTile())
        self.tile:getModData().WB_NoStones = true
        self.tile:transmitModData()
        self.character:getXp():AddXP(Perks.PlantScavenging, 1)
        self.character:addLineChatElement("I didn't find anything.", 0.8, 0.8, 1.0)
    end

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WBDigForStones:swapTile(type)
    local square = self.tile:getSquare()

    local index = self.tile:getObjectIndex()
    local name = self.tile:getName()

    square:transmitRemoveItemFromSquare(self.tile)
    square:getObjects():remove(self.tile)
    
    local newObj = IsoObject.getNew(square, type, name, false)
    square:transmitAddObjectToSquare(newObj, index)

    self.tile = newObj
end

function WBDigForStones:new(character, tile, shovel)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character
    o.tile = tile
    o.shovel = shovel
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = 500
    if o.character:isTimedActionInstant() then o.maxTime = 1; end
    o.caloriesModifier = 5;
    return o
end
