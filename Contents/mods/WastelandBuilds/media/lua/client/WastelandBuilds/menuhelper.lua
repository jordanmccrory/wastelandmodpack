WastelandBuilds = WastelandBuilds or {}
WastelandBuilds.MenuHelper = WastelandBuilds.MenuHelper or {}

local sLine = " <LINE> "
local sLineLine = " <LINE><LINE> "
local sRed = " <RGB:1,0,0> "
local sGreen = " <RGB:0,1,0> "
local sWhite = " <RGB:1,1,1> "
local sGrey = " <RGB:0.6,0.6,0.6> "

local function hasRequiredMaterial(player, material)
    local item = ISBuildMenu.GetItemInstance(material.Id)
    if not item then
        return false
    end
    if instanceof(item, "DrainableComboItem") then
        local remaining = material.Amount
        local items = player:getInventory():getAllTypeRecurse(material.Id)
        for i=1,items:size() do
            local playerItem = items:get(i-1)
            if playerItem:getDrainableUsesInt() > 0 then
                remaining = remaining - playerItem:getDrainableUsesInt()
                if remaining <= 0 then
                    return true
                end
            end
        end
        return false
    else
        local count = 0
        local items = player:getInventory():getItemsFromFullType(material.Id, true)
        for i=1,items:size() do
            local playerInvItem = items:get(i-1)
            if not instanceof(playerInvItem, "InventoryContainer") or playerInvItem:getInventory():getItems():isEmpty() then
                count = count + 1
            end
        end
        local type = string.split(material.Id, "\\.")[2]
        for k,v in pairs(ISBuildMenu.materialOnGround) do
            if k == type then count = count + v end
        end
        return count >= material.Amount
    end
end

local function hasRequiredSkill(player, skill, level)
    local perk = Perks.FromString(skill)
    if not perk then return false end
    return player:getPerkLevel(perk) >= level
end

local function hasRequiredTrait(player, trait)
    return player:HasTrait(trait)
end

local function predicateNotBroken(item)
	return not item:isBroken()
end

local function hasRequiredTool(player, tool)
    if type(tool) == "string" then
        if luautils.stringStarts(tool, "Tag:") then
            return player:getInventory():containsTagEvalRecurse(string.sub(tool, 5), predicateNotBroken)
        else
            return player:getInventory():containsTypeEvalRecurse(tool, predicateNotBroken)
        end
    end

    if type(tool) == "table" then
        for _, subTool in ipairs(tool) do
            if hasRequiredTool(player, subTool) then
                return true
            end
        end
        return false
    end

    return false
end

local function getToolPlayerHas(player, tool)
    if not type(tool) == "table" then
        return nil
    end
    for _, subTool in ipairs(tool) do
        if hasRequiredTool(player, subTool) then
            return subTool
        end
    end
    return nil
end

local function equipTools(player, tools)
    -- TODO
end

local function getToolName(tool)
    if type(tool) == "string" then
        if luautils.stringStarts(tool, "Tag:") then
            return string.sub(tool, 5)
        else
            return getItemNameFromFullType(tool)
        end
    end

    if type(tool) == "table" and #tool > 0 then
        return getToolName(tool[1])
    end

    return "Mod Error"
end

local function getSkillName(skill)
    local perk = Perks.FromString(skill)
    if perk then
        return perk:getName()
    end
    return "Mod Error"
end

local function getHealth(player, config)
    local health = config.Health
    if config.BonusHealthSkill then
        for perk, amount in pairs(config.BonusHealthSkill) do
            local perk = Perks.FromString(perk)
            if perk then
                local level = player:getPerkLevel(perk)
                health = health + level * amount
            end
        end
    end
    if config.BonusHealthTrait then
        for trait, amount in pairs(config.BonusHealthTrait) do
            if player:HasTrait(trait) then
                health = health + amount
            end
        end
    end
    return health
end

function WastelandBuilds.MenuHelper.DoMenu(context, menu, player)
	ISBuildMenu.materialOnGround = buildUtil.checkMaterialOnGround(player:getCurrentSquare())
    for name, menuOptions in pairs(menu) do
        local subMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, name)
        WastelandBuilds.MenuHelper.DoMenu(subMenu, menuOptions.subMenu, player)
        for _, item in ipairs(menuOptions.items) do
            WastelandBuilds.MenuHelper.AddItem(subMenu, player, item)
        end
    end
end

function WastelandBuilds.MenuHelper.AddItem(context, player, config)
    local option = context:addOption(config.Name, nil, nil)
    local canBuild = true

    local tooltip = ISToolTip:new()
    tooltip:initialise()
    tooltip:setVisible(false)
    tooltip:setName(config.Name)
    tooltip:setTexture(config.Tiles[1])
    option.toolTip = tooltip

    tooltip.description = config.Description .. sLineLine
    if config.Health then
        tooltip.description = tooltip.description .. sWhite .. "Health: " .. tostring(getHealth(player, config)) .. sLineLine
    end

    tooltip.description = tooltip.description .. sGrey .. "Needs:"
    for _, material in ipairs(config.RequiredMaterials) do
        if not hasRequiredMaterial(player, material) then
            canBuild = false
            tooltip.description = tooltip.description .. sLine .. sRed .. tostring(material.Amount) .. " " .. getItemNameFromFullType(material.Id)
        else
            tooltip.description = tooltip.description .. sLine .. sGreen .. tostring(material.Amount) .. " " .. getItemNameFromFullType(material.Id)
        end
    end

    for skill, level in pairs(config.RequiredSkills) do
        if not hasRequiredSkill(player, skill, level) then
            canBuild = false
            tooltip.description = tooltip.description .. sLine .. sRed .. "Skill: " .. getSkillName(skill) .. " " .. tostring(level)
        else
            tooltip.description = tooltip.description .. sLine .. sGreen .. "Skill: " .. getSkillName(skill) .. " " .. tostring(level)
        end
    end

    --TODO: get proper trait names
    for _, trait in ipairs(config.RequiredTraits) do
        if not hasRequiredTrait(player, trait) then
            canBuild = false
            tooltip.description = tooltip.description .. sLine .. sRed .. "Trait: " .. trait
        else
            tooltip.description = tooltip.description .. sLine .. sGreen .. "Trait: " .. trait
        end
    end

    for _, tool in ipairs(config.RequiredTools) do
        if not hasRequiredTool(player, tool) then
            canBuild = false
            tooltip.description = tooltip.description .. sLine .. sRed .. "Tool: " .. getToolName(tool)
        else
            tooltip.description = tooltip.description .. sLine .. sGreen .. "Tool: " .. getToolName(tool)
        end
    end

    if canBuild or ISBuildMenu.cheat then
        option.onSelect = WastelandBuilds.MenuHelper.BuildItem
        option.target = config
    else
        option.notAvailable = true
    end
end

function WastelandBuilds.MenuHelper.BuildItem(config)
    if config.Type == "wall" or config.Type == "windowwall" or config.Type == "lowwall" then
        WastelandBuilds.MenuHelper.BuildWall(config)
    elseif config.Type == "doorwall" then
        WastelandBuilds.MenuHelper.BuildDoorFrame(config)
    elseif config.Type == "stairs" then
        WastelandBuilds.MenuHelper.BuildStairs(config)
    elseif config.Type == "pillar" then
        WastelandBuilds.MenuHelper.BuildPillar(config)
    elseif config.Type == "door" then
        WastelandBuilds.MenuHelper.BuildDoor(config)
    elseif config.Type == "floor" then
        WastelandBuilds.MenuHelper.BuildFloor(config)getTextManager():MeasureStringX(UIFont.Small, getText("UI_characreation_random"))
    else
        print("Unknown type: " .. tostring(config.Type))
    end
end

local function builderBasics(entity, player, config)
    entity.player = player:getPlayerNum()
    entity.name = config.Name

    for _, material in ipairs(config.RequiredMaterials) do
        local item = ISBuildMenu.GetItemInstance(material.Id)
        if not item then
            print("Unknown item: " .. tostring(material.Id))
            return
        end
        if instanceof(item, "DrainableComboItem") then
            entity.modData['use:' .. material.Id] = material.Amount
        else
            entity.modData['need:' .. material.Id] = material.Amount
        end
    end

    if config.Health then
        entity.WB_baseHealth = getHealth(player, config)
        function entity:getHealth()
            return self.WB_baseHealth
        end
    end

    if config.Xp then
        for xp, amount in pairs(config.Xp) do
            entity.modData['xp:' .. xp] = amount
        end
    end

    if not config.RequiredTools or #config.RequiredTools == 0 or config.RequiredTools[1] ~= "Tag:Hammer" then
        entity.noNeedHammer = true
    end

    -- if config.RequiredTools and config.RequiredTools[1] then
    --     if type(config.RequiredTools[1]) == "string" then
    --         if luautils.stringStarts(config.RequiredTools[1], "Tag:") then
    --             entity.firstItem = string.sub(config.RequiredTools[1], 5)
    --         else
    --             entity.firstItem = config.RequiredTools[1]
    --         end
    --     else
    --         entity.firstItem = getToolPlayerHas(player, config.RequiredTools[1])
    --     end
    -- end

    -- if config.RequiredTools and config.RequiredTools[2] then
    --     if type(config.RequiredTools[2]) == "string" then
    --         entity.secondItem = config.RequiredTools[2]
    --     else
    --         entity.secondItem = getToolPlayerHas(player, config.RequiredTools[2])
    --     end
    -- end
end

function WastelandBuilds.MenuHelper.BuildWall(config)
    local player = getPlayer()
    local wall = ISWoodenWall:new(config.Tiles[1], config.Tiles[2], config.Tiles[3])
    wall.canBePlastered = false
    wall.dismantable = true
    if config.Type == "windowwall" then
        wall.canBarricade = true
        wall.isThumpable = false
        wall.hoppable = true
    elseif config.Type == "lowwall" then
        wall.canBarricade = false
        wall.isThumpable = true
        wall.hoppable = true
    else
        wall.canBarricade = false
        wall.isThumpable = true
    end
    wall.modData['wallType'] = 'wall'
    builderBasics(wall, player, config)
    equipTools(player, config.RequiredTools)
    getCell():setDrag(wall, player:getPlayerNum())
end

function WastelandBuilds.MenuHelper.BuildDoorFrame(config)
    local player = getPlayer()
    local doorFrame = ISWoodenDoorFrame:new(config.Tiles[1], config.Tiles[2], config.Tiles[3])
    doorFrame.canBePlastered = false
    doorFrame.dismantable = true
    doorFrame.modData['wallType'] = 'doorframe'
    builderBasics(doorFrame, player, config)
    equipTools(player, config.RequiredTools)
    getCell():setDrag(doorFrame, player:getPlayerNum())
end

function WastelandBuilds.MenuHelper.BuildPillar(config)
    local player = getPlayer()
    local pillar = ISWoodenWall:new(config.Tiles[1], config.Tiles[2])
    pillar.canPassThrough = true
    pillar.canBarricade = false
    pillar.isCorner = true
    pillar.dismantable = true
    builderBasics(pillar, player, config)
    equipTools(player, config.RequiredTools)
    getCell():setDrag(pillar, player:getPlayerNum())
end

function WastelandBuilds.MenuHelper.BuildStairs(config)
    local player = getPlayer()
    local stairs = ISWoodenStairs:new(config.Tiles[1], config.Tiles[2], config.Tiles[3], config.Tiles[4], config.Tiles[5], config.Tiles[6], config.Tiles[7], config.Tiles[8])
    stairs.isThumpable = false
    stairs.dismantable = true
    builderBasics(stairs, player, config)
    equipTools(player, config.RequiredTools)
    getCell():setDrag(stairs, player:getPlayerNum())
end

function WastelandBuilds.MenuHelper.BuildDoor(config)
    local player = getPlayer()
    local door = ISWoodenDoor:new(config.Tiles[1], config.Tiles[2], config.Tiles[3], config.Tiles[4])
    builderBasics(door, player, config)
    equipTools(player, config.RequiredTools)
    getCell():setDrag(door, player:getPlayerNum())
end

function WastelandBuilds.MenuHelper.BuildFloor(config)
    local player = getPlayer()
    local floor = ISWoodenFloor:new(config.Tiles[1], config.Tiles[1])
    builderBasics(floor, player, config)
    equipTools(player, config.RequiredTools)
    getCell():setDrag(floor, player:getPlayerNum())
end