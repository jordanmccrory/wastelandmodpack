require "TimedActions/ISBaseTimedAction"

WBCrushStones = ISBaseTimedAction:derive("WBCrushStones")

function WBCrushStones:isValid()
    return WBStoning.GetStonesCount(self.character) >= 10
    and WBStoning.GetCrushingHammer(self.character) ~= nil
    and self.character:getPrimaryHandItem() == self.hammer
end

function WBCrushStones:update()
    self.character:setMetabolicTarget(Metabolics.HeavyWork)
end

function WBCrushStones:start()
    self:setActionAnim(CharacterActionAnims.BuildLow)
    self:setOverrideHandModels(self.hammer, nil)
end

function WBCrushStones:perform()
    for i = 1, 10 do
        local stone = WBStoning.GetStone(self.character)
        if not stone then break end
        self.character:getInventory():Remove(stone)
    end

    if self.hammer:isUseEndurance() then
        local use = self.hammer:getWeight() * self.hammer:getFatigueMod(self.character) * self.character:getFatigueMod() * self.hammer:getEnduranceMod() * 0.03
        use = use * (self.maxTime / 500)
        self.character:getStats():setEndurance(self.character:getStats():getEndurance() - use)
    end

    local hammerLowerChance = math.ceil((self.hammer:getConditionLowerChance() + self.character:getMaintenanceMod()) / 4)
    print("hammerLowerChance: " .. hammerLowerChance)
    if ZombRand(hammerLowerChance) == 0 then
        self.hammer:setCondition(self.hammer:getCondition() - 1)
        ISWorldObjectContextMenu.checkWeapon(self.character)
    else
        self.character:getXp():AddXP(Perks.Maintenance, 1)
    end

    local roll = WBMineStones.rollOre()
    if roll ~= "Base.Stone" then
        self.character:getInventory():AddItem(roll)
        self.character:Say("I found a " .. getItemNameFromFullType(roll) .. "!")
    else
        self.character:Say("I found nothing useful.")
    end

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WBCrushStones:new(character, hammer)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = 500
    o.hammer = hammer
    if o.character:isTimedActionInstant() then o.maxTime = 1 end
    o.caloriesModifier = 5
    return o
end
