local function walkToSquare(playerObj, square)
	if playerObj:isTimedActionInstant() then
		return true;
	end
	if AdjacentFreeTileFinder.isTileOrAdjacent(playerObj:getCurrentSquare(), square) then
		return true
	end
	local adjacent = AdjacentFreeTileFinder.Find(square, playerObj)
	if adjacent == nil then return false end
	ISTimedActionQueue.add(ISWalkToTimedAction:new(playerObj, adjacent))
	return true
end

local function startMining(player, tile, pickaxe)
    if not walkToSquare(player, tile:getSquare()) then return end
    ISInventoryPaneContextMenu.transferIfNeeded(player, pickaxe)
    ISTimedActionQueue.add(ISEquipWeaponAction:new(player, pickaxe, 50, true, true))
    ISTimedActionQueue.add(WBMineStones:new(player, tile, pickaxe))
end

local function predicateNotBroken(item)
    return not item:isBroken()
end

Events.OnFillWorldObjectContextMenu.Add(function(playerIdx, context, worldObjects)
    if not worldObjects and not worldObjects[1] then return end

    local square = worldObjects[1]:getSquare()

    if WBStoning.IsSuitableSquare(square) then
        local option = context:addOption("Dig for Stones", nil, function ()
            local option = WBDigCursor:new(getSpecificPlayer(playerIdx))
            getCell():setDrag(option, playerIdx)
        end)
        local tooltip = ISToolTip:new()
        tooltip:initialise()
        tooltip:setVisible(false)
        tooltip:setName("Dig for Stones")
        option.toolTip = tooltip
        if not WBStoning.HasEnoughEndurance(playerIdx) then
            option.notAvailable = true
        end
        tooltip.description = "Investigate the ground and look for a stone deposit."
        if not WBStoning.HasEnoughEndurance(playerIdx) then
            tooltip.description = tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Too Tired"
        end
        if not WBStoning.IsSuitableZone(square) then
            tooltip.description = tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Must be in Deep Forest"
        end
        if not WBStoning.IsClear(square) then
            tooltip.description = tooltip.description .. " <LINE> <RGB:1,0.2,0.2> Ground must be clear of foilage and items."
        end
    end

    local objects = square:getObjects()

    if objects:size() == 0 then return end

    local object = objects:get(0)
    if not object then return end
    if object:getModData().WB_NoStones then return end

    local objectTextureName = object:getTextureName()

    if WBStoning.IsMineableTile(objectTextureName) and object:getModData().WB_StonesRemaining and object:getModData().WB_StonesRemaining > 0 then
        local player = getSpecificPlayer(playerIdx)
        local inv = player:getInventory()
        local pickaxe = inv:getFirstTypeEvalRecurse("PickAxe", predicateNotBroken)
        if not pickaxe then
            pickaxe = inv:getFirstTagEvalRecurse("Sledgehammer", predicateNotBroken)
        end
        if not pickaxe then
            pickaxe = inv:getFirstTypeEvalRecurse("HammerStone", predicateNotBroken)
        end
        if pickaxe then
            local option = context:addOption("Mine for Stones", player, startMining, object, pickaxe)
            if player:getStats():getEndurance() <= 0.2 then
                option.notAvailable = true
            end
        end
    end
end)