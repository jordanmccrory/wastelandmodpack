module Base {

    item WastelandBuildsBrick
    {
        DisplayCategory = Material,
        Weight = 0.5,
        Type = Normal,
        DisplayName = Brick,
        Icon = WastelandBuildsBrick,
        Tooltip = Tooltip_Brick,
		WorldStaticModel = WastelandBuildsBrick,
    }

    model WastelandBuildsBrick
	{
		mesh = WastelandBuildsBrick,
		texture = WastelandBuildsBrick,
		scale = 0.05,
	}

    item WastelandBuildsBrickMoldEmpty
    {
        Weight = 0.1,
        Type = Normal,
        DisplayCategory = Tool,
        DisplayName = Brick Mold (Empty),
        Icon = WastelandBuildsBrickMoldEmpty,
        Tooltip = Tooltip_BrickMold_Empty,
    }

    item WastelandBuildsBrickMoldDry
    {
        Weight = 0.6,
        Type = Normal,
        DisplayCategory = Tool,
        DisplayName = Brick Mold (Dry),
        Icon = WastelandBuildsBrickMoldDry,
        Tooltip = Tooltip_BrickMold_Dry,
    }

    item WastelandBuildsBrickMoldWet
    {
        Weight = 1.1,
        Type = Food,
        DisplayCategory = Tool,
        DisplayName = Brick Mold (Wet),
        Icon = WastelandBuildsBrickMoldWet,
        Tooltip = Tooltip_BrickMold_Wet,
        IsCookable = TRUE,
        CookingSound = BoilingFood,
        MinutesToCook = 30,
        ReplaceOnCooked = WastelandBuildsBrickMoldDry,
    }

    item WastelandBuildsLimestone
    {
        DisplayCategory = Material,
        Weight	=	0.3,
        Type	=	Normal,
        DisplayName	=	Limestone,
        Icon	=	TZ_Stone,
        WorldStaticModel = WastelandBuildsLimestone,
    }

    item WastelandBuildsSaltpeter
    {
        DisplayCategory = Material,
        Weight	=	0.3,
        Type	=	Normal,
        DisplayName	=	Saltpeter,
        Icon	=	WastelandBuildsSaltpeter,
        WorldStaticModel = WastelandBuildsSaltpeter,
    }

    item WastelandBuildsPyrite
    {
        DisplayCategory = Material,
        Weight	=	0.3,
        Type	=	Normal,
        DisplayName	=	Pyrite,
        Icon	=	WastelandBuildsPyrite,
        WorldStaticModel = WastelandBuildsPyrite,
    }

    item WastelandBuildsIronOre
    {
        DisplayCategory = Material,
        Weight	=	0.3,
        Type	=	Normal,
        DisplayName	=	Iron Ore,
        Icon	=	WastelandBuildsIronOre,
        WorldStaticModel = WastelandBuildsIronOre,
    }

    item WastelandBuildsIronOrePowder
    {
        DisplayCategory = Material,
        Weight	=	0.2,
        Type	=	Normal,
        DisplayName	=	Crushed Iron Ore,
        Icon	=	WastelandBuildsIronOrePowder,
        WorldStaticModel = WastelandBuildsIronOrePowder,
    }

    item WastelandBuildsBrickMoldWet
    {
        Weight = 1.1,
        Type = Food,
        DisplayCategory = Tool,
        DisplayName = Brick Mold (Wet),
        Icon = WastelandBuildsBrickMoldWet,
        Tooltip = Tooltip_BrickMold_Wet,
        IsCookable = TRUE,
        CookingSound = BoilingFood,
        MinutesToCook = 30,
        ReplaceOnCooked = WastelandBuildsBrickMoldDry,
    }

    model WastelandBuildsLimestone
	{
		mesh = WorldItems/Stone,
		texture = WastelandBuildsLimestone,
		scale = 0.5,
	}

    model WastelandBuildsSaltpeter
	{
		mesh = WorldItems/Stone,
		texture = WastelandBuildsSaltpeter,
		scale = 0.5,
	}

    model WastelandBuildsPyrite
	{
		mesh = WorldItems/Stone,
		texture = WastelandBuildsPyrite,
		scale = 0.5,
	}

    model WastelandBuildsIronOre
	{
		mesh = WorldItems/Stone,
		texture = WastelandBuildsIronOre,
		scale = 0.5,
	}

    model WastelandBuildsIronOrePowder
	{
		mesh = WastelandBuildsLoosePile,
		texture = WastelandBuildsIronOrePowder,
		scale = 1.5,
	}

    item WastelandBuildsSuppliesChainLink
    {
        DisplayCategory = Material,
        Weight	=	8,
        Type	=	Drainable,
        DisplayName	=	Supplies (Chain Link Fence),
        Icon	=	WastelandBuildsSupplies,
        WorldStaticModel = WastelandBuildsSuppliesChainLink,
        UseDelta = 0.125,
    }

    item WastelandBuildsSuppliesWoodDoor
    {
        DisplayCategory = Material,
        Weight	=	4,
        Type	=	Drainable,
        DisplayName	=	Supplies (Wood Door),
        Icon	=	WastelandBuildsSupplies,
        WorldStaticModel = WastelandBuildsSuppliesWoodDoor,
        UseDelta = 0.25,
    }

    item WastelandBuildsSuppliesWoodFloor
    {
        DisplayCategory = Material,
        Weight	=	8,
        Type	=	Drainable,
        DisplayName	=	Supplies (Wood Floor),
        Icon	=	WastelandBuildsSupplies,
        WorldStaticModel = WastelandBuildsSuppliesWoodFloor,
        UseDelta = 0.125,
    }

    item WastelandBuildsSuppliesBrickFloor
    {
        DisplayCategory = Material,
        Weight	=	12,
        Type	=	Drainable,
        DisplayName	=	Supplies (Brick Tile),
        Icon	=	WastelandBuildsSupplies,
        WorldStaticModel = WastelandBuildsSuppliesBrickFloor,
        UseDelta = 0.25
    }

    model WastelandBuildsSuppliesChainLink
    {
        mesh = WastelandBuildsSupplies,
        texture = WastelandBuildsSuppliesChainLink,
    }

    model WastelandBuildsSuppliesWoodDoor
    {
        mesh = WastelandBuildsSupplies,
        texture = WastelandBuildsSuppliesWoodDoor,
    }

    model WastelandBuildsSuppliesWoodFloor
    {
        mesh = WastelandBuildsSupplies,
        texture = WastelandBuildsSuppliesWoodFloor,
    }

    model WastelandBuildsSuppliesBrickFloor
    {
        mesh = WastelandBuildsSupplies,
        texture = WastelandBuildsSuppliesBrickFloor,
    }

}