Events.LevelPerk.Remove(xpUpdate.levelPerk)
xpUpdate.originalLevelPerk = xpUpdate.levelPerk
xpUpdate.levelPerk = function(owner, perk, level, addBuffer)
    if owner then
        local md = owner:getModData()
        if not md.WSK_StartingStrengthPerk then
            if owner:getTraits():contains("Weak") then
                md.WSK_StartingStrengthPerk = "Weak"
            elseif owner:getTraits():contains("Feeble") then
                md.WSK_StartingStrengthPerk = "Feeble"
            elseif owner:getTraits():contains("Stout") then
                md.WSK_StartingStrengthPerk = "Stout"
            elseif owner:getTraits():contains("Strong") then
                md.WSK_StartingStrengthPerk = "Strong"
            else
                md.WSK_StartingStrengthPerk = "None"
            end
        end
        if not md.WSK_StartingFitnessPerk then
            if owner:getTraits():contains("Unfit") then
                md.WSK_StartingFitnessPerk = "Unfit"
            elseif owner:getTraits():contains("Out of Shape") then
                md.WSK_StartingFitnessPerk = "Out of Shape"
            elseif owner:getTraits():contains("Fit") then
                md.WSK_StartingFitnessPerk = "Fit"
            elseif owner:getTraits():contains("Athletic") then
                md.WSK_StartingFitnessPerk = "Athletic"
            else
                md.WSK_StartingFitnessPerk = "None"
            end
        end
    end
    xpUpdate.originalLevelPerk(owner, perk, level, addBuffer)
end
Events.LevelPerk.Add(xpUpdate.levelPerk);