if getActivatedMods():contains("WastelandRpChat") then
    require "Chat/WRC"
end

local strengthKey = "WSK_StartingStrength"
local fitnessKey = "WSK_StartingFitness"
local cachedMediaLineGuids = {}
local cachedRpLanguages = {}
local lastKnownMediaCount = 0
local lastKnownRecipesCount = 0
local lastKnownKills = 0

WSK_Lib = WSK_Lib or {}

function WSK_Lib.PlayerCreated()
    lastKnownMediaCount = 0
    lastKnownRecipesCount = 0
    cachedRpLanguages = {}
end

local function CacheMediaLineGuids()
    cachedMediaLineGuids = {}
    local recordedMedia = getZomboidRadio():getRecordedMedia()
    local categories = recordedMedia:getCategories()
    for i=1,categories:size() do
        local category = categories:get(i-1)
        local mediaType = RecordedMedia.getMediaTypeForCategory(category)
        local mediaList = recordedMedia:getAllMediaForType(mediaType)
        for j=1,mediaList:size() do
            local mediaData = mediaList:get(j-1)
            for k=1, mediaData:getLineCount() do
                local mediaLine = mediaData:getLine(k-1)
                if mediaLine then
                    for l = 0, getNumClassFields(mediaLine) - 1 do
                        local field = getClassField(mediaLine, l)
                        if string.find(tostring(field), "%.text") then
                            table.insert(cachedMediaLineGuids, getClassFieldVal(mediaLine, field))
                        end
                    end
                end
            end
        end
    end
end

local strAndFitnessTraits = {
    ["Weak"] = true,
    ["Feeble"] = true,
    ["Stout"] = true,
    ["Strong"] = true,
    ["Unfit"] = true,
    ["Out of Shape"] = true,
    ["Fit"] = true,
    ["Athletic"] = true,
}

local function GetXpBoostMap()
    local xpBoostMap = {
        Strength = 5,
        Fitness = 5,
    }
    local player = getPlayer()

    local profession = ProfessionFactory.getProfession(player:getDescriptor():getProfession())
    if profession then
        local professionXpMap = transformIntoKahluaTable(profession:getXPBoostMap())
        if professionXpMap then
            for perk,level in pairs(professionXpMap) do
                local perkStr = perk:getId()
                xpBoostMap[perkStr] = (xpBoostMap[perkStr] or 0) + tonumber(tostring(level))
            end
        end
    end

    local playerTraits = player:getTraits()
    for i=0, playerTraits:size()-1 do
        local playerTrait = playerTraits:get(i)
        if not strAndFitnessTraits[playerTrait] then
            local trait = TraitFactory.getTrait(playerTrait)
            if trait then
                local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
                if traitXpMap then
                    for perk,level in pairs(traitXpMap) do
                        local perkStr = perk:getId()
                        xpBoostMap[perkStr] = (xpBoostMap[perkStr] or 0) + tonumber(tostring(level))
                    end
                end
            end
        end
    end
    
    local modData = player:getModData()
    -- We need to do this because PZ is giving trait bumbs REALLY quickly.
    if modData.WSK_StartingStrengthPerk and modData.WSK_StartingStrengthPerk ~= "none" then
        local trait = TraitFactory.getTrait(modData.WSK_StartingStrengthPerk)
        if trait then
            local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
            if traitXpMap then
                for perk,level in pairs(traitXpMap) do
                    local perkStr = perk:getId()
                    xpBoostMap[perkStr] = (xpBoostMap[perkStr] or 0) + tonumber(tostring(level))
                end
            end
        end
    end
    if modData.WSK_StartingFitnessPerk and modData.WSK_StartingFitnessPerk ~= "none" then
        local trait = TraitFactory.getTrait(modData.WSK_StartingFitnessPerk)
        if trait then
            local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
            if traitXpMap then
                for perk,level in pairs(traitXpMap) do
                    local perkStr = perk:getId()
                    xpBoostMap[perkStr] = (xpBoostMap[perkStr] or 0) + tonumber(tostring(level))
                end
            end
        end
    end
    
    -- Because PZ automatically gives you traits for strength and fitness
    -- we need to log what they were at the start of the character.
    if not modData[strengthKey] then
        modData[strengthKey] = xpBoostMap["Strength"]
    else
        xpBoostMap["Strength"] = modData[strengthKey]
    end

    if not modData[fitnessKey] then
        modData[fitnessKey] = xpBoostMap["Fitness"]
    else
        xpBoostMap["Fitness"] = modData[fitnessKey]
    end

    if getActivatedMods():contains("WastelandProfessions") then
        for perk,level in pairs(xpBoostMap) do
            if WastelandProfession_DoubleableSkills[perk] then
                xpBoostMap[perk] = math.min(level * 2, 6)
            end
        end
    end

    return xpBoostMap
end

function WSK_Lib.GetPlayerMedia(player)
    local mediaLines = {}
    for _, lineGuid in ipairs(cachedMediaLineGuids) do
        if player.isKnownMediaLine and player:isKnownMediaLine(lineGuid) then
            table.insert(mediaLines, lineGuid)
        end
    end
    return mediaLines
end

function WSK_Lib.SetPlayerMedia(player, media)
    print("WSK Restoring Media")
    for _, line in ipairs(media) do
        player:addKnownMediaLine(line)
    end
end

function WSK_Lib.IsNewMedia(player)
    local total = 0
    for _, lineGuid in ipairs(cachedMediaLineGuids) do
        if player.isKnownMediaLine and player:isKnownMediaLine(lineGuid) then
            total = total + 1
        end
    end

    if total > lastKnownMediaCount then
        lastKnownMediaCount = total
        return true
    end

    return false
end


function WSK_Lib.GetPlayerRecipes(player)
    local recipes = {}
    local knownRecipes = player:getKnownRecipes()

    for i=0, knownRecipes:size()-1 do
        local recipeID = knownRecipes:get(i)
        recipes[recipeID] = true
    end

    local playerDesc = player:getDescriptor()
    local playerTraits = player:getTraits()
    for i=0, playerTraits:size()-1 do
        local playerTrait = playerTraits:get(i)
        local trait = TraitFactory.getTrait(playerTrait)
        if trait then
            local traitRecipes = trait:getFreeRecipes()
            for ii=0, traitRecipes:size()-1 do
                local traitRecipe = traitRecipes:get(ii)
                recipes[traitRecipe] = nil
            end
        end
    end

    local playerProfession = playerDesc:getProfession()
    local profession = ProfessionFactory.getProfession(playerProfession)
    if profession then
        local profFreeRecipes = profession:getFreeRecipes()
        for i=0, profFreeRecipes:size()-1 do
            local profRecipe = profFreeRecipes:get(i)
            recipes[profRecipe] = nil
        end
    end

    local returnedGainedRecipes = {}
    for recipeID,_ in pairs(recipes) do
        table.insert(returnedGainedRecipes, recipeID)
    end

    return returnedGainedRecipes
end

function WSK_Lib.SetPlayerRecipes(player, recipes)
    print("WSK Restoring Recipes")
    for _, recipeID in ipairs(recipes) do
        player:learnRecipe(recipeID)
    end
end

function WSK_Lib.IsNewRecipes(player)
    local knownRecipesCount = player:getKnownRecipes():size()
    if lastKnownRecipesCount ~= knownRecipesCount then
        lastKnownRecipesCount = knownRecipesCount
        return true
    end
    return false
end

function WSK_Lib.GetPlayerSkills(player)
    local skills = {}
    local xpBoostMap = GetXpBoostMap()

    for i=1, Perks.getMaxIndex()-1 do
        local perks = Perks.fromIndex(i)
        local perk = PerkFactory.getPerk(perks)
        if perk then
            local currentXP = player:getXp():getXP(perk)
            local perkStr = perk:getId()
            local bonusLevels = (xpBoostMap[perkStr] or 0)
            local savingXP = currentXP-perk:getTotalXpForLevel(bonusLevels)
            if savingXP > 0 then
                skills[perkStr] = savingXP
            end
        end
    end

    return skills
end

function WSK_Lib.SetPlayerSkills(player, skills, numDeaths)
    print("WSK Restoring XP")
    local percentKeep = 100 - SandboxVars.WastelandOptions.SkillKeeperPunishPercent
    local hasWastelandXP = getActivatedMods():contains("WastelandXP")
    local xpBoostMap = GetXpBoostMap()
    local xp = player:getXp()
    for i=1, Perks.getMaxIndex()-1 do
        local perks = Perks.fromIndex(i)
        local perk = PerkFactory.getPerk(perks)
        if perk then
            local perkStr = perk:getId()
            if skills[perkStr] then
                if numDeaths > SandboxVars.WastelandOptions.SkillKeeperMaxFreeDeaths then
                    skills[perkStr] = skills[perkStr] * (percentKeep / 100)
                end
                local bonusLevels = (xpBoostMap[perkStr] or 0)
                local totalXp = skills[perkStr] + perk:getTotalXpForLevel(bonusLevels)
                if hasWastelandXP then
                    local cap = getLevelCap(player, perk)
                    if cap then
                        totalXp = math.min(totalXp, perk:getTotalXpForLevel(cap))
                    end
                end
                local neededXp = totalXp - xp:getXP(perk)
                if neededXp > 0 then
                    xp:AddXP(perk, neededXp, false, false, true)
                end
            end
        end
    end
    if numDeaths > SandboxVars.WastelandOptions.SkillKeeperMaxFreeDeaths then
        player:addLineChatElement("Recovered " .. percentKeep .. "% of XP", 1, 0, 0)
    else
        player:addLineChatElement("Recovered all XP", 1, 0, 0)
    end
end

function WSK_Lib.GetPlayerLanguages(player)
    if getActivatedMods():contains("roleplaychat") then
        local modData = player:getModData()
        return {modData.rpLanguage1, modData.rpLanguage2}
    end
    if getActivatedMods():contains("WastelandRpChat") then
        return WRC.Meta.GetKnownLanguages()
    end
    return nil
end

function WSK_Lib.SetPlayerLanguages(player, languages)
    if getActivatedMods():contains("roleplaychat") and languages then
        print("WSK Restoring RP Languages")
        cachedRpLanguages.rpLanguage1 = languages[1] or "Empty Slot"
        cachedRpLanguages.rpLanguage2 = languages[2] or "Empty Slot"
        local modData = player:getModData()
        modData.rpLanguage1 = cachedRpLanguages.rpLanguage1
        modData.rpLanguage2 = cachedRpLanguages.rpLanguage2
        ISChat.instance.rpLanguage1 = cachedRpLanguages.rpLanguage1
        ISChat.instance.rpLanguage2 = cachedRpLanguages.rpLanguage2
    end

    if getActivatedMods():contains("WastelandRpChat") and languages then
        print("WSK Restoring WastelandRpChat Languages")
        for _, language in ipairs(languages) do
            WRC.Meta.AddKnownLanguage(language)
        end
    end
end

function WSK_Lib.IsNewLanguages(player)
    if getActivatedMods():contains("roleplaychat") then
        local modData = player:getModData()
        return modData.rpLanguage1 ~= cachedRpLanguages.rpLanguage1 or modData.rpLanguage2 ~= cachedRpLanguages.rpLanguage2
    end
    if getActivatedMods():contains("WastelandRpChat") then
        local areNew = false
        local langs = WRC.Meta.GetKnownLanguages()
        for i, lang in ipairs(langs) do
            if lang ~= cachedRpLanguages[i] then
                areNew = true
                break
            end
        end
        cachedRpLanguages = langs
        return areNew
    end
    return false
end

function WSK_Lib.GetPlayerKills(player)
    local kills = player:getZombieKills()
    lastKnownKills = kills
    return kills
end

function WSK_Lib.SetPlayerKills(player, kills)
    print("WSK Restoring Kills")
    player:setZombieKills(kills)
end

function WSK_Lib.IsNewPLayerKills(player)
    local kills = player:getZombieKills()
    if kills ~= lastKnownKills then
        return true
    end
    return false
end

WSK_Lib.DeathButton = ISUIElement:derive("WSK_DeathButton")

function WSK_Lib.DeathButton:create(player, data)
    local textHeight = getTextManager():MeasureStringY(UIFont.Small, "XXXXXX")
    local textWidth = getTextManager():MeasureStringX(UIFont.Small, "XXXXXX")
    local w = textWidth + 6
    local h = textHeight * 3 + 10
    local o = ISUIElement:new(getCore():getScreenWidth() - w - 3, getCore():getScreenHeight()/2 - h/2, w, h)
    setmetatable(o, self)
    self.__index = self
    o.player = player
    o.background = true
    o.backgroundColor = {r=0, g=0, b=0, a=0.5}
    o.borderColor = {r=1, g=1, b=1, a=0.5}
    o.endTime = data.timestamp + SandboxVars.WastelandOptions.SkillKeeperDeathTpAllowTime
    o.tpX = data.x
    o.tpY = data.y
    o.tpZ = data.z
    o.textHeight = textHeight
    o:initialise()
    o:instantiate()
    o:addToUIManager()
end

function WSK_Lib.DeathButton:prerender()
    local timeleft = self.endTime - getTimestamp()

    if self.player:isDead() or timeleft <= 0 then
        self:removeFromUIManager()
        return
    end

    ISPanel.prerender(self)
    self:drawTextCentre("TP to", self.width / 2, 3, 1, 1, 1, 1, UIFont.Small)
    self:drawTextCentre("Death", self.width / 2, 3 + self.textHeight, 1, 1, 1, 1, UIFont.Small)
    self:drawTextCentre("( " .. tostring(timeleft) .. "s )", self.width / 2, 7 + self.textHeight * 2, 1, 1, 1, 1, UIFont.Small)

    if self:isMouseOver() then
        self:drawRect(0, 0, self.width, self.height, 0.3, 0.5, 0.5, 0.5)
    end
end

function WSK_Lib.DeathButton:onMouseUp()
    if not self:isMouseOver() then return end
    WL_UserData.Set("WSK_DeathPoint", false, self.player:getUsername(), true)
    WL_Utils.teleportPlayerToCoords(self.player, self.tpX, self.tpY, self.tpZ)
    WL_Utils.makePlayerSafe(SandboxVars.WastelandOptions.SkillKeeperDeathTpSafetime)
    self:removeFromUIManager()
end

function WSK_Lib.DropItemsToBox(player)
    local playerInv = player:getInventory()
    local box = InventoryItemFactory.CreateItem("Base.WSKDeathBox")
    local boxInv = box:getInventory()
    box:setCustomName(true)
    box:setName(player:getUsername() .. "'s Death Box")
    box:getModData().owner = player:getUsername()
    box:getModData().time = getTimestamp()

    local items = playerInv:getItems()
    for i=items:size()-1,0,-1 do
        local item = items:get(i)
        playerInv:DoRemoveItem(item)
        boxInv:AddItem(item)
    end

    player:getCurrentSquare():AddWorldInventoryItem(box, 0.5, 0.5, 0)
end

Events.OnGameStart.Add(function()
    CacheMediaLineGuids()
end)