if AttachedWeaponDefinitions then
    AttachedWeaponDefinitions.femurStomach = {
        chance = 30,
        weaponLocation = {"Stomach"},
        bloodLocations = {"Torso_Lower","Back"},
        addHoles = true,
        daySurvived = 0,
        weapons = {
            "GreysWLWeaponsMelee.Femur",
        },
    }
end