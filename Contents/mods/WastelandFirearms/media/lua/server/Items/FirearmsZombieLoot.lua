require 'Items/SuburbsDistributions'

---@param item string
---@param weightBothOrMale number
---@param weightFemale number|nil
local function addLootItem(item, weightBothOrMale, weightFemale)
    if not weightFemale then weightFemale = weightBothOrMale end
    table.insert(SuburbsDistributions["all"]["inventorymale"].items, item);
    table.insert(SuburbsDistributions["all"]["inventorymale"].items, weightBothOrMale);
    table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, item);
    table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, weightFemale);
end

addLootItem("Base.ShellCasings", 1)
addLootItem("Base.GunPowder", 0.5) --TODO Temporary until we can craft it?

addLootItem("GreysWLWeaponsMelee.Femur", 2)

-- For last resort gun
table.insert(SuburbsDistributions.all.Outfit_Ranger.items, "Base.FlareGun");
table.insert(SuburbsDistributions.all.Outfit_Ranger.items, 10);

-- For pneumatic rifle
table.insert(SuburbsDistributions.all.Outfit_FiremanFullSuit.items, "Base.Extinguisher");
table.insert(SuburbsDistributions.all.Outfit_FiremanFullSuit.items, 5);