
function Recipe.OnGiveXP.Gunsmith5(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Gunsmith, 5);
end

function Recipe.OnGiveXP.Gunsmith10(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Gunsmith, 10);
end

function Recipe.OnGiveXP.Gunsmith15(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Gunsmith, 15);
end

function Recipe.OnGiveXP.Gunsmith20(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Gunsmith, 20);
end

function Recipe.OnGiveXP.Gunsmith30(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Gunsmith, 30);
end

function Recipe.OnGiveXP.MetalWelding5(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.MetalWelding, 5);
end

-- Recipe.OnGiveXP.MetalWelding10 defined in vanilla

function Recipe.OnGiveXP.MetalWelding15(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.MetalWelding, 15);
end

function Recipe.OnGiveXP.MetalWelding20(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.MetalWelding, 20);
end

-- Recipe.OnGiveXP.MetalWelding25 defined in vanilla

-- Recipe.OnGiveXP.WoodWork5 defined in vanilla

function Recipe.OnGiveXP.Woodwork10(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 10);
end

function Recipe.OnGiveXP.Woodwork15(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 15);
end

function Recipe.OnGiveXP.Woodwork20(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 20);
end