require("Firearms_Silencer")

---@diagnostic disable-next-line: undefined-global
Events.OnPlayerAttackFinished.Remove(breakSuppressor)
function WF_breakSuppressor(playerObj, weapon)
    if not SandboxVars.Firearms.SuppressorBreak then return end
    if not playerObj or playerObj:isDead() then return end
    if not weapon then return end
    if not weapon:isRanged() then return end

    local canon = weapon:getCanon()
    if not canon then return end

    local impSil = string.find(canon:getType(), "ImprovisedSilencer")
    local bottleSil = string.find(canon:getType(), "Silencer_PopBottle")
    if not impSil and not bottleSil then return end

    local modData = canon:getModData()
    if modData.WF_SuppressorShotsFired == nil then
        modData.WF_SuppressorShotsFired = 0
    end
    modData.WF_SuppressorShotsFired = modData.WF_SuppressorShotsFired + 1

    if getDebug() then
        print("Suppressor shots fired: " .. modData.WF_SuppressorShotsFired)
    end

    if (impSil and modData.WF_SuppressorShotsFired > 230) or
       (bottleSil and modData.WF_SuppressorShotsFired > 35) then
        local square = playerObj:getCurrentSquare()
        getSoundManager():PlayWorldSoundImpl("BreakMetalItem", false, square:getX(), square:getY(), square:getZ(), 0, 5, 1, false);
        weapon:detachWeaponPart(canon)
        square:AddWorldInventoryItem(canon:getType() .. "_Broken", 0.0, 0.0, 0.0)
        playerObj:resetEquippedHandsModels()
        local scriptItem = weapon:getScriptItem()
        weapon:setSoundVolume(scriptItem:getSoundVolume())
        weapon:setSoundRadius(scriptItem:getSoundRadius())
        weapon:setSwingSound(scriptItem:getSwingSound())
    end
end
Events.OnPlayerAttackFinished.Add(WF_breakSuppressor)
