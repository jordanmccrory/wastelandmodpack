module Base
{
	item 22LRBulletsMold
	{
		DisplayCategory = Ammo,
		Weight = 0.5,
		Type = Normal,
		DisplayName	= .22 Bullets Mold,
		Icon = BulletMold,
		MetalValue = 15,
		WorldStaticModel = ShotGunShellsMold_Ground,
	}

	item 3006BulletsMold
    {
        DisplayCategory = Ammo,
        Weight = 0.5,
        Type = Normal,
        DisplayName	= .30-06 Bullets Mold,
        Icon = BulletMold,
        MetalValue = 15,
        WorldStaticModel = ShotGunShellsMold_Ground,
    }

	item 38SpecialBulletsMold
    {
        DisplayCategory = Ammo,
        Weight = 0.5,
        Type = Normal,
        DisplayName	= .38 Special Bullets Mold,
        Icon = BulletMold,
        MetalValue = 15,
        WorldStaticModel = ShotGunShellsMold_Ground,
    }

	item 44MagnumBulletsMold
    {
        DisplayCategory = Ammo,
        Weight = 0.5,
        Type = Normal,
        DisplayName	= .44 Magnum Bullets Mold,
        Icon = BulletMold,
        MetalValue = 15,
        WorldStaticModel = ShotGunShellsMold_Ground,
    }

	item 4440WCFBulletsMold
	{
		DisplayCategory = Ammo,
		Weight = 0.5,
		Type = Normal,
		DisplayName	= .44-40 WCF Bullets Mold,
		Icon = BulletMold,
		MetalValue = 15,
		WorldStaticModel = ShotGunShellsMold_Ground,
	}

	item 45ACPBulletsMold
	{
		DisplayCategory = Ammo,
		Weight = 0.5,
		Type = Normal,
		DisplayName	= .45 ACP Bullets Mold,
		Icon = BulletMold,
		MetalValue = 15,
		WorldStaticModel = ShotGunShellsMold_Ground,
	}

	item 762x49mmBulletsMold
	{
	    DisplayCategory = Ammo,
	    Weight = 0.5,
	    Type = Normal,
	    DisplayName	= 7.62x39mm Bullets Mold,
	    Icon = BulletMold,
	    MetalValue = 15,
	    WorldStaticModel = ShotGunShellsMold_Ground,
	}

}