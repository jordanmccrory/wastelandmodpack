require "MF_ISMoodle"
MF.createMoodle("Bloodlust")

-- The variables below imply the following:
-- You need to kill 40 zombies to satisfy full bloodlust.
-- Takes 2 hours before your bloodlust starts to affect you.
-- Takes 4 hours to reach full bloodlust.
-- You gain unhappyness at a rate of 5 per minute at full bloodlust.
-- You lose 5 unhappyness per zombie killed.
local pointsPerZombie = 3
local pointsPerMinute = 1
local pointsBeforeUnhappyness = 120
local pointsMaxUnhappyness = 240
local pointsMaxOverall = 240
local maxUnhappynessPerMinute = 5
local happynessPerZombieKilled = 5

BloodLustDebug = false

local function debugPrint(message)
    if BloodLustDebug then
        print(message)
    end
end

local function doHappynessKills(player, zombiesKilled)
    player:getBodyDamage():setUnhappynessLevel(math.max(0, player:getBodyDamage():getUnhappynessLevel() - zombiesKilled * happynessPerZombieKilled))
end

local function getUnhappiness(currentPoints)
    if currentPoints < pointsBeforeUnhappyness then
        return 0
    end
    local ratio = ((currentPoints - pointsBeforeUnhappyness) / (pointsMaxUnhappyness - pointsBeforeUnhappyness))
    return math.max(0, math.min(maxUnhappynessPerMinute, ratio * maxUnhappynessPerMinute))
end

local function BloodLustCheck()
    local player = getPlayer()
    if player and player:HasTrait("Bloodlust") then
        if SandboxVars.WastelandProfessions.DisableBloodlust then
            MF.getMoodle("Bloodlust").disable = true
            return
        else
            MF.getMoodle("Bloodlust").disable = false
        end

        local modData = player:getModData()

        if not modData.WP_Bloodlust_LastUpdate then
            modData.WP_Bloodlust_LastUpdate = getTimestamp()
            return
        end

        local lastUpdate = modData.WP_Bloodlust_LastUpdate
        local now = getTimestamp()
        local ratio = (now - lastUpdate) / 60
        modData.WP_Bloodlust_LastUpdate = now

        local bloodlustPoints = modData.WP_Bloodlust_Points or 0
        local lastZombieKillCount = modData.WP_Bloodlust_LastCount or 0
        local currentZombieKillCount = player:getZombieKills()

        if lastZombieKillCount ~= currentZombieKillCount then
            local zombiesKilled = currentZombieKillCount - lastZombieKillCount
            modData.WP_Bloodlust_LastCount = currentZombieKillCount
            bloodlustPoints = math.max(0, bloodlustPoints - zombiesKilled * pointsPerZombie)
            doHappynessKills(player, zombiesKilled)
        end

        bloodlustPoints = math.min(pointsMaxOverall, bloodlustPoints + (pointsPerMinute*ratio))
        debugPrint("Bloodlust points: " .. bloodlustPoints)
        if player:isGodMod() then
            bloodlustPoints = 0
            return
        end
        modData.WP_Bloodlust_Points = bloodlustPoints

        MF.getMoodle("Bloodlust"):setValue(1.0 - math.min(1.0, bloodlustPoints / pointsMaxUnhappyness))

        local amountToRaise = getUnhappiness(bloodlustPoints) * ratio
        if amountToRaise > 0 then
            debugPrint("Raising unhappyness by " .. amountToRaise)
            player:getBodyDamage():setUnhappynessLevel(player:getBodyDamage():getUnhappynessLevel() + amountToRaise)
        end
    end
end

local function SetBloodlustThresholds(playerIndex, player)
    MF.getMoodle("Bloodlust"):setThresholds(0.125,0.25,0.375,0.5,0.5,0.625,0.75,0.875)
    if player then
        player:getModData().WP_Bloodlust_LastUpdate = getTimestamp()
    end
end

Events.EveryOneMinute.Add(BloodLustCheck)
Events.OnCreatePlayer.Add(SetBloodlustThresholds)
