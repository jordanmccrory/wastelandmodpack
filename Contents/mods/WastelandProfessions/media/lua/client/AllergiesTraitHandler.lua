-- For hotreloading
if WP_Allergies then
    Events.EveryOneMinute.Remove(WP_Allergies.CheckPlayer)
    Events.OnTick.Remove(WP_Allergies.CheckSneezeStop)
    Events.OnTick.Remove(WP_Allergies.SneezeDelay)
    local player = getPlayer()
    if player then
        player:getModData().WP_Allergies_IsSneezing = false
    end
else
    require "MF_ISMoodle"
    MF.createMoodle("Allergies")
end

WP_Allergies = WP_Allergies or {}

AllergiesDebug = false

local function debugPrint(message)
    if AllergiesDebug then
        print(message)
    end
end

-- forageSkills.Allergies = {
--     name = "Allergies",
--     type = "trait",
--     visionBonus = -2.0,
--     weatherEffect = 0,
--     darknessEffect = 0,
--     specialisations = {},
-- }

-- Should be called every minute
-- Updates the player's allergens, allergies moodle, and unhappyness
-- Checks if the player should start sneezing
function WP_Allergies.CheckPlayer()
    local player = getPlayer()
    if not player or not player:HasTrait("Allergies") then
        return
    end

    local minutesSinceLastCheck = WP_Allergies.GetMinutesSinceLastCheck(player)

    local delta = minutesSinceLastCheck * SandboxVars.WastelandProfessions.AllergiesRate
    WP_Allergies.UpdateAllergens(player, delta)
    WP_Allergies.UpdateAllergiesMoodle(player)
    WP_Allergies.UpdateUnhappyness(player, delta)
    WP_Allergies.CheckStartSneezing(player)
end

function WP_Allergies.GetMinutesSinceLastCheck(player)
    local modData = player:getModData()
    if not modData.WP_Allergies_LastApplied then
        modData.WP_Allergies_LastApplied = getTimestamp()
    end
    local now = getTimestamp()
    modData.WP_Allergies_LastApplied = math.min(modData.WP_Allergies_LastApplied, now)
    return (now - modData.WP_Allergies_LastApplied) / 60
end

-- Updates the player's allergens
-- God Mode clears allergens
-- If over 50 allergens, you gain or lose slower because your already congested
-- Allergens are clamped between 0 and 100
function WP_Allergies.UpdateAllergens(player, delta)
    local modData = player:getModData()

    if player:isGodMod() then
        modData.WP_Allergies_Allergens = 0
        return
    end

    local allergens = modData.WP_Allergies_Allergens or 0
    local allergenRate = WP_Allergies.GetAllergenRate(player)
    if allergenRate > 0 and allergens > 50 then
        allergenRate = allergenRate / 3 -- you gain slower when you're already over 50
    end

    allergenRate = allergenRate * delta

    modData.WP_Allergies_LastApplied = getTimestamp()
    allergens = math.min(math.max(allergens + allergenRate, 0), 100) -- Clamp between 0 and 100
    modData.WP_Allergies_Allergens = allergens
    debugPrint("Allergens: " .. allergens)
end

-- Determines how much allergens the player gains or loses
-- Returns a number between -5 and 3
-- If the player is in a vehicle, returns -1
-- Summer and Spring: gain when outside, 1 and 3 respectively
-- Autumn and Winter: gain when inside, 2 and 1 respectively
-- Otherwise, lose 3
function WP_Allergies.GetAllergenRate(player)
    if player:isSeatedInVehicle() then
        return -1
    end

    local season = getClimateManager():getSeasonName()
    if player:isOutside() then
        if season == "Spring" then
            return 3
        elseif season == "Early Summer" then
            return 2
        elseif season == "Late Summer" then
            return 1
        end
    else
        if season == "Autumn" then
            return 2
        elseif season == "Winter" then
            return 1
        end
    end
    return -5
end

function WP_Allergies.UpdateAllergiesMoodle(player)
    local moodle = MF.getMoodle("Allergies")
    local moodleValue = 1 - (player:getModData().WP_Allergies_Allergens / 100)
    moodle:setValue(moodleValue)
end

-- Updates the player's unhappyness
-- If over 50 allergens, gain unhappyness to a max of 50%
-- Unhappyness is gained at a rate of 0.01% at 51, and 0.5% at 100
function WP_Allergies.UpdateUnhappyness(player, delta)
    local modData = player:getModData()
    if modData.WP_Allergies_Allergens <= 50 then
        return
    end
    local currentUnhappyness = player:getBodyDamage():getUnhappynessLevel()
    if currentUnhappyness >= 50 then return end
    local unhappynessIncrease = ((modData.WP_Allergies_Allergens - 50) / 100) * delta
    player:getBodyDamage():setUnhappynessLevel(math.min(currentUnhappyness + unhappynessIncrease, 50))
end

-- Checks if the player should start sneezing
-- If the player is already sneezing, do nothing
-- If the player has less than 50 allergens, do nothing
-- If the player has more than 50 allergens, roll for chance to start sneezing
--    and then schedule a sneeze to happen in some ticks
function WP_Allergies.CheckStartSneezing(player)
    local modData = player:getModData()

    if modData.WP_Allergies_IsSneezing then
        return
    end

    if modData.WP_Allergies_Allergens < 50 then
        return
    end

    local chanceStartSneeze = WP_Allergies.GetChanceStartSneeze(modData.WP_Allergies_Allergens)
    if ZombRand(chanceStartSneeze) ~= 0 then
        return
    end

    modData.WP_Allergies_IsSneezing = true
    modData.WP_Allergies_SneezeStartDelay = ZombRand(0, 300)
    modData.WP_Allergies_SneezeLength = WP_Allergies.GetSneezeLength(modData.WP_Allergies_Allergens)

    Events.OnTick.Add(WP_Allergies.SneezeDelay)

    debugPrint(
        "Starting a Sneeze!" ..
        "\n - Allergens: " .. modData.WP_Allergies_Allergens ..
        "\n - Chance to start sneezing: " .. chanceStartSneeze ..
        "\n - Delay: " .. modData.WP_Allergies_SneezeStartDelay ..
        "\n - Length: " .. modData.WP_Allergies_SneezeLength
    )
end

-- Roll for chance to start sneezing
-- Example: 50 allergens = 1 in 17 chance to start sneezing
-- Example: 100 allergens = 1 in 5 chance to start sneezing
function WP_Allergies.GetChanceStartSneeze(allergens)
    return math.floor((70 - (allergens - 50)) / 4)
end

-- Amount in time in milliseconds to keep sneezing
function WP_Allergies.GetSneezeLength(allergens)
    -- will be a number between 0 and 3 at 100 allergens
    local ratio = ZombRand(math.floor((allergens - 50) / 16))

    if ratio == 0 then
        return ZombRand(1000, 3000)
    elseif ratio == 1 then
        return ZombRand(3000, 6000)
    elseif ratio == 2 then
        return ZombRand(6000, 10000)
    else
        return ZombRand(10000, 20000)
    end
end

-- Check to see if the player should start sneezing yet
function WP_Allergies.SneezeDelay()
    local player = getPlayer()
    local modData = player:getModData()

    if modData.WP_Allergies_SneezeStartDelay > 0 then
        modData.WP_Allergies_SneezeStartDelay = modData.WP_Allergies_SneezeStartDelay - 1
        return
    end

    Events.OnTick.Remove(WP_Allergies.SneezeDelay)
    modData.WP_Allergies_SneezeStartDelay = nil
    WP_Allergies.StartSneezing(player)
end

-- Start the sneezing
-- Schedule a check to stop sneezing
function WP_Allergies.StartSneezing(player)
    local modData = player:getModData()
    modData.WP_Allergies_SneezeStartTimestamp = getTimestampMs()
    player:getBodyDamage():setSneezeCoughActive(1)
    Events.OnTick.Add(WP_Allergies.CheckSneezeStop)

    debugPrint("Player is sneezing")
end

-- Check to see if the player should stop sneezing yet
function WP_Allergies.CheckSneezeStop()
    local player = getPlayer()
    local modData = player:getModData()

    if modData.WP_Allergies_SneezeStartTimestamp + modData.WP_Allergies_SneezeLength > getTimestampMs() then
        return
    end
    WP_Allergies.StopSneezing(player)
    Events.OnTick.Remove(WP_Allergies.CheckSneezeStop)
end

-- Stop the sneezing
function WP_Allergies.StopSneezing(player)
    player:getBodyDamage():setSneezeCoughActive(0)
    local modData = player:getModData()
    modData.WP_Allergies_SneezeStartTimestamp = nil
    modData.WP_Allergies_SneezeLength = nil
    modData.WP_Allergies_IsSneezing = false
    debugPrint("Player stopped sneezing")
end

Events.EveryOneMinute.Add(WP_Allergies.CheckPlayer)

local function SetAllergiesThresholds(playerIdx, player)
    MF.getMoodle("Allergies"):setThresholds(0.125,0.25,0.375,0.5,0.5,0.625,0.75,0.875)
    if player then
        player:getModData().WP_Allergies_IsSneezing = false
        player:getModData().WP_Allergies_LastApplied = getTimestamp()
    end
end
Events.OnCreatePlayer.Add(SetAllergiesThresholds)

Events.OnGameBoot.Add(function ()
    -- Redefine NasalSpray to quell the allergeries
    function OnEat_NasalSpray(food, character, percent)
        character:getModData().WP_Allergies_Allergens = math.max(character:getModData().WP_Allergies_Allergens - 50, 0)
        WP_Allergies.CheckPlayer()
    end
end)

