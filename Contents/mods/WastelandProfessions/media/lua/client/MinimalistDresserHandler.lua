WB_MiniDressHandler = {}

WB_MiniDressHandler.lastMs = nil
function WB_MiniDressHandler.CheckClothing()
    local player = getPlayer()
    if not player or not player:HasTrait("MinimalistDresser") then return end

    local tsms = getTimestampMs()
    if WB_MiniDressHandler.lastMs == nil then
        WB_MiniDressHandler.lastMs = tsms
        return
    end

    local diff = tsms - WB_MiniDressHandler.lastMs
    WB_MiniDressHandler.lastMs = tsms
    local ratio = diff / 1000
    local clothingCount = player:getWornItems():size()

    if clothingCount <= 3 then
        player:getBodyDamage():setUnhappynessLevel(math.max(player:getBodyDamage():getUnhappynessLevel() - 0.01 * ratio, 0))
    elseif clothingCount >= 7 then
        local amount = (clothingCount - 6) * ratio * 0.05
        player:getBodyDamage():setUnhappynessLevel(math.min(player:getBodyDamage():getUnhappynessLevel() + amount, 50))
    end
end

Events.OnTick.Add(WB_MiniDressHandler.CheckClothing)