WP_MotionSickness = {}
WP_MotionSickness.runningData = nil

function WP_MotionSickness.OnEnterVehicle(player)
    if not player or not player:HasTrait("MotionSickness") then
        return
    end

    local vehicle = player:getVehicle()
    if not vehicle then
        return
    end

    if not player:getVehicle():isDriver(player) then
        return
    end

    if not player:getModData().motionSickness then
        player:getModData().motionSickness = 0
    end

    WP_MotionSickness.runningData = {
        player = player,
        timestampMs = getTimestampMs(),
        previousSpeed = 0,
        previousAngle = vehicle:getAngleY()
    }

    Events.OnTick.Add(WP_MotionSickness.MonitorPlayerInVehicle)
end

function WP_MotionSickness.OnExitVehicle()
    Events.OnTick.Remove(WP_MotionSickness.MonitorPlayerInVehicle)
end


function WP_MotionSickness.StartMotionSickness()
    local player = getPlayer()
    player:getModData().lastMotionSicknessCheck = getTimestampMs()
    if not player:getModData().motionSickness then
        player:getModData().motionSickness = 0
    end
    if player:getModData().motionSickness <= 0 then
        return
    end

    WP_MotionSickness.isMonitoringPlayerSickness = true
    Events.OnTick.Add(WP_MotionSickness.MonitorPlayerSickness)

    local texture = getTexture("media/ui/MotionSicknessGuage.png")
    local x = getCore():getScreenWidth()/2 - texture:getWidth()/2
    local y = getCore():getScreenHeight() - texture:getHeight()
    local nx = texture:getWidth() / 2
    local ny = texture:getHeight() - 1
    local minAngle = 180
    local maxAngle = 0
    WP_MotionSickness.GuageUI = ISVehicleGauge:new(x, y, texture, nx, ny, minAngle, maxAngle)
    WP_MotionSickness.GuageUI:initialise()
    WP_MotionSickness.GuageUI:instantiate()
    WP_MotionSickness.GuageUI:addToUIManager()
end

function WP_MotionSickness.StopMotionSickness()
    WP_MotionSickness.isMonitoringPlayerSickness = false
    Events.OnTick.Remove(WP_MotionSickness.MonitorPlayerSickness)
    WP_MotionSickness.GuageUI:removeFromUIManager()
    WP_MotionSickness.GuageUI = nil
end

function WP_MotionSickness.MonitorPlayerSickness()
    local player = getPlayer()
    local modData = player:getModData()
    if player:isGodMod() then modData.motionSickness = 0 end
    if modData.motionSickness <= 0 then
        getPlayer():getModData().originalFoodsickness = 0
        WP_MotionSickness.StopMotionSickness()
        return
    end
    local currentTimeMs = getTimestampMs()
    local timeDiff = (currentTimeMs - modData.lastMotionSicknessCheck)/1000
    modData.motionSickness = modData.motionSickness - timeDiff

    if modData.motionSickness < 0 then
        modData.motionSickness = 0
        return
    end

    if player:getVehicle() and player:getVehicle():isDriver(player) then
        local db = getPlayerVehicleDashboard(player:getPlayerNum())
        WP_MotionSickness.GuageUI:setY(getCore():getScreenHeight() - WP_MotionSickness.GuageUI.texture:getHeight() - db:getHeight() + 5)
    else
        local hb = getPlayerHotbar(player:getPlayerNum())
        WP_MotionSickness.GuageUI:setY(getCore():getScreenHeight() - WP_MotionSickness.GuageUI.texture:getHeight() - hb:getHeight())
    end

    WP_MotionSickness.GuageUI:setValue(modData.motionSickness / 100)

    local currentFoodSickness = player:getBodyDamage():getFoodSicknessLevel()
    if currentFoodSickness >= modData.originalFoodsickness then
        player:getBodyDamage():setFoodSicknessLevel(math.max(modData.originalFoodsickness, modData.motionSickness))
    end
    modData.lastMotionSicknessCheck = currentTimeMs
end

WP_MotionSickness.timeDelay = 100
function WP_MotionSickness.MonitorPlayerInVehicle()
    local currentTimeMs = getTimestampMs()
    local timeDiff = currentTimeMs - WP_MotionSickness.runningData.timestampMs
    if timeDiff <= WP_MotionSickness.timeDelay then
        return
    end
    WP_MotionSickness.runningData.timestampMs = currentTimeMs

    local player = WP_MotionSickness.runningData.player
    local vehicle = player:getVehicle()
    if not vehicle then
        WP_MotionSickness.OnExitVehicle()
        return
    end

    if not player:getVehicle():isDriver(player) then
        return
    end

    local previousSpeed = WP_MotionSickness.runningData.previousSpeed
    local previousAngle = WP_MotionSickness.runningData.previousAngle
    local speed = math.abs(vehicle:getCurrentSpeedKmHour())
    local angle = vehicle:getAngleY()
    WP_MotionSickness.runningData.previousSpeed = speed
    WP_MotionSickness.runningData.previousAngle = angle

    -- we dont care about deceleration, its too hard to deal with
    local acceleration = 0
    if speed > 0 then
        acceleration = math.floor(((speed - previousSpeed) / timeDiff)*10000)
    end

    if acceleration < 0 then
        acceleration = acceleration / -2 -- make it less severe
    end

    local spin = math.abs(math.floor(((angle - previousAngle) / timeDiff)*100))* speed

    local timeScale = timeDiff/1000

    local gForce = (acceleration + spin)*timeScale
    local sickness = (gForce - (80 * timeScale)) / (140*timeScale)

    if sickness > 10 then
        sickness = 10
    end

    local modData = player:getModData()
    if sickness > 0 then
        modData.motionSickness = math.min(modData.motionSickness + sickness, 100)
    end
    if player:isGodMod() then
        modData.motionSickness = 0
    end

    if modData.motionSickness > 0 and not WP_MotionSickness.isMonitoringPlayerSickness then
        if not player:getModData().originalFoodsickness then
            player:getModData().originalFoodsickness = player:getBodyDamage():getFoodSicknessLevel()
        end
        WP_MotionSickness.StartMotionSickness()
    end
end

Events.OnEnterVehicle.Add(WP_MotionSickness.OnEnterVehicle)
Events.OnExitVehicle.Add(WP_MotionSickness.OnExitVehicle)
Events.OnCreatePlayer.Add(WP_MotionSickness.StartMotionSickness)
