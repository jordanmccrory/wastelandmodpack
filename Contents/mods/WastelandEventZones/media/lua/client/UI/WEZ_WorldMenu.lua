if not isClient() then return end

require "WEZ_EventZone"
require "UI/WEZ_CreateZone"
require "UI/WEZ_ManageZone"
require "UI/WEZ_ListZones"
require "WL_ContextMenuUtils"
require "WL_Utils"

local WEZ_WorldMenu = {}

WEZ_WorldMenu.doMenu = function(playerIdx, context)
    if not isClient() or WL_Utils.canModerate(getPlayer()) then
        local player = getPlayer(playerIdx)
        local x, y = ISCoordConversion.ToWorld(getMouseXScaled(), getMouseYScaled(), player:getZ())
        local zones = WEZ_EventZone.getZonesAt(x, y, player:getZ())

        for i=1,#zones do
            local zone = zones[i]
            context:addOption("Manage: " .. zone.name, zone, WEZ_WorldMenu.manageZone)
        end

        local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Zones")
        submenu:addOption("List Event Zones" , nil, WEZ_WorldMenu.listZones)
        submenu:addOption("New Event Zone", nil, WEZ_WorldMenu.createZone)
    end
end

function WEZ_WorldMenu.listZones()
    WEZ_ListZones:show()
end

function WEZ_WorldMenu.createZone()
    WEZ_CreateZone:show()
end

--- @param zone WEZ_EventZone
function WEZ_WorldMenu.manageZone(zone)
    WEZ_ManageZone:show(zone)
end

Events.OnFillWorldObjectContextMenu.Add(WEZ_WorldMenu.doMenu)
