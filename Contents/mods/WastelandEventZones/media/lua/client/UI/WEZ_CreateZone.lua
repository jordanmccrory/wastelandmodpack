if not isClient() then return end

require "GravyUI"
require "GroundHightlighter"
require "ISUI/ISPanel"
require "UI/WEZ_ManageZone"
require "UI/WEZ_ListZones"

WEZ_CreateZone = ISPanel:derive("WEZ_CreateZone")
WEZ_CreateZone.instance = nil

function WEZ_CreateZone:show()
    if WEZ_ManageZone.instance then
        WEZ_ManageZone.instance:onClose()
    end
    if WEZ_CreateZone.instance then
        WEZ_CreateZone.instance:onClose()
    end
    if WEZ_ListZones.instance then
        WEZ_ListZones.instance:onClose()
    end

    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local w = 200 * scale
    local h = 160 * scale
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    setmetatable(o, self)
    o.__index = self
    o:initialise()
    o:addToUIManager()
    WEZ_CreateZone.instance = o
    return o
end

function WEZ_CreateZone:initialise()
    ISPanel.initialise(self)
    self.player = getPlayer()
    self.startX = 0
    self.startY = 0
    self.startZ = 0
    self.endX = 0
    self.endY = 0
    self.endZ = 0
    self.fullHeight = true
    self.moveWithMouse = true
    self.groundHighlighter = GroundHightlighter:new()
    self.groundHighlighter:enableXray(true)
    self.lastHighlight = {x1 = 0, y1 = 0, x2 = 0, y2 = 0, z = -1}

    local win = GravyUI.Node(self.width, self.height):pad(5)
    local header, body, footer = win:rows({30, 1, 50}, 5)
    local header, headerRight = header:cols({1, 100}, 5)
    local nameInput, fullHeight, startPoint, endPoint = body:grid(4, {0.5, 0.5}, 5, 5)
    local fr1, fr2 = footer:grid(2, 2, 5, 5)

    self.headerBox = header

    self.showHighlightCheckbox = headerRight:makeTickBox()
    self.nameLabel = nameInput[1]
    self.nameInput = nameInput[2]:makeTextBox("")
    self.fullHeightLabel = fullHeight[1]
    self.fullHeightInput = fullHeight[2]:makeTickBox()
    self.startPointLabel = startPoint[1]
    self.startPointDisplay = startPoint[2]
    self.endPointLabel = endPoint[1]
    self.endPointDisplay = endPoint[2]

    self.setStartPointButton = fr1[1]:makeButton("Set Start Point", self, self.onSetStartPoint)
    self.setEndPointButton = fr2[1]:makeButton("Set End Point", self, self.onSetEndPoint)
    self.createZoneButton = fr1[2]:makeButton("Create Zone", self, self.onCreateZone)
    self.closeButton = fr2[2]:makeButton("Close", self, self.onClose)


    self:addChild(self.showHighlightCheckbox)
    self:addChild(self.nameInput)
    self:addChild(self.fullHeightInput)
    self:addChild(self.setStartPointButton)
    self:addChild(self.setEndPointButton)
    self:addChild(self.createZoneButton)
    self:addChild(self.closeButton)

    self.showHighlightCheckbox:addOption("Highlight?")
    self.showHighlightCheckbox:setSelected(1, true)

    self.fullHeightInput:addOption("")
    self.fullHeightInput:setSelected(1, true)
end

function WEZ_CreateZone:prerender()
    ISPanel.prerender(self)

    if self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type == "none" then
        self:showHightlight()
    elseif not self.showHighlightCheckbox:isSelected(1) and self.groundHighlighter.type ~= "none" then
        self.groundHighlighter:remove()
    end

    if self.showHighlightCheckbox:isSelected(1) and self.player:getZ() ~= self.lastHighlight.z then
        self:showHightlight()
    end

    if self.fullHeightInput:isSelected(1) then
        self.fullHeight = true
    else
        self.fullHeight = false
    end

    self:drawTextCentre("Event Zones", self.headerBox.left + (self.headerBox.width/2), self.headerBox.top, 1, 1, 1, 1, UIFont.Medium)

    self:drawTextRight("Zone Name:", self.nameLabel.right, self.nameLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawTextRight("Full Height:", self.fullHeightLabel.right, self.fullHeightLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawTextRight("Start Point:", self.startPointLabel.right, self.startPointLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawTextRight("End Point:", self.endPointLabel.right, self.endPointLabel.top, 1, 1, 1, 1, UIFont.Small)

    self:drawText(self:getStartPointString(), self.startPointDisplay.left, self.startPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText(self:getEndPointString(), self.endPointDisplay.left, self.endPointDisplay.top, 1, 1, 1, 1, UIFont.Small)
end

function WEZ_CreateZone:onSetStartPoint()
    self.startX = self.player:getX()
    self.startY = self.player:getY()
    self.startZ = self.player:getZ()
    self:showHightlight()
end

function WEZ_CreateZone:onSetEndPoint()
    self.endX = self.player:getX()
    self.endY = self.player:getY()
    self.endZ = self.player:getZ()
    self:showHightlight()
end

function WEZ_CreateZone:onCreateZone()
    if self.startX == 0 or self.startY == 0 then return end
    if self.endX == 0 or self.endY == 0 then return end
    if self.nameInput:getText() == "" then return end
    if self.fullHeight then
        self.startZ = 0
        self.endZ = 7
    end
    local newZone = WEZ_EventZone:new(self.nameInput:getText(), self.startX, self.startY, self.startZ, self.endX, self.endY, self.endZ)
    self.startX = 0
    self.startY = 0
    self.startZ = 0
    self.endX = 0
    self.endY = 0
    self.endZ = 0
    WEZ_ManageZone:show(newZone)
    self:onClose()
end

function WEZ_CreateZone:onClose()
    WEZ_CreateZone.instance = nil
    self:removeFromUIManager()
end

function WEZ_CreateZone:removeFromUIManager()
    self.groundHighlighter:remove()
    ISPanel.removeFromUIManager(self)
end

function WEZ_CreateZone:getStartPointString()
    if self.fullHeight then
        z = 0
    else
        z = self.startZ
    end
    return string.format("(%d, %d, %d)", self.startX, self.startY, z)
end

function WEZ_CreateZone:getEndPointString()
    if self.fullHeight then
        z = 7
    else
        z = self.endZ
    end
    return string.format("(%d, %d, %d)", self.endX, self.endY, z)
end

function WEZ_CreateZone:showHightlight()
    if self.startX == 0 or self.startY == 0 then return end
    if not self.showHighlightCheckbox:isSelected(1) then return end
    local sx, sy = self.startX, self.startY
    local ex, ey
    if self.endX > 0 and self.endY > 0 then
        ex, ey = self.endX, self.endY
    else
        ex, ey = self.player:getX(), self.player:getY()
    end
    z = self.player:getZ()
    local minZ, maxZ;
    if self.fullHeight then
        minZ = 0
        maxZ = 7
    else
        minZ = math.min(self.startZ, self.endZ)
        maxZ = math.max(self.startZ, self.endZ)
    end
    sx,ex = math.floor(math.min(sx, ex)), math.floor(math.max(sx, ex))
    sy,ey = math.floor(math.min(sy, ey)), math.floor(math.max(sy, ey))

    if z < minZ or z > maxZ then
        return
    end

    if self.lastHighlight.x1 == sx and self.lastHighlight.y1 == sy and self.lastHighlight.x2 == ex and self.lastHighlight.y2 == ey and z == self.lastHighlight.z then
        return
    end

    self.lastHighlight.x1 = sx
    self.lastHighlight.y1 = sy
    self.lastHighlight.x2 = ex
    self.lastHighlight.y2 = ey
    self.lastHighlight.z = z
    self.groundHighlighter:highlightSquare(sx, sy, ex, ey, z)
end