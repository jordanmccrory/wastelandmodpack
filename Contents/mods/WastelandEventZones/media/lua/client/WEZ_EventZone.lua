if not isClient() then return end

require "WL_Zone"
-- TODO: Add required clothing for a zone

--- @class WEZ_EventZone
WEZ_EventZone = WEZ_EventZone or WL_Zone:derive("WEZ_EventZone")
--- @type WEZ_EventZone[]
WEZ_EventZones = {}

function WEZ_EventZone.getZonesAt(x, y, z)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInZone(x, y, z) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getWarningZonesAt(x, y)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInWarningZone(x, y) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getNoCarZonesAt(x, y)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone.noCars and zone:isInZone(x, y, 0) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getZone(zoneId)
    return WEZ_EventZones[zoneId]
end

function WEZ_EventZone.dump()
    print(WEZ_tabledump(WEZ_EventZones))
end

function WEZ_EventZone:new(name, x1, y1, z1, x2, y2, z2)
    --- @type WEZ_EventZone
    local o = WEZ_EventZone.parentClass.new(self, x1, y1, z1, x2, y2, z2)  -- call inherited constructor
    o:init()
    o.name = name
    o:save()
    return o
end

function WEZ_EventZone:loadFrom(data)
    local o = WEZ_EventZone.parentClass.new(self, data.minX, data.minY, data.minZ, data.maxX, data.maxY, data.maxZ)
    o:init()

    for key, value in pairs(data) do
        o[key] = value
    end

    return o
end

function WEZ_EventZone:init()
    --- @type string
    self.id = getRandomUUID()
    self.name = ""
    self.isAdminOnly = false
    self.isJail = false
    self.teleportX = 0
    self.teleportY = 0
    self.teleportZ = 0
    self.damageRate = 0
    self.damagePreventItems = ""
    self.preventZombies = false
    self.minCharecterAgeDays = 0
    self.maxCharecterVisits = 0
    self.minTimeBetweenVisitsHours = 0
    self.isRpZone = false
    self.playerVisits = {}
    self.percentageSprinters = 0
    self.percentageFastShamblers = 0
    self.percentageSlowShamblers = 0
    self.warningBuffer = 0
    self.warningMessage = ""
    self.noDamage = false
    self.noCars = false
    self.carTime = 1440
    self.mapType = "Event Zone"
    self.mapColor = {0.3, 0.8, 0.8}
end

function WEZ_EventZone:save()
    sendClientCommand(getPlayer(), "WastelandEventZones", "SetZone", {
        id = self.id,
        name = self.name,
        minX = self.minX,
        minY = self.minY,
        maxX = self.maxX,
        maxY = self.maxY,
        minZ = self.minZ,
        maxZ = self.maxZ,
        isAdminOnly = self.isAdminOnly,
        isJail = self.isJail,
        teleportX = self.teleportX,
        teleportY = self.teleportY,
        teleportZ = self.teleportZ,
        damageRate = self.damageRate,
        damagePreventItems = self.damagePreventItems,
        preventZombies = self.preventZombies,
        minCharecterAgeDays = self.minCharecterAgeDays,
        maxCharecterVisits = self.maxCharecterVisits,
        minTimeBetweenVisitsHours = self.minTimeBetweenVisitsHours,
        isRpZone = self.isRpZone,
        playerVisits = self.playerVisits,
        percentageSprinters = self.percentageSprinters,
        percentageFastShamblers = self.percentageFastShamblers,
        percentageSlowShamblers = self.percentageSlowShamblers,
        warningBuffer = self.warningBuffer,
        warningMessage = self.warningMessage,
        noDamage = self.noDamage,
        noCars = self.noCars,
        carTime = self.carTime
    });
end

function WEZ_EventZone:getMapName()
    return self.name or "Unamed Event Zone"
end

function WEZ_EventZone:delete()
    sendClientCommand(getPlayer(), "WastelandEventZones", "DeleteZone", {id = self.id})
end

function WEZ_EventZone:isInWarningZone(x, y)
    if self.warningBuffer == 0 then
        return false
    end
    if x >= (self.minX-self.warningBuffer) and x <= (self.maxX+1+self.warningBuffer) and y >= (self.minY-self.warningBuffer) and y <= (self.maxY+1+self.warningBuffer) then
        return true
    end
    return false
end

function WEZ_EventZone:isPlayerInWarningZone(player)
    return self:isInWarningZone(player:getX(), player:getY(), player:getZ())
end

function WEZ_EventZone:setIsTeleport(isTeleport, x, y, z)
    self.isTeleport = isTeleport
    self.teleportX = x
    self.teleportY = y
    self.teleportZ = z
end

function WEZ_EventZone:setPreventZombies(preventZombies)
    self.preventZombies = preventZombies
end

function WEZ_EventZone:setMinCharecterAgeDays(minCharecterAgeDays)
    self.minCharecterAgeDays = minCharecterAgeDays
end

function WEZ_EventZone:setMaxCharecterVisits(maxCharecterVisits)
    self.maxCharecterVisits = maxCharecterVisits
end

function WEZ_EventZone:setMinTimeBetweenVisitsHours(minTimeBetweenVisitsHours)
    self.minTimeBetweenVisitsHours = minTimeBetweenVisitsHours
end

function WEZ_EventZone:initPlayer(player)
    local username = player:getUsername()
    if not self.playerVisits then self.playerVisits = {} end
    if not self.playerVisits[username] then self.playerVisits[username] = {} end
    if not self.playerVisits[username].count then self.playerVisits[username].count = 0 end
    if not self.playerVisits[username].lastVisit then self.playerVisits[username].lastVisit = 0 end
end

function WEZ_EventZone:isPlayerAllowed(player)
    self:initPlayer(player)
    local username = player:getUsername()

    if self.isAdminOnly and not player:isAccessLevel("admin") then
        return false, "You must be an admin to enter this zone."
    end
    if self.minCharecterAgeDays > 0 and player:getHoursSurvived() < (self.minCharecterAgeDays * 24) then
        return false, "You must be at least " .. self.minCharecterAgeDays .. " days old to enter this zone."
    end
    if self.maxCharecterVisits > 0 and self.playerVisits[username].count >= self.maxCharecterVisits then
        return false, "You have already visited this zone " .. self.maxCharecterVisits .. " times."
    end
    if self.minTimeBetweenVisitsHours > 0 and self.playerVisits[username].lastVisit + self.minTimeBetweenVisitsHours > getGameTime():getWorldAgeHours() then
        return false, "You must wait " .. self.minTimeBetweenVisitsHours .. " hours before visiting this zone again."
    end

    return true
end

function WEZ_EventZone:onPlayerVisit(player)
    self:initPlayer(player)

    local username = player:getUsername()
    self.playerVisits[username].count = self.playerVisits[username].count + 1
    self.playerVisits[username].lastVisit = getGameTime():getWorldAgeHours()
    self:save()
end
