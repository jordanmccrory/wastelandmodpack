if not isClient() then return end

require "WL_Utils"

WEZ_MonitorPlayer = {}
WEZ_MonitorPlayer.checkTimeout = 0
WEZ_MonitorPlayer.checkInterval = 20
WEZ_MonitorPlayer.zonesIn = {}
WEZ_MonitorPlayer.zonesWarned = {}
WEZ_MonitorPlayer.noCarWarned = {}
WEZ_MonitorPlayer.jailZoneId = false
WEZ_MonitorPlayer.stillInJail = false
WEZ_MonitorPlayer.cancelRun = false
WEZ_MonitorPlayer.healthInfoOnEnter = {}

function WEZ_MonitorPlayer.Check()
    if WEZ_MonitorPlayer.checkTimeout > 0 then
        WEZ_MonitorPlayer.checkTimeout = WEZ_MonitorPlayer.checkTimeout - 1
        return
    end
    WEZ_MonitorPlayer.checkTimeout = WEZ_MonitorPlayer.checkInterval

    local currentlyInZones = {}
    local currentlyWarnedZones = {}
    local currentlyCarWarnedZones = {}
    local player = getPlayer()
    if player then
        if player:isGodMod() then return end
        WEZ_MonitorPlayer.stillInJail = false
        WEZ_MonitorPlayer.cancelRun = false
        local x, y, z = player:getX(), player:getY(), player:getZ()
        local zones = WEZ_EventZone.getZonesAt(x, y, z)
        for _, zone in pairs(zones) do
            currentlyInZones[zone.id] = true
            if not WEZ_MonitorPlayer.cancelRun then
                WEZ_MonitorPlayer.CheckZone(player, zone)
            end
        end
        WEZ_MonitorPlayer.CheckTPBackToJail(player)
        local warnedZones = WEZ_EventZone.getWarningZonesAt(x, y)
        for _, zone in pairs(warnedZones) do
            if not WEZ_MonitorPlayer.zonesWarned[zone.id] then
                WEZ_MonitorPlayer.zonesWarned[zone.id] = true
                WEZ_MonitorPlayer.showWarning(player, zone)
            end
            currentlyWarnedZones[zone.id] = true
        end

        if WEZ_MonitorPlayer.isInCar(player) then
            local noCarWarnedZones = WEZ_EventZone.getNoCarZonesAt(x, y)
            for _, zone in pairs(noCarWarnedZones) do
                if not WEZ_MonitorPlayer.noCarWarned[zone.id] then
                    WEZ_MonitorPlayer.noCarWarned[zone.id] = true
                    WEZ_MonitorPlayer.showNoCarWarning(player, zone)
                end
                currentlyCarWarnedZones[zone.id] = true
            end
        end
    end

    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesIn) do
        if not currentlyInZones[zoneId] then
            WEZ_MonitorPlayer.zonesIn[zoneId] = false
        end
    end
    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesWarned) do
        if not currentlyWarnedZones[zoneId] then
            WEZ_MonitorPlayer.zonesWarned[zoneId] = false
        end
    end
    for zoneId, _ in pairs(WEZ_MonitorPlayer.noCarWarned) do
        if not currentlyCarWarnedZones[zoneId] then
            WEZ_MonitorPlayer.noCarWarned[zoneId] = false
        end
    end
end

function WEZ_MonitorPlayer.CheckZone(player, zone)
    if WEZ_MonitorPlayer.CheckBoot(player, zone) then
        return
    end

    if not WEZ_MonitorPlayer.zonesIn[zone.id] then
        WEZ_MonitorPlayer.zonesIn[zone.id] = true
        if zone.noDamage then
            WEZ_MonitorPlayer.CheckAndTagHealth(player)
        end
        zone:onPlayerVisit(player)
    end

    if zone.noDamage and WEZ_MonitorPlayer.healthInfoOnEnter then
        WEZ_MonitorPlayer.EnsureGoodHealth(player)
    end
    WEZ_MonitorPlayer.CheckJail(player, zone)
    WEZ_MonitorPlayer.CheckTeleport(player, zone)
    WEZ_MonitorPlayer.CheckDamage(player, zone)
    WEZ_MonitorPlayer.CheckRP(player, zone)
end

function WEZ_MonitorPlayer.CheckAndTagHealth(player)
    local bodyDamage = player:getBodyDamage()
    local bodyParts = bodyDamage:getBodyParts()
    local hasInjury = false

    for i=0, bodyParts:size()-1 do
        local bodyPart = bodyParts:get(i)
        if bodyPart:HasInjury() then
            hasInjury = true
            break
        end
    end
    if hasInjury then
        player:setHaloNote("Injured, Event zone will not protect!!!", 255, 0, 0, 60.0)
        WEZ_MonitorPlayer.wasHealthyOnEnter = false
    else
        player:setHaloNote("No Damage Zone", 0, 255, 0, 60.0)
        WEZ_MonitorPlayer.wasHealthyOnEnter = true
    end
end

function WEZ_MonitorPlayer.EnsureGoodHealth(player)
    local bodyDamage = player:getBodyDamage()
    bodyDamage:setOverallBodyHealth(100)
    local bodyParts = bodyDamage:getBodyParts()
    for i=0, bodyParts:size()-1 do
        local bodyPart = bodyParts:get(i)
        bodyPart:SetHealth(100)
        bodyPart:setBurnTime(0)
        bodyPart:SetBitten(false)
        bodyPart:setBleedingTime(0)
        bodyPart:setScratched(false, true)
        bodyPart:setScratchTime(0)
        bodyPart:setCut(false, true)
        bodyPart:setCutTime(0)
        bodyPart:setDeepWounded(false)
        bodyPart:setDeepWoundTime(0)
        bodyPart:setInfectedWound(false)
        bodyPart:SetInfected(false)
        bodyPart:setHaveBullet(false, 0)
        bodyPart:setHaveGlass(false)
        bodyPart:setFractureTime(0)
    end
end

function WEZ_MonitorPlayer.CheckTPBackToJail(player)
    if WEZ_MonitorPlayer.jailZoneId and not WEZ_MonitorPlayer.stillInJail then
        local targetZone = WEZ_EventZone.getZone(WEZ_MonitorPlayer.jailZoneId)
        if targetZone and targetZone.isJail then
            local x, y = targetZone:getClosestPointInsideZone(player:getX(), player:getY(), player:getZ())
            WL_Utils.teleportPlayerToCoords(player, x, y, 0)
        else
            WEZ_MonitorPlayer.jailZoneId = false
        end
    end
end

function WEZ_MonitorPlayer.CheckJail(player, zone)
    if zone.isJail then
        WEZ_MonitorPlayer.jailZoneId = zone.id
        WEZ_MonitorPlayer.stillInJail = true
    end
end

function WEZ_MonitorPlayer.CheckBoot(player, zone)
    local isPlayerAllowed, reason = zone:isPlayerAllowed(player)
    if not isPlayerAllowed then
        local x, y, z = player:getX(), player:getY(), player:getZ()
        local newX, newY, newZ = zone:getClosestPointOutsideZone(x, y, z)
        WEZ_MonitorPlayer.jailZoneId = false
        WEZ_MonitorPlayer.cancelRun = true
        WL_Utils.teleportPlayerToCoords(player, newX, newY, newZ)
        player:setHaloNote(reason, 255, 0, 0, 60.0)
        return true
    end
    return false
end

function WEZ_MonitorPlayer.CheckTeleport(player, zone)
    if zone.teleportX ~= 0 and zone.teleportY ~= 0 then
        if math.floor(zone.teleportX) == math.floor(player:getX()) and math.floor(zone.teleportY) == math.floor(player:getY()) then
            return
        end
        WEZ_MonitorPlayer.jailZoneId = false
        WEZ_MonitorPlayer.cancelRun = true
        WL_Utils.teleportPlayerToCoords(player, zone.teleportX, zone.teleportY, zone.teleportZ)
        player:setHaloNote("Whoosh", 0, 255, 0, 60.0)
    end
end

function WEZ_MonitorPlayer.CheckDamage(player, zone)
    if zone.damageRate > 0 then
        local items = {}
        for item in string.gmatch(zone.damagePreventItems, "([^;]+)") do
            table.insert(items, item)
        end
        local wornItems = WEZ_MonitorPlayer.getWornItems(player)
        for _, item in ipairs(items) do
            for _, wornItem in ipairs(wornItems) do
                if item == wornItem then
                    return
                end
            end
        end
        player:getBodyDamage():ReduceGeneralHealth(zone.damageRate/100)
    end
end

function WEZ_MonitorPlayer.getWornItems(player)
    local wornItems = {}
    local playerItems = player:getInventory():getItems()
    for i=0, playerItems:size()-1 do
        local item = playerItems:get(i)
        if player:isEquippedClothing(item) then
            table.insert(wornItems, item:getFullType())
        end
    end
    return wornItems
end

function WEZ_MonitorPlayer.isInCar(player)
    local vehicle = player:getVehicle()
    if vehicle and vehicle:getDriver() == player then
        return true
    end
    return false
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.CheckRP(player, zone)
    if zone.isRpZone then
        player:getStats():setHunger(0.0)
        player:getStats():setThirst(0.0)
        player:getStats():setFatigue(0.0)
        player:getStats():setThirst(0.0)
        player:getBodyDamage():setBoredomLevel(0)
        player:getBodyDamage():setUnhappynessLevel(0)
        player:getBodyDamage():getThermoregulator():reset()
    end
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showWarning(player, zone)
    if zone.warningMessage == "" then
        return
    end
    player:addLineChatElement(zone.warningMessage, 255, 0, 0)
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showNoCarWarning(player, zone)
    local vehicle = player:getVehicle()
    vehicle:getModData().WEZ_ZoneEnterTime = getTimestamp()
    player:addLineChatElement("This is a no car zone. Cars will be automatically deleted from this area. This is your only warning.", 255, 0, 0)
end

Events.OnTick.Add(WEZ_MonitorPlayer.Check)