WLL = WLL or {}
WLL.DoorLock = WLL.DoorLock or {}

function WLL.DoorLock.getSquareDoor(square)
    for i=0,square:getObjects():size()-1 do
        local obj = square:getObjects():get(i)
        if WLL.DoorLock.IsObjectDoor(obj) then
            return obj
        end
    end
end

function WLL.DoorLock.IsObjectDoor(object)
    if object and (instanceof(object, "IsoDoor") or (instanceof(object, "IsoThumpable") and object:isDoor())) then
        return true
    end
    return false
end

function WLL.DoorLock.PlayerCanPickDoorLock(player)
    return (player:HasTrait("Burglar") or player:HasTrait("Locksmith"))
    and player:getInventory():FindAndReturn("Screwdriver")
    and player:getInventory():FindAndReturn("Paperclip")
end

function WLL.DoorLock.GetDoorKeyId(door)
    local keyID = -1
    if instanceof(door, "IsoDoor") then
        keyID = door:checkKeyId()
    elseif instanceof(door, "IsoThumpable") then
        keyID = door:getKeyId()
    end
    return keyID
end

function WLL.DoorLock.HasLock(door)
    return WLL.DoorLock.GetDoorKeyId(door) ~= -1 and door:getModData().CustomLock
end

function WLL.DoorLock.CreateKey(door)
    local keyID = WLL.DoorLock.GetDoorKeyId(door)
    if keyID ~= -1 then
        local item = InventoryItemFactory.CreateItem("Base.Key1")
        item:setKeyId(keyID)
        return item
    end
end

function WLL.DoorLock.SetDoorKey(door, keyID)
    door:setKeyId(keyID)
    door:getModData().CustomLock = keyID ~= -1
    door:transmitModData()

    local doubleDoorObjects = buildUtil.getDoubleDoorObjects(door)
    for i=1,#doubleDoorObjects do
        local object = doubleDoorObjects[i]
        object:setKeyId(keyID)
        object:getModData().CustomLock = keyID ~= -1
        object:transmitModData()
    end

    local garageDoorObjects = buildUtil.getGarageDoorObjects(door)
    for i=1,#garageDoorObjects do
        local object = garageDoorObjects[i]
        object:setKeyId(keyID)
        object:getModData().CustomLock = keyID ~= -1
        object:transmitModData()
    end
end

function WLL.DoorLock.CanUnlockDoor(player, door)
    local keyID = WLL.DoorLock.GetDoorKeyId(door)
    if keyID == -1 then
        return true
    end
    return player:getInventory():haveThisKeyId(keyID)
end

function WLL.DoorLock.OnPickLock(player, door)
    local paperclip = player:getInventory():FindAndReturn("Paperclip")
    local screwdriver = player:getInventory():FindAndReturn("Screwdriver")
    if not paperclip or not screwdriver then
        return
    end

    local action = WLLPickDooorLockAction:new(player, door)
    ISTimedActionQueue.add(action)
end

function WLL.DoorLock.OnRemoveLock(player, door)
    local keyID = WLL.DoorLock.GetDoorKeyId(door)
    if keyID ~= -1 then
        local item = player:getInventory():AddItem("WLLDoorLock")
        item:getModData().KeyId = keyID
        item:setName("Door Lock (Key: " .. keyID .. ")")
        item:setCustomName(true)
        WLL.DoorLock.SetDoorKey(door, -1)
        WLL.ShowInfo(player, "Removed the lock from the door.")
    end
end

function WLL.DoorLock.OnCreateKey(player, door)
    local key = WLL.DoorLock.CreateKey(door)
    if key then
        player:getInventory():AddItem(key)
        local item = player:getInventory():FindAndReturn("ScrapMetal")
        item:getContainer():DoRemoveItem(item)
        WLL.ShowInfo(player, "Created a key for the door.")
    end
end

function WLL.DoorLock.OnRekeyDoor(player, door)
    local keyID = ZombRand(1, 2000000000)
    WLL.DoorLock.SetDoorKey(door, keyID)
    local key = WLL.DoorLock.CreateKey(door)
    if key then
        player:getInventory():AddItem(key)
    end
    WLL.ShowInfo(player, "Rekeyed the door.")
end

function WLL.DoorLock.OnAddLock(player, door)
    local item = player:getInventory():FindAndReturn("WLLDoorLock")
    if item then
        local keyID = item:getModData().KeyId
        if not keyID then
            keyID = ZombRand(1, 2000000000)
        end
        WLL.DoorLock.SetDoorKey(door, keyID)
        item:getContainer():DoRemoveItem(item)
        WLL.ShowInfo(player, "Added a lock to the door. Make sure you have a key or to make a key before locking the door.")
    end
end

function WLL.DoorLock.DoorMenu(player, door, context)
    local doorContext = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Door Lock")

    local isLocksmith = player:HasTrait("Locksmith")
    local hasScrewdriver = player:getInventory():FindAndReturn("Screwdriver")
    local hasScrapMetal = player:getInventory():FindAndReturn("ScrapMetal")

    if WLL.DoorLock.HasLock(door) then
        local isBurglar = player:HasTrait("Burglar")
        local hasPaperclip = player:getInventory():FindAndReturn("Paperclip")

        if door:isLocked() then
            if (isLocksmith or isBurglar) and hasScrewdriver and hasPaperclip then
                doorContext:addOption("Pick Lock", player, WLL.DoorLock.OnPickLock, door)
            end
        elseif isLocksmith and hasScrewdriver then
            doorContext:addOption("Remove Lock", player, WLL.DoorLock.OnRemoveLock, door)
        end

        if isLocksmith and not door:isLocked() and hasScrapMetal then
            doorContext:addOption("Create a Key", player, WLL.DoorLock.OnCreateKey, door)
            doorContext:addOption("Re-key Door", player, WLL.DoorLock.OnRekeyDoor, door)
        end
    else
        local doorLock = player:getInventory():FindAndReturn("WLLDoorLock")
        if isLocksmith and doorLock and hasScrewdriver then
            doorContext:addOption("Add Door Lock", player, WLL.DoorLock.OnAddLock, door)
        end
    end

    if doorContext:isEmpty() then
        context:removeOptionByName("Door Lock")
    end
end

function WLL.DoorLock.OnPreFillWorldObjectContextMenu(player, context, worldobjects, test)
    if not test then
        for _, object in ipairs(worldobjects) do
            if WLL.DoorLock.IsObjectDoor(object) then
                WLL.DoorLock.DoorMenu(player, object, context)
                return
            end
        end
    end
end

if not WLL.DoorLock._didBindEvents then
    Events.OnPreFillWorldObjectContextMenu.Add(function (player, context, worldobjects, test)
        WLL.DoorLock.OnPreFillWorldObjectContextMenu(getSpecificPlayer(player), context, worldobjects, test)
    end)
    WLL.DoorLock._didBindEvents = true
end