WLL = WLL or {}
WLL.BaseLock = WLL.BaseLock or {}

function WLL.BaseLock.IsLockableContainer(container)
    local parent = container:getParent()
    if parent then
        return true
    end
    local item = container:getContainingItem()
    if item then
        -- TODO: Check if approved bag
        return true
    end
    return false
end

function WLL.BaseLock.GetContainerModData(container)
    local parent = container:getParent()
    if parent then
        return parent:getModData()
    end
    local item = container:getContainingItem()
    if item then
        return item:getModData()
    end
    return nil
end

function WLL.BaseLock.SetContainerModData(container, values)
    local parent = container:getParent()
    if parent then
        local modData = parent:getModData()
        for k, v in pairs(values) do
            modData[k] = v
        end
        parent:transmitModData()
    end
    local item = container:getContainingItem()
    if item then
        local modData = item:getModData()
        for k, v in pairs(values) do
            modData[k] = v
        end
    end
end

function WLL.BaseLock.ClearContainerModData(container, keys)
    local parent = container:getParent()
    if parent then
        local modData = parent:getModData()
        for _, k in ipairs(keys) do
            modData[k] = nil
        end
        parent:transmitModData()
    end
    local item = container:getContainingItem()
    if item then
        local modData = item:getModData()
        for _, k in ipairs(keys) do
            modData[k] = nil
        end
    end
end

function WLL.BaseLock.GetSquareForContainer(container)
    local parent = container:getParent()
    if parent then
        return parent:getSquare()
    end
    return nil
end

function WLL.BaseLock.PlayerCanPickLock(player, paperclip, screwdriver)

    if not player:HasTrait("Locksmith") and
       not player:HasTrait("Burglar") and
       not WL_Utils.isAtLeastGM(player) then
        return false
    end

    if not paperclip then
        if not player:getInventory():contains("Paperclip") then
            return false
        end
    else
        -- make sure in primary hand
        if player:getPrimaryHandItem() ~= paperclip then
            return false
        end
    end

    if not screwdriver then
        if not player:getInventory():contains("Screwdriver") then
            return false
        end
    else
        -- make sure in secondary hand
        if player:getSecondaryHandItem() ~= screwdriver then
            return false
        end
    end

    return true
end

function WLL.BaseLock.OnPickLock(player, system, container)
    ISTimedActionQueue.add(WLLPickLockAction:new(player, system, container))
end
