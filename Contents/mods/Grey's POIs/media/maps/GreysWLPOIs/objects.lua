objects = {
  { name = "", type = "TownZone", x = 15486, y = 5329, z = 0, width = 14, height = 14 },
  { name = "", type = "TownZone", x = 15484, y = 5350, z = 0, width = 15, height = 13 },
  { name = "", type = "TownZone", x = 15507, y = 5337, z = 0, width = 12, height = 13 },
  { name = "", type = "TownZone", x = 15506, y = 5354, z = 0, width = 15, height = 16 },
  { name = "", type = "TownZone", x = 15527, y = 5366, z = 0, width = 11, height = 16 },
  { name = "", type = "TownZone", x = 15478, y = 5409, z = 0, width = 35, height = 41 },
  { name = "", type = "Nav", x = 9351, y = 7165, z = 0, width = 7, height = 35 },
  { name = "", type = "DeepForest", x = 9526, y = 6900, z = 0, width = 74, height = 300 },
  { name = "", type = "DeepForest", x = 9358, y = 7165, z = 0, width = 168, height = 35 },
  { name = "", type = "DeepForest", x = 9300, y = 6900, z = 0, width = 219, height = 41 },
  { name = "", type = "DeepForest", x = 9300, y = 6941, z = 0, width = 51, height = 259 },
  { name = "", type = "Nav", x = 3746, y = 8100, z = 0, width = 16, height = 300 },
  { name = "", type = "DeepForest", x = 6300, y = 11700, z = 0, width = 300, height = 300 }
}
