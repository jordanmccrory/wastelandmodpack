local MAX_CHECKS = 120
local safehousesToCheck = {}
local function checkPendingSafehouses()
    local itemsToKeep = {}
    for _,safehouseData in pairs(safehousesToCheck) do
        local safehouse = SafeHouse.getSafeHouse(safehouseData.x,safehouseData.y,safehouseData.w,safehouseData.h)
        if safehouse and safehouse:getOwner() == safehouseData.temp_owner_name then
            safehouse:setOwner(safehouseData.player:getUsername())
            sendServerCommand(safehouseData.player, "WastelandSafehouses", "SyncSuccess", {safehouseData.x,safehouseData.y,safehouseData.w,safehouseData.h,safehouseData.temp_owner_name})
        else
            safehouseData.checks = safehouseData.checks + 1
            if safehouseData.checks < MAX_CHECKS then
                table.insert(itemsToKeep, safehouseData)
            else
                sendServerCommand(safehouseData.player, "WastelandSafehouses", "SyncFailed")
            end
        end
    end
    safehousesToCheck = itemsToKeep
    if #safehousesToCheck == 0 then
        Events.OnTick.Remove(checkPendingSafehouses)
    end
end

Events.OnClientCommand.Add(function (module, command, player, args)
    if module == "WastelandSafehouses" then
        if command == "CreateNew" then
            table.insert(safehousesToCheck, {
                player=player,
                x=tonumber(args[1]),
                y=tonumber(args[2]),
                w=tonumber(args[3]),
                h=tonumber(args[4]),
                temp_owner_name=args[5],
                checks = 0,
            })
            if #safehousesToCheck == 1 then
                Events.OnTick.Add(checkPendingSafehouses)
            end
        end
    end
end)