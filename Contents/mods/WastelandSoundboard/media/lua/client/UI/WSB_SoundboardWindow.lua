---
--- WSB_SoundboardWindow.lua
--- 05/08/2023
---

if not isClient() then return end

require "GravyUI"
require "ISUI/ISPanel"
require "WastelandSoundSystem"

WSB_SoundboardWindow = ISPanel:derive("WSB_SoundboardWindow")
WSB_SoundboardWindow.instance = nil

function WSB_SoundboardWindow:show()
	local w = 1050
	local h = 800
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	o.__index = self
	o:initialise()
	o:addToUIManager()
	WSB_SoundboardWindow.instance = o
	return o
end

function WSB_SoundboardWindow:initialise()
	self:onSetSourcePoint()
	self.moveWithMouse = true
	local window = GravyUI.Node(self.width, self.height):pad(5)
	local header, soundSource, _, body, footer = window:rows({30, 30, 10, 1, 40}, 5)
	self.headerLabel = header

	local soundSrcText, soundSrcButtonSpace  = soundSource:cols({1, 300}, 5)
	self.setStartPointButton = soundSrcButtonSpace:makeButton("Set sound source location to my position", self, self.onSetSourcePoint)
	self:addChild(self.setStartPointButton)
	self.soundSrcLabel = soundSrcText
	self.soundCategoryLabels = {}
	self:buildSoundButtons(body)

	local _, rightBtn = footer:cols({ 1, 50 })
	self.cancelButton = rightBtn:makeButton("Close", self, self.onClose)
	self:addChild(self.cancelButton)
end

function WSB_SoundboardWindow:buildSoundButtons(body)
	self.buttonDict = {}
	local maxRows = 20
	local maxCols = 6
	local bodyGrid = {body:grid(maxRows, maxCols, 5, 5)}

	local rowNumber = 1
	local colNumber = 1
	for category, sounds in pairs(WSB_SoundLibrary) do
		if(colNumber > maxCols) then return end
		local labelNode = bodyGrid[rowNumber][colNumber]
		table.insert(self.soundCategoryLabels, {labelNode = labelNode, name = category})

		rowNumber = rowNumber + 1
		if(rowNumber > maxRows) then
			rowNumber = 1
			colNumber = colNumber + 1
		end

		for _, soundName in ipairs(sounds) do
			if(colNumber > maxCols) then return end
			local button = bodyGrid[rowNumber][colNumber]:makeButton(soundName, self, self.playSound, {soundName})
			self:addChild(button)
			self.buttonDict[soundName] = button
			if WastelandSoundSystem:isActivelyLoopingSound(soundName) then
				button.title = "Stop"
			end

			rowNumber = rowNumber + 1
			if(rowNumber > maxRows) then
				rowNumber = 1
				colNumber = colNumber + 1
			end
		end
	end
end

function WSB_SoundboardWindow:prerender()
	ISPanel.prerender(self)
	self:drawTextCentre("Soundboard", self.headerLabel.left + (self.headerLabel.width/4),
			self.headerLabel.top, 1, 1, 1, 1, UIFont.Medium)
	self:drawText(self:getStartPointString(), self.soundSrcLabel.left,
			self.soundSrcLabel.top, 1, 1, 1, 1, UIFont.Medium)

	for _, categoryLabel in ipairs(self.soundCategoryLabels) do
		self:drawText(categoryLabel.name, categoryLabel.labelNode.left,
				categoryLabel.labelNode.top, 1, 1, 1, 1, UIFont.Medium)
	end
end

function WSB_SoundboardWindow:getStartPointString()
	return string.format("Current Sound Source: %d, %d, %d", self.soundX, self.soundY, self.soundZ)
end

function WSB_SoundboardWindow:onSetSourcePoint()
	local player = getPlayer()
	self.soundX = player:getX()
	self.soundY = player:getY()
	self.soundZ = player:getZ()
	self.square = player:getSquare()
end

function WSB_SoundboardWindow:playSound(_, soundName)
	if WastelandSoundSystem:isLoopingSound(soundName) then
		local button = self.buttonDict[soundName] -- ISButton
		if button.title == "Stop" then
			WastelandSoundSystem:stopLoopingSound(soundName)
			button.title = soundName
			return -- Don't play it again!
		else
			button.title = "Stop"
		end
	else
		WastelandSoundSystem:playSound(soundName, self.square)
	end
end

function WSB_SoundboardWindow:onClose()
	WSB_SoundboardWindow.instance = nil
	self:removeFromUIManager()
end