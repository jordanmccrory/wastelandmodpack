---
--- SoundLibrary.lua
--- 05/08/2023
---

WSB_SoundLibrary = {
	Vanilla = {
		"ManScreaming", "WomanScreaming", "WomanScreaming2", "WomanScreaming3", "WomanShortScream", "ManShortScream",
		"ManShortScream2", "DogBarking", "DistantRifleShot", "DistantRifleShot2", "DistantRifleBurst", "ShotgunPump",
		"DistantRifleBurst2", "BurglarAlarm", "BurglarAlarm2", "Helicopter", "AlarmClock", "ShortBeeping", "Explosion",
		"Owl", "Bushes1", "Bushes2",
	},
	General = {
		"AirRaidSiren", "JetOverhead", "AirlinerOverhead", "ZombieHorde", "ZombieHorde2", "LargeFireworksDisplay",
		"WilhelmScream", "CreepyLaughLoud", "CreepyLaughQuiet", "EvilLaughterMale",
		"EvilLaughterMale2", "KnifeStabbing",
	},
	Train = {
		"SteamWhistle1", "DistantWhistleTracks", "TrainArrival", "VeryDistantWhistle", "TrainStationAmbience",
		"SteamSounds",
	},
	Arena = {
		"ArenaStart", "ArenaEnd", "ArenaOneMinute", "ArenaFiveMinutes",
	},
	Druids = {
		"DistantDrums",
	},
	Swarm = {
		"WindWhistles", "BulwarkTaunt",
	},
	Siren = {
		"Siren1", "Siren2", "SirenDeath",
	},
	SpecialInfected = {
		"LurkerClick", "LurkerScream", "LurkerMoving",
	},
	Bull = {
		"BullBreathing", "BullCharge", "BullWalking"
	}
}
