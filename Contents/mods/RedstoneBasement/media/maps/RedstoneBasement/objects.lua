objects = {
  { name = "Army", type = "ZombiesType", x = 125, y = 16691, z = 0, width = 30, height = 10 },
  { name = "Army", type = "ZombiesType", x = 138, y = 16596, z = 0, width = 4, height = 29 },
  { name = "Army", type = "ZombiesType", x = 125, y = 16625, z = 0, width = 30, height = 61 },
  { name = "Doctor", type = "ZombiesType", x = 142, y = 16670, z = 0, width = 13, height = 16 },
  { name = "Army", type = "ZombiesType", x = 135, y = 16590, z = 0, width = 10, height = 6 },
  { name = "Army", type = "ZombiesType", x = 129, y = 16588, z = 1, width = 22, height = 2 },
  { name = "Army", type = "ZombiesType", x = 135, y = 16590, z = 1, width = 10, height = 6 },
  { name = "Army", type = "ZombiesType", x = 125, y = 16625, z = 1, width = 30, height = 10 },
  { name = "", type = "SpawnPoint", x = 139, y = 16726, z = 0, width = 1, height = 1, properties = { Professions = "all" } }
}
