require "WLR_gui"
require "WL_Utils"

local WLR_menu = {}

WLR_menu.doMenu = function(player, context)
  if not isClient() or WL_Utils.canModerate(getPlayer()) then
    local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
    submenu:addOption("Manual Loot Respawner", player, WLR_menu.onRefillGUI)
  end
end

function WLR_menu.onRefillGUI(player)
    local modal = WLR_gui:new()
    modal:initialise()
end

Events.OnFillWorldObjectContextMenu.Add(WLR_menu.doMenu)