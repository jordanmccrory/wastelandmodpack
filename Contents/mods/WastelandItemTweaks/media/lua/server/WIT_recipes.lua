WIT_recipes = {}

function WIT_recipes.OnMakePropaneTank(items, result, player)
    result:setUsedDelta(0)
end

function WIT_recipes.OnMakeZombieEarRope(items, result, player)
    result:getModData().countUsed = 1
    result:setName("Zombie Ear Rope [1]")
    result:setCustomName(true)
end

function WIT_recipes.OnGiveXP_CraftLamp(recipe, ingredients, result, player)
    player:getXp():AddXP(Perks.Electricity, 5)
end

function WIT_recipes.OnGiveXP_Tailoring10(recipe, ingredients, result, player)
    player:getXp():AddXP(Perks.Tailoring, 10)
end

function WIT_recipes.OnGiveXp_Doctor10(recipe, ingredients, result, player)
    player:getXp():AddXP(Perks.Doctor, 10)
end

function WIT_recipes.Give5TailoringXP(recipe, ingredients, result, player)
    player:getXp():AddXP(Perks.Tailoring, 5)
end

-- These are because sprouts forgot to add it....
function Recipe.OnGiveXP.Cooking5(recipe, ingredients, result, player)
    player:getXp():AddXP(Perks.Cooking, 5)
end
function Recipe.OnGiveXP.Cooking15(recipe, ingredients, result, player)
    player:getXp():AddXP(Perks.Cooking, 15)
end

function WIT_recipes.OnTest_False()
    return false
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.extractKevlar(items, result, player)

    -- Check the ingredients for the vest to see if it has holes, and if we're dealing with a light vest
    local lightVestOnly = false
    local hasHoles = false
    for i=0,items:size() - 1 do
        local item = items:get(i)
        if(item:getType() == "Vest_BulletLight") then
            lightVestOnly = true
        end

        if item:IsClothing() and item:getHolesNumber() > 0 then
            hasHoles = true
        end
    end

    -- Figure out how many kevlar sheets we will give back
    local sheets = 7 -- 8 total, because +1 from the recipe script
    if lightVestOnly then
        sheets = 4 -- 5 total, because +1 from the recipe script
    end

    if hasHoles then -- Vest is damaged
        if lightVestOnly then
            sheets = sheets - (ZombRand(0, 3) + 1) -- Subtract 1 to 3 sheets because of the damage
        else
            sheets = sheets - (ZombRand(0, 3) + 2) -- Subtract 2 to 4 sheets because of the damage
        end
    end

    for i = 1, sheets do
        player:getInventory():AddItem("Base.KevlarSheet")
    end

    if not lightVestOnly then -- Normal vests only
        if hasHoles then
            if ZombRand(0, 3) > 0 then  -- 2/3 chance we get it as ZombRand returns 0, 1, or 2
                player:getInventory():AddItem("Base.SmallSheetMetal")
            end
        else -- No damage, so they get the metal plate out!
            player:getInventory():AddItem("Base.SmallSheetMetal")
        end
    end
end

--- Unstack 50 into 5 tens. One comes from the recipe already
function Recipe.OnCreate.unstackFiftyCoins(items, result, player)
    for i = 1, 4 do
        player:getInventory():AddItem("Base.GoldCurrencyTen")
    end
end

--- Unstack a 10 into 10 separate coins. One comes from the recipe already
function Recipe.OnCreate.unstackTenCoins(items, result, player)
    for i = 1, 9 do
        player:getInventory():AddItem("Base.GoldCurrency")
    end
end

function Recipe.OnTest.checkShopKeepStock(item, result)
    if item:getFullType() ~= "Base.WITShopKeep" then
        return true
    end
    if not item then return false end
    local resultType = result:getFullType()
    local currentStock = WIT_ShopKeepAdmin.GetStock(item, resultType)
    if currentStock == nil or currentStock == 0 then
        return false
    end
    return true
end

function Recipe.OnCreate.updateShopKeepStock(items, result, player)
    if not items or items:size() == 0 then return end
    local shopKeep
    for i = 0, items:size() - 1 do
        local item = items:get(i)
        if item:getFullType() == "Base.WITShopKeep" then
            shopKeep = item
            break
        end
    end
    if not shopKeep then return end
    local resultType = result:getFullType()
    local currentStock = WIT_ShopKeepAdmin.GetStock(shopKeep, resultType)
    if currentStock ~= nil and currentStock > 0 then
        WIT_ShopKeepAdmin.UpdateStock(shopKeep, resultType, currentStock - 1)
    end
end

function Recipe.OnTest.checkZombieEarRopeForTrade(item, result)
    if item:getFullType() == "Base.ZombieEarRope" and item:getModData().countUsed ~= 500 then
        return false
    end
    return true
end