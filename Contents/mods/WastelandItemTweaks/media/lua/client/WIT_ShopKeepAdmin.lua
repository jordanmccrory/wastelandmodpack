WIT_ShopKeepAdmin = WIT_ShopKeepAdmin or {}

WIT_ShopKeepAdmin.PossibleItems = {}
WIT_ShopKeepAdmin.PossibleItems["AuthenticZClothing.AuthenticCanteenGrey"] = getItemNameFromFullType("AuthenticZClothing.AuthenticCanteenGrey")
WIT_ShopKeepAdmin.PossibleItems["AuthenticZClothing.Bag_B4BWalker_Tier_3"] = getItemNameFromFullType("AuthenticZClothing.Bag_B4BWalker_Tier_3")
WIT_ShopKeepAdmin.PossibleItems["AuthenticZClothing.Bag_MedicalBag_Tier_3"] = getItemNameFromFullType("AuthenticZClothing.Bag_MedicalBag_Tier_3")
WIT_ShopKeepAdmin.PossibleItems["AuthenticZClothing.Bag_UtilityBeltFront"] = getItemNameFromFullType("AuthenticZClothing.Bag_UtilityBeltFront")
WIT_ShopKeepAdmin.PossibleItems["Base.223Clip"] = getItemNameFromFullType("Base.223Clip")
WIT_ShopKeepAdmin.PossibleItems["Base.22Clip"] = getItemNameFromFullType("Base.22Clip")
WIT_ShopKeepAdmin.PossibleItems["Base.44Clip"] = getItemNameFromFullType("Base.44Clip")
WIT_ShopKeepAdmin.PossibleItems["Base.45Clip"] = getItemNameFromFullType("Base.45Clip")
WIT_ShopKeepAdmin.PossibleItems["Base.556Clip"] = getItemNameFromFullType("Base.556Clip")
WIT_ShopKeepAdmin.PossibleItems["Base.9mmClip"] = getItemNameFromFullType("Base.9mmClip")
WIT_ShopKeepAdmin.PossibleItems["Base.AK_Mag"] = getItemNameFromFullType("Base.AK_Mag")
WIT_ShopKeepAdmin.PossibleItems["Base.Bag_BigHikingBag"] = getItemNameFromFullType("Base.Bag_BigHikingBag")
WIT_ShopKeepAdmin.PossibleItems["Base.Bag_NormalHikingBag"] = getItemNameFromFullType("Base.Bag_NormalHikingBag")
WIT_ShopKeepAdmin.PossibleItems["Base.CBX_Sumk_1M_R"] = getItemNameFromFullType("Base.CBX_Sumk_1M_R")
WIT_ShopKeepAdmin.PossibleItems["Base.FN_FAL_Mag"] = getItemNameFromFullType("Base.FN_FAL_Mag")
WIT_ShopKeepAdmin.PossibleItems["Base.Glock17Mag"] = getItemNameFromFullType("Base.Glock17Mag")
WIT_ShopKeepAdmin.PossibleItems["Base.GunPowder"] = getItemNameFromFullType("Base.GunPowder")
WIT_ShopKeepAdmin.PossibleItems["Base.M14Clip"] = getItemNameFromFullType("Base.M14Clip")
WIT_ShopKeepAdmin.PossibleItems["Base.M1GarandClip"] = getItemNameFromFullType("Base.M1GarandClip")
WIT_ShopKeepAdmin.PossibleItems["Base.M60Mag"] = getItemNameFromFullType("Base.M60Mag")
WIT_ShopKeepAdmin.PossibleItems["Base.Mac10Mag"] = getItemNameFromFullType("Base.Mac10Mag")
WIT_ShopKeepAdmin.PossibleItems["Base.MP5Mag"] = getItemNameFromFullType("Base.MP5Mag")
WIT_ShopKeepAdmin.PossibleItems["Base.PetrolCan"] = getItemNameFromFullType("Base.PetrolCan")
WIT_ShopKeepAdmin.PossibleItems["Base.UZIMag"] = getItemNameFromFullType("Base.UZIMag")
WIT_ShopKeepAdmin.PossibleItems["MoreBrews.BottleCapsBag"] = getItemNameFromFullType("MoreBrews.BottleCapsBag")
WIT_ShopKeepAdmin.PossibleItems["Sprout.CoffeeBagSeed"] = getItemNameFromFullType("Sprout.CoffeeBagSeed")
WIT_ShopKeepAdmin.PossibleItems["Sprout.TeaBagSeed"] = getItemNameFromFullType("Sprout.TeaBagSeed")
WIT_ShopKeepAdmin.PossibleItems["Base.CarBatteryCharger"] = getItemNameFromFullType("Base.CarBatteryCharger")
WIT_ShopKeepAdmin.PossibleItems["Base.Mov_MannequinMale"] = getItemNameFromFullType("Base.Mov_MannequinMale")
WIT_ShopKeepAdmin.PossibleItems["Base.Mov_MannequinFemale"] = getItemNameFromFullType("Base.Mov_MannequinFemale")
WIT_ShopKeepAdmin.PossibleItems["Base.WRCRecorderTape"] = getItemNameFromFullType("Base.WRCRecorderTape")
WIT_ShopKeepAdmin.PossibleItems["Base.MedicalClothing_Blueprints"] = getItemNameFromFullType("Base.MedicalClothing_Blueprints")
WIT_ShopKeepAdmin.PossibleItems["Base.ThompsonSmgBlueprints"] = getItemNameFromFullType("Base.ThompsonSmgBlueprints")

function WIT_ShopKeepAdmin.UpdateStock(shopKeep, itemType, amount)
    shopKeep:getModData()["WIT_ShopKeep_" .. itemType] = amount
    if shopKeep:getWorldItem() then
        shopKeep:getWorldItem():getModData()["WIT_ShopKeep_" .. itemType] = amount
        shopKeep:getWorldItem():transmitModData()
        shopKeep:getWorldItem():transmitCompleteItemToServer()
    end
end

function WIT_ShopKeepAdmin.GetStock(shopKeep, itemType)
    if shopKeep:getWorldItem() then
        local amount = shopKeep:getWorldItem():getModData()["WIT_ShopKeep_" .. itemType]
        if amount ~= nil then return amount end
    end
    return shopKeep:getModData()["WIT_ShopKeep_" .. itemType]
end

function WIT_ShopKeepAdmin.UpdateStockLevelDialogClick(shopKeep, btn, itemType)
    if not WIT_ShopKeepAdmin.UpdateLevelDialog then return end
    if btn.internal == "CANCEL" then
        WIT_ShopKeepAdmin.UpdateLevelDialog:removeFromUIManager()
        WIT_ShopKeepAdmin.UpdateLevelDialog = nil
        return
    end
    WIT_ShopKeepAdmin.UpdateStock(shopKeep, itemType, tonumber(WIT_ShopKeepAdmin.UpdateLevelDialog.entry:getText() or "0"))
end

function WIT_ShopKeepAdmin.ShowUpdateStockDialog(shopKeep, itemType)
    if WIT_ShopKeepAdmin.UpdateLevelDialog then
        WIT_ShopKeepAdmin.UpdateLevelDialog:removeFromUIManager()
        WIT_ShopKeepAdmin.UpdateLevelDialog = nil
    end
    local name = getItemNameFromFullType(itemType)
    local currentStock = WIT_ShopKeepAdmin.GetStock(shopKeep, itemType) or 0
    WIT_ShopKeepAdmin.UpdateLevelDialog = ISTextBox:new(
        getCore():getScreenWidth()/2 - 350/2, getCore():getScreenHeight()/2 - 150/2,
        350, 150,
        "Update " .. name .. " Stock (-1 for Unlimited)", tostring(currentStock), shopKeep, WIT_ShopKeepAdmin.UpdateStockLevelDialogClick, getPlayer():getPlayerNum(), itemType)
    WIT_ShopKeepAdmin.UpdateLevelDialog:initialise()
    WIT_ShopKeepAdmin.UpdateLevelDialog:setOnlyNumbers(true)
    WIT_ShopKeepAdmin.UpdateLevelDialog:addToUIManager()
end

function WIT_ShopKeepAdmin.RemoveItemFromStock(shopKeep, itemType)
    WIT_ShopKeepAdmin.UpdateStock(shopKeep, itemType, nil)
end

Events.OnFillInventoryObjectContextMenu.Add(function(playerIdx, context, items)
    local player = getSpecificPlayer(playerIdx)
    if not WL_Utils.isStaff(player) then return end

    items = ISInventoryPane.getActualItems(items)
	for _, shopKeep in ipairs(items) do
		if shopKeep:getFullType() == "Base.WITShopKeep" then
            local subMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Shop Keep Admin")
            local addMenuItem = WL_ContextMenuUtils.getOrCreateSubMenu(subMenu, "Add to Stock")

            for itemType, itemName in pairs(WIT_ShopKeepAdmin.PossibleItems) do
                local currentStock = WIT_ShopKeepAdmin.GetStock(shopKeep, itemType)
                if currentStock == nil then
                    addMenuItem:addOption(itemName, shopKeep, WIT_ShopKeepAdmin.UpdateStock, itemType, -1)
                else
                    local amount = currentStock < 0 and "Unlimited" or currentStock == 0 and "Out of Stock" or tostring(currentStock)
                    local updateMenuItem = WL_ContextMenuUtils.getOrCreateSubMenu(subMenu, itemName .. " (" .. amount .. ")")
                    updateMenuItem:addOption("Update Stock Level", shopKeep, WIT_ShopKeepAdmin.ShowUpdateStockDialog, itemType)
                    updateMenuItem:addOption("Remove from Stock", shopKeep, WIT_ShopKeepAdmin.RemoveItemFromStock, itemType)
                end
            end

            return
        end
	end
end)
