local ToDisable = {};

local function doDisable()
    for i=0, getAllRecipes():size()-1 do
        local recipe = getAllRecipes():get(i);
        if recipe ~= nil then
            for _,v in pairs(ToDisable) do
                if recipe:getName() == v then
                    recipe:setNeedToBeLearn(true);
                    recipe:setIsHidden(true);
                    recipe:setLuaTest("WIT_recipes.OnTest_False");
                    print("Disabling Recipe: " .. v);
                end
            end
        end
    end
end

local function DisableRecipe(recipe)
    table.insert(ToDisable, recipe);
end
Events.OnGameBoot.Add(doDisable);

-- DisableRecipe("Grind Meat")


if not getActivatedMods():contains("SGarden-Homestead") then
    DisableRecipe("Buy Coffee Seeds")
    DisableRecipe("Buy Tea Seeds")
end

if not getActivatedMods():contains("ClothesBoxRedux") then
    DisableRecipe("Buy Medic Bag")
end

if not getActivatedMods():contains("Authentic Z - Current") then
    DisableRecipe("Buy Canteen")
    DisableRecipe("Buy Walker's Bag")
    DisableRecipe("Buy Trauma Bag")
    DisableRecipe("Buy Utility Belt")
end

if not getActivatedMods():contains("MoreBrews") then
    DisableRecipe("Exchange for Alternate Currency")
end

-- Disables Churn Butter 

if getActivatedMods():contains("sapphcooking") then
    DisableRecipe("Churn Butter")
end

-- Disables Spinning Thread and Spinning yarn in Favour of our own.

if getActivatedMods():contains("SGarden-Homestead") then
    DisableRecipe("Spin Thread")
end

if getActivatedMods():contains("SGarden-Homestead") then
    DisableRecipe("Spin Yarn")
end


