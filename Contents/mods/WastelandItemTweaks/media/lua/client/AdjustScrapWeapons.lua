if getActivatedMods():contains("ScrapWeapons(new version)") then
    WL_Utils.setItemProperties("SWeapons.ScrapPickaxe", {
        ["ConditionLowerChanceOneIn"] = 10,
        ["ConditionMax"] = 8,
        ["MinDamage"] = 0.8,
        ["MaxDamage"] = 1.8,
    })

    WL_Utils.setItemProperties("SWeapons.BigScrapPickaxe", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 1.1,
        ["MaxDamage"] = 1.8,
    })

    WL_Utils.setItemProperties("SWeapons.PipewithScissors", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 8,
        ["MinDamage"] = 0.8,
        ["MaxDamage"] = 1.2,
    })

    WL_Utils.setItemProperties("SWeapons.TireIronAxe", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 0.8,
        ["MaxDamage"] = 1.7,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedPipeWrench", {
        ["ConditionLowerChanceOneIn"] = 25,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 0.8,
        ["MaxDamage"] = 1.3,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedClimbingAxe", {
        ["ConditionLowerChanceOneIn"] = 20,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 0.7,
        ["MaxDamage"] = 1.05,
    })

    WL_Utils.setItemProperties("SWeapons.SharpenedStopSign", {
        ["ConditionLowerChanceOneIn"] = 10,
        ["ConditionMax"] = 13,
        ["MinDamage"] = 3.5,
        ["MaxDamage"] = 4,
    })

    WL_Utils.setItemProperties("SWeapons.HugeScrapPickaxe", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 2,
        ["MaxDamage"] = 3,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedCrowbar", {
        ["ConditionLowerChanceOneIn"] = 35,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 0.7,
        ["MaxDamage"] = 1.30,
    })

    WL_Utils.setItemProperties("SWeapons.BoltBat", {
        ["ConditionLowerChanceOneIn"] = 20,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 1,
        ["MaxDamage"] = 1.5,
    })

    WL_Utils.setItemProperties("SWeapons.ChainBat", {
        ["ConditionLowerChanceOneIn"] = 20,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 1,
        ["MaxDamage"] = 1.5,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedPipe", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 25,
        ["MinDamage"] = 0.6,
        ["MaxDamage"] = 2,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedSledgehammer", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 2,
        ["MaxDamage"] = 3,
    })

    WL_Utils.setItemProperties("SWeapons.GearMace", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 3,
        ["MaxDamage"] = 4,
    })

    WL_Utils.setItemProperties("SWeapons.WireBat", {
        ["ConditionLowerChanceOneIn"] = 20,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 1,
        ["MaxDamage"] = 1.5,
    })

    WL_Utils.setItemProperties("SWeapons.Micromaul", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 4,
        ["MaxDamage"] = 6,
    })

    WL_Utils.setItemProperties("SWeapons.ScrapSword", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 1.8,
        ["MaxDamage"] = 2.5,
    })

    WL_Utils.setItemProperties("SWeapons.ScrapMachete", {
        ["ConditionLowerChanceOneIn"] = 20,
        ["ConditionMax"] = 12,
        ["MinDamage"] = 1.0,
        ["MaxDamage"] = 2.2,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedNightstick", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 20,
        ["MinDamage"] = 1.0,
        ["MaxDamage"] = 2,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedMachete", {
        ["ConditionLowerChanceOneIn"] = 20,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 2,
        ["MaxDamage"] = 2.8,
    })

    WL_Utils.setItemProperties("SWeapons.ScrapBlade", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 2,
        ["MaxDamage"] = 3.1,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedBlade", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 3.0,
        ["MaxDamage"] = 5.0,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedCleaver", {
        ["ConditionLowerChanceOneIn"] = 15,
        ["ConditionMax"] = 20,
        ["MinDamage"] = 5.5,
        ["MaxDamage"] = 8,
    })

    WL_Utils.setItemProperties("SWeapons.SharpenedScrewdriver", {
        ["ConditionLowerChanceOneIn"] = 4,
        ["ConditionMax"] = 13,
        ["MinDamage"] = 0.3,
        ["MaxDamage"] = 0.7,
    })

    WL_Utils.setItemProperties("SWeapons.GlassShiv", {
        ["ConditionLowerChanceOneIn"] = 2,
        ["ConditionMax"] = 13,
        ["MinDamage"] = 0.3,
        ["MaxDamage"] = 0.7,
    })

    WL_Utils.setItemProperties("SWeapons.ScrapShiv", {
        ["ConditionLowerChanceOneIn"] = 4,
        ["ConditionMax"] = 10,
        ["MinDamage"] = 0.4,
        ["MaxDamage"] = 0.8,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedShiv", {
        ["ConditionLowerChanceOneIn"] = 6,
        ["ConditionMax"] = 30,
        ["MinDamage"] = 0.4,
        ["MaxDamage"] = 1.8,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedShivO", {
        ["ConditionLowerChanceOneIn"] = 6,
        ["ConditionMax"] = 30,
        ["MinDamage"] = 0.9,
        ["MaxDamage"] = 1.5,
    })

    WL_Utils.setItemProperties("SWeapons.ScrapClub", {
        ["ConditionLowerChanceOneIn"] = 30,
        ["ConditionMax"] = 11,
        ["MinDamage"] = 0.6,
        ["MaxDamage"] = 1.2,
    })

    WL_Utils.setItemProperties("SWeapons.SalvagedClub", {
        ["ConditionLowerChanceOneIn"] = 30,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 0.8,
        ["MaxDamage"] = 1.7,
    })

    WL_Utils.setItemProperties("SWeapons.SpearSharpenedScrewdriver", {
        ["ConditionLowerChanceOneIn"] = 4,
        ["ConditionMax"] = 7,
        ["MinDamage"] = 1,
        ["MaxDamage"] = 1.7,
    })

    WL_Utils.setItemProperties("SWeapons.SpearScrapShiv", {
        ["ConditionLowerChanceOneIn"] = 4,
        ["ConditionMax"] = 6,
        ["MinDamage"] = 1.2,
        ["MaxDamage"] = 1.8,
    })

    WL_Utils.setItemProperties("SWeapons.SpearScrapMachete", {
        ["ConditionLowerChanceOneIn"] = 10,
        ["ConditionMax"] = 12,
        ["MinDamage"] = 1.3,
        ["MaxDamage"] = 1.8,
    })

    WL_Utils.setItemProperties("SWeapons.SpearSalvaged", {
        ["ConditionLowerChanceOneIn"] = 12,
        ["ConditionMax"] = 15,
        ["MinDamage"] = 2.3,
        ["MaxDamage"] = 3,
    })

    WL_Utils.setItemProperties("SWeapons.ScrapSpear", {
        ["ConditionLowerChanceOneIn"] = 8,
        ["ConditionMax"] = 12,
        ["MinDamage"] = 1.3,
        ["MaxDamage"] = 2,
    })
end