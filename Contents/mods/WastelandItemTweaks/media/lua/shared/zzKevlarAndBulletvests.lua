---
--- zzKevlarAndBulletvests.lua
--- 03/08/2023
---

-- This doesn't actually work to patch holes, but we need to override the mod making vests leather so I left it in
ClothingRecipesDefinitions["FabricType"]["Kevlar"] = {}
ClothingRecipesDefinitions["FabricType"]["Kevlar"].material = "Base.KevlarSheet";
ClothingRecipesDefinitions["FabricType"]["Kevlar"].tools = "Base.Scissors";
ClothingRecipesDefinitions["FabricType"]["Kevlar"].noSheetRope = true;


local function makeVestKevlarAndHeavier(vest)
	if vest then
		local item = ScriptManager.instance:getItem(vest)
		if item then
			item:DoParam("FabricType = Kevlar")
			item:DoParam("Weight = 2.0")
		end
	end
end

local function addBulletProtectionToHelmet(helmet)
	if helmet then
		local item = ScriptManager.instance:getItem(helmet)
		if item then
			item:DoParam("BulletDefense = 10")
		end
	end
end

local bulletVests = { "Base.Vest_BulletArmy", "Base.Vest_BulletCivilian", "Base.Vest_BulletPolice"}
for i=1, #bulletVests do
	makeVestKevlarAndHeavier(bulletVests[i])
end

if getActivatedMods():contains("WastelandClothing") then
	local wastelandVests = { "Base.Vest_BulletArmy_IB", "Base.Chinatown_Sheriff_Vest"}
	for i=1, #wastelandVests do
		makeVestKevlarAndHeavier(wastelandVests[i])
	end

	addBulletProtectionToHelmet("Base.Hat_Army_IB")
end

if getActivatedMods():contains("Authentic Z - Current") then
	local authZBulletVests = { "AuthenticZClothing.Vest_BulletBlack", "AuthenticZClothing.Vest_BulletRPD",
	                           "AuthenticZClothing.Vest_BulletKilla", "AuthenticZClothing.Vest_BulletTagilla",
	                           "AuthenticZClothing.Vest_BulletTV110_BulletVest"}
	for i=1, #authZBulletVests do
		makeVestKevlarAndHeavier(authZBulletVests[i])
	end
end

if getActivatedMods():contains("Swatpack-Wasteland") then
	makeVestKevlarAndHeavier("Base.Vest_BulletSwat")
	addBulletProtectionToHelmet("Base.Hat_SwatHelmet")
end

addBulletProtectionToHelmet("Base.Hat_Army")

if getActivatedMods():contains("UndeadSuvivorTweaked-Wasteland") then
	addBulletProtectionToHelmet("UndeadSurvivor.PrepperHelmet")
end

if getActivatedMods():contains("JordanExtraStuff") then
	local jordanVests = { "Base.Vest_BulletArmy_Urban", "Base.Vest_BulletArmy_Desert", "Base.Olive_BulletproofVest",
	                      "Base.FDRF_BulletproofVest_01", "Base.Press_BulletproofVest", "Base.BastionVest" }
	for i=1, #jordanVests do
		makeVestKevlarAndHeavier(jordanVests[i])
	end
end