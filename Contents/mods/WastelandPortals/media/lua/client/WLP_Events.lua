require("WLP_PortalRegistry")

Events.OnConnected.Add(function ()
    WLP_PortalRegistry.OnConnected()
end)

Events.OnReceiveGlobalModData.Add(function (key, portalData)
    WLP_PortalRegistry.OnReceiveGlobalModData(key, portalData)
end)