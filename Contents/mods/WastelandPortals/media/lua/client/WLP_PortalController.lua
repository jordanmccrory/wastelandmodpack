---
--- WLP_PortalController.lua
--- 19/10/2023
---

require "WLBaseObject"

WLP_PortalController = WLBaseObject:derive("WLP_PortalController")

function WLP_PortalController:new()
	local o = WLP_PortalController.parentClass.new(self)
	return o
end

function WLP_PortalController:getMenuName(player, portal)
	if portal.groupName then
		return portal.groupName
	end
	return nil
end

function WLP_PortalController:getEnterText(portal)
	if portal.name then
		return portal.name
	end
	return "Enter"
end

function WLP_PortalController:generateTooltip(player, portal)
	local tooltip = "Travel"

	if portal.name then
		if portal.tooltipAction then
			tooltip = portal.tooltipAction .. " to " .. portal.name
		else
			tooltip = tooltip .. " to " .. portal.name
		end
	end

	if portal.requiredItem then
		local name = portal.requiredItemName or getItemNameFromFullType(portal.requiredItem)
		tooltip = tooltip .. " <LINE> Requires " .. portal.requiredItemCount .. " " .. name
		if portal.takeRequiredItem then
			tooltip = tooltip .. " (consumed)"
		end
	end
	if portal.requiresPower then
		tooltip = tooltip .. " <LINE> Requires Power"
	end
	return tooltip
end

function WLP_PortalController:canEnter(player, portal)
	if player:isGodMod() then return true end

	if portal.requiresPower then
		local square = getCell():getGridSquare(portal.center.x, portal.center.y, portal.center.z)
		if not square then
			return false
		end
		if not square:haveElectricity() then
			return false
		end
	end
	if portal.requiredItem then
		if player:getInventory():getItemCountFromTypeRecurse(portal.requiredItem) < portal.requiredItemCount then
			return false
		end
		if portal.requiredItemName then
			local item = player:getInventory():getFirstTypeRecurse(portal.requiredItem)
			if not item then
				return false
			end
			if item:getName() ~= portal.requiredItemName then
				return false
			end
		end
	end
	return true
end

function WLP_PortalController:getTooltipSprite(portal)
	return nil
end

function WLP_PortalController:playerTeleported(player, portal)
	local fromLocation = portal.center.x .. "," .. portal.center.y .. "," .. portal.center.z
	local toLocation = portal.target.x .. "," .. portal.target.y .. "," .. portal.target.z
	local cost = "free"
	if portal.requiredItem and portal.takeRequiredItem then
		cost = portal.requiredItemCount .. " " .. getItemNameFromFullType(portal.requiredItem)
		for i = 1, portal.requiredItemCount do
			local item = player:getInventory():getFirstTypeRecurse(portal.requiredItem)
			if item then
				item:getContainer():Remove(item)
			end
		end
	end
	if portal.safetime then
		WL_Utils.makePlayerSafe(portal.safetime)
	end
	sendClientCommand(player, "WastelandPortals", "Log", {fromLocation, toLocation, cost})
end