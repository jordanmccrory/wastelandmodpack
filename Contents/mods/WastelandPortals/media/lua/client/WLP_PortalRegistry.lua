---
--- WLP_PortalRegistry.lua
--- 22/10/2023
---

require "WLP_Portal"

WLP_PortalRegistry = WLP_PortalRegistry or {}
WLP_Portals = WLP_Portals or {}

function WLP_PortalRegistry.createPortal(areaNW, areaSE, target, portalName)
	local newPortal = WLP_Portal:new(areaNW[1], areaNW[2], areaNW[3], areaSE[1], areaSE[2], areaSE[3])
	newPortal:setTeleportTo(target[1], target[2], target[3])
	if portalName then
		newPortal:setName(portalName)
	end
	table.insert(WLP_Portals, newPortal)
	return newPortal
end

function WLP_PortalRegistry.createPortalGroup(portalListConfig, groupName)
	local portals = {}
	for i, iEntry in ipairs(portalListConfig) do
		for j, jEntry in ipairs(portalListConfig) do
			if i ~= j then
				local portal = WLP_PortalRegistry.createPortal(iEntry[1], iEntry[2], jEntry[3], jEntry[4])
				if groupName then
					portal:setGroupName(groupName)
				end
				table.insert(portals, portal)
			end
		end
	end
	return portals
end

function WLP_PortalRegistry.getPortalsAt(x, y, z)
	local zones = {}
	for _, zone in pairs(WLP_Portals) do
		if zone:isInZone(x, y, z) then
			table.insert(zones, zone)
		end
	end
	return zones
end

function WLP_PortalRegistry.getPortalById(id)
	for _, portal in pairs(WLP_Portals) do
		if portal.modDataId == id then
			return portal
		end
	end
	return nil
end

function WLP_PortalRegistry.removePortalById(id)
	for i, portal in pairs(WLP_Portals) do
		if portal.modDataId == id then
			portal:remove()
			table.remove(WLP_Portals, i)
			return
		end
	end
end

function WLP_PortalRegistry.OnConnected()
	ModData.request("WLP_Portals")
end

local function applyStandardData(portal, portalData)
	if portalData.type == "bus" then
		portal:setPortalController(WLP_BusPortalController:new())
	end
	if portalData.requiresPower then
		portal:setRequiresPower()
	end
	if portalData.soundName then
		portal:setSound(portalData.soundName)
	end
	if portalData.fadeoutTime then
		portal:setFadeoutTime(portalData.fadeoutTime)
	end
	if portalData.homingRange then
		portal:setHomingRange(portalData.homingRange, portalData.homingZRange)
	end
	if portalData.requiredItem then
		portal:setRequiredItem(portalData.requiredItem, portalData.takeRequiredItem, portalData.requiredItemCount, portalData.requiredItemName)
	end
	if portalData.tooltipAction then
		portal:setTooltipAction(portalData.tooltipAction)
	end
	if portalData.disabled then
		portal.disabled = true
		portal.homingArrow.enabled = false
	end
	if portalData.safetime then
		portal:setSafetime(portalData.safetime)
	end

end

local function applyBusData(portal, portalData)
	if portalData.busCurrency then
		portal.busCurrency = portalData.busCurrency
	end
	if portalData.busBaseCost then
		portal.busBaseCost = portalData.busBaseCost
	end
	if portalData.busFreeWeight then
		portal.busFreeWeight = portalData.busFreeWeight
	end
	if portalData.busWeightIncrement then
		portal.busWeightIncrement = portalData.busWeightIncrement
	end
	if portalData.busCostPerWeight then
		portal.busCostPerWeight = portalData.busCostPerWeight
	end
	if portalData.busFreePass then
		portal.busFreePass = portalData.busFreePass
	end
	if portalData.busFreePassName then
		portal.busFreePassName = portalData.busFreePassName
	end
	if portalData.busFreePassConsumed then
		portal.busFreePassConsumed = portalData.busFreePassConsumed
	end
end

-- TODO: Break into smaller functions
function WLP_PortalRegistry.OnReceiveGlobalModData(key, portalData)
	if key ~= "WLP_Portals" then return end

	local seenPortalIds = {}

	for _, portalData in ipairs(portalData.Singles) do
		local portal = WLP_PortalRegistry.getPortalById(portalData.id)
		if portal then
			WLP_PortalRegistry.removePortalById(portalData.id)
		end
		portal = WLP_PortalRegistry.createPortal(portalData.areaNW, portalData.areaSE, portalData.target, portalData.name)
		portal:setModDataId(portalData.id)
		applyStandardData(portal, portalData)
		if portalData.type == "bus" then
			portal:setPortalController(WLP_BusPortalController:new())
			applyBusData(portal, portalData)
		end
		seenPortalIds[portalData.id] = true
	end

	for _, portalGroupData in ipairs(portalData.Groups) do
		-- remove all portal that start with group id
		local portalsToRemove = {}
		for _, portal in pairs(WLP_Portals) do
			if portal.groupModDataId and portal.groupModDataId == portalGroupData.id then
				table.insert(portalsToRemove, portal)
			end
		end
		for _, portal in ipairs(portalsToRemove) do
			WLP_PortalRegistry.removePortalById(portal.modDataId)
		end
		-- create new portals
		local portalDefs = {}
		for name, portalDef in pairs(portalGroupData.portals) do
			table.insert(portalDefs, { portalDef.areaNW, portalDef.areaSE, portalDef.target, name })
		end
		local portals = WLP_PortalRegistry.createPortalGroup(portalDefs, portalGroupData.name)
		local portalIndex = 1
		for _, portal in ipairs(portals) do
			portal.groupModDataId = portalGroupData.id
			portal:setModDataId(portalGroupData.id .. "_" .. (portal.name or portalIndex))
			applyStandardData(portal, portalGroupData)
			applyStandardData(portal, portalGroupData.portals[portal.name])
			if portalGroupData.type == "bus" or portalGroupData.portals[portal.name].type == "bus" then
				portal:setPortalController(WLP_BusPortalController:new())
				applyBusData(portal, portalGroupData)
				applyBusData(portal, portalGroupData.portals[portal.name])
			end
			seenPortalIds[portal.modDataId] = true
			portalIndex = portalIndex + 1
		end
	end
	-- remove all portals that were not in the data
	local portalsToRemove = {}
	for _, portal in pairs(WLP_Portals) do
		if portal.modDataId and not seenPortalIds[portal.modDataId] then
			table.insert(portalsToRemove, portal)
		end
	end
	for _, portal in ipairs(portalsToRemove) do
		WLP_PortalRegistry.removePortalById(portal.modDataId)
	end
end