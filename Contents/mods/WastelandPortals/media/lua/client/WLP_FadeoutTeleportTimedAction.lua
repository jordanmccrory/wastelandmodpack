---
--- WLP_FadeoutTeleportTimedAction.lua
--- 18/10/2023
---

require "TimedActions/ISBaseTimedAction"
require "WL_Utils"

local DEFAULT_FADEOUT_TIME = 300

WLP_FadeoutTeleportTimedAction = ISBaseTimedAction:derive("WLP_FadeoutTeleportTimedAction")

function WLP_FadeoutTeleportTimedAction:new(character, target, soundName, fadeoutTime, teleportObserver, portal)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character
	o.target = target
	o.isFadeOut = false
	o.soundName = soundName
	o.maxTime = fadeoutTime or DEFAULT_FADEOUT_TIME
	o.teleportObserver = teleportObserver
	o.portal = portal
	return o
end

function WLP_FadeoutTeleportTimedAction:isValid()
	return true
end

function WLP_FadeoutTeleportTimedAction:update()
	-- speed 1 = 1, 2 = 5, 3 = 20, 4 = 40
    local uispeed = UIManager.getSpeedControls():getCurrentGameSpeed()
    local speedCoeff = { [1] = 1, [2] = 5, [3] = 20, [4] = 40 }
	local timeLeftNow =  (1 - self:getJobDelta()) * self.maxTime

	if self.isFadeOut == false and timeLeftNow < self.maxTime * speedCoeff[uispeed] then
		UIManager.FadeOut(self.character:getPlayerNum(), 1)
        self.isFadeOut = true
	end
end

function WLP_FadeoutTeleportTimedAction:start()
	if self.soundName and self.maxTime > 0 then
		self.character:getEmitter():playSoundImpl(self.soundName, nil)
	end
end

function WLP_FadeoutTeleportTimedAction:stop()
    if self.isFadeOut == true then
		UIManager.FadeIn(self.character:getPlayerNum(), 1)
		UIManager.setFadeBeforeUI(self.character:getPlayerNum(), false)
	end

	if self.soundName and self.maxTime > 0 then
		self.character:getEmitter():stopSoundByName(self.soundName)
	end
	ISBaseTimedAction.stop(self)
end

function WLP_FadeoutTeleportTimedAction:perform()
    local playerNum = self.character:getPlayerNum()
	UIManager.FadeIn(playerNum, 1)
	UIManager.setFadeBeforeUI(playerNum, false)
	WLP_Next_Teleport = { character = self.character, x = self.target.x, y = self.target.y, z = self.target.z,
	                      teleportObserver = self.teleportObserver, portal = self.portal }
    ISBaseTimedAction.perform(self)
end

WLP_Next_Teleport = nil
Events.OnTick.Add(function()
	if WLP_Next_Teleport ~= nil then
		WL_Utils.teleportPlayerToCoords(WLP_Next_Teleport.character, WLP_Next_Teleport.x,
				WLP_Next_Teleport.y, WLP_Next_Teleport.z)
		if WLP_Next_Teleport.teleportObserver then
			WLP_Next_Teleport.teleportObserver:playerTeleported(WLP_Next_Teleport.character, WLP_Next_Teleport.portal)
		end
		WLP_Next_Teleport = nil
	end
end)

