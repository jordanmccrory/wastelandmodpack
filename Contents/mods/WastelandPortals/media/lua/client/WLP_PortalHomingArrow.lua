---
--- WLP_PortalHomingArrow.lua
--- 22/10/2023
---

require "WL_HomingArrow"
require "WL_Zone"

WLP_PortalHomingArrow = WL_HomingArrow:derive("WLP_PortalHomingArrow")

function WLP_PortalHomingArrow:new(x, y, z, zone)
	local o = WLP_PortalHomingArrow.parentClass.new(self, x, y, z, 9, 0, nil, nil)
	o.zone = zone
	o.enabled = true
	return o
end

function WLP_PortalHomingArrow:isVisible(player)
	local x, y, z = player:getX(), player:getY(), player:getZ()
	return self.enabled and not self.zone:isInZone(x, y, z) -- Hide the arrow if the player goes inside the area or its disabled
end