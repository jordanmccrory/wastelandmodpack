---
--- WLP_Portal.lua
--- 18/10/2023
---

require "WL_Zone"
require "WL_Utils"
require "WLP_PortalController"
require "WL_HomingArrows"
require "WLP_PortalHomingArrow"

WLP_Portal = WL_Zone:derive("WLP_Portal")

--- Don't use this directly, make a portal in the registry instead
function WLP_Portal:new(x1, y1, z1, x2, y2, z2)
	local o = WLP_Portal.parentClass.new(self, x1, y1, z1, x2, y2, z2)  -- call inherited constructor
	o.controller = WLP_Portal.defaultController
	o.center = o:getCenterPoint()
	o.homingArrow = WL_HomingArrows.addCustomArrow(WLP_PortalHomingArrow:new(o.center.x, o.center.y, o.center.z, o))
	o.mapType = "Portal"
	o.safetime = 5
	return o
end

function WLP_Portal:getMapName()
	local name = self.name or "Portal"

	if self.groupName then
		name = name .. " (" .. self.groupName .. ")"
	end

	if self.target then
		name = name .. " to " .. self.target.x .. "," .. self.target.y .. "," .. self.target.z
	else
		name = name .. " to nowhere"
	end

	return name
end

function WLP_Portal:setModDataId(id)
	self.modDataId = id
end

function WLP_Portal:setPortalController(portalController)
	self.controller = portalController
end

function WLP_Portal:setTeleportTo(x, y, z)
	self.target = { x = x, y = y, z = z}
end

function WLP_Portal:setName(name)
	self.name = name
end

--- Sets the prefix of the tooltip for a portal, by default they use "Travel" for this string, e.g. Travel to Louisville
---@param tooltipAction string for example: "Take elevator" or "Take train" or "Sail boat"
function WLP_Portal:setTooltipAction(tooltipAction)
	self.tooltipAction = tooltipAction
end

function WLP_Portal:setGroupName(groupName)
	self.groupName = groupName
end

function WLP_Portal:setRequiresPower()
	self.requiresPower = true
end

function WLP_Portal:setSound(soundName)
	self.soundName = soundName
end

function WLP_Portal:setFadeoutTime(time)
	self.fadeoutTime = time
end

function WLP_Portal:setHomingRange(range, zRange)
	self.homingArrow:setRange(range, zRange)
end

function WLP_Portal:setRequiredItem(itemId, takeItem, takeCount, requiredName)
	self.requiredItem = itemId
	self.takeRequiredItem = takeItem or false
	self.requiredItemCount = takeCount or 1
	self.requiredItemName = requiredName
end

function WLP_Portal:setFadeOutTime(time)
	self.fadeoutTime = time
end

function WLP_Portal:setSafetime(time)
	self.safetime = time
end

function WLP_Portal:getMenuName(player)
	return self.controller:getMenuName(player, self)
end

function WLP_Portal:getEnterText()
	return self.controller:getEnterText(self)
end

function WLP_Portal:getTooltipSprite()
	return self.controller:getTooltipSprite(self)
end

function WLP_Portal:getEntranceTooltip(player)
	return self.controller:generateTooltip(player, self)
end

function WLP_Portal:getModDataId()
	return self.modDataId
end

function WLP_Portal:canEnter(player)
	return self.controller:canEnter(player, self)
end

function WLP_Portal:playerClickedEnter(player)
	if self.target then
		if not self:isPlayerInZone(player) then
			local x, y, z = self:getClosestPointInsideZone(player:getX(), player:getY(), player:getZ())
			local cell = getWorld():getCell();
			local square = cell:getGridSquare(x, y, z);
			ISTimedActionQueue.add(ISWalkToTimedAction:new(player, square));
		end
		ISTimedActionQueue.add(WLP_FadeoutTeleportTimedAction:new(player, self.target, self.soundName,
				self.fadeoutTime, self.controller, self))
	end
end

function WLP_Portal:remove()
	self:delete()
	WL_HomingArrows.removeArrow(self.homingArrow)
end


WLP_Portal.defaultController = WLP_PortalController:new()
