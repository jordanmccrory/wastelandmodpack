require "WLP_PortalRegistry"

local portals = WLP_PortalRegistry.createPortalGroup({
    { {12552, 1376, 0}, {12555, 1380, 0}, {12555, 1378, 0}, "Floor 1" },
    { {12552, 1376, 1}, {12555, 1380, 1}, {12555, 1378, 1}, "Floor 2" },
    { {12552, 1376, 2}, {12555, 1380, 2}, {12555, 1378, 2}, "Floor 3" },
    { {12552, 1376, 3}, {12555, 1380, 3}, {12555, 1378, 3}, "Floor 4" }
}, "Elevator")

for _, portal in ipairs(portals) do
    portal:setFadeOutTime(60)
    portal:setRequiresPower()
    portal:setSound("ElevatorDoors")
end
