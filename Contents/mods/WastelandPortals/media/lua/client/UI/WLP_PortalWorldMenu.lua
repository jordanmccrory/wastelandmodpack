---
--- WLP_PortalWorldMenu.lua
--- 18/10/2023
---
---
require "WLP_PortalRegistry"
require "WLP_Portal"

local WLP_PortalWorldMenu = {}

-- TODO: Break this into smaller functions
WLP_PortalWorldMenu.doMenu = function(playerIdx, context)
    local player = getPlayer(playerIdx)
    local x, y = ISCoordConversion.ToWorld(getMouseXScaled(), getMouseYScaled(), player:getZ())
    local portals = WLP_PortalRegistry.getPortalsAt(x, y, player:getZ())

    local menus = {}

    for i=1,#portals do
        local portal = portals[i]
        if not portal.disabled then
            local menu = context
            local menuName = portal:getMenuName(player)
            if menuName then
                if not menus[menuName] then
                    menus[menuName] = WL_ContextMenuUtils.getOrCreateSubMenu(context, menuName)
                    menu = menus[menuName]
                else
                    menu = menus[menuName]
                end
            end
            local option = menu:addOption(portal:getEnterText(), portal, WLP_PortalWorldMenu.enterPortal)
            local tooltip = ISToolTip:new()
            tooltip:initialise()
            tooltip:setVisible(false)
            tooltip.description = portal:getEntranceTooltip(player)
            tooltip:setTexture(portal:getTooltipSprite())
            option.toolTip = tooltip
            if not portal:canEnter(player) then
                option.notAvailable = true
                option.onSelect = nil
            end
        end
    end

    if WL_Utils.isStaff(player) then
        local wlAdminMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
        local tpMenu = WL_ContextMenuUtils.getOrCreateSubMenu(wlAdminMenu, "Portals")

        local groupMenuOptions = {}
        local seenGroupTitles = {}

        for _, portal in pairs(WLP_Portals) do
            if portal.modDataId ~= nil and portal.groupModDataId == nil then
                local menuItem
                if portal.disabled then
                    menuItem = tpMenu:addOption("Enable Portal: " .. portal:getEnterText(), portal, WLP_PortalWorldMenu.enablePortal)
                else
                    menuItem = tpMenu:addOption("Disable Portal: " .. portal:getEnterText(), portal, WLP_PortalWorldMenu.disablePortal)
                end
                local tooltip = ISToolTip:new()
                tooltip:initialise()
                tooltip:setVisible(false)
                tooltip.description = "Portal Position: " .. portal.minX .. ", " .. portal.minY .. " to " .. portal.maxX .. ", " .. portal.maxY .. " <LINE>"
                    .. "Portal Target: " .. portal.target.x .. ", " .. portal.target.y .. ", " .. portal.target.z
                menuItem.toolTip = tooltip
            end
            if portal.groupModDataId ~= nil then
                if not groupMenuOptions[portal.groupModDataId] then
                    local menuItem
                    if portal.disabled then
                        menuItem = tpMenu:addOption("Enable Group: " .. portal.groupName, portal, WLP_PortalWorldMenu.enablePortalGroup)
                    else
                        menuItem = tpMenu:addOption("Disable Group: " .. portal.groupName, portal, WLP_PortalWorldMenu.disablePortalGroup)
                    end
                    local tooltip = ISToolTip:new()
                    tooltip:initialise()
                    tooltip:setVisible(false)
                    tooltip.description = "Portal Group: " .. portal.groupName .. " <LINE>" .. portal:getEnterText()
                    menuItem.toolTip = tooltip
                    groupMenuOptions[portal.groupModDataId] = menuItem
                    seenGroupTitles[portal.groupModDataId] = { [portal:getEnterText()] = true }
                elseif not seenGroupTitles[portal.groupModDataId][portal:getEnterText()] then
                    seenGroupTitles[portal.groupModDataId][portal:getEnterText()] = true
                    groupMenuOptions[portal.groupModDataId].toolTip.description = groupMenuOptions[portal.groupModDataId].toolTip.description .. " <LINE>" .. portal:getEnterText()
                end
            end
        end

        if isAdmin() then
            tpMenu:addOption("Load Portals JSON", nil, WLP_PortalWorldMenu.importFromJson)
        end
    end
end

function WLP_PortalWorldMenu.enterPortal(portal)
    portal:playerClickedEnter(getPlayer(), portal)
end

function WLP_PortalWorldMenu.importFromJson()
    sendClientCommand(getPlayer(), "WastelandPortals", "ImportFromJson", {})
end

function WLP_PortalWorldMenu.enablePortal(portal)
    sendClientCommand(getPlayer(), "WastelandPortals", "EnablePortal", { portal.modDataId })
end

function WLP_PortalWorldMenu.disablePortal(portal)
    sendClientCommand(getPlayer(), "WastelandPortals", "DisablePortal", { portal.modDataId })
end

function WLP_PortalWorldMenu.enablePortalGroup(portal)
    sendClientCommand(getPlayer(), "WastelandPortals", "EnablePortalGroup", { portal.groupModDataId })
end

function WLP_PortalWorldMenu.disablePortalGroup(portal)
    sendClientCommand(getPlayer(), "WastelandPortals", "DisablePortalGroup", { portal.groupModDataId })
end

Events.OnPreFillWorldObjectContextMenu.Add(WLP_PortalWorldMenu.doMenu)
