if isClient() then return end

require("WLP_ModDataPortals")

Events.OnInitGlobalModData.Add(function ()
    WLP_ModDataPortals.OnInitGlobalModData()
end)

Events.OnClientCommand.Add(function (module, command, player, args)
    WLP_ModDataPortals.OnClientCommand(module, command, player, args)
end)