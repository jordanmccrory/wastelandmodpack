if isClient() then return end

local json = require("WLP_json")

WLP_ModDataPortals = {}

function WLP_ModDataPortals.OnInitGlobalModData()
    WLP_ModDataPortals.StoredPortalData = ModData.getOrCreate("WLP_Portals")
    if not WLP_ModDataPortals.StoredPortalData.Singles then
        WLP_ModDataPortals.StoredPortalData.Singles = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.Groups then
        WLP_ModDataPortals.StoredPortalData.Groups = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.DisabledSingles then
        WLP_ModDataPortals.StoredPortalData.DisabledSingles = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.DisabledGroups then
        WLP_ModDataPortals.StoredPortalData.DisabledGroups = {}
    end
end

function WLP_ModDataPortals.Save()
    ModData.add("WLP_Portals", WLP_ModDataPortals.StoredPortalData)
    ModData.transmit("WLP_Portals")
end

function WLP_ModDataPortals.ImportFromJson()
    local fileReaderObj = getFileReader("PortalImport.json", true)
    local jsonData = ""
    local line = fileReaderObj:readLine()
    while line ~= nil do
        jsonData = jsonData .. line
        line = fileReaderObj:readLine()
    end
    fileReaderObj:close()
    if jsonData and jsonData ~= "" then
        print("Json Data Found, decoding")
        local decoded = json.Decode(jsonData)
        if decoded then
            if not decoded.Singles then decoded.Singles = {} end
            if not decoded.Groups then decoded.Groups = {} end

            for i=#WLP_ModDataPortals.StoredPortalData.Singles,1,-1 do
                if WLP_ModDataPortals.StoredPortalData.Singles[i].fromJson then
                    table.remove(WLP_ModDataPortals.StoredPortalData.Singles, i)
                end
            end
            for i=#WLP_ModDataPortals.StoredPortalData.Groups,1,-1 do
                if WLP_ModDataPortals.StoredPortalData.Groups[i].fromJson then
                    table.remove(WLP_ModDataPortals.StoredPortalData.Groups, i)
                end
            end
            for _, portalData in ipairs(decoded.Singles) do
                portalData.fromJson = true
                if WLP_ModDataPortals.StoredPortalData.DisabledSingles[portalData.id] then
                    portalData.disabled = true
                end
                table.insert(WLP_ModDataPortals.StoredPortalData.Singles, portalData)
            end
            for _, portalGroupData in ipairs(decoded.Groups) do
                portalGroupData.fromJson = true
                if WLP_ModDataPortals.StoredPortalData.DisabledGroups[portalGroupData.id] then
                    portalGroupData.disabled = true
                end
                table.insert(WLP_ModDataPortals.StoredPortalData.Groups, portalGroupData)
            end
            WLP_ModDataPortals.Save()
        end
    end
end

function WLP_ModDataPortals.OnClientCommand(module, command, player, args)
    if module == "WastelandPortals" then
        if command == "ImportFromJson" then
            WLP_ModDataPortals.ImportFromJson()
        elseif command == "EnablePortal" then
            WLP_ModDataPortals.StoredPortalData.DisabledSingles[args[1]] = nil
            for _, portal in ipairs(WLP_ModDataPortals.StoredPortalData.Singles) do
                if portal.id == args[1] then
                    portal.disabled = false
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "DisablePortal" then
            WLP_ModDataPortals.StoredPortalData.DisabledSingles[args[1]] = true
            for _, portal in ipairs(WLP_ModDataPortals.StoredPortalData.Singles) do
                if portal.id == args[1] then
                    portal.disabled = true
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "EnablePortalGroup" then
            WLP_ModDataPortals.StoredPortalData.DisabledGroups[args[1]] = nil
            for _, portalGroup in ipairs(WLP_ModDataPortals.StoredPortalData.Groups) do
                if portalGroup.id == args[1] then
                    portalGroup.disabled = false
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "DisablePortalGroup" then
            WLP_ModDataPortals.StoredPortalData.DisabledGroups[args[1]] = true
            for _, portalGroup in ipairs(WLP_ModDataPortals.StoredPortalData.Groups) do
                if portalGroup.id == args[1] then
                    portalGroup.disabled = true
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "Log" then
            writeLog("PortalUse", string.format("%s used portal %s to %s for %s", player:getUsername(), args[1], args[2], args[3]))
        end
    end
end