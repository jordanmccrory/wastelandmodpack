module NotePaper

{
      	item NotePaper
	{
		DisplayCategory = Literature,
		Weight = 0.1,
		Type = Map,
		DisplayName = Note Paper,
		Icon = Paper,
		Map = NotePaper,
		WorldStaticModel = SheetOfPaper
	}
	
	item LABSNote
	{
		DisplayCategory = Literature,
		Weight = 0.1,
		Type = Map,
		DisplayName = LABS Note,
		Icon = Paper,
		Map = LABS,
		WorldStaticModel = SheetOfPaper
	}
	
	
	item SwarmNote
	{
		DisplayCategory = Literature,
		Weight = 0.1,
		Type = Map,
		DisplayName = Swarm Note,
		Icon = Paper,
		Map = SwarmNote,
		WorldStaticModel = SheetOfPaper
	}
}

