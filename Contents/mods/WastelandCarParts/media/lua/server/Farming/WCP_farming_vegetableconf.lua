require "Farming/farming_vegetableconf"

--Icons
farming_vegetableconf.icons["WCPRubberBush"] = "Item_CuredRubber";

-- Item_RubberBush

farming_vegetableconf.props["WCPRubberBush"] =  {}
farming_vegetableconf.props["WCPRubberBush"].seedsRequired = 2;
farming_vegetableconf.props["WCPRubberBush"].texture = "wcp_rubberbush_5";
farming_vegetableconf.props["WCPRubberBush"].waterLvl = 45;
farming_vegetableconf.props["WCPRubberBush"].timeToGrow = ZombRand(56,62);
farming_vegetableconf.props["WCPRubberBush"].vegetableName = "Base.RawRubber";
farming_vegetableconf.props["WCPRubberBush"].seedName = "Base.RubberBushSeed";
farming_vegetableconf.props["WCPRubberBush"].growCode = "farming_vegetableconf.growRubberBush";
farming_vegetableconf.props["WCPRubberBush"].seedPerVeg = 1;
farming_vegetableconf.props["WCPRubberBush"].minVeg = 2;
farming_vegetableconf.props["WCPRubberBush"].maxVeg = 6;
farming_vegetableconf.props["WCPRubberBush"].minVegAutorized = 6;
farming_vegetableconf.props["WCPRubberBush"].maxVegAutorized = 8;
farming_vegetableconf.props["WCPRubberBush"].waterConsumption = 2;
farming_vegetableconf.props["WCPRubberBush"].phaseName5 = "Farming_Blooming";
farming_vegetableconf.props["WCPRubberBush"].phaseName6 = "Farming_Ready for Harvest";

farming_vegetableconf.sprite["WCPRubberBush"] = {
"wcp_rubberbush_0",
"wcp_rubberbush_1",
"wcp_rubberbush_2",
"wcp_rubberbush_3",
"wcp_rubberbush_4",
"wcp_rubberbush_5",
"wcp_rubberbush_6",
"wcp_rubberbush_7",
}

farming_vegetableconf.growRubberBush = function(planting, nextGrowing, updateNbOfGrow)
	local nbOfGrow = planting.nbOfGrow;
	local water = farming_vegetableconf.calcWater(planting.waterNeeded, planting.waterLvl);
	local diseaseLvl = farming_vegetableconf.calcDisease(planting.mildewLvl);
	if (nbOfGrow == 0) then -- young
			planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting.waterNeeded = 75;
	elseif (nbOfGrow <= 4) then -- young
		if(water >= 0 and diseaseLvl >= 0) then
			planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting.waterNeeded = farming_vegetableconf.props[planting.typeOfSeed].waterLvl;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (nbOfGrow == 5) then -- mature
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (nbOfGrow == 6) then -- mature with seed
		if(water >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, 248);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
			planting.hasSeed = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (planting.state ~= "rotten") then -- rotten
		planting:rottenThis()
	end
	return planting;
end

