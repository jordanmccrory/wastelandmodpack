local function WAT_moveHordeToPosition(x, y, z, d)
    local sx = math.floor(x) - d
    local sy = math.floor(y) - d
    local ex = sx + d*2
    local ey = sy + d*2
    for cx = sx,ex do for cy = sy,ey do for cz = 0,7 do
        local square = getCell():getGridSquare(cx, cy, cz)
        if square then
            local movingEntities = square:getMovingObjects()
            for i=0,movingEntities:size()-1 do
                local movingEntity = movingEntities:get(i)
                if instanceof(movingEntity, "IsoZombie") then
                    movingEntity:pathToLocationF(x, y, z)
                end
            end
        end
    end end end
end


Events.OnClientCommand.Add(function (module, command, player, args)
    if module ~= "WAT" then return end
    if command == "reboot" then
        PzWebStats.RequestReboot("", "medium")
    end
    if command == "simpleRepair" then
        WAT_simpleRepair(args.vehicle)
    end
    if command == "moveHordeToPosition" then
        WAT_moveHordeToPosition(args.x, args.y, args.z, args.d)
    end
end)