local WastelandLoggingCommands = {}

local function getVehicleCondition(vehicle)
    local totalParts = 0
    local totalCondition = 0
    for i=1,vehicle:getPartCount() do
		local part = vehicle:getPartByIndex(i-1)
		local category = part:getCategory() or "Other";
		if category ~= "nodisplay" then
            totalParts = totalParts + 1
            totalCondition = totalCondition + part:getCondition()
        end
    end
    return math.floor(totalCondition / totalParts, 2)
end

local function getPartName(part)
    local invItem = part:getInventoryItem()
    if invItem then return invItem:getName() end
    local type = part:getItemType()
    if type:size() > 0 then return type:get(0) end
    return part:getCategory() .. ":" .. part:getArea()
end

function WastelandLoggingCommands.VehicleParts(playerObj, vehicle, part, action)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = playerObj:getUsername()
    local partName = getPartName(part)
    local vehicleName = vehicle:getScript():getName()
    local vehicleCondition = getVehicleCondition(vehicle)
    local partCondition = part:getCondition()
    local logMessage = string.format("%s %s %s (%s%%) on %s (%s%%) at %s,%s,%s", username, action, partName, partCondition, vehicleName, vehicleCondition, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.ItemTransfer(playerObj, x, y, z, item, count, source, dest)
    x, y, z = math.floor(x), math.floor(y), math.floor(z)
    local username = playerObj:getUsername()
    local logMessage = string.format("%s transferred %s %s from %s to %s at %s,%s,%s", username, count, item, source, dest, x, y, z)
    writeLog("ItemTransfer", logMessage)
end

function WastelandLoggingCommands.Trade(player, otherPlayer, itemsGiven, itemsReceived)
    local x, y, z = math.floor(player:getX()), math.floor(player:getY()), math.floor(player:getZ())
    local username = player:getUsername()
    local receivedItems, givenItems
    if #itemsGiven > 0 then
        givenItems = table.concat(itemsGiven, ", ")
    else
        givenItems = "nothing"
    end
    if #itemsReceived > 0 then
        receivedItems = table.concat(itemsReceived, ", ")
    else
        receivedItems = "nothing"
    end
    local logMessage = string.format("%s traded %s for %s with %s at %s,%s,%s", username, givenItems, receivedItems, otherPlayer, x, y, z)
    writeLog("ItemTransfer", logMessage)
end

function WastelandLoggingCommands.OnClientCommand(module, command, player, args)
    if module == 'vehicle' then
        if command == "installPart" then
            local vehicle = getVehicleById(args.vehicle)
            local part = vehicle:getPartById(args.part)
            WastelandLoggingCommands.VehicleParts(player, vehicle, part, "Installed")
        end
    elseif module == 'WastelandLogging' then
        if command == "itemTransfer" then
            WastelandLoggingCommands.ItemTransfer(player, args.x, args.y, args.z, args.item, args.count, args.source, args.dest)
        elseif command == "uninstallPart" then
            local vehicle = getVehicleById(args.vehicle)
            local part = vehicle:getPartById(args.part)
            WastelandLoggingCommands.VehicleParts(player, vehicle, part, "Uninstalled")
        elseif command == "trade" then
            local otherPlayer = args[1]
            local countItemsGiven = args[2]
            local itemsGiven = {}
            for i=1,countItemsGiven do
                table.insert(itemsGiven, args[2+i])
            end
            local countItemsReceived = args[2+countItemsGiven+1]
            local itemsReceived = {}
            for i=1,countItemsReceived do
                table.insert(itemsReceived, args[2+countItemsGiven+1+i])
            end
            WastelandLoggingCommands.Trade(player, otherPlayer, itemsGiven, itemsReceived)
        end
    end
end


Events.OnClientCommand.Add(WastelandLoggingCommands.OnClientCommand)